package fr.uge.jee.hibernate.ex1;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import utils.Database;
import utils.HibernateUtils;

import java.util.List;
import java.util.Optional;

public class EmployeeDAO {
    private final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    /**
     * Create an employee with the given information
     * @param firstName
     * @param lastName
     * @param salary
     * @return the id of the newly created employee
     */

    public long create(String firstName, String lastName, int salary) {
       return Database.execute((session -> {
           Employee e = new Employee(firstName, lastName, salary);
           session.persist(e);
           return e.getId();
       }));
    }

    /**
     * Remove the employee with the given id from the DB
     * @param id
     * @return true if the removal was successful
     */

    public boolean delete(long id) {
        return Database.execute((session -> {
            Employee e = session.get(Employee.class, id);
            if (e == null) return false;
            session.delete(e);
            return true;
        }));
    }

    /**
     * Update the salary of the employee with the given id
     * @param id
     * @param salary
     * @return true if the removal was successful
     */

    public boolean updateSalary(long id, long salary) {
        return Database.execute((session -> {
            Employee e = session.get(Employee.class, id);
            if (e == null) return false;
            e.setSalary(salary);
            session.merge(e);
            return true;
        }));
    }

    /**
     * Update the salary of the employee with the given id
     * 10% augmentation or 100 euros if the salary is less than 1000
     * @param id
     * @return
     */
    public boolean updateSalary(long id) {
        return Database.execute((session -> {
            Employee e = session.get(Employee.class, id);
            if (e == null) return false;
            long salary = e.getSalary();
            long newSalary = salary < 100 ? salary + 100 : (long) (salary * 1.1);
            e.setSalary(newSalary);
            session.merge(e);
            return true;
        }));
    }

    /**
     * Retrieve the employee with the given id
     * @param id
     * @return the employee wrapped in an {@link Optional}
     */

    public Optional<Employee> get(long id) {
        return Database.execute((session -> {
            Employee e = session.get(Employee.class, id);
            if (e == null) return Optional.empty();
            return Optional.of(e);
        }));
    }

    /**
     * Return the list of the employee in the DB
     */

    public List<Employee> getAll() {
        return Database.execute((session -> (List<Employee>) session.createQuery("from Employee").getResultList()));
    }

    public List<Employee> getAllByFirstName(String firstName) {
        return Database.execute((session -> {
            Query q = session.createQuery("select e from Employee e where e.firstName LIKE :firstName");
            q.setParameter("firstName", firstName);
            return (List<Employee>) q.getResultList();
        }));
    }
}
