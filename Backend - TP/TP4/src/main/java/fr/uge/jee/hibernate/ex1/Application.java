package fr.uge.jee.hibernate.ex1;

public class Application {
    public static void main(String[] args) {
        // Exercise 1

        /*
        Database.execute((session -> {
            Employee e = new Employee("Harry", "Potter", (long) 1000);
            session.persist(e);
        }));
        */

        // Exercise 2
        EmployeeDAO employeeManager = new EmployeeDAO();

        employeeManager.create("Bob", "Moran", 500);
        employeeManager.create("Bob", "Dylan", 600);
        long idLisa = employeeManager.create("Lisa", "Simpson", 100);
        employeeManager.create("Merge", "Simpson", 1000);
        long idHomer = employeeManager.create("Homer", "Simpson", 450);

        employeeManager.delete(idLisa);

        employeeManager.get(idHomer).ifPresentOrElse(
            e -> employeeManager.updateSalary(e.getId(), e.getSalary() + 100),
            () -> System.out.println("Employee with ID " + idHomer + " doesn't exists")
        );

        employeeManager.getAll().forEach(System.out::println);
        employeeManager.getAll().forEach(e -> employeeManager.updateSalary(e.getId()));
        employeeManager.getAll().forEach(System.out::println);

        employeeManager.getAllByFirstName("Bob").forEach(System.out::println);
    }
}
