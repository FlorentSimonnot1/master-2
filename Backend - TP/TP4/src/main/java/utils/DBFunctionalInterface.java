package utils;

import org.hibernate.Session;

@FunctionalInterface
public interface DBFunctionalInterface<E> {
    E apply(Session session);
}
