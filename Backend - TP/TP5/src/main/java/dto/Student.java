package dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", unique = true)
    private Address address;

    @ManyToOne
    @JoinColumn(name = "university")
    private University university;

    @ManyToMany
    @JoinTable(name = "students_lectures")
    private Set<Lecture> lectures;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public Student() { }

    public Student(String firstName, String lastName, Address address, University university) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.university = university;
        this.lectures = new HashSet<>();
        this.comments = new ArrayList<>();
    }

    public Student(String firstName, String lastName, Address address, University university, Set<Lecture> lectures, List<Comment> comments) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.university = university;
        this.lectures = lectures;
        this.comments = comments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public Set<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(Set<Lecture> lectures) {
        this.lectures = lectures;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addLecture(Lecture lecture) {
        this.lectures.add(lecture);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address=" + address +
                ", university=" + university +
                '}';
    }
}
