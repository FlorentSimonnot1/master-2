import dao.LectureRepository;
import dao.StudentRepository;
import dao.UniversityRepository;
import dto.Address;
import dto.Lecture;
import dto.Student;
import dto.University;

public class App {

    public static void main(String[] args) {
        StudentRepository studentRepository = new StudentRepository();
        UniversityRepository universityRepository = new UniversityRepository();
        LectureRepository lectureRepository = new LectureRepository();


        University university = universityRepository.createUniversity("UPEM", new Address(1, "Boulevard Galillée"));
        University university2 = universityRepository.createUniversity("UPEC", new Address(2, "Boulevard Général de Gaulle"));

        universityRepository.getAll().forEach(System.out::println);

        Student student = studentRepository
                .createStudent(
                        "Jonathan",
                        "Chu",
                        new Address(12, "Rue des Haricots"),
                        university
                );

        Student student2 = studentRepository
                .createStudent(
                        "Ailton",
                        "Lopes",
                        new Address(13, "Rue des Haricots"),
                        university
                );

        Student student3 = studentRepository
                .createStudent(
                        "Jean-Baptiste",
                        "Pilorget",
                        new Address(14, "Rue des Haricots"),
                        university2
                );

        studentRepository.getAll().forEach(System.out::println);

        universityRepository.updateAddress(university, new Address(2, "Boulevard Paré"));

        studentRepository.updateAddress(student2, new Address(99, "Rue du Général de Gaulle"));

        studentRepository.getAll().forEach(System.out::println);

        universityRepository.updateAddress(university, new Address(1, "Boulevard Lavoisier"));
        universityRepository.deleteUniversity(university2);

        Lecture course1 = lectureRepository.saveLecture(new Lecture("Java EE"));
        Lecture course2 = lectureRepository.saveLecture(new Lecture("Android"));


        studentRepository.addLecture(student2, course1);
        studentRepository.addLecture(student2, course2);

        // studentRepository.addAllLectures(student2, Set.of(course1, course2));
        studentRepository.addLecture(student, course1);

        lectureRepository.findStudentByLecture(course1).forEach(System.out::println);

        lectureRepository.deleteLecture(course1);

        studentRepository.findStudentsLecture(student2).forEach(System.out::println);

    }
}
