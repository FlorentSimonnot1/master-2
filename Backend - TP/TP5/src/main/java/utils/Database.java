package utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Database {
    public static <T> T execute(DBFunctionalInterface<T> fun) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = null;
        T res = null;
        try {
            tx = session.beginTransaction();
            res = fun.apply(session);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) tx.rollback();
            throw e;
        }
        return res;
    }

}
