package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Repository provides a generic interface for repositories
 * @param <T> the object type we want to manage
 * @param <K> the id type
 */
public interface Repository<T, K extends Serializable> {

    /**
     * Gets the current session
     * @return a hibernate session
     */
    Session getCurrentSession();

    /**
     * Gets the object class
     * @return the class of manipulated object
     */
    Class<T> getClaszz();

    /**
     * Persists an object
     * @param obj - the object to persist
     * @return the object
     */
    default T create(T obj) {
        return executeInSession(session -> {
            session.persist(obj);
            return obj;
        });
    }

    /**
     * Deletes the object which have the id identifier
     * @param id - the object identifier
     * @return true if the object is delete successfully, false else
     */
    default boolean delete(K id) {
        return executeInSession(session -> {
            Object obj = session.get(getClaszz(), id);
            if (obj == null) return false;
            session.delete(obj);
            return true;
        });
    }

    /**
     * Updates the object
     * @param obj - the object we want to update
     * @return the object
     */
    default T update(T obj) {
        return executeInSession(session -> (T) session.merge(obj));
    }

    /**
     * Gets the object which have the id identifier
     * @param id - the object identifier
     * @return an optional of the object
     */
    default Optional<T> get(K id) {
        return executeInSession(session -> {
            return Optional.ofNullable(session.get(getClaszz(), id));
        });
    }

    /**
     * Gets all objects from database
     * @return a list of T objects
     */
    default List<T> getAll(){
        return executeInSession(session -> {
            return session.createQuery("from " + getClaszz().getName(), getClaszz()).list();
        });
    }

    /**
     * Provides lambda which take session, executes action and returns an object
     * @param action - the action to execute
     * @param <E> the return type
     * @return the result of action
     */
    default <E> E executeInSession(Function<Session, E> action){
        Session session = getCurrentSession();
        Transaction tx = null;
        E res;
        try {
            tx = session.beginTransaction();
            res = action.apply(session);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) tx.rollback();
            throw e;
        }
        return res;
    }

    /**
     * Provides lambda which take session and executes action
     * @param action - the action to execute
     */
    default void executeInSession(Consumer<Session> action) {
        Session session = getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            action.accept(session);
            tx.commit();
        } catch(HibernateException e) {
            if (tx != null) tx.rollback();
            throw e;
        }
    }

}
