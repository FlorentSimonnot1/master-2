package dao;

import dto.Address;
import dto.Lecture;
import dto.Student;
import dto.University;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import utils.Database;
import utils.HibernateUtils;

import java.util.List;
import java.util.Set;

public class StudentRepository implements Repository<Student, Long> {

    private final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    /**
     * Save a student
     * @param firstName - the student first name
     * @param lastName - the student last name
     * @param address the student address
     * @param university - the student university
     * @return the student created
     */
    public Student createStudent(String firstName, String lastName, Address address, University university) {
        Student student = new Student(firstName, lastName, address, university);
        return create(student);
    }

    /**
     * Get students which studying in university
     * @param university - the university to search
     * @return a list of student
     */
    public List<Student> getUniversityStudents(University university) {
        return Database.execute((session -> {
            Query query = session.createQuery("select s from Student s where s.university.id = :university_id");
            query.setParameter("university_id", university.getId());
            return (List<Student>) query.getResultList();
        }));
    }

    /**
     * Update the student address
     * @param student - the student to update
     * @param address the new address
     * @return - the student updated
     */
    public Student updateAddress(Student student, Address address) {
        student.setAddress(address);
        return update(student);
    }

    /**
     * Add a lecture to student
     * @param student - the student which follow the lecture
     * @param lecture - the lecture
     * @return the student updated
     */
    public Student addLecture(Student student, Lecture lecture) {
        return executeInSession(session -> {
            Student s = session.get(getClaszz(), student.getId());
            if (s == null) return student;

            s.addLecture(lecture);
            return (Student) session.merge(s);
        });
    }

    /**
     * Add many lectures to a student
     * @param student - the student which follow the lectures
     * @param lectures - the lectures
     * @return
     */
    public boolean addAllLectures(Student student, Set<Lecture> lectures) {
        lectures.forEach(lecture -> addLecture(student, lecture));
        return true;
    }

    /**
     * Get all lectures following by a student
     * @param student - the student
     * @return a set of lectures
     */
    public Set<Lecture> findStudentsLecture(Student student) {
        return executeInSession(session -> {
            Student s = session.get(getClaszz(), student.getId());
            Hibernate.initialize(s.getLectures());
            return s.getLectures();
        });
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<Student> getClaszz() {
        return Student.class;
    }
}
