package dao;

import dto.Address;
import dto.University;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import utils.HibernateUtils;

public class UniversityRepository implements Repository<University, Long> {
    private final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    private final StudentRepository studentRepository = new StudentRepository();

    /**
     * Save a university
     * @param name the name of university
     * @param address the address of university
     * @return the university created
     */
    public University createUniversity(String name, Address address) {
        University university = new University(name, address);
        return create(university);
    }

    /**
     * Delete a university
     * Delete all students which studying in the university
     * @param university - the university to delete
     * @return boolean
     */
    public boolean deleteUniversity(University university) {
        studentRepository.getUniversityStudents(university).forEach(student -> studentRepository.delete(student.getId()));
        return delete(university.getId());
    }

    /**
     * Update the address of the university
     * @param university the university to update
     * @param address the new address
     * @return the university updated
     */
    public University updateAddress(University university, Address address) {
        university.setAddress(address);
        return update(university);
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<University> getClaszz() {
        return University.class;
    }
}
