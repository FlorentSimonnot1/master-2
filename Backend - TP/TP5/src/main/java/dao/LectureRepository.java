package dao;

import dto.Lecture;
import dto.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.Set;
import java.util.stream.Collectors;


public class LectureRepository implements Repository<Lecture, Long> {

    private final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    public Lecture saveLecture(Lecture lecture) {
        return create(lecture);
    }

    /**
     * Removes a lecture
     * Remove all students which following the lecture before
     * @param lecture to delete
     * @return true if the lecture is delete successfully, false else.
     */
    public boolean deleteLecture(Lecture lecture) {
        return executeInSession(session -> {
            Lecture l = session.get(getClaszz(), lecture.getId());
            if (l == null) return false;

            for(Student s : l.getStudents()) {
                s.getLectures().remove(l);
            }

            session.delete(l);
            return true;
        });
    }

    /**
     * Find student by lecture
     * @param lecture - the lecture
     * @return - list of student which follow the lecture
     */
    public Set<Student> findStudentByLecture(Lecture lecture) {
        return executeInSession(session -> {
            Query query = session.createQuery("select stud from Student stud join stud.lectures as l where l.id = :id");
            query.setParameter("id", lecture.getId());
            return (Set<Student>) query.stream().collect(Collectors.toSet());
        });
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<Lecture> getClaszz() {
        return Lecture.class;
    }
}
