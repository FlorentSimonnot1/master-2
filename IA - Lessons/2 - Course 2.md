# SPARQL

## Introduction

![Web semantic](./img/sparql_archi.png)

SPARQL : Simple Protocol And RDF Query Language

C'est un langage de requête pour RDF. Il permet de rechercher, ajouter, modifier ou supprimer des données RDF.

## Requêtes avec SPARQL

### Requêtage

Les requêtes sont basées sur la notion de **graphes de triplets** appelé Basic Graph Pattern (BGP). Elles sont décrites par des motifs (**patterns**) et des variables.

Les patterns sont des triplets en format **TURTLE**.
Les triplets sont séparés par le symbole **" . "**.
Les variables apparaissent dans les patterns et commencent avec le symbole **" ? "**.

Les résultats sont sous forme de table de valeurs. 

### Types de requêtage

4 types de requêtage : 

- Select : Rechercher des ressources du modèle, résultats restitués sous un format tabulaire. Retourne un array de où chaque élément est une ligne du résultat.
- Ask : Indiquer si la requête retourne un résultat non vide (test de vacuité). Retourne un booléen.
- Describe : Obtenir des informations à propos de resources présents dans le modèle.
- Construct : Construire de nouveaux graphes RDF à partir des résultats de la requête servant de template.

### Forme générale d'une requête

Une requête SPARQL est constitué d'un **Prologue**, d'un **Head** et d'un **Body**.

#### Prologue 

- **BASE** : Suivi d'un raccourci pour une URI de base auxquelles les URIs seront concaténées. 

Note : Une seule base URL par requête.

- **PREFIX** : Suivi d'une déclaration d'un raccourci pour des espaces de noms. 

Note : Il peut y avoir plusieurs préfixe.

#### Head

- **SELECT** : Suivi d'une liste de variables dont on souhaite connaître les valeurs répondant à la requête.
- **CONSTRUCT** : Suivis d'un template de triplets pour construire un entrepôt des triplets répondant à la requête.
- **ASK** : Pour tester s'il existe des solutions vérifiant les conditions de la requête.

#### Body

- **FROM** (optionnel) : Permet de spécifier différents graphes de triplets dans lesquels il faut chercher. Si pas mentionné, on suppose qu'un graphe a été spécifié au processeur SPARQL.
- **WHERE** : Suivis d'une déclaration de pattern pouvant contenir : plusieurs patterns, plusieurs patterns de base, des filtres, des unions, des présences optionnelles.
- **ORDER BY** : trier par...
- **LIMIT** : pour donner un nombre maximal de réponse
- **OFFSET** : pour commencer à partir d'un numéro de réponse
- **OPTIONAL** : utiliser l'information requise si elle est présente et de ne pas éliminer les solutions où elle est absente. **Utiliser pour les jointures**.
- **FILTER** : permet de filtrer les données selon des critères prédéfinis.

Note : Un tuple de variable satisfait le pattern si tous les triplets sont satisfaits par le tuple : on parle de requête conjonctive.

### Exemples

![Graph](./img/sparql-graph.png)

* Requête très simple

	    PREFIX voc:http://www.monvoc.fr 
	    SELECT * WHERE {?subject ?predicate ?object}

![Simple](./img/sparql-very-simple.png)

* Requête simple

		PREFIX voc:http://www.monvoc.fr 
		SELECT ?prenom WHERE { ?auteur voc:prénom ?prenom }

![Very Simple](./img/sparql-simple.png)

* Requête avec Distinct

		PREFIX voc:http://www.monvoc.fr
		SELECT DISTINCT ?nom WHERE { ?auteur voc:nom ?nom. }

![Distinct](./img/sparql_distinct.png)

* Requête avec Order BY

		PREFIX voc:http://www.monvoc.fr 
		SELECT ?prenom 
		WHERE { ?auteur voc:prénom ?prenom.} 
		ORDERBY ?prenom

![Order](./img/sparql_order.png)

* Requête avec Limit

		PREFIX voc:http://www.monvoc.fr 
		PREFIX xsd:http://www.w3.org/2001/XMLSchema# 
		SELECT ?prenom 
		WHERE { ?auteur voc:prénom ?prenom. } 
		LIMIT 1

![Limit](./img/sparql_limit.png)

* Requête avec Offset

		PREFIX voc:http://www.monvoc.fr 
		PREFIX xsd:http://www.w3.org/2001/XMLSchema# 
		SELECT ?prenom 
		WHERE { ?auteur voc:prénom ?prenom. } 
		OFFSET 1

![Offset](./img/sparql_offset.png)

* Requête avec Filter

		PREFIX voc:http://www.monvoc.fr 
		PREFIX xsd:http://www.w3.org/2001/XMLSchema# 
		SELECT ?prenom ?age 
		WHERE { 
			?auteur voc:prenom ?prenom. ?auteur voc:age ?age. 
			FILTER (?age> 40) 
		}

![Filter](./img/sparql_filter.png)

On peut utiliser des filtres avec des regex.

		PREFIX voc:http://www.monvoc.fr 
		PREFIX xsd:http://www.w3.org/2001/XMLSchema# 
		SELECT ?titre 
		WHERE {
			?livre voc:titre ?titre. 
			FILTER regex (?titre, "^L'homme" )
		 }

![Regex](./img/sparql_regex.png)

Filter supports les opérateurs de comparaison : 

- equal ('**=**')
- greater than ('**>**')
- less than or equal ('**<=**')
- greater than or equal ('**>=**')
- not equal ('**!='**)

Mais aussi les opérateurs booléen : 

- and ('**&&**')
- or ('**||**')
- not ('**!**')

Les opérateurs arithmétiques ('**+**', '**-**', '**\***', '**/**').

Des opérateurs spéciaux tels que : 

- **isURI(a)**
- **isBLANK(a)**
- **isLITERAL(a)**
- **STR(a)**
- **LANG(a)**
- **DATATYPE(a)**
- **REGEX(a, b)**
- **langMATCHES(a, b)**

* Requête avec Union

Permet de faire l'addition / l'union de 2 graphes RDF.

		PREFIX voc:http://www.monvoc.fr 
		PREFIX xsd:http://www.w3.org/2001/XMLSchema# 
		SELECT ?prenom 
		WHERE { 
			?auteur voc:prenom ?prenom . 
			UNION {	?auteur voc:firstname ?prenom}. 
		}

![Union](./img/sparql_union.png)

* Requête avec jointures

**Jointure Classique :**

	PREFIX … 
	SELECT élément à afficher 
	WHERE { 
		sujet voc:propriété objet .
		sujet2 voc:propriété2 objet2
	}

Le point correspond à une jointure entre 2 graphes.

![Join](./img/sparql_join.png)

**Jointure à gauche avec optional :** 

	PREFIX … 
	SELECT élément à afficher 
	WHERE { 
		sujet voc:propriété objet . 
		OPTIONAL {
			sujet2 voc:propriété2 objet2 
		}
	 }

L'utilisation de Optional correspond à une jointure à gauche. Les éléments du graphe de gauche sont conservés même s'ils ne répondent pas à la clause présente dans la partie optionnelle (à droite).

![Left Join](./img/sparql_left_join.png)

* Requêtes avec une négation

SPARQL propose 2 styles pour la négation. Le premier est basé sur un filtre des résultat et l'autre sur la suppression de solutions d'un autre pattern (une soustraction des 2 résultats).

    PREFIX rdf: ...
    PREFIX foaf: ...
    SELECT ?person
    WHERE {
    	?person rdf:type foaf:Person .
    	FILTER NOT EXISTS { ?person foaf:name ?name }
    }

    PREFIX rdf: ...
    PREFIX foaf: ...
    SELECT ?s
    WHERE {
    	?s ?p ?o .
    	MINUS { ?s foaf:givenName "Bob" . }
    }

Note : ces 2 méthodes peuvent renvoyer des résultats différents pour certains cas.

* Requêtes avec Bind

Le formulaire de liaison (Bind) permet d'affecter une valeur à une variable à partir d'un motif de graphique de base ou expression de chemin de propriété.

	SELECT ?title ?price 
	WHERE { 
		?x ns:price ?p . ?x ns:discount ?discount 
		BIND (?p*(1-?discount) AS ?price) 
		FILTER(?price < 20) ?x dc:title ?title . 
	}

* Requêtes avec des agrégats 

Les agrégats appliquent des expressions sur des groupes de solutions.
Les agrégats définis dans la version 1.1 de SPARQL sont COUNT,
SUM, MIN, MAX, AVG, Group By, Group CONCAT, et SAMPLE.

	PREFIX : ...
	SELECT (SUM(?lprice) AS ?totalPrice)
	WHERE { 
		?org :affiliates ?auth . 
		?auth :writesBook ?book . 
		?book :price ?lprice . 
	}
	GROUP BY ?org 
	HAVING (SUM(?lprice) > 10)

### Property Path

Property Path est une route possible à travers un graphique entre deux nœuds. Un cas trivial est un chemin de propriété de longueur
exactement 1, qui est un triple motif. Les extrémités du chemin peuvent être des termes ou des variables RDF. Les Variables ne peuvent pas être utilisées en tant que partie du chemin lui-même, seulement les extrémités.

Les chemins de propriété permettent des expressions plus concises pour certains SPARQL basic Graph patterns et ils ajoutent également la possibilité de matcher de la connexion de deux ressources par une longueur arbitraire chemin.

Quelques exemples : 

- **elt1 / elt2** (elt1 suivis de elt2)
- **elt1 | elt2** (chemin alternatif elt1 ou elt2)
- **elt*** (0 ou + matchs)
- **elt+** (1 ou + matchs), 
- **ˆelt** (l'inverse de elt), 
- **!elt** (negation de elt)

### Sous requêtes

Les sous-requêtes sont un moyen d'intégrer des requêtes SPARQL dans d'autres requêtes, normalement pour obtenir des résultats qui ne peuvent pas être autrement atteint, comme la limitation du nombre de résultats de certains la sous-expression à l'intérieur de la requête.

En raison de la nature ascendante de L'évaluation des requêtes SPARQL, les sous-requêtes sont évaluées logiquement premier, et les résultats sont projetés à l'extérieur de la requête.

    SELECT ?y ?minName 
    WHERE { 
    	:alice :knows ?y . 
    	{ 
    		SELECT ?y (MIN(?name) AS ?minName) 
    		WHERE { 
    			?y :name ?name. 
	   		} 
    		GROUP BY ?y 
    	} 
    }

Une requête SPARQL peut spécifier l'ensemble de données à utiliser pour la correspondance en utilisant la clause FROM et la clause FROM
NAMED pour décrire l'ensemble de données RDF.

Chaque clause FROM contient un IRI qui indique un graphique à
être utilisé pour former le graphique par défaut.

Une requête peut fournir des IRIs pour les graphs nommés dans le RDF Dataset en utilisant la clause FROM NAMED.

Le mot clé GRAPH est utilisé pour faire correspondre les motifs avec les graphs nommés. 
GRAPH peut fournir un IRI pour sélectionner un graph ou utiliser
une variable qui va s'étendre sur L'IRI de tous les graphs nommés dans l'ensemble de données RDF de la requête.

	SELECT ?who ?g ?mbox 
	FROM ...
	FROM NAMED ...
	FROM NAMED ...
	WHERE { 
		?g dc:publisher ?who . 
		GRAPH ?g { 
			?x foaf:mbox ?mbox
		 }
	 }

## Federated Request

C'est une extension de QL pour déléguer explicitement certaines
sous-requêtes vers différents points de terminaison SPARQL.

Le mot clé SERVICE permet d'accéder aux triplets à partir d'un endpoint à distance SPARQL.

	PREFIX foaf: ...
    SELECT ?name
    FROM ...
    WHERE {
	    ... foafknows ?person .
	    SERVICE ... { ?person foaf:name ?name . }
    }

Les requêtes peuvent autoriser explicitement les demandes de SERVICE ayant échoué avec utilisation du mot clé SILENT.

Le mot clé SILENT indique que des erreurs se sont produites pendant l'accès à un point de terminaison SPARQL distant et donc doit être ignoré le traitement de la requête.

La clause de SERVICE défaillante est traitée comme si elle avait un résultat de solution unique sans binding.

	PREFIX foaf: ...
    SELECT ?name
    FROM ...
    WHERE {
	    ... foafknows ?person .
	    SERVICE SILENT ... { ?person foaf:name ?name . }
    }

## RDF Entailment

Diverses normes W3C, y compris RDF et OWL, fournissent des interprétations sémantiques pour les graphiques RDF qui permettent de déduire des instructions RDF supplémentaires à partir d'assertions explicitement données.

Cette spécification définit les réponses à donner sous quel régime d'engagement, en précisant les régimes d'engagement pour RDF, schéma RDF, d-Entailment, OWL et RIF.

Sous RDFS entailment il n'y a pas seulement plus d'entailments
qu'avec juste RDF, ce qui entraîne peut-être plus de requêtes
réponses, mais les graphiques RDF peuvent également être incohérents sous les interprétations RDFS.

Chaque incohérence est due à un littéral de type rdf: XMLLiteral,
où la forme lexicale est une chaîne XML mal formée.

Le régime D-implication est défini pour datatyped interprétations, qui donnent de la sémantique aux types de données.

## Conversion du résultat des requêtes

Il est possible de sérialiser les résultats des requêtes (SELECT et ASK) en JSON. 

**Select :** 


    { 
        "head": { "vars": [ "book" , "title" ] } , 
        "results": { 
    	    "bindings": [ 
    		    { "book": { 
    			    "type": "uri" , 
    			    "value": "http://example.org/book/book6" 
    		    } , 
    		    "title": { 
    			    "type": "literal" , 
    			    "value": "Harry Potter and the Half-Blood Prince" 
    		    } 
    	    } , 
    	    ... 
    	    ] 
        } 
    }

**Ask :** 

    { 
    	"head" : { } , 
    	"boolean" : true 
    }

Il est également possible de sérialiser les résultats de requêtes (SELECT et ASK) en CSV et TSV.

Le format de résultats CSV SPARQL est un encodage avec perte d'un tableau de résultats. Il n'encode pas tous les détails de chaque terme RDF dans les résultats mais donne simplement une chaîne sans indiquant le type du terme (IRI, littéral, littéral avec type de données, littéral avec langue ou nœud vide).

Le format TSV encode les détails des termes RDF dans le tableau des résultats à l'aide de la syntaxe que SPARQL et Turtle utilisent.

## Langage de mise à jour

Les opérations de mise à jour peuvent consister en plusieurs requêtes séquentielles et sont effectuées sur une collection de graphiques dans un magasin de graphiques.
Des opérations sont fournies pour mettre à jour, créer et supprimer des graphiques RDF dans un store de graphiques.

Il y a un opérateur INSERT et DELETE mais pas de UPDATE directement.

	PREFIX dc: ...
	PREFIX ns: ...
	INSERT DATA { 
		GRAPH ...  
		{ ... ns:price 42}
	}

	PREFIX dc: ...
	DELETE DATA { 
		... dc:title "new value";
			dc:cretor "new value".
	}

On update un triple par la succession d'une suppression et d'une insertion.

	WITH DELETE { a b c } 
	INSERT { x y z } 
	WHERE { ... } is equivalent to: DELETE { 
		GRAPH { a b c }
	} 
	INSERT { 
		GRAPH { x y z } 
	} 
	USING WHERE { ... }

Exemple : 

Avant la mise à jour : 

![Update1](./img/sparql_update1.png)

Mise à jour : 

	PREFIX foaf: ...
	WITH ...
	DELETE { 
		?person foaf:givenName ’Bill’ 
	} 
	INSERT { 
		?person foaf:givenName ’William’ 
	} 
	WHERE { 
		?person foaf:givenName ’Bill’ 
	}

Après la mise à jour :

![Update2](./img/sparql_update2.png)
