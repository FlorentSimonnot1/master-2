﻿# OWL 

## Introduction 

### Bref Historique

Il existe plusieurs langages informatiques spécialisés dans la création et manipulation d'ontologies : 

- **OKBC** (1997) : API permettant d'accéder à des connaissances
- **KIF** (1998) : Langage destiné à faciliter des échanges de savoirs entre systèmes informatiques hétérogènes.
- **LOOM** : Langage de représentation de connaissances dont le but avoué est de "permettre la construction d'applications intelligentes".
- **DAML-ONT** (2000) : Fondé sur XML

En 2004, **OWL 1** devient recommandation du W3C.

Entre 2008 et 2011, la version 2 - **OWL2** est définis.

### Le besoin de ces nouveaux langages

La naissance de ces langages est lié aux **limitations** de RDF et RDF-S. 

- RDF-S ne permet pas d'exprimer que **2 classes sont disjointes.**
	*ex: les classes Hommes et Femmes son disjointes.*
- RDF-S ne permet pas de créer des classes par **combinaison ensembliste** d'autres classes (**Union, Intersection, Complément**).
	*ex: on veut construire la classe Personne, l'union disjointe des classes Hommes et Femmes.*
- RDF-S ne permet pas de définir de **restriction sur le nombre d'occurrences de valeurs** que peut prendre une **propriété**.
*ex: on ne peut pas dire qu'une personne a exactement 2 parents.*
- RDF-S ne permet pas de caractériser les propriétés comme la **transitivité, l'unicité ou encore la propriété inverse**.

### Introduction à OWL

OWL (Web Ontology Language) est un **langage de description d'ontologies** conçu pour la **publication** et le **partage d'ontologies** sur le **Web sémantique**.

OWL est un langage XML.

Ce langage permet aux hommes et aux machines : 

- une riche **représentation des connaissances** (propriétés, classes avec identité, équivalence, contraire, cardinalité, symétrie, transitivité...)
- de **raisonner** sur ces connaissances en s'appuyant sur une axiomatique formelle (Logique de Description).

XML, RDF et OWL constituent les 3 couches de base du Web sémantique :

- XML : support de **sérialisation** sur lequel s’appuient RDF/RDF-S et OWL 
- RDF/RDF-S et OWL : permettent de définir des **structures de données** et les **relations logiques** qui les lient.

![Couche WS](./img/ws_couches.png)

Une ontologie décrit certaines ressources mais ces ressources peuvent être **étendues**.
On peut **rajouter** de nouvelles informations mais pas en **enlever**.

### Syntaxe 

Tout document OWL est une ontologie :

- qui peut avoir un **identificateur unique** représenté par une **URI**
- qui contient : 
	- des **faits** qui sont des descriptions d'individus
	- des **axiomes** qui fournissent les descriptions de concepts

Un document OWL à la forme suivante : 

    ontologie ::= Ontology( [ ontologieID ] { directive } ) 
    directive ::= axiome | fait

L'ontology la plus simple que l'on peut écrire est : 

	ontology()

Notons que OWL contient 2 classes pré-définies : 

- owl:Thing qui correspond à ⊤.
- owl:nothing qui correspond à ⊥.

Exemple d'ontologie en LD et OWL.

LD : **Personne ⊓ #aEnfant.(Docteur U ∃aEnfant.Docteur)**

OWL :

![OWL Exemple](./img/owl_example.png)

Il y a différentes syntaxes pour stocker, partager, éditer des ontologies OWL : 

- la syntaxe d'échange **RDF/XML** officiellement recommandée.
- des syntaxes standard
- des syntaxes spécialement conçus pour des applications et buts particuliers

**Quelle que soit la syntaxe utilisée, le langage OWL n'est pas défini à l'aide d'une syntaxe concrète particulière, mais est défini par une spécification structurelle abstraite de haut niveau, qui est ensuite traduite dans diverses syntaxes concrètes.**

Les principales syntaxes concrètes sont : 

- Fonctionnelle
- RDF/XML
- Turtle
- OWL/XML
- Manchester

**1) Syntaxe Fonctionnelle**

Première étape vers des syntaxes concrètes.

C'est une syntaxe simple de base qui sert de pont entre la spécification abstraite/structurelle et diverses syntaxes concrètes.

Elle n'est pas destinée à être utilisée comme une syntaxe d'échange, mais est pour traduire la spécification structurelle dans d'autres syntaxes concrètes.

*Exemple : 
Un axiome de classes equivalents spécifiant que “Teenager” équivaut à une personne dont l'âge est compris entre 12 et 20 ans.*

    EquivalentClasses(:Teenager 
	    ObjectIntersectionOf(:Person 
		    DataSomeValuesFrom(:hasAge 
			    DatatypeRestriction(xsd:integer 
				    xsd:maxExclusive "20"^^xsd:integer 
				    xsd:minExclusive "12"^^xsd:integer
			    )
		    )
	    )
    )

**2) Syntaxe OWL RDF/XML**

Syntaxe concrète RDF très verbeuse et difficile à lire pour un humain

Elle est utilisée par la plupart des outils OWL comme syntaxe par défaut pour enregistrer les ontologies.

Exemple :

![OWL Exemple](./img/owl_rdf_xml_example.png)

**3) Syntaxe OWL Turtle**

Autre syntaxe concrète RDF moins verbeuse et un plus lisible que la syntaxe RDF/XML.

Reste encore difficile à lire pour un humain.

![OWL Exemple](./img/owl_turtle_example.png)

**4) Syntaxe OWL OWL/XML**

Syntaxe plus régulière et plus simple mais reste encore verbeuse.
Format de représentation concret pour les ontologies OWL.
Dérivé directement de la syntaxe fonctionnelle.

Exemple :

![OWL Exemple](./img/owl_owl_xml_example.png)

**5) Syntaxe OWL Manchester**

Fournit une représentation compacte pour les ontologies OWL, facile à lire et à écrire.
Principale motivation : pouvoir être utilisé pour la modification des expressions de classe dans des outils tels que Protege 3 et 4, Top Braid,...
Elle a été étendue pour représenter des ontologies complètes.
Elle est maintenant spécifiée dans une note W3C.

Exemple :

    Class: Teenager 
	    EquivalentTo: Person and (
		    hasAge some integer[> 12 , < 20]
	    )

### Les 3 profiles de OWL1

OWL1 a 3 profiles : 

- OWL1-Lite : LD SHIF(D)
- OWL1-DL : LD SHOIN(D)
- OWL1-FULL : OWL1-DL + RDF

OWL1-Lite et OWL1-DL ne sont pas des extensions de RDF. Un triplet RDF n'est pas nécessairement valide dans ces 2 sous langages.
Ils possèdent une syntaxe abstraite définie.

OWL1-Lite proposent les avantages et inconvénients suivants : 

- **simple et facile à programmer**
- **expressivité limitée** à des hiérarchies de classes et contraintes de cardinalité 0 ou 1
- raisonnements **complets** et **rapides**

OWL1-DL :

- DL pour logique de description
- **expressivité élevée** : contient tout OWL avec certains restrictions 
- raisonnements **plus lents** que OWL-Lite. Tous les calculs seront terminés dans un temps fini (**décidable**). Tous les inférences seront assurés d'être prise en compte (**complétude**)

OWL1-FULL : 

- **expressivité maximale**
- compatibilité complète avec RDF/RDFS
- raisonnements souvent très **complexes**, **lents**, **incomplets** et **indécidables**.

![OWL-Profiles](./img/owl_profiles.png)

## OWL2

### Pourquoi OWL2

OWL2 est une extension et une revision de OWL 1. Il est compatible avec OWL1.

Comme OWL 1, OWL 2 est conçu pour faciliter le développement d’ontologies et leur partage sur le Web.
Avec le but ultime de rendre accessible le contenu du Web aux machines.

OWL2 (DL) est basé sur la LD SROIQ et est orienté vers des ontologies avec un haut degré d'expressivité.

### 3 Profils de OWL

OWL2 a 3 profils faisant des compromis entre expressivité et calculabilité.

Les sous langages de OWL2 se différencient par des possibilités d'implémentation (avec un langage de requête de BD).

**1) OWL2-EL**

**EL pour « Existential Languages ».**

Vise les grandes ontologies (biomédicales).

Correspond à la famille de logique de description EL (ne permet que la quantification existentielle).

Algorithmes polynomiaux pour vérifier la satisfiabilité, classification, vérification d’instances.

**2) OWL2-QR**

**QL pour « Query Language ».**

Vise les **ontologies simples** avec un grand nombre d'entités (p.e. thesaurus).

Pouvoir d'expression similaire à celle des **schémas entités-relation** ou **UML**.

**Intégration possible avec les BD relationnelles**.

Raisonnement possible à l'aide de **réécriture de requêtes SQL**.

**3) OWL2-RL**

**RL pour « Rule Language ».**

Equivalent à **RDF** enrichi avec des **règles**.

Le plus expressif des profils existants.

Quelques limites sur l'expressivité pour espérer garder une certaine efficacité.

**Raisonnement** à l'aide de **systèmes de règles.**

## Limites de OWL1 et OWL2 

OWL1 ne peut pas exprimer la relation "oncle" (chaîne des relations parents et de frères et soeurs).

OWL2 ne peut pas exprimer les relations entre les individus référencés par les propriétés. ex : pas possible d’exprimer le concept “enfant de parents mariés”

