# Introduction au Web Sémantique

## Bref Histoire

Le **web** = Système hypertexte public fonctionnant sur **internet**.

Permet de consulter des **pages** (pages web) mises en lignes sur des **sites**, grâce à un **navigateur**.

Les **hyperliens** entre les pages donnent la métaphore de la toile d'araignée.

Quelques dates : 

1989 : Tim Berners-Lee commence le développement d'un système hypertexte.

1990 : Premières définitions pour HTTP, HTML, URL = Naissance du web 1.0

1992 : Première annuaire des sites : 26 sites.

1994 : Création du W3C.

1995 : Microsoft ne croit pas au web puis change d'avis.

1998 : Plus de 2M de sites. Création de Google.

2000 : XHTML 1.0

2004 : Firefox 1.0

2005 : Plus de 60M de sites web

2019 : Plus de 1 Milliard de sites web

3 grandes générations du Web : 

- Web 1.0 (1990)
- Web 2.0 (Actuel)
- Web 3.0 (En développement) 

### Web 1.0

Web **statique** ayant évolué vers le web **dynamique**.
Langages de **scripts**, navigateur web plus riches (Javascript en 1996).

### Web 2.0

**Interaction entre les utilisateurs** : web social, web participatif.
Evolutions technologiques permettant d'utiliser tout type d'application via un navigateur  (Ajax).
Commerce électronique.

### Web 3.0

Technologies pour rendre le contenu des **resources** du Web **accessible** et **utilisable** par les **programmes** et **agents logiciels**.
Grâce à des **métadonnées formelles** formalisées dans des **langages développés par le W3C**.

## Faiblesse du Web actuel

Les informations sur le web sont principalement prévue pour être affichée (écran, imprimante) et lue par des humains.

Seuls les humains peuvent interpréter le contenu des pages web.

L'interaction entre un utilisateur et le web passe surtout par un moteur de recherche.

Ces moteurs font essentiellement une recherche syntaxique de mots-clés.

Usage de "Wrappers" pour une extraction automatique à partir de pages Web.

**Le web actuel est essentiellement syntaxique : contenu quasi inaccessible aux traitements machines.**

**Le Web actuel entrave la recherche, l'extraction, la maintenance et la génération d'INFORMATION.**

La majorité des **données sur le Web** est sous une **forme qui ne permet pas de l'utiliser à grande échelle**.

**Pas de système global de publication de données**, permettant aux **machines** et aux **humains** de les **traiter** : 

Par exemple, les évènements sportifs, météo, guides TV, guides cinéma, ... sont présentés par de **nombreux sites Web**, mais presque tous au **format HTML**.

**Actuellement, pas d'accès réel au CONTENU des documents.**

Contenu et information pas accessible ni interprétables par des machines.

Pas possible de composer dynamiquement des documents cohérents et adaptés aux utilisateurs.

**Les INFORMATIONS sont cachées dans le code HTML :** 

- code HTML contient l'expression dans une langue naturelle des informations, images, fichiers sonores, vidéos.
- moteurs de recherche (sur le texte) ne font aucun traitement sophistiqué pour des raisons de performance 

La **recherche de mot** est **très différent** de la **recherche d'information**.

**Les SERVICES sont cachés dans le code HTML :** 

Comment connaître ce que propose un service ?
Comment utiliser conjointement plusieurs services ?

Exemple : Un service consistant à organiser un voyage...

- Horaires de trains & horaires d'avion = 2 documents HTML 

Comment croiser les 2 documents pour un trajet de train puis d'avion ?
Les documents HTML ne peuvent être utilisés car les documents HTML sont une **présentation de données**.
Pourtant, à la base, les données sont souvent stockées de **façon structurée** (SGBD par ex.).
Mais le schéma de la base des trains est sans doute **très différent** de celui de la base des avions.
Il faudrait une **représentation commune**, utilisant un **langage standard** pour pouvoir **croiser automatiquement** les données.


## Quelles solutions ? 

Usage de Meta-données, pour annoter les données du Web dans un langage standard, en utilisant un vocabulaire standard permettant aux machines : 

- des **comparaisons de documents**
- des **raisonnements** pour résoudre une requête
- la prise en compte de **documents multimédias**
- des **réponses formées** de **plusieurs documents** ou de parties de documents
- **communiquer** entre elles, **coopérer** dans une recherche d'information, échanger leurs résultats et les fusionner.

## Le web sémantique

**« Le Web sémantique est une extension du Web actuel dans lequel l'information est munie d'une signification bien définie permettant aux ordinateurs et aux personnes de mieux travailler en coopération »**

Les connaissances sont formalisés, il existe un lien sémantique entre informations, annotations plus riches, utilisation du XML.

**« Le Web de données est le Web des données qui peuvent être traitées directement ou indirectement par des machines pour aider leurs utilisateurs à créer de nouvelles connaissances »**

Le Web de données consiste à lier et structurer l'information pour accéder simplement à la connaissance qu'elle contient.

Les objectifs du web de données : 

- Mettre à disposition des données en utilisant des techniques standardisées qui garantissent **l'interopérabilité**.
- **Relier** les données elles-mêmes (linked data)
- Rendre ces données **interprétables par les machines**
- « Permettre aux données d’être partagées et réutilisées au-delà des limites applicatives, organisationnelles ou communautaires »

Le web Sémantique met en oeuvre le web de données.

**Données ouvertes (open data)** : données pouvant  être publiées et rendues publiques sous une licence ouverte sans les lier à d’autres sources.

<div style="width: 100%; font-weight: bold" align="center">
≠
</div>

**Données liées (linked data)** : données pouvant être liées aux URIs d’autres sources de données, en utilisant des standards ouverts tels que RDF, sans être disponibles publiquement sous une licence.

« Les données liées (linked data) sont un ensemble de principes de conception pour le partage de données lisibles par les machines sur le Web pour une utilisation par les administrations publiques, les entreprises et les citoyens. »

Il y a 4 principes de conception des données liées : 

- Utiliser des URI pour nommer les choses
- Utiliser des URIs HTTP de sorte que les humains puissent consulter ces addresses
- Fournir les URI des informations utiles en utilisant les standards du Web Sémantique (RDF, SPARQL).
- Inclure des liens vers d'autres URIs afin qu'ils puissent découvrir plus de choses.

| Web actuel | Web Sémantique  |
|--|--|
| Ensemble de documents | Ensemble d'information / connaissance  |
| Basé essentiellement sur HTML | Basé essentiellement sur XML et RDF(s), owl |
| Recherche par mots-clés | Recherche par concepts (ontologie) |
| Utilisable par l'être humain | Utilisable par la machine |

## Architecture du Web Sémantique

La **Semantic Web Stack** est une illustration représentant l'architecture du Web Sémantique spécifiant des briques fonctionnelles.

![Stack](./img/semantic_stack.png)

Ces briques peuvent être regrouper en grandes fonctions : représentation, requêtes, raisonnement et confiance.

### Brique de représentation

**1) URL, URI, IRI**

**L'objectif général est de nommer les ressources.**

URI / IRI permet d'identifier une ressource (physique ou abstraite) sur le web.

URL : URI qui donne le moyen d'accéder à la ressource (ex: https://www.wikipedia.org/)

URN : URI qui permet d'identifier une ressource par son nom dans un espace de nom (ex : urn:isbn:0-395-36341-1)

En XML, ls espaces de noms permettent d'utiliser des vocabulaires XML différents, dans un même document.

**2) XML**

**L'objectif général est de représenter les resources pour les machines**

Le XML permet de séparer le fond de la forme.

C'est un méta langage qui permet de définir des langages de documents.

**3) RDF**

**RDF est la base du Web Sémantique**

Il décrit des données sur les ressources du Web.

Les données sont représentés par des **Triplets (Sujet, Prédicat, Valeur)** : 
- Sujet : une ressource qui peut être identifiée par un URI
- Prédicat : une spécification réutilisée et identifiée par URI de la propriété
- Objet : une ressource ou constante à laquelle le Sujet est lié

Les Triplets peuvent être sérialisés de différentes façons (XML, Turtle).

Permet de constituer des graphe RDF, des bases de données RDF (Triples-stores).

### Briques requêtes

L'objectif général est de faire des requêtes sur ses ressources exprimées en RDF (et RDF-S).

**SPARQL**

C'est un langage de requête pour RDF.

C'est un protocole. Afin d'émettre et envoyer des requêtes SPARQL vers des servers dédiés et en recevoir les résultats.

Utilise un format XML pour l'affichage des résultats obtenus.

Les requêtes peuvent être fines et précises (à base de SELECT et ASK).

Il permet aussi d'ajouter, modifier ou supprimer des opérations de données RDF.

La syntaxe et fonctionnalités sont inspirés de SQL.

### Briques de raisonnement

**Ontologies**

L'objectif est de formaliser des connaissances sur un domaine spécifique pour leur utilisation par des machines.

L'ontologie est un ensemble structuré de termes et concepts, relation entre concepts, représentant le sens (les connaissances) d'un domaine d'informations particulier.

L'ontologie doit permettre aux machines de raisonner sur ces connaissances formalisées du domaine.

Le rôle des ontologies dans le Web Sémantique est : 

- de définir de manière déclarative un vocabulaire commun résultat d'un consensus social dans un domaine donné. 
	- chaque élément de vocabulaire possède une interprétation unique partagée par tous les membres du domaine.
- décrire la sémantique des termes et leurs relations : 
	- l'interprétation de chaque terme est unique et résulte d'une sémantique formelle
	- l'ensemble des termes et leurs relations fournissent un cadre interprétatif dépourvu d’ambiguïté pour chaque terme.
- fournir des mécanismes d'inférence qui respectent la sémantique formelle.

Il y a 2 langages de description d'ontologie du W3C : 

- RDF-schema : 
	- Permet de décrire un vocabulaire RDF spécifique à un domaine 
	- Fournit une sémantique à ce vocabulaire en décrivant les propriétés et les classes des ressources RDF
	- Utilisé pour formaliser des ontologies légères en permettant un raisonnement limité sur ces ontologies

- OWL : Web Ontology Language
	- Langage plus puissant de formalisation d'ontologies : relations entre classes, contraintes de cardinalité, propriété de typage plus riche.
	- Utilisé pour formaliser des ontologies lourdes en permettant un raisonnement puissant sur ces ontologies en s'appuyant sur les logiques de description (LD).

### Briques de confiance

Des briques en cours de développement : Proof, Trust et Crypto.

Elles ne correspondent pas encore a des technologies standardisés.

Les objectifs : 

- Définir un langage universel pour une logique monotone, qui permet de décrire des relations plus riches qu'en OWL.
- Définir un système d'inférence à base de règles permettant de dériver des assertions à partir des assertions connus
- Usage de la cryptographie pour assurer que les assertions utilisées et le système d'inférence sont fiables


### Synthèse

![Stack](./img/synthese.png)


