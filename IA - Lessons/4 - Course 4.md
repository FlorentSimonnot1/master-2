# Logique de description

## Introduction aux Logiques de Description (LD)

Les LD sont des langages de représentations mettant l'accent sur le raisonnement.

L'objectif est de raisonner efficacement (temps de réponse minimal) pour la prise de décision.

Les LD utilisent une approche ontologique pour représenter la connaissance d'un domaine les LD demande la définition : 

- de catégories générales d'individus
- de relations logiques que les individus ou catégories peuvent entretenir entre eux.

Cette approche ontologique est naturelle pour le raisonnement.

Les LD s'appuient sur la logique des Prédicat, les Schémas, les Réseaux Sémantiques.

1) **1ère génération de LD (1980-1990)** : Langages de représentation des connaissances mettant l'accent sur le raisonnement.

Liés aux travaux sur les systèmes à base de connaissances : KL-ONE, LOOM, BACK.
Raisonnements et inférences en temps polynomial : 

- Avec des algorithmes de vérification de **subsomption** de type **normalisation/comparaison**.
- Réservés à des LD peu expressives, sinon incomplets, incapables de prouver certaines formules vraies.

2) **2ème génération de LD (1990 à aujourd'hui)** : Logiques de Description Expressives (LDE).

- De nouveaux algorithmes de vérifications basés sur la méthode des tableaux.
- Raisonnant sur des LD dites expressives ou très expressives.
- En temps exponentiel

Cette grande expressivité à ouvert la porte à de nouvelles applications comme le Web Sémantique.

## Architecture

Les LD permettent de modéliser des connaissances avec 2 niveaux : 

- Le niveau terminologique (TBox) 
- Le niveau factuel (ABox)

![LD](./img/ld_archi.png)

### TBox

Décrit les connaissances générales d'un domaine.
Définit les **concepts** (classes) et les **roles** (relations).

Les concepts et roles sont les entités élémentaires de la LD. On les appelles les **entités atomiques**.

Il y a 4 concepts et rôles atomiques prédéfinis minimaux :

- le concept **⊤** et le rôle **⊤R** : les plus généraux
- le concept **⊥** et le rôle **⊥R**, les plus spécifiques

Les concepts et rôles atomiques peuvent être combinés au moyen de **constructeurs**, pour former des entités dites **composées**.

Quelques conventions : 

A et B dénotent des concepts **atomiques**.
C et D dénotent des concepts **composés**.
R dénote un **Role**.
Les noms de concepts commencent par une **majuscule**.
Les noms de rôles par une **minuscule**.

Les **constructeurs** permettent de combinés des concepts et des rôles atomiques pour former des entités composées.

*Exemple : Mâle ⊓ Femelle est la combinaison du concept Mâle et Femelle par le constructeur ⊓.*

Plus les LD ont de constructeurs, plus elles sont expressives et ont des chances d'être non décidables ou de complexité très élevée.

Les TBox contiennent également des **axiomes terminologiques** avec 2 formes possibles : 

- **C ≡ D** énonçant une relation d'équivalence entre concepts : C équivaut à D.
- **C ⊑ D** énonçant une relation d'inclusion : C est inclus dans D.

Quelques définitions : 

- Un concept et **consistant,** s'il existe au moins un individu membre de cette classe.
- La **subsomption** consiste à déduire qu'un concept, est une sous classe d'un autre classe.

*Exemple : si Humain est une sous classe de Animal et si Mère est une sous classe de Humain, alors Mère est une sous classe de Animal.*

Tout concept est associé à un ensemble d'individus.

Une **interprétation** I suppose l’existence :

- d’un **domaine d’interprétation** ∆I, ensemble non vide représentant les entités du monde décrit et composé d’individus.
- d’une **fonction d'interprétation** I , assignant :
	- à chaque concept atomique A, un ensemble AI , tel que AI ⊆ ∆I
	- à chaque rôle atomique R, une relation binaire RI , telle que RI ⊆ ∆I x ∆I

**L’interprétation I satisfait un axiome d’équivalence C ≡ D ssi CI = DI
L’interprétation I satisfait un axiome d’inclusion C ⊑ D ssi CI ⊆ DI
L’interprétation I satisfait un axiome d’inclusion C ⊑ D ssi CI ⊆ DI**


### ABox 

Décrit les individus en les nommants et en spécifiant leur classes et attributs.
spécifie des assertions portant sur ces individus nommés.

Une ABox contient 2 types d’assertions sur des individus :

- des assertions d'appartenance : spécifiant leur classe et leurs attributs

> Exemple : Marie est une femme et elle a 2 enfants ; Marie est une Mère (individu instance de la classe mère)

- des assertions de rôle : spécifiant les relations existantes entre individus 

> Ex : une mère doit avoir au moins un enfant : la ABox devra contenir au moins un autre individu, et une relation entre celui-ci et Marie indiquant qu’il est un de ses enfants.

Convention : 

Les individus nommés sont représentés par des lettres a, b.

Une **fonction d'interprétation** I, associe à chaque nom d’individu nommé a, un individu aI tel que aI ∈ ∆I.

Interprétation d’une Assertion d'appartenance d'une ABox :
- Soit une assertion d’appartenance notée C(a) déclarant que pour cette ABox, il existe un individu nommé a, membre du concept C de la TBox associée : une interprétation I satisfait C(a) ssi aI ∈ CI

Interprétation d’une Assertion de rôle d’une ABox :
- soit une assertion de rôle R(a, b) déclarant que pour cette ABox, il existe un individu nommé a, en relation avec un individu nommé b par le rôle R (défini dans la TBox associée), tel que a fait partie du domaine de R et b fait partie de l'image de R : une interprétation I satisfait R(a,b) ssi (aI ,bI ) ∈ RI

**Une interprétation I satisfait une ABox A (I est un modèle de la ABox A) ssi I satisfait toutes les assertions de A.**

## Logique de description minimale ALC

**ALC** = Attributive Language with Complement est **minimale,** dans le sens où la logique moins expressive représente peu d'intérêt.

ALC est l’extension de la LD de base AL à la négation de concept composé (C - complément), et à la quantification existentielle complète.

ALC est la logique de description la plus importante, car elle constitue la base de toutes les LD pratiques.

La LD ALC est défini par un tuple ordonné ∑ = (**C, R, O**) de 3 alphabets disjoints : 

- l’ensemble **C** de noms de concepts
- l’ensemble **R** de noms de rôles
- l’ensemble **O** de noms d’objets (ou noms d’individus)

Les noms de concepts et de rôles sont aussi appelés concepts atomiques et rôles atomiques.

### Syntaxe de AL

Soit : 

- **A** un **concept atomique** 
- **C** et **D** des **concepts atomiques ou complexes** 
- **R** une relation (**rôle**) 
- **⊤** : le **concept universel** 
- **⊥** : le **concept impossible** (ou le plus spécifique)

| Constructeur | Description |
|--|--|
| **¬ A** | La négation atomique |
| **C ⊓ D** | L'intersection de concept |
| **∀R.C** | La restriction de valeur (quantification universelle complète) |
| **∃R.⊤** | La quantification existentielle limitée |



### Syntaxe de ALC

- **C** et **D** des **concepts atomiques ou complexes** 
- **R** une relation (**rôle**) 
- **⊤** : le **concept universel** 
- **⊥** : le **concept impossible** (ou le plus spécifique)

| Constructeur | Description |
|--|--|
| **¬ C** | non C ou Complément de C |
| **C ∪ D** | L'union de concept |
| **C ⊓ D** | L'intersection de concept |
| **∀R.C** | La quantification universelle |
| **∃R.C** | La quantification existentielle |
