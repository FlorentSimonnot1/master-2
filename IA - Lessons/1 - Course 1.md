﻿# RDF (Resource Description Framework)

## Qu'est ce que RDF ?

RDF est un framework pour la représentation d'information sur le Web. C'est un modèle de graphe qui représente les resources Web et leurs métadonnées, afin de permettre leur traitement automatique. 
Développé par le W3C, il est le langage de base du Web sémantique.

![Web semantic](./img/rdf_archi.png)

Il a pou objectif d'attacher à une ressource, un ensemble de propriétés (métadonnées) qui la caractérise au mieux, et les partager.

Un document structuré en RDF est un ensemble de triplets : 

- Sujet / Ressource : la resource à décrire
- Prédicat : un type de propriété applicable à cette ressource
- Objet : une donnée ou une autre ressource -> la valeur de la propriété

		<sujet> <prédicat> <objet>

![RDF](./img/rdf.png)

Exemple en pseudo-code :

	<Arnaud> <est une> <personne>.
	<Arnaud> <est un ami de> <Béatrice>.
	<Arnaud> <est né> <le 14 juillet 1990>. 
	<Arnaud> <s'intéresse à> <la Joconde>.
	<la Joconde> <a été créée par> <Léonard de Vinci>.
	<la vidéo 'La Joconde à Washington'> <est à propos de> <la Joconde>.

Un ensemble de déclarations RDF forme un graphe RDF. Les sujets et Objets forment des noeuds tandis que les prédicats forment des arcs.

![RDF](./img/example.png)

Note: Ce graphe pourra être extrait à partir de SPARQL.

Il y a 3 types de données qui apparaissent dans des triplets : 

- les IRI
- les Littéraux
- Les noeuds vides

### A) Les IRIS

Une IRI identifie une ressource. Elles peuvent apparaître aux **trois** positions d'un triplet.

> Allocation : processus d'association d'une IRI  à une ressource. 
> Référent : Ressource associé à une IRI.

Une IRI est la généralisation d'une URI permettant une plus large gamme de caractère unicode. Toute URI et URL sont une URI mais cette relation n'est pas réciproque !

### B) Les Littéraux

Les littéraux sont des valeurs de base qui ne sont pas des IRI. Les littéraux sont associés à un type de données, les <b>Datatypes</b>, permettant d'être interprété correctement (chaine, date, nombre). Les littéraux de type date peuvent être associés à une balise langue (fr pour du français, zh pour le chinois etc...).

Types :

Par défaut, un littéral est une chaine de caractères. On peut changer le type d'un littéral grâce aux datatypes.

	xsd:integer
	xsd:float
	xsd:string
	xsd:dateTime
	xsd:boolean
	rdf:XMLLiteral rdfs:Literal

Exemple :

    <!ENTITY xsd ”http://www.w3.org/2001/XMLSchema#” > 
    <c:Person>
	    <c:age rdf:datatype='&xsd;integer'>43</c:age>
	    <c:date rdf:datatype='&xsd;dateTime'>2004-01-05</c:date>
    </c:Person>

Langue : 

    rdf:langString

Les littéraux ne peuvent apparaître qu’en position d’**objet** d’un triplet.

### C) Les noeuds vides

Les noeuds vides sont utilisés pour introduire des noeuds sans URI.
Ce sont des identifiants à portée locale.
Ils peuvent être remplacer par une IRI.

Les noeuds vides peuvent apparaître en position de **sujet** et d'**objet**.

Les noeuds vides sont utiles pour les n-aires relations et la réification.

## La Réification

La réification est le processus de transformation d'un concept en un objet informatique.

En RDF, la réification permet de considérer un triplet comme un noeud.

Exemple : 

Soit le graphe G1 : 

    <ex:un_sujet> <ex:une_propriété> <ex:un_objet>

Le graphe G2 ci dessous est une réification de G1 : 

	_:xxx rdf:type rdf:Statement
	_:xxx rdf:subject <ex:un_sujet>
	_:xxx rdf:predicate <ex:une_propriété>
	_:xxx rdf:object <ex:un_objet>

## Dataset

Un document RDF qui décrit plusieurs graphe est appelé un **Dataset**.

Dans un dataset, tous les graphes, sauf un, on une IRI ou un noeuds blanc associé. Ils sont appelés **graphes nommés**. Le graphe restant est appelé graphe par défaut.

## RDF Triples, Graphs et Datasets généralisés

Un graphe RDF généralisé est un ensemble de triplets RDF généralisés.

Un dataset RDF généralisé comprend un graphe RDF généralisé, et zéro ou plusieurs paires chacune associer une IRI, un nœud vide ou un littéral à un graphe RDF généralisé.

## Serialisation

Pour échanger des données, on a besoin de sérialiser.

Il existe de nombreux formats de sérialisation.

Les plus connus sont : 

- RDF/XML
- N-Triples
- Turtle

### RDF/XML 

Norme RDF/XML : syntaxe XML pour représenter un graphe RDF.

- Élément **Description** pour décrire une ressource attribut **about** pour le sujet.
- Sous-élément pour la propriété, contenu du sous-élément pour la propriété (qui peut être parfois simplifié en attribut)
- On peut regrouper dans un même élément Description toutes les propriétés dont cette ressource est sujet.

-> **Difficile à lire par un humain, réservé à la machine**

### N-Triples

- Sérialisation sous forme de triplets 
- Chaque triplet est écrit sous la forme :

`<IRI du sujet>  <IRI du prédicat> <IRI de l'objet ou littéral>`

-> **Plus facile à lire par un humain**

### N-Quad

N-Quads est un format de texte brut basé sur la ligne pour coder un RDF dataset.
IRIs ne peut être écrit que comme IRIs absolu.

Les nœuds vides RDF dans N-Quads sont exprimés grâce au caractère **_** suivis par
étiquette de nœud qui est une série de caractères de nom.

### Turtle (Terse RDF Triple Language)

Dérivé de N-Triples, plus concis, avec des facilités syntaxiques pour rendre le code plus lisible.

      @prefix  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  .
	  @prefix  dc:  <http://purl.org/dc/elements/1.1/>  .
	  @prefix  ex:  <http://example.org/stuff/1.0/>  .
  
	  <http://www.w3.org/TR/rdf-syntax-grammar>
	  dc:title  "RDF/XML Syntax Specification (Revised)"  ;
	  ex:editor  [
	  ex:fullname  "Dave Beckett";
	  ex:homePage  <http://purl.org/net/dajobe/>
	  ]  .

-> **Plus concis, plus facile encore à lire par un humain**

### TriG

Extension de Turtle pour utiliser plusieurs graphes

### JSON-LD

Linked Data est un moyen de créer un réseau de normes de données interprétables par la machine sur différents documents et sites.

JSON-LD est une syntaxe légère pour sérialiser les données liées dans JSON.

JSON-LD est principalement destiné d'être un moyen d'utiliser Linked Data dans des environnements de programmation Web, à construire des services web interopérables, et pour stocker des données liées dans des moteurs de stockages basé sur JSON.

JSON-LD spécifie un certain nombre de jetons de syntaxe et de mots clés qui sont une partie centrale de la langue: @ context, @id (identifier choses-IRIs, nœuds vides), @value (données d'une propriété), @langue, @type @inverse (inverse de la propriété).

JSON: facile à analyser et à générer mais difficile à intégrer différentes sources et manque d'un support pour les liens.

    { "name": "Manu Sporny", 
    "homepage": "http://manu.sporny.org/", 	
    "image": "http://manu.sporny.org/images/manu.png" 
    } { 
    "http://schema.org/name": "Manu Sporny", 
    "http://schema.org/url": { "@id": "http://manu.sporny.org/" }, 
	    # The ’@id’ keyword means ’This value is an identifier that is an 
		    IRI’ "http://schema.org/image": { "@id": 
	    	"http://manu.sporny.org/images/manu.png" 
	    } 
    }

### RDFa

RDFa (RDF in attributes) est une technique qui permet d'ajouter des données structurées dans des pages HTML.

Il fournit un ensemble d'attributs de balisage pour augmenter le visuel informations sur le Web avec des conseils lisibles par machine.

RDFa 1.1 fonctionne à partir de XHTML, HTML5 et de tout XML
langue (par exemple, SVG).

## Schéma RDF

Un schéma RDF fournit un vocabulaire de modélisation de données pour des données RDF. Il permet de définir une organisation hiérarchique des classes et propriétés.

![Web semantic](./img/rdfs_archi.png)

Le système de classe et de propriété d'un schéma RDF est similaire à un système de type de langages de programmation orienté objet.

C'est une extension du vocabulaire basique de RDF.
C'est un premier langage de définition d'ontologie.

Le schéma RDF décrit les propriétés en termes de classes de ressource à laquelle ils s'appliquent.

Par rapport à la définition de classes et propriétés d’un langage de POO comme Java :

- RDFS opte pour une approche centrée sur les propriétés : Au lieu de définir une classe en donnant leurs propriétés, on définit des propriétés en donnant leur domaine et co-domaine. 

Exemple : 

En java, la classe **Document** à un attribut **author** de type **Personne**.
En RDFS, la propriété **author** a pour domaine la classe **Document**, et pour co-domaine la classe **Personne**.

- Une ressource peut avoir plusieurs type
- Une ressource peut être instance de plusieurs classes

### Classes

Une classe est une ressource. On utilise la propriété `rdfs:Class`

Exemple :

![Class](./img/example_class.png)

Une ressource peut avoir plusieurs types de classe.

![Multi_Class](./img/example_multi_class.png)

Une classe peut être une sous classe d'une ou plusieurs classes.
Pour cela, on utilise la propriété `rdfs:subClassOf`.

![Sub_Class](./img/example_sub_class.png)

L'ensemble des instances d'une classe est appelée son extension.

### Propriétés 

Une propriété est de type `rdf:Property`.

On peut définir une relation de sous propriété grâce à la propriété `rdfs:subPropertyOf`. On peut également définir le type du sujet (domaine) et/ou l'objet (co-domaine) d'une propriété grâce aux attributs suivants : 

- `rdfs:domain` : définit la classe des sujets liés à une propriété.
- `rdfs:range` : définit la classe ou le type de données des valeurs de la propriété.

Quelques propriétés particulières : 

 - `rdfs:seeAlso` : permet d'associer 2 classes ou 2 propriétés.
 - `rdfs:isDefinedBy` : permet d'indiquer d'une ressource définissant la ressource sujet.
 - `rdfs:label` : (documentation) permet d'associer aux classes et propriétés que l'on définit des noms (labels) compréhensibles par des humains.
 - `rdfs:comment` : (documentation) permet d'associer un commentaire à une classe ou une propriété, pour en donner une description.

### Container et classes

Il est fréquent de devoir faire référence à plusieurs ressources (par exemple : un livre écrit par plusieurs auteurs).

Les containers permettent de décrire des **groupes**.
Les choses contenus dans un container sont appelés **membres du groupe**.

Il existe 3 types de containers prédéfinis : 

- **rdf:Bag** : liste non ordonnée de ressources ou de littéraux
- **rdf:Seq** : liste ordonnée de ressources ou de littéraux
- **rdf:Alt** : liste de ressources ou de littéraux qui représentent des alternatives pour une valeur unique

On peut indiqué le type d'un container par la propriété **rdf:type**.

`rdfs:Container` : super classe de `rdf:Bag`, `rdf:Seq`, `rdf:Alt`.
`rdf:Bag`, `rdf:Seq`, `rdf:Alt` sont les classes de Bag, Séquence et Alternative Containers.
`rdfs:ContainerMembershipProperty`
`rdfs:member`

Note : Les containers sont **ouverts**, i.e. il peut exister d’autres membres du container que ceux indiqués par la description dont on dispose (qui s’y rajoutent …)

### Collections

Une collection est une liste.

Une liste de type **rdf:List** avec : 

- un premier élement **rdf:first** et
- une suite **rdf:rest**

La liste vide a la valeur **rdf:nil**.

La valeur suivante est représenté par la propriété **rdf:next**.

Une collection est une liste **fermée**.

### Réification

`rdfs:Statement` : permet de représenter une class de statements.
`rdf:subject`, `rdf:predicate`, `rdf:object` : sont des instances de `rdf:Property` et permettent de déclarer le sujet, prédicat et objet d'un statement.
