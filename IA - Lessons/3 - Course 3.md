﻿# Logique

## Introduction

Logique = raisonnement

Les logiques sont des langages formels pour représenter l'information de telle sorte que les conclusions peuvent
être tirées.

Syntaxe = se rapporte à la forme et définit les phrases dans la langue
Sémantique = se rapporte au sens et définit la vérité d'une phrase dans un monde.

Les éléments élémentaires de la logique propositionnelle sont : 

- énoncés atomiques qui ne peuvent plus être décomposés: propositions , par exemple ”il pleut”.
- connecteurs logiques ”et” (∧), ”ou” (∨), ”pas” (), ”implication” (→), ”l'équivalence” ( ↔ ) avec lequel nous pouvons construire propositionnel formule.

Exemple : 

"Si je regarde dans le ciel et que je suis alerte alors soit je verrai la soucoupe volante soit si Je ne le suis pas alerte alors je ne verrai pas la soucoupe volante".

A : "Je regarde dans le ciel"
B : "Je suis alerte"
C : "Je vois une soucoupe volante"

**(A ∧ B) → (C ∨ (¬B → ¬C))**

Syntaxe : 

- Formule atomic : T, ⊥ ou si c’est une variable propositionnelle.
- Formule propositionnelle : 
	¬φ negation 
	φ ∧ ψ conjonction 
	φ ∨ ψ disjunction 
	φ → ψ implication 
	φ ↔ ψ equivalence

Un *littéral* est une **formule atomique ou sa négation**.

Une *clause* est une **disjonction de littéral**.

## Équivalence 

Commutativity : 
φ ∨ ψ ≡ ψ ∨ φ 
φ ∧ ψ ≡ ψ ∧ φ 
φ ↔ ψ ≡ ψ ↔ φ 

Associativity : 
(φ ∨ ψ) ∨ χ ≡ φ ∨ (ψ ∨ χ) 
(φ ∧ ψ) ∧ χ ≡ φ ∧ (ψ ∧ χ) 

Idempotence : 
φ ∨ φ ≡ φ 
φ ∧ φ ≡ φ 

Absorption : 
φ ∨ (φ ∧ ψ) ≡ φ 
φ ∧ (φ ∨ ψ) ≡ φ 

Distributivity : 
(φ ∨ ψ) ∧ χ ≡ (φ ∧ χ) ∨ (ψ ∧ χ)

Tautology : 
φ ∨ T ≡ T

Unsatisfiability : 
φ ∧ ⊥ ≡ ⊥ 

Negation : φ ∨ ¬φ ≡ > φ ∧ ¬φ ≡ ⊥ 

Neutrality : φ ∧ > ≡ φ φ ∨ ⊥ ≡ φ 

Double negation : ¬¬ φ ≡ φ 

De Morgan : 
¬(φ ∨ ψ) ≡ ¬φ ∧ ¬ ψ 
¬ ( φ ∧ ψ ) ≡ ¬ φ ∨ ¬ ψ 

Implication : 
φ → ψ ≡ ¬φ ∨ ψ

Les déclarations atomiques peuvent être vraies ou fausses.

La valeur de vérité des formules est déterminée par les valeurs de vérité des atomes.

Exemple : ((a ∨ b) ∧ c) : si a et b sont faux et c est vrai, alors la formule est pas vrai.

## Implication et Interprétation

L'implication signifie qu'**une chose découle d'une autre** :

KB |= α

La Base de Connaissances (KB) implique la phrase : α si et
seulement si α est vrai dans tous les mondes où KB est vrai.

L'implication est une relation entre les phrases
(c'est-à-dire la syntaxe) qui est basée sur la sémantique.

--- 

Une interpretation pour un ensemble de variable propositionnelles P est une fonction **I : P → B**. 
B, est l’ensemble {0, 1}.

La valeur de vérité d’une formule A par rapport a une interprétation I, notée [A]I est définie intuitivement de la manière suivante :

- Si p est une variable propositionnelle, [p]I = I(p).
- [T]I = 1 et [⊥]I = 0.
- Si A est une formule logique alors [¬A]I = f¬([A]I ).
- Si A et B sont deux formules logiques alors : 
	– [A ∨ B]I = f∨([A]I , [B]I ) 
	– [A ∧ B]I = f∧([A]I , [B]I ) 
	– [A ⇒ B]I = f⇒([A]I , [B]I ) 
	– [A ⇔ B]I = f⇔([A]I , [B]I )

## Modèles

Les logiciens pensent généralement en termes de modèles, qui sont des mondes formellement structurés par rapport à laquelle la vérité peut être évaluée.

On dit que m est un modèle d'une phrase φ si φ est vrai dans m.

M(φ) est l'ensemble de tous les modèles de φ.

KB |= φ ssi M(KB) ⊆ M(φ)

Exercice : 

- α = a ∨ b
- KB = (a ∨ c) ∧ (b ∨ ¬c)
- KB |= α tient-il ?

## Inférence, Raisonnement, Déduction

KB |- I φ signifie que la phrase φ peut être dérivée de KB par la procédure I.

Une procédure de décision résout un problème avec la réponse oui ou non.

La forme d'une argumentation déductive est **correcte** (soundness)  si et seulement si elle est valide et que toutes ses prémisses sont effectivement vraies.

Un système formel est dit **complet** (completeness) par rapport à une propriété particulière si chaque formule possédant cette propriété peut être prouvé par une démonstration formelle à l'aide de ce système, c'est-à-dire, par l'un de ses théorèmes.

Nous nous intéressons aux logiques expressives (qui
permet d'exprimer presque n'importe quoi d'intérêt) pour lequel il existe une correction et procédure d'inférence complète.

## Satisfaisant VS valide

Une forme φ est : 

**satisfaisante**, s'il y a un I qui satisfait φ
**insatisfaisante**, si φ n'est pas satisfaisant.
**falsifiable**, s'il y a un I qui ne satisfait pas φ.
**valide** (ou une tautologie), si chaque I est un modèle de φ.

Deux formules sont logiquement équivalentes (écrit φ ≡ ψ), si pour tout I : I |= ϕ ssi I |= ψ.

## Conséquence

- φ est une tautologie si φ n'est pas satisfaisant
- φ est insatisfaisant ssi ¬φ est une tautologie
- φ ≡ ψ ssi φ ↔ ψ est une tautologie

## Équivalence

## CNF et DNF

La **forme normale conjonctive** (CNF) est une conjonction de disjonctions de littéraux (clauses). CNF nous dit quelque chose à savoir si un la formule est une tautologie (si toutes les clauses contiennent T).

Exemple : (φ ∨ ψ) ∧ (χ ∨ ω)

La **disjonction des littéraux** (DNF) est une disjonction des conjonctions des littéraux. DNF indique quelque chose quant à savoir si une formule est satisfiable (si toutes les disjonctions contiennent ⊥).

Exemple : (φ ∧ ψ) ∨ (χ ∧ ω ∧ ¬α)

Pour toutes formules, il existe une formule CNF et DNF équivalente.

## Consistence, Procédure de décision et Réductions

Une collection de déclarations est cohérente si les déclarations peuvent toutes être vraies simultanément.

---

Les tables de vérité fournissent une procédure de décision solide et complète pour tester la satisfiabilité, la validité et l'implication dans la logique propositionnelle.
La preuve est basée sur l'observation que les tables de vérité énumèrent tous les possibles modèle.
Satisfiabilité, validité et implication dans les logiques propositionnelles sont donc des problèmes décidables.

--- 

Une formule φ est satisfiable si il y a une interprétation I qui satisfait φ (I |= φ).

La validité, l'équivalence et l'implication peuvent être réduit à satisfiabilité :
– φ est valide ssi φ n'est pas satisfaisante.
– φ implique ψ (φ |= ψ) ssi φ → ψ est valide.
– φ est équivalent à ψ ssi φ ↔ ψ est valide.

## Tableau sémantique 

Le tableau sémantique est une procédure de décision résolvant le problème de la satisfiabilité.

Si une formule est satisfiable, la procédure présentera de manière constructive un modèle de la formule.

Idée: construire progressivement le modèle en
en regardant la formule, en décomposant en haut en bas. 
La procédure examine de manière exhaustive toutes les possibilités,
pour qu'il puisse éventuellement prouver qu'aucun modèle pourrait être trouvé pour satisfaire la formule.

Chaque fois qu'une forme logique A et sa négation apparaissent dans une branche d'un tableau, une incohérence est indiqué dans cette branche et il est dit "fermé".

Si toutes les branches sont fermées, alors les expressions logiques sont dit inconsistent.

![Rules](./img/tree_rules.png)

Les règles de tableau sémantique peuvent être visualisées
purement syntaxiquement.

Ils peuvent également être considérés comme fournissant le
moyens de construire des modèles de déclarations symbolisées par les expressions logiques.

Avec cette approche, notre compréhension des tableaux sémantiques sera en termes de théorie des modèles plutôt que de théorie de la preuve.

À partir d'une branche non fermée, il est facile d'attribuer des valeurs de vérité à travers lesquelles les formules peuvent toutes être vérifiées.

![Arbre 1](./img/tree_1.png)

Si nous voulons savoir si une conclusion suit
logiquement, à partir d'un ensemble de locaux, nous pouvons utiliser
la stratégie suivante :
recherche si la négation de la conclusion est incompatible avec les locaux.

![Arbre 2](./img/tree_2.png)

Le tableau est complètement fermé, donc les déclarations sont incompatibles.

L'une des déclarations était la négation de la conclusion, on en déduit que, si le prémisses sont vraies, alors la négation de la conclusion ne peut pas être vraie.

Par conséquent, la conclusion est vraie.

La complexité des tableaux dépend la structure syntaxique des formules dans le KO.

Le tableau sémantique est une procédure de décision pour le calcul de la satisfiabilité, de la validité et l'implication dans les logiques propositionnelles: c'est un algorithme complet, correcte et terminé.

La résolution est une méthode mécanique de traitement des problèmes en logique.

Pour utiliser la résolution, la formule doit être sous une forme normale: CNF.

## Convertir en CNF

1) A ↔ B ≡ (A → B) ∧ (B → A) pour éliminer l'équivalence.

2) A → B ≡ ¬A∨B pour éliminer l'implication.

3) Utilisation de la loi de Morgan pour ajouter la négation devant la formule.

4) Utiliser ¬¬A ≡ A pour éliminer les doubles négations.

5) Loi de distributivité pour terminer la conversion.

## Résolution d'arguments

Considérons les clauses (¬A ∨ B) et (¬B ∨ C).

La première clause contient B tandis que le second contient ¬B.

Les 2 clauses peuvent être réduites en une simple clause (¬A ∨ C). On l'appelle résolveur.

Exemple :

![Résolution d'arguments](./img/resolvent.png)

La dérivation de la clause vide signale que les clauses avec lesquelles nous avons commencé étaient incompatibles. Il représente une façon de construire un argument de réfutation standard, c'est-à-dire de nier la conclusion et de montrer qu'elle est incompatible avec les prémisses.

Une déduction de résolution d'une clause C à partir d'un ensemble de S de clauses est une séquence finie de clauses C1, C2, ..., Cn, de telle sorte que chaque Ci soit un membre de S ou un résolveur de deux clauses tirées de S ou de membres antérieurs de la séquence.


