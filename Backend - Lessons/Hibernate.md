# Hibernate

## ORM - What is it ?

In computer science, ORM is a programming technique for converting an application to a relational database by creating 
an oriented object database.

ORM manage objects in memory. Objects can be persistent or not.

The idea is to associate a Java class (POJO) to a or many database tables.
The SQL is automatically generated.

ORM use the mapping to realize that.

## Hibernate Sessions

Sessions are the interface between Java and hibernate. They represent a connection to JDBC. Sessions contains persistent
objects and use transactions.

To create a session, hibernate provides a factory. There is only one factory for an application.
This factory is thread-safe contrary to the sessions.

    public class HibernateUtils {
         static final private SessionFactory sessionFactory;
         static final private StandardServiceRegistry registry;
         // thread-safe implementation of the singleton pattern
         static {
         // Create sessionFactory from the hibernate.cfg.xml file
         registry = new StandardServiceRegistryBuilder().configure().build();
         MetadataSources sources = new MetadataSources(registry);
         Metadata metadata = sources.getMetadataBuilder().build();
         sessionFactory = metadata.getSessionFactoryBuilder().build();
         }
         static public SessionFactory getSessionFactory(){
         return sessionFactory;
         }
    }


Hibernate interactions have always the same methodology :

    var sessionFactory = HibernateUtils.getSessionFactory();
    try (Session session = sessionFactory.openSession()) {
         Transaction tx = null;
         try {
             tx= session.beginTransaction();
             // Do what you want
             tx.commit();
         } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            throw e;
         }
    }
    
We get the session, begin a transaction, execute what we want (save, delete, update...), commit our changes
and close the session (thanks to try-with-resources the session is close automatically).

Hibernate provide a method to get the current session. 

    Session session = sessionFactory.getCurrentSession();
    Transaction tx = null;
    try {
        tx= session.beginTransaction();
        ...
        tx.commit();
    } catch (HibernateException e) {
        if (tx != null) tx.rollback();
        throw e;
    }

Be careful when you use this method because it is not thread safe !

Note : To use the method `getCurrentSession()` we have to add a property in configuration file.

    <property name="current_session_context_class">thread</property>

## Save, delete, update and retrieve data

### Save - Persist

Save a transient object.

    persist(object)
    
The object have to have a fixed id.

### Retrieve - Get

Gets an object in according to its id.

    get(Clazss.class, id): Object
    
The first parameter is the object type we want to retrieve.

### Update - Merge

Update an object.

    merge(object): Object
    
The object to update have to have a fixed id.

### Delete - Delete

Delete an object.

    delete(object)
    
Like merge method, the object have a fixed id. 
After a get for example.
    
## Objects state

In Hibernate session, Java objects can have one of these 3 states : 

- Transient
- Persistent
- Detached

### Transient

A transient object is a java object which is not associate to a session.
This is the result of an object creation (new) for example.

### Persistent

A persistent object is a java object associated to a session (when you retrieve data thanks to get method for example).
All modification on a persistent object is update on the next commit.

### Detached

A detached object is an object which was already associated to a session (there is the primary key in the object) but no longer.

Schema : https://www.baeldung.com/hibernate-save-persist-update-merge-saveorupdate

### Merge case

The merge method is a bit tricky. The object take as parameter is a detached object and return 
a persistent object with the same id of the detached object.

So be careful not to use the detached object anymore.

## Persist composed object

In Java, we often use composed objects. That's mean objects can contain another objects.
For example, if we represent a student we can use another object to represent his address.

    public class Student {
        private long id;
        private String firstName;
        private String lastName;
        private Address address;
        
        public Student(){}
        
        public Student(String firstName, String lastName, Address address) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
        }
        
        // getters and setters...
    }
    
    public class Address {
        private int streetNumber;
        private String street;
        
        public Address(){}
        
        public Address(int streetNumber, String street) {
            this.street = street;
            this.streetNumber = streetNumber;
        }
        
        // getters and setters...
    }
    
In a database there are 2 solutions to represent this case.

### Only one table

We can create a table for students and add all address fields in the student table.

| Id | firstName | lastName | streetNumber | street |
|---|:---:|:---:|:---:|:---:|
| 1 | Bob | Morane | 55 | Aventure Street |

### Two tables 

Another solution, may be more interesting, is to create 2 tables.
The first table is using to save student. The second table represents the addresses.
In student table we use a foreign key which represent the address.

| Id | firstName | lastName | addressId |
|---|:---:|:---:|:---:|
| 1 | Bob | Morane | 1 |

| Id | streetNumber | street |
|---|:---:|:---:|
| 1 | 55 | Aventure Street |

## Relationships

When we use composed object, there are 4 cases to create relation between objects : 

- One-To-One relation
- One-To-Many relation
- Many-To-One relation
- Many-To-Many relation

More, there are two directions for a relation : 

- unidirectional
- bidirectional

### Directions

Bidirectional relationship provides navigational access in both directions, so that you can access the other side without explicit queries. Also, it allows you to apply cascading options
to both directions.


Considering the unidirectional relationship, you can imagine this relation as an owner - tenant relation.
The owner can have some information about the tenant. You can so retrieve some data from the owner.
But you can't get information about the owner from the tenant.

Hibernate recommend you to prefer bidirectional associations because unidirectional association are more difficult to query.
That's why in large application, almost all associations must be navigable in both directions in queries.

However, keep in mind that sometimes, it's not necessary to retrieve data of the owner from the tenant.

By default, relationship are unidirectional. We can use annotation or change this property in xml file.

_Note : All relation types can be bidirectional by using an annotation or changing property in xml file._

### Relationship types

**1) One-To-One**

This relation used to represent an object which contains a maximum of 1 object. Vice versa, this object belongs to only one object.

For example, a mobile phone have only one telephone number, and a telephone number belong to only one mobile phone.

This type of relationship is translate in database by adding a foreign key in the owner.
We can set the name of the foreign key column thanks to an annotation, or a property in xml file.

**2) One-To-Many and Many-To-One**

This two relations are close.
This relationship links one entity to many other entities.

To explain these relations we use the following example : 

Considering a teacher and their courses. A teacher can give multiple courses, but a course is given by only one teacher.

    public class Teacher {
    
        private String firstName;
        private String lastName;
    }
    
    public class Course {
        private String title;
    }
    
In teacher class, we have to add a list of course (Or Set or Map). Generally, in the database, the owner have a column for the foreign key.


### Optional or not ?

A relationship may be optional or mandatory.

Considering the One-To-Many side, it's always optional. We can't change it.

The Many-To-One relationship can be turn to mandatory (optional by default).

### Cascade or not ?

Entity relationship often depend on the existence of another entity. For example, the Person - Address relationship.
Without the person, the address entity doesn't have any meaning. When we delete the Person entity, our address entity should also get deleted.

Cascading is the way to achieve this. When we perform some action on an entity, the same action will be applied to the associated entity.

It's exist many cascade type : 

- ALL
- PERSIST
- MERGE
- REMOVE
- REFRESH
- DETACH

Note : Sometimes, we don't use the cascade property. Imagine you have a Student entity and a University entity. The relation is @OneToMany, and the owner is the student.
But if you delete a student for example, the university have not to be deleted.

### Lazy or not ?

When we have relationship between entities, it could be interesting to avoid loading all entities if this is unnecessary.

The example with teacher and courses is a good example. Maybe sometimes we want to retrieve information about a teacher without checking their courses.
So, it's unnecessary to load courses in that case.

Hibernate have the notion of lazy. We load data if we need the data. But be careful. All relationships have not the same fetch type value by default.

Note : Lazy loading is not very important in One-To-One relationship.

| Relationship | Fetch value |
|:---:|:---:|
|One-To-One| Not Lazy|
|One-To-Many| Lazy|
|Many-To-One| Not lazy|
|Many-To-Many| Lazy|

With lazy loading, one may have an error called proxy error. That's mean we try to show an object, but some fields are not loading because there are in a lazy fetch mode.

You have 2 solutions to treat this problem.

First, be sure the lazy information are important. Maybe you don't have to show them.

Second, you can use a method providing by Hibernate : `Hibernate.initialize(entity.getXXX);`

Example : 

> We have two entities : Student and Department. A department have many students but a student belows to only one department. 
> This is a OneToMany relationship. The owner is department.

Imagine we have a toString method in Department which show all fields, even all students which are in the department.
We probably have a problem because of the students. Probably they are not loading. The question is :
Do you need to show students when you display a department ? Maybe you can show only information like name, place and create another function to get all students which are in the department.

Now imagine you want to get all students which below to a department. First you have to get the object Department in a session.

    public List<Student> findStudentsByDepatment(Department dep) {
        Department data = session.get(Department.class, dep.id);
        ...
    }
    
Obviously you would like to use the method `getStudents()` in department class to get all students of this department.

    public List<Student> findStudentsByDepatment(Department dep) {
        Department data = session.get(Department.class, dep.id);
        return data.getStudents();
    }
    
But you can't ! Why ? Because students are in lazy loading fetch mode.

After retrieve data, you can call `Hibernate.initialize()` method on the `getStudents()` method.

    public List<Student> findStudentsByDepatment(Department dep) {
        Department data = session.get(Department.class, dep.id);
        Hibernate.initialize(data.getStudents());
        return data.getStudents();
    }
    
No it's work fine !

## Mapping

In this chapter, we'll learn to set the mapping for hibernate.

There are 2 methods to create mapping. First we can use xml files, but we can use annotations too.

### Mapping with files

For each class we want to map, we have to create a file with the suffix **.hbm.xml**.
These files have to be located in resources folder.

This following mapping is use for the first solution. We have a student with it fields and the address fields.

    <hibernate-mapping>
        <class name = "fr.uge.jee.hibernate.cours.Student" table = "Students">
            <id name = "id" type = "long" column = "id">
                <generator class="native"/>
            </id>
            
            <property name = "firstName" column = "first_name" type = "string"/>
            <property name = "lastName" column = "last_name" type = "string"/>
            
            <component name= "address" class="fr.uge.jee.hibernate.cours.Address">
                <property name = "streetNumber" column = "street_name" type = "int"/>
                <property name = "street" column = "street" type = "string"/>
            </component>
        </class>
    </hibernate-mapping>

The second solution is to create a file for student class, and a file for address class.

The address file seam like : 

    <hibernate-mapping>
        <class name = "fr.uge.jee.hibernate.cours.Address" table = "Addresses">
            <id name = "id" type = "long" column = "id">
                <generator class="native"/>
            </id>
        
            <property name = "streetNumber" column = "street_number" type = "int"/>
            <property name = "street" column = "street" type = "string"/>
        </class>
    </hibernate-mapping>

    <hibernate-mapping>
        <class name = "fr.uge.jee.hibernate.cours.Student" table = "Students">
            <id name = "id" type = "long" column = "id">
                <generator class="native"/>
            </id>
            <property name = "firstName" column = "first_name" type = "string"/>
            <property name = "lastName" column = "last_name" type = "string"/>
            <many-to-one name="address" column ="address_id" unique="true" class="fr.uge.jee.hibernate.cours.Address"/>
        </class>
    </hibernate-mapping>

As you can see we have to use a many-to-one relation. We will speak about this relation later.
We can use one-to-one too, but it's preferable to use many-to-one relation because one-to-one relation create the address with the same id that student.
As we use many-to-one relation, we have to use the unique attribute.


### Mapping with annotation - simple objects

In my point of view, it's easier to use annotation to set the mapping.

First, when we create some POJO object, if you use annotations you have to use the `@Entity` annotation.
This annotation is used to precise the object is a bean entity.
This is the same effect to create a xml file for this class.

Then you can use the `@Table` annotation. It's not necessary but this annotation create the table with the attributes set in this annotation.

Example : 

    @Entity
    @Table(name="students")
    public class Student {
        ...
    }
    
Here we create a table named student.

Note : If you don't use the table annotation, hibernate create a table with the class name.

When you work with annotations, you have to use the `@Id` annotation.
It used to precise which field is the primary key. You can set the generation type too thank to 
`@GeneratedValue` annotation.

Example :

    @Entity
    @Table(name="students")
    public class Student {
        
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private long id;
    }
   
It's exist some generation type but this is not important to learn them.

In this example, we have a table students with a column for the id which is automatically generated by hibernate.

For other fields, you have to use the `@Column` annotation. This annotation is useful to set the name of column for example.
We can precise if the value have to be unique, nullable and some others properties.

Example : 

     @Entity
     @Table(name="students")
     public class Student {
         
         @Id
         @GeneratedValue(strategy = GenerationType.IDENTITY)
         private long id;
         
         @Column(name= "first_name", nullable= false, length= 100)
         private String firstName;
     }

In this example, hibernate will map the firstName field with a column named "first_name". This column is not nullable and the 
max length is 100 characters.

### Mapping with annotation - composed objects

**1) One-To-One**

The annotation for mapping a single entity is `@OneToOne`.
We can precise the name of column which contains the foreign key thanks to the following annotation : `@JoinColumn(name = "name_fk", referencedColumnName = "ID")`


**2) One-To-Many and Many-To-One**

The annotation for mapping a Many-To-One/One-To-Man relationship is `@OneToMany` or `@ManyToOne`.

As OneToOne relationship, we can configure the foreign key column by `@JoinColumn` annotation.

**3) Many-To-Many**

A Many-To-Many relation involves an auxiliary table referencing both other tables.
This table contains an id, and the id of both objects associated.

To present this case we'll take the example of students and courses.

A student can attend multiple courses, and a course can be followed by multiple students.

To map a Many-To-Many relation we have to use the `@ManyToMany` annotation.
We can use the `@JoinTable` annotation to set some property on our auxiliary table.
By default, the name of auxiliary will be `table1_table2`.

Example (in course class) : 

    @ManyToMany
    @JoinTable(
      name = "students_courses",
    )
    private List<Student> students;
    
Thanks to joinColumns and inverseJoinColumns properties, we can precise the owning side and the other direction.

In the previous example, the owner is a course, and the tenant is a student.

**4) Relationship directions**

In a One-To-One relationship, we can precise the tenant thanks to this code : 

    // In telephone class
    @OneToOne()
    @JoinColumn(name = "number_id", referencedColumnName = "ID")
    private PhoneNumber class;

    // In phone number class
    @OneToOne(mappedBy = "telephone")
    private Telephone telephone;
    
In this example, a telephone is the owner of a telephone number. A telephone number belongs to a telephone. We use the property mappedBy to precise the owner in the tenant.

This is the same process for Many-To-One/One-To-Many relation.

    @Entity
    public class Teacher {
        // ...
    
        @OneToMany(mappedBy = "teacher")
        private List<Course> courses;
    }
    
    @Entity
    public class Course {
        // ...
        
        @ManyToOne
        @JoinColumn(name = "teacher_id", referencedColumnName = "ID")
        private Teacher teacher;
    }
    
Here, we decided that Course is the owner. We configure the foreign key column. In teacher class, the list of courses is the reverse direction. You can see
the parameter **mappedBy**.

In a Many-To-Many association, we use joinColumns and inverseJoinColumns properties from `@JoinTable` annotation to precise the owning side and the other direction.

     @ManyToMany
     @JoinTable(
       name = "students_courses",
       joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "ID"),
       inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "ID")
     )
     
     private List<Student> students;        
     
In this example, course is the owner.                 

**5) Optional**

For the Many-To-One relation : `@ManyToOne(optional = false)`

**6) Cascade and Lazy options**

Cascade : 

To set the cascade mode, we use the **cascade** parameter in the annotation @OneToOne, @ManyToOne, @OneToMany and @ManyToMany.

    @OneToOne(cascade = CascadeType.ALL)
    @OneToMany(cascade = CascadeType.REMOVE)
    ...

Lazy : 

In @OneToOne, @ManyToOne, @OneToMany and @ManyToMany annotation, we can declare the relationship Lazy or not by using the parameter **fetch**.

Example : 

    @OneToOne(fetch = FetchType.LAZY) // Lazy
    
    @ManyToMany(fetch = FetchType.EAGER) // Not lazy

