import org.apache.spark.sql.SparkSession

object SparkRunner extends App {

  val spark = SparkSession.builder.master("local").getOrCreate
  val sc = spark.sparkContext

  val sqlContext= new org.apache.spark.sql.SQLContext(sc)

  import sqlContext.implicits._

  // Question 1 - Load the dataset and the two dictionaries. Provide the sizes of these 3 RDDs.

  val path = "./resources/"

  val trip = sc.textFile(path + "univ1.nt").map(x=>x.split(" ")).map(x=>(x(1),(x(0),x(2))))
  val conc = sc.textFile(path + "univConcepts.txt").map(x=>x.split(" ")).map(x=>(x(1),x(0).toLong))
  val prop = sc.textFile(path + "univProps.txt").map(x => x.split(" ")).map(x => (x(1), x(0).toLong))

  println(trip.count() + " - " + conc.count() + " - " + prop.count()) // 100543 - 43 - 33

  // Question 2 - Encode the properties of the dataset. Provide the number of properties used in this new version of the
  // dataset. Are all dictionary properties used in the dataset?

  val trip1 = trip.join(prop).map{ case(p, ((s, o), pid) ) => (s, pid, o) }.setName("trip1").persist
  val pCount = trip1.map(x => (x._2, 1)).reduceByKey((a: Int, b: Int) => a + b)
  println(pCount.count)

  // Question 3 -
  // You need to create the dictionary of individuals (that is, the dataset entries that are not properties or
  //concepts). Each individual must have a unique identifier. To assign these identifiers, you can use one of
  //the RDD zip methods (on http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.
  //spark.rdd.RDD). How many unique individual do you have in this RDD.

  val ind = trip1.filter(x => x._2 != 0)
    .map(x => (x._1,x._3) )
    .flatMap{ case(s,o) => Array(s,o) }
    .distinct
    .zipWithUniqueId.setName("ind").persist

  println(ind.count)

  // Question 4 -
  // You have sufficient dictionaries (concepts, properties and individuals) to now encode the rest of the dataset.
  // Starting from the RDD created in 1.2, transform the URIs and individuals into their respective identifiers

  val trip2 = trip1.map{case(s,pid,o)=>(o,(s,pid))}.setName("trip2").persist
  println(trip2.count)
  trip1.unpersist(_)
  println(trip1.count)

  val trip3 = trip2.join(conc).map{case(o,((s,pid),oid))=>(s,(pid,oid.toLong))}.union(trip2.join(ind).map{case(o,((s,pid),oid))=>(s,(pid,oid.toLong))}).setName("trip3").persist
  println(trip3.count)

  // Question 5

  // From the RDD of 1.4, encode the subjects of the triples. Check that no triplets were lost during processing
  // by providing the number of entries in this last RDD. Check the size of the encoded dataset. Was it worth
  // encoding this data set in terms of memory footprint?

  val tripEncoded = trip3.join(ind).map{case(s,((pid,oid),sid))=>(sid,pid,oid)}.setName("tripEncoded").persist
  println(tripEncoded.count)

  // Part 2

  val tripDF = tripEncoded.toDF("s","p","o")
  tripDF.createOrReplaceTempView("triple")
  val res = spark.sql("SELECT t1.s, t2.s,t3.s FROM triple t1, triple t2, triple t3  WHERE t1.p=1233125376 AND t2.p=1136656384 AND t3.p=1224736768 AND t1.o=t2.s AND t2.o=t3.s")
  res.count
  res.show()

  val propDist = spark.sql("SELECT t.p, count(*) FROM triple t GROUP BY t.p ORDER BY count(*) desc")
  propDist.show()

}
