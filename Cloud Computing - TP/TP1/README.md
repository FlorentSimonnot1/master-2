﻿# TP 1

## Configuration

To realize this TP please check you have installed Scala, SBT and Intellij.

You have to create a new project from SBT. Then, we just configure the build file.

Your **build.sbt** file have to seem like : 

    name := "TP1"  
      
    version := "0.1"  
      
    scalaVersion := "2.11.8"  
      
    libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.4.1"  
    libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.0.2"

*Note :  Here we not use the latest Scala version. You can try to configure it for another version yourself.*

Finally, verify that you have the resources file *mobyDick.txt* in your resources folder.

## Questions 

### 1) From the Scala REPL (scala in a terminal of your machine), load the mobyDick file and compute the number of lines

    import scala.io.Source._  
  
	object Main extends App {  
	  val file = fromFile("./resources/mobyDick.txt")  
	  val lines = file.getLines().toList  
	  println(lines.size)  
	  file.close()  
	}

We count 21932 lines.

### 2) Install Spark. From the bin folder, start the Spark REPL: spark-shell. Execute the code of Question #1


### 3) Using the Spark documentation (https://spark.apache.org/), compute the number of lines in the book. You should use the Spark API, that is use the sc variable (Spark context). You should create an RDD to process this question and the remaining ones

    val myFile = "/Users/florentsimonnot/Downloads/mobyDick.txt"  
  
	val conf = new SparkConf().setAppName("TP1").setMaster("local")  
	val sc = new SparkContext(conf)  
	  
	val moby = sc.textFile(myFile)  
	println(moby)

Result : 

    Users/florentsimonnot/Downloads/mobyDick.txt MapPartitionsRDD[1] at textFile at Main.scala:16

### 4) From the structure (RDD) created in the previous question, select the lines containing the “CHAPTER” string. How many do you have?

    val chap = moby.filter(x=> x.contains("CHAPTER"))  
    println(chap.count())

Result : 

    285

### 5) How many empty lines (ie, length is 0) does the file contain?

    val emptyLines = moby.filter(l => l.length == 0)  
    println(emptyLines.count())

Result :

	3019

### 6) Using the structure of the previous question and inspired by the WordCount approach (2.1 Example in the MapReduce paper), compute the number of occurrence of each word in the Moby Dick book. Your solution needs to use a flatMap, map and reduceByKey operations. How many entries do you have in the structure?

    val wc = moby.flatMap(x => x.split(" ")).map(x => (x,1)).reduceByKey(_+_)  
    println(wc.count())

Result : 

	33087

### 7) How many entries do you have in the structure of Question #6 which contain the “captain” string?

	val cap = wc.filter(x => x._1.contains("captain"))  
	println(cap.count)

Result :

	19

### 8) Display some records of the structure of the previous question.

	cap foreach {case (key, value) => println(key + " --> " + value)}

Result : 

    sea-captain, --> 2
	captain’s --> 12
	captains --> 8
	captain’s, --> 1
	captain—this --> 1
	captain, --> 27
	captain. --> 4
	captain!—noble --> 1
	captain—Dr. --> 1
	captain). --> 1
	captain);—as --> 1
	captain; --> 3
	sea-captains --> 2
	captain --> 57
	captains? --> 1
	captains; --> 1
	sea-captain --> 1
	captains. --> 2
	captains, --> 6

### Bonus - Display 10 most view words

	val sorted = wc.map(x=>(x._2,x._1)).top(10)  
	sorted foreach {case (k, v) => println(k + " --> " + v)}

Result : 

	13694 --> the
	6531 --> of
	5932 --> and
	4493 --> a
	4459 --> to
	3850 --> in
	2679 --> that
	2428 --> his
	1723 --> I
	1649 --> with

### 9) In a Web browser, load the Spark Web UI from localhost:4040. Re-execute some of the last questions and observe the jobs, tasks tabs.

