import scala.io.Source._
import org.apache.spark.{SparkConf, SparkContext}

object Main extends App {
  //Question 1
  /*val file = fromFile("./resources/mobyDick.txt")
  val lines = file.getLines().toList
  println(lines.size)
  file.close()*/

  val myFile = "/Users/florentsimonnot/Downloads/mobyDick.txt"


  val file = fromFile(myFile)

  val conf = new SparkConf().setAppName("TP1").setMaster("local")
  val sc = new SparkContext(conf)

  val moby = sc.textFile(myFile)
  println(moby)

  val chap = moby.filter(x=> x.contains("CHAPTER"))
  println(chap)
  println(chap.count())

  val emptyLines = moby.filter(l => l.length == 0)
  println(emptyLines.count())

  val wc = moby.filter(l => l.length > 0).flatMap(x => x.split(" ")).map(x => (x,1)).reduceByKey(_+_)
  wc sortByKey () foreach {case (key, value) => println (key + " --> " + value)}

  val cap = wc.filter(x => x._1.contains("captain"))
  cap foreach {case (key, value) => println(key + " --> " + value)}

  val sorted = wc.map(x=>(x._2,x._1)).top(10)
  sorted foreach {case (k, v) => println(k + " --> " + v)}
}
