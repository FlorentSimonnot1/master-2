name := "TP6"

version := "0.1"

scalaVersion := "2.12.12"

lazy val spark = "org.apache.spark"

libraryDependencies += spark %% "spark-core" % "3.0.1"
libraryDependencies += spark %% "spark-sql"  % "3.0.1"
libraryDependencies += spark %% "spark-graphx" % "3.0.1"
