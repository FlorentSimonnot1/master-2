import org.apache.spark.sql.SparkSession
import org.apache.spark.graphx.{Edge, Graph}

object SparkRunner extends App {

  val spark = SparkSession.builder.master("local").getOrCreate
  val sc = spark.sparkContext

  val drugs = sc.textFile("./resources/med.txt").map(x => x.split("\t")).map(x => (x(0).toLong, x(1), x(6)))
  println(drugs.count())
  drugs.collect().foreach(x => println(x))

  val status = drugs.map(x => (x._3, 1)).reduceByKey(_+_)
  status.collect().foreach(x => println(x))

  val comDrugs = drugs.filter(x => !x._3.contains("Non")).map{case(a, b, _) => (a, b)}
  comDrugs.collect().foreach(x => println(x))

  val sub = sc.textFile("./resources/compo.txt").map(x => x.split("\t")).map(x => (x(0).toLong, (x(2).toLong, x(3), x(6))))
  sub.collect().foreach(x => println(x))
  println(sub.count())

  val tmp = sub.map(x => (x._2._3, 1)).reduceByKey(_+_)
  tmp.collect().foreach(x => println(x))

  val saSub = sub.filter{case (id, (a, b, c)) => c == "SA"}.map{case (id, (a, b, c)) => (id, (a, b))}
  saSub.collect().foreach(x => println(x))

  val edges = saSub.map{case (id, (a, _)) => ((id, a), 1)}.reduceByKey(_+_)
  edges.collect().foreach(x => println(x))

  val tmp2 = edges.filter{case(t, u) => u > 1}
  tmp2.collect().foreach(x => println(x))

  println("Size " + tmp2.count())

  val edges2 = saSub.map{case (id, (code, name)) => Edge(id.toLong, code.toLong)}.distinct()
  println(edges2.count())

  val saSubV = saSub.map{case(cis,(code,nom))=> (code,nom)}.distinct
  println(saSubV.count())

  val vertices = comDrugs.union(saSubV)
  println(vertices.count())

  val edges3 = saSub.map{case(cis,(code,nom))=>Edge(cis.toLong,code.toLong,null)}.distinct
  val drugGraph = Graph(vertices, edges3)

  println(drugGraph.vertices.count)

}
