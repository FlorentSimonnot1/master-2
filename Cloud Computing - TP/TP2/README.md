# TP 2

## 1) Settings

Use Intellij !

Create a project from sbt.

In `build.sbt` file, add this dependency : 

    libraryDependencies += "org.apache.spark" %% "spark-core" % "3.0.1"
    

## 2) Triples manipulation

> In this question, we are going to manipulate a set of tuples of size 3. We will call them triples and they enable
us to represent a graph. In fact, the elements of the triple are respectively named a subject, a predicate and
an object. Hence, the triple (1,2,3) states that a node graph identified by id=1 is related to node graph with
id=3 via the predicate identified with the value 2.
Our running example corresponds to the following set of tuples:
((1,0,5),(5,1,8),(8,2,1),(2,0,6),(3,0,6),(6,1,9),(5,1,9),(9,3,11),(9,4,12),(4,0,7),(7,1,9),(7,2,10),(14,1,15),
(15,1,16),(14,1,16),(17,0,18),(18,0,19),(19,1,20),(20,0,17))
Draw this graph in order to easily verify your answers to the following questions.


### Question 1

> Create a new Scala object (in the same project as Question 1) that creates an RDD, named triples, from
  our running example set of triples.

      val triples = sc.parallelize(Array(
        (1,0,5),(5,1,8),(8,2,1),(2,0,6),(3,0,6),(6,1,9),(5,1,9),(9,3,11),
        (9,4,12),(4,0,7),(7,1,9),(7,2,10),(14,1,15),(15,1,16),(14,1,16),
        (17,0,18),(18,0,19),(19,1,20),(20,0,17)
      ))
    
      println(triples.count())
      
Show 19.

### Question 2

> From the RDD in the previous section, create an RDD, named soPairs, that contains only the subject and
  objects of the triples.

      val soPairs = triples.map(t => (t._1, t._3))
      soPairs.foreach(pair => println(pair))
      
Display : 

    (1,5)
    (5,8)
    (8,1)
    (2,6)
    (3,6)
    (6,9)
    (5,9)
    (9,11)
    (9,12)
    (4,7)
    (7,9)
    (7,10)
    (14,15)
    (15,16)
    (14,16)
    (17,18)
    (18,19)
    (19,20)
    (20,17)
    
### Quetion 3

> Using soPairs, find the graph nodes which correspond to roots of the graph (no in-going edges on these graph
  nodes). Name that RDD as roots.

      val subjects = soPairs.map({case(s, o) => (s)}).distinct
      val objects = soPairs.map({case(s, o) => (o)}).distinct
    
      subjects.persist(org.apache.spark.storage.StorageLevel.MEMORY_ONLY)
      subjects.count
      
      var roots = subjects.subtract(objects).map(x => (x,x)).distinct
      println(roots)
      
      
### Question 4

> Create an RDD, denoted leaves, that contains the ids of the graph nodes corresponding to leaves (no outgoing edges).

      var leaves = objects.subtract(subjects)
      println(leaves)
      
### Question 5

> Create a new RDD that contains the set of nodes accessible from each node of the graph. This corresponds
  to the transitive closure of the graph). Example, given a → b → c, we will end up with (a,b),(b,c), which
  we already had, and (a,c). Help: You can use the join RDD operation to perform this task.

      var oldCount = 0L
      var nextCount = soPairs.count()
      val osPairs = soPairs.map(x => (x._2, x._1))
      do {
        oldCount = nextCount
        // Perform the join, obtaining an RDD of (y, (z, x)) pairs,
        // then project the result to obtain the new (x, z) paths.
        soPairs = soPairs.union(soPairs.join(osPairs).map(x => (x._2._2, x._2._1))).distinct().cache()
        nextCount = soPairs.count()
      } while (nextCount != oldCount)
      soPairs.sortByKey().foreach(println(_))
      
### Question 6

> Display, in sorted order on the subject value, only the subject,object pairs that have been added in the
  transitive closure (that is the pairs that were not originally in soPairs).

      val addedSOPairs = soPairs.subtract(osPairs.map(x=>(x._2,x._1)))
      //addedSOPairs.sortByKey().foreach(println(_))

### Question 7 

> Given the RDDs created so far, create an RDD, named rooted, which contains the set of nodes accessible
  from a root. The resulting RDD should look like a tuple with the root in the first position and a sorted list
  of all accessible nodes in the second position.

      val rooted = roots.join(soPairs.groupByKey()).map(x=>(x._2._1,x._2._2.toList.sorted))
      //println("rooted paths")
      //rooted.foreach(println(_))

### Question 8

> Create an RDD, denoted cycles, that contains this graph’s set of cycles (that is its set of graph nodes). 

      val cyc = soPairs.filter(x => x._1 == x._2)
      val cycles = cyc.join(soPairs).map(x=>(x._2._2, x._1)).join(cyc).groupByKey().map(x=> x._2.map(y=>y._1)).distinct
      cycles.foreach(println(_))