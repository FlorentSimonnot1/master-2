import org.apache.spark.{SparkConf, SparkContext}

object SparkRunner extends App {
  val conf = new SparkConf().setAppName("simpleSparkApp").setMaster("local")
  val sc = new SparkContext(conf)

  val triples = sc.parallelize(Array(
    (1,0,5),(5,1,8),(8,2,1),(2,0,6),(3,0,6),(6,1,9),(5,1,9),(9,3,11),
    (9,4,12),(4,0,7),(7,1,9),(7,2,10),(14,1,15),(15,1,16),(14,1,16),
    (17,0,18),(18,0,19),(19,1,20),(20,0,17)
  ))

  println(triples.count())

  var soPairs = triples.map(t => (t._1, t._3))
  soPairs.foreach(pair => println(pair))

  val subjects = soPairs.map({case(s, o) => (s)}).distinct
  val objects = soPairs.map({case(s, o) => (o)}).distinct

  subjects.persist(org.apache.spark.storage.StorageLevel.MEMORY_ONLY)

  var roots = subjects.subtract(objects).map(x => (x,x)).distinct
  println("roots " + roots.count)
  roots.foreach(r => println(r))

  var leaves = objects.subtract(subjects)
  println("leaves " + leaves.count)
  leaves.foreach(l => println(l))

  var oldCount = 0L
  var nextCount = soPairs.count()
  val osPairs = soPairs.map(x => (x._2, x._1))
  do {
    oldCount = nextCount
    // Perform the join, obtaining an RDD of (y, (z, x)) pairs,
    // then project the result to obtain the new (x, z) paths.
    soPairs = soPairs.union(soPairs.join(osPairs).map(x => (x._2._2, x._2._1))).distinct().cache()
    nextCount = soPairs.count()
  } while (nextCount != oldCount)
  soPairs.sortByKey().foreach(println(_))

  val addedSOPairs = soPairs.subtract(osPairs.map(x=>(x._2,x._1)))
  addedSOPairs.sortByKey().foreach(println(_))

  val rooted = roots.join(soPairs.groupByKey()).map(x=>(x._2._1,x._2._2.toList.sorted))
  println("rooted paths")
  rooted.foreach(println(_))

  val cyc = soPairs.filter(x => x._1 == x._2)
  val cycles = cyc.join(soPairs).map(x=>(x._2._2, x._1)).join(cyc).groupByKey().map(x=> x._2.map(y => y._1)).distinct
  cycles.foreach(println(_))
}
