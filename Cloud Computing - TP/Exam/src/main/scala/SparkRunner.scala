import org.apache.spark.sql.SparkSession


object SparkRunner extends App {

  val spark = SparkSession.builder.master("local").getOrCreate
  val sc = spark.sparkContext

  val stations = sc.parallelize(
    ((48.850593, 2.321548), 1, 46, "Paris")::
    ((48.850594, 2.321549), 2, 46, "Lyon")::
    ((48.850595, 2.321550), 3, 52, "Marseille")::
    ((48.850596, 2.321551), 4, 16, "Cherbourg")::
    Nil
  )

  // Create RDD stationsId with signatory : (Id, Coords, Number of stations, City)

  val stationsId = stations.map{case((a, b), c, d, e) => (c, ((a, b), d, e))}
  stationsId.collect().foreach(x => println(x))

  val stationsId2 = stations.map(x => ((x._2), (x._1, x._3, x._4)))
  stationsId2.collect().foreach(x => println(x))

  // Create RDD trajets from file.csv in resources

  val trajets = sc.textFile("./resources/file.csv")
    .map(r => r.split(","))
    .map(r => (r(4).toInt, r(0).toLong, r(1).toInt, r(7).toInt))

  trajets.collect().foreach(x => println(x))

  // Create RDD from trajets with the same signatory but only keeping trajets with a duration > 1200 sec
  // Warning : file keep duration as minutes !!

  val trajets1200 = trajets.filter(x => x._3 * 60 > 1200)

  trajets1200.collect().foreach(x => println(x))

  // From trajets1200, create RDD which associate city and occurrences

  val occVile = trajets1200.map(x => (x._1, 1)).reduceByKey(_+_)
  occVile.collect().foreach(x => println(x))

  val occVille2 = trajets1200.map(x => (x._1, 1)).reduceByKey{case(a, b) => a + b}
  occVille2.collect().foreach(x => println(x))

  // We want to found the city name for each occVille.

  val occVilleJoin = occVile.join(stationsId).map(x => (x._1, (x._2._1, x._2._2._3)))
  occVilleJoin.collect().foreach(x => println(x))

  val occVilleJoin2 = occVile.join(stationsId).map{case(a, (b, c)) => (a, (b, c._3))}
  occVilleJoin2.collect().foreach(x => println(x))

  // Count number of trajets per city (use the city name) from occVille2. Name this RDD occVille3

  val occVille3 = occVilleJoin.map{case(_, (b, c)) => (c, b)}
  occVille3.collect().foreach(x => println(x))

  // By using subtract, check if all occurrences of occVille3 are in stationsId RDD.

  println(stationsId.map(s => s._2._3).subtract(occVille3.map(c => c._1)).count == 0)

}
