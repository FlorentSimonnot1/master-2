import org.apache.spark.sql.{Row, SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

object SparkRunner extends App {

  val spark = SparkSession.builder.master("local").getOrCreate
  val sc = spark.sparkContext

  val schema = StructType(
    StructField("o", StringType, false) ::
    StructField("p", StringType, false) ::
    StructField("q", StringType, false) :: Nil
  )

  val triples = sc.textFile("./resources/drugbankExt.nt")
    .map(x => x.split(" "))
    .map(t => Row(t(0), t(1), t(2)))

  val df = spark.createDataFrame(triples, schema)

  // df.write.csv("./resources/drugsCsv")
  // df.write.json("./resources/drugsJson")

  val df2 = spark.read.schema("s STRING, p STRING, o STRING").csv("./resources/drugsCsv/part-00000-11f91d35-5915-4302-be0a-658ca15bbd28-c000.csv")

  df2.createOrReplaceTempView("trip")

  // Q1 - Retrieve all brandNames for drugs.

  val brRes = spark.sql("SELECT * FROM trip t WHERE t.p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName>'")

  brRes.show()

  // Q2 - Retrieve all brandNames for drugs that are given a possible disease target information.
  val brandNames = df2.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName>'").select("s", "o")
  brandNames.show()

  val disease = df2.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/possibleDiseaseTarget>'").select("s")
  disease.show()

  val disDSL3 = brandNames.join(disease, Seq("s"))
  disDSL3.show()

}
