﻿# MongoDB

## Introduction

### About MongoDB

MongoDB is a document oriented database. The first version was available in 2009. MongoDB is Open source and written in C++.
A document is an associatif array like PHP Array, Python Dict, Ruby Hash, JSON Object...
Data are serialized in BSON. MongoDB supports primary and secondary indexes.

With MongoDB there are no joins, no ACID transactions, no schema, no durability on one computer.

### Modeling

Modeling in a document oriented database takes into account : 
	- read/write ratio
	- modification rate of elements
	- growing a document rate

In according to this rules, we will distribute data in many collections. We'll maybe have to include sub document in document too.

### Querying

We write queries in shell client as Javascript scripts.
We have to choose a database thanks the command : `use db`
We can obtain database list with the command `show dbs`.
To write queries, we use JS syntax. `db.myCollection.myOperation(...)`

## Retrieve Data

### Basic command

    db.myCollection.find(...)
    db.myCollection.findOne(...)

> Tips : Use pretty after a request to stylize display.

### Use filters

If we want to retrieve data in according to certain conditions, we have to precise in `find` method which attributs and which values we want to search.

	db.myCollection.find({attribut: valueToLook})


### Light queries result

By default, query return all of attributs of a collection. We can precise in our query if we want only some attributs.

	db.myCollection.find({attribut1: 1 (true)}, {attribut2: 0 (false)})


We have to set 0 (false) if we dont want to get this attribut. Set 1 or true to retrieve this attribut.

### Selectors

**Condition selectors**

When we search data, we often use comparison on certain attribut. For example, we can compare an equality between to value, look if a value is greater than another value etc...

In MongoDB there are some **selectors** in order to add *'condition'* in our request.

| Selector | Condition equivalence |
|--|--|
| `$eq` | Matches values that are equal to a specified value. |
| `$gt` | Matches values that are greater than a specified value. |
| `$gte` | Matches values that are greater or equal than a specified value. |
| `$lt` | Matches values that are less than a specified value. |
| `$lte` | Matches values that are less or equal than a specified value. |
| `$ne` | Matches all values that are not equal to a specified value. |
| `$in` | Matches any of the values specified in an array. |
| `$nin` | Matches none of the values specified in an array. |

Example : 

    db.myCollection.find({year: { $eq: "2017" }})
    db.myCollection.find({year: { $in: ["2017", "2020"] }})

**Logical selectors**

In MongoDB there are too some selectors which are logical selectors (and, or, not, nor).

| Selector | Logical equivalence |
|--|--|
| `$or` | Joins query clauses with a logical OR returns all documents that match the conditions of either clause. |
| `$and` | Joins query clauses with a logical AND returns all documents that match the conditions of both clauses. |
| `$not` | Inverts the effect of a query expression and returns documents that do not match the query expression. |
| `$nor` | Joins query clauses with a logical NOR returns all documents that fail to match both clauses. |

Example : 

	db.myCollection.find({$or : [{gender: "Male"}, {age: "20"}]})
	db.myCollection.find({year: {$not: {$lte: "2016"}}})

 
**Element selectors**

We can use some selectors to test if an attribut exists or check the type of an attribut.

| Selector | Element equivalence |
|--|--|
| `$exists` | Matches documents that have the specified field. |
| `$type` | Selects documents if a field is of the specified type. |

Example : 

    db.myCollection.find({annee : {$exists : 1}) 
    db.myCollection.find({annee : {$type : 2}) // 2 = string type

You can found all type here : https://docs.mongodb.com/manual/reference/operator/query/type/#op._S_type

**Evaluation selectors**

| Selector | Evaluation equivalence |
|--|--|
| `$mod` | Performs a modulo operation on the value of a field and selects documents with a specified result. |
| `$regex` | Selects documents where values match a specified regular expression. |
| `$text` | Performs text search. (must have an index) |
| `$where` | Matches documents that satisfy a JavaScript expression. |

Example :

    db.maCollection.find({annee : {$mod : [divisor, remainder]}}) 
    { <field>: { $regex: /pattern/, $options: '<options>' } } 
    { <field>: { $regex: 'pattern', $options: '<options>' } } 
    { <field>: { $regex: /pattern/<options> } } 
    db.movies.find({title : {$regex: ‘iolence’}},{title:1, year:1})

**About text selector**

We have to create an index : 

	db.myCollection.createIndex({ subject: "text" })
	db.myCollection.find( { $text: { $search: "coffee" } } )

**Arrays selector**

| Selector | Array operation  |
|--|--|
| `$all` | Matches arrays that contain all elements specified in the query |
| `$elemMatch`| Selects documents if element in the array field matches all the specified $elemMatch conditions |
| `$size` | Selects documents if the array field is a specified size |

Example :

	db.myCollection.find({drinks: {$all: ["Coffe", "Tea"]}})
	db.myCollection.({drinks: {$size: 2}})
	db.myCollection.find({personnes: {$elementMatch: {firstName: "James"}}})

### Projections

| Projections | Operation |
|--|--|
| `$` | Projects the first element in an array that matches the query condition |
| `$elemMatch` | Projects the first element in an array that matches the specified $elemMatch condition |
| `$meta` | Projects the document's score assigned during $text operation |
| `$slice` | Limits the number of elements projected from an array. Support skip and limit slices. |

Example : 

	db.myCollection.find({year: {$eq: 2005}}, {"personnes.$": 1})
	db.myCollection.find({year: {$eq: 2005}}, {personnes: {$slice: 2}})


## Insertion

Insert one document in a collection : 

	db.myCollection.insertOne({...})

Insert many documents in a collection :

	db.myCollection.insertMany([d1,d2])

Insert one or many documents :

	db.myCollection.insert({...})

We can use Javascript variable to insert document:

	d1 = {name:"a",tags:["a","b","c"]}
	d2 = {name:"b", tags:["b","c","d"]}
	db.test.insertMany([d1,d2])
	db.test.find()
	db.test.count() // Gets 2


## Aggregations

**Remember** : In Database management, an aggregate function is a function where the values of multiple rows are grouped together to form a single summary form. There are many aggregate function such as average, count, maximum, minium...

### Count

We can count elements in a collection : `db.myCollection.count()`

### Distinct

We can obtain distinct elements in a collection : `db.myCollection.distinct(field, query)`

### Aggregation pipeline

MongoDB contains a framework named aggregation pipeline. This framework allow to transform documents to produce the result we want.

We call this a pipeline because the process contains steps. Each step is a transformation.

To call an aggregation we use aggregate() method on our collection.

	db.myCollection.aggregate(...)

In this method we have to define a list of stage to execute. There are some stage with MongoDB.

`$match, $sort, $group` ...

Generally, stages can be appear multiple times in a pipeline except `$out`, `$merge` and `$geoNear`.

Example of aggregation pipeline :

![Aggregation](./img/aggregation.png)

### Map Reduce

Map-reduce is a data processing paradigm for condensing large volumes of data into useful _aggregated_ results. To perform map-reduce operations, MongoDB provides the `mapReduce` database command.

All map-reduce functions in MongoDB are Javascript.

![MapReduce](./img/map_reduce.png)

## Indexation

### Primary key

In MongoDB `_id` is the primary key. This is an automatic indexation. If we don't give the `_id` when we insert a document, this index is automatically created.
Mongo use an ObjectId to create an index. This object has 12 bytes (4 bytes for timestamp, 5 for value and 3 for incrementing counter). It guarantees uniqueness.

### Secondary indexation

With MongoDB we can create secondary index on JSON elements in document.

We have to use the function `createIndex()`.

Example : 

	db.myCollection.createIndex({attribut: 1}) // 1 for ASC sort/ -1 for DESC sort

We can create an index on an attribut in a sub document.

	db.myCollection.createIndex({sub.attribut: 1})

Or create an index on all attributs of a sub document.

	db.myCollection.createIndex({sub: 1})

We can create indexes with many keys :

	db.myCollection.createIndex({sub.att1: 1, sub.att2: -2})

This works with array too !

	db.myCollection.createIndex({array.attribut: 1})

___
**NOTE**: The index creation block all other operations (read/write) on the db. So we can create an index by using the `background` operator.

    db.myCollection.createIndex({attribut: 1}, {background: 1})
___

### Hint

We can force usage of an index by the method `hint()`.

Example : 

	db.users.find().hint( { age: 1 } )

Returns all documents in the collections users using the index on the age field.

Note : The index have to be exist.

### Explain

The explain operation allow to see the query execution plan.

	db.collection.find({..}).explain()

### Manage index

We can obtain all collection indexes by the method `getIndexes()`.

	db.myCollection.getIndexes()

To obtain all indexes of a db, use the following method : `listIndexes()`.

## Administration

### Save a Database

We can save database as dump thanks to the command `mongodump`.
The server have to be running and we have to create a folder to save the dump. Then we juste have to go at the folder and run the `mongodump` executable which is in `/bin` folder of mongo.

We can use some options like choose a database or a collection to save. Add authentication.

The folder where we have dump the database contains a folder named dump. It contains a **BSON** file for each collection and a **JSON** file for each collection.


### Restore a Database

We can restore a database thanks to a dump file. For this we have to use the command `mongorestore`. We have to run this command since the folder `dump`. 
As a dump, we can use some options (restore a Database, a collection, add authentication...).

___
**NOTE**: Option `--drop` to clean database before we restore a dump.
___

### Import file (CSV, JSON...)

We can import a file into our Database. The file type can be a CSV, a TSV or a JSON.
To do this action we have to use the command `mongoimport`.

## Manage users

### Create a user

We can add users and define their rights with mongo.
First we have to switch to admin database.

    use admin

Then we can create a user thanks to the command `createUser`. This function take a json document as parameter.
We have to set the user, his password and we can define it roles as an array.

    var user = {
	    user: 'MyUser',
	    pwd: 'MyPassword',
	    roles: [{role: 'value1', db: 'theDb1'}, {role: 'value2', db: 'theDb2'}]
    }
    db.createUser(user)

### Remove a user

We can use the function `dropUser('user')`. 

*Note we have to be authenticated before remove a user and selected the good database.*

### Update user password 

We can use the function `changeUserPassword('user', 'newPassword')`. 

*Note we have to be authenticated before remove a user and selected the good database.*

### Update user roles

We can use the function `updateUser('user', 'roles')`.

*Note we have to be authenticated before remove a user and selected the good database.*

### The different roles

There are many roles possible with MongoDB.

|Role| Description |
|--|--|
| Read | Obvious |
| readWrite | Already obvious |
| dbAdmin | Run administration operations (create index, see stats) |
| userAdmin | Create, remove, admin users for a db |
| dbOwner | ReadWrite + dbAdmin + userAdmin |
| clusterManager | Only admin database |
| clusterMonitor | Only admin database. Access to stats |
| readAnyDatabase | Authorize read on all db|
| readWriteAnyDatabase| Authorize read on all db |
| userAdminAnyDatabase| Create, remove, admin users for all db |
| dbAdminAnyDatabase| Run administration operations (create index, see stats) on all db |

### Authentication

When we run the server, we have to add option `--auth` if we want to active authentication. Then we choose the admin database and run this command : 

    db.auth(user, password)


## Manage logs

By default, MongoDB logs are display in the console. We can redirect the logs in a file thanks to the option `--logpath myPath` when we run the server.

	mongod --logpath myLogFile

## Replication 

### Why ?

- Improve scalability.
		- Less redundancy -> many data centers -> locality search
		- Improve performance -> parallel queries

- Improve reliability and durability.
		- Recovery on failure

### Replica set

A replica set is a cluster master/slave with automatic recovery on failure. There are one master (primary) and several slave (secondary).
A replica set has to contain at least 3 members. Max 50 members. Preferences for an odd (2k + 1) number.

### Replica set - Primary

The primary is the referent for the replica set state. This is the only node which can receive write for an application. Secondary are synchronized on this primary node. Secondary vote to choose the primary, an absolute vote ! Be primary is ephemeral. 😢

### Replica set - Secondary

The secondary stockes a copy of data. He can be read. Warning : The replication is asynchronous. Slaves can be not updated !
He can become the primary is secondary vote for him.

### Oplog

Master stockes his operations in an operation log (oplog).
Slaves synchronize themselves with the master thanks to this file.
All members stocke an oplog.

## Sharding 

### Scalability

The scalability is the availability for a system/network/processus to deal with a growing volume of tasks with efficiently or it availability to adapt itself to this increase.

There are 2 approaches : 

- Vertical scaling -> Add memory, Add disks, Add CPU
- Horizontal scaling -> Uses Functional scaling or do Sharding

### Functional scaling VS sharding

The functional scaling is the process to regroup data in functions. There are share in many computers.

The sharding is the process to divide data on a separate database server instance. Some data can be on several shard but some appears only in a single shard.

![Partitions method](./img/partitions_type.png)

We can see in this previous image, the difference between this 2 partitioning methods.
The vertical partitioning can be cut some table columns and move this data in another server. The horizontal partitioning treat rows and can move some rows in another server.

### Sharding in MongoDB

MongoDB divide the collections and deals these into many shards.
We can run the sharding by running command : `mongos`.
This command treat the query distribution.
This users don't know how this executable works. Everything is transparent.

The user have to define a key for the sharding. The shard keys allow to define ranges. Every range is save into a server.

For example : 

We have a collection with users. We have 2 servers and we decide to set the user name as the sharding key. The users which have a name beginning with A to M will be save in the first server. The rest of users will be save into the second server.

MongoDB manage the shards balance.

### Sharding benefits and drawbacks

Thanks to the sharding, we low the chance of unavailability data because they are deal into several servers.

We increase the chance to have failure because there are more servers.

Sharding is often use with replication to construct a powerful system.

### Config server

MongoDB use a config server when sharding is use. 
The config server stocks metadata on the cluster, for example he stocks where found a set of documents identified by a sharding key range.

We can shard data with only one machine. We can host many servers in one computer.

### Shard key

Best practice is to use key using in query. It size is limited. We have to create an index on the key. The key is immutable.

### When use sharding ?

We can use sharding when we have **problem with driver space** or when we want to **write faster** or use **more data in Ram**.

## GridFS

By default, size of documents is 16Mo in order to assure performance. We can overcome this restriction thanks to GridFS. GridFS is a new method to stocke files in MongoDB.

We can use GridFS when a document is too heavy or if we don't want to read all document. (Example : We want read only n pages of document or switch to the k page of book).
Don't use GridFS if you want do atomic modifications on file !

GridFS has 2 collections: 

- The first collection (*files*) stockes the file name and metadata like size.
- The second collection (*chunks*) contains data cut in chunk which have limited size, 255Ko (configurable but max size is 16Mo).

These collections are stocke in `fs` data space. We can configure this place.
