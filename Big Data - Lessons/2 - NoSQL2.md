﻿# Big Data - NoSQL Advanced

In this lesson, we will study in more detail the families of NoSQL.

Remember that there are 4 families in NoSQL which are characterized by a different structure : 

- Key/Value
- Document
- Column
- Graph

## Key/Value family

### Characteristics

- Data in key-value store is organized around the associative array (Dictionary, Map, Hash..). 
- Key-values stores contain collections of key-value pair with each key is unique in the collection. 
- They operate as caches or data structures. They don't use SQL and have a flexible schema. 
- They offer a scale out architecture. 
- Consistency only for operations on a single key (operations get, put or delete on a single key). 
- Optimistic writes can be performed but are very expensive to implement. Eventually consistency (for example Riak update conflicts).
- There are different strategies for KV stores but in general, there are no guarantees on writes.

### Use cases

- Storing session information
- User profiles and preferences
- Shopping cart data
- Don't use for joins between keys, multioperation transactions, operations by sets

### Memcached

- Stands for Memory Cache Daemon. 
- First implementation started in 2003
- Implemented in C
- Used by Youtube, Facebook, Twitter...
- Distributed memory, caching system.
- It's a server that caches **Name Value Paris** in memory.
- Value can be anything : rows of data, HTML, binary objects.
- When some data are needed, the system checks if it is available in Memcached and if it is not the case, retrieves it from disk and stores it in Memcached for future accesses.

There are 4  main commands which can work with TCP or UDP connection.

- SET : add a new item or replace an existing one with new data
- ADD : only store the data if the key does not exist.
- REPLACE : only store the data if the key already exists.
- GET : return the data

A Memory Cache Daemon is not a persistent database. We can't dumped it in disk. There is no native security mechanism. Least Recently Used (LRU) algorithm to remove data when space is needed. Expiration time can be associated to NVP (TTL).

In Memory Cache Daemon system there are 2 components : 

- A server : A NVP server. Basically, stores and retrieves data stored with a key (Key length < 250 & size value < 1MB). Server is atomic.
- A client : knows which server contains an NVP

### Memcached with Redis

- Redis started in 2009. 
- High performance key-value store. 
- A data structure server: support for lists, sets, sorted sets, hashes, hyperloglogs, etc. 
- Flexible schema.
- Partial ACID compliance and full compliance if configured with appenfsync always (durability)
- Single threaded so guarantees consistency and isolation
- Supports publish-subscribe messaging
- Redis is not for large dataset with only a small part of “hot” data
- Redis is not for dataset that do not fit in main memory
- Value limit size is 512MB

<b>Hashes</b>

- collections of key-value pairs
- map between string keys and string values
- efficient for representing objects and tabular data
- operations : HLEN, HKEYS, HVALS, HDEL, HEXISTS, HINCRBY, HINCRBYFLOAT

<b>Sets</b>

- unordered collections of strings
- similar to lists but all member are unique
- possible to add the same element multiple times without needing to check its existence in the set
- add, remove and test existence of members in <u>constant time</u>
- set-based operations (union, intersection, difference)
- operations : SADD, SREM, SISMEMBER, SMEMBERS, SDIFF, SDIFFSTORE, SINTER, SINTERSTORE, SPOP...

<b>Sorted sets</b>

- ordered collections of strings
- a rank of field to determinate the order
- members are automatically sorted by rank
- members must be unique
- can retrieve elements by position or rank
- set-based operations (union, intersection, difference)
- add, remove and updating of members is in <u>O(log(n))</u>
- operations : ZADD, ZREM, ZSCORE, ZRANGE

Usefull to implement the LRU algorithm. 

<b>Bitmaps</b>

- bit operations
- can count the number of bits set
- perform AND, OR, XOR, NOT operations
- operations  : SETBIT, GETBIT, BITOP AND, BITOP OR, BITOP NOT

<b>Hyperloglogs</b>

- a probabilistic data structure to count unique elements -> gives an estimation
- do not need to keep a copy of all members

Redis has support for persistence.

Persistence solution : 

- snapshotting where db is saved on disk in RDB format (compressed db dump)
- append only file (AOF)

- Redis support master-slave replication
- Redis cluster : data is automatically sharded across multiple redis nodes. It provides some degree of availability during partitions.

### Some KV Databases 

<b>Oracle NoSQL</b>

- uses the BerkeleyDB storage engine
- provides transactional semantics with fined-grained concurrency
- primary and secondary index
- high availability

<b>Amazon DynamoDB</b>

- automatic sharding
- client : adRoll serves 100 billion ad impressions per day

<b>Aerospike</b>

- hybrid in-memory : dynamic random access memory (DRAM)
- self-healing architecture

<b>Riak</b>

- Riak secondary indexes, full-text search, MapReduce
- Couchbase: membase

## Graph family

### Characteristics

- stores entities (= nodes) and relationships (= edges) between these entities
- nodes and edges can have properties (= attributes) in the property graph model
- edges may be directed or not
- a query on a graph is known as **traversing** the graph
- traversing is fast because joins are not calculated but persisted
- some solutions do not supporting distributing the nodes so there no consistency problems -> ACID
- system adopt a master-slave approach. Some where slaves accept writes and synchronize with the master

### Scalling - Use cases

- partition the graph using round-robin, range, hash, graph partitioning, cluster ML approaches (K-means)
- use cases : connected data (social networks), routing and location-based services, recommendations engines
- no adapted to use cases where many nodes need to be modified at once

### Neo4J

- started in 2009 and implemented in Java
- characteristics : represent everything with nodes and relationships, persisted, fully transactional
- Relationships between nodes can be both directed and bidirectional
- Data on nodes and relationships is an arbitrary number of key/value pairs
- navigate from a starting node via relationships to the node matching a criteria
- usually takes form of a JAVA API
- can be queried with Cypher (declarative) or Gremlin (procedural)
- Every operation must happen inside a transaction
- Persisted on disk with a custom binary disk format
- sharding is hard and needs interactions of end-user
- architecture based on master-slave
- concurrency control : locks
- no supporting MapReduce

<b>Cypher language</b>

- Query language specialized for graph
- Declarative language inspired by SQL
- Uses patterns to describe graph data which are matched to a data graph
- Create a graph node with `()` and create data thanks to clause `CREATE`
- Exemple : `CREATE (p:Person fname:”Joe”, lname:”Foo”, position:”Professor” )`
- `MATCH` clause to describe a pattern of nodes and relationships
- `WHERE` clause to constrain the query results, v.name=”Joe”
- `RETURN` clause to retrieve particular results
- Exemple :  `MATCH (v:Person) WHERE v.name = ”Joe” RETURN v;`
- 2 clauses to delete data : 
		- `REMOVE` -> remove label properties of nodes and relationships 
		- `DELETE` -> delete node and relationships 

## Document family

### Characteristics

- Documents are the main concept in document databases
- Stores and retrieves XML, JSON, BSON etc documents
- The documents are stored in the value of a key-value store
- The documents are self-describing, hierarchical tree data structures consisting of maps, collections ad scalar values
- The “schema” of documents can be different but they can still belong to the same collection (like a table in RDBMS).
- no empty or null-values attributes in documents
- if an attribut is not found, we assume that is not set
- Documents allow for new attributes to be created without the need to define them or to change the existing documents
- Popular document stores are: MongoDB, CouchDB, Terrastore, OrientDB, RavenDB, ArrangoDB

### Transactions

- As aggregate-oriented stores, document database systems support transactions at a single document level (→ atomic transactions).
- RavenDB supports transactions across multiple operations

### Querying

- Compared to KV stores, it is frequently possible to the data inside the document without having to retrieve the whole document by its key
- CouchDB: query via views which can be materialized (automatically updated when queried if any data has changed since the last update) or dynamic

### Use cases

- Event logging, especially when events keep changing
- Content Management systems
- Blogging platforms
- Web and real-time analytics
- E-commerce applications: due to rich schema flexibility for products and orders.
- Not to use for complex transactions spanning different operations, queries against varying aggregate structure

### MongoDB

See course on <a href="#">MongoDB</a>

## Column family

### Characteristics 

- Also know as Wide row stores and wide column stores
- Aggregate-oriented stores where the value can have multiple columns.
- A column is a set of data values of a particular type
- Column-family dbs store and process data by column instead of row.
- Popular systems are Cassandra, HBase and Google’s BigTable.

### Use cases

- Event logging
- CMS
- Blogging platforms
- Counters: count and categorize visitors of a page to calculate analytics
- Expiring usage: you can define expiring columns (remove ad banners on a website for a specific time). Columns are removed automatically after a given time. (Time To Live = TTL).
- Not adapted to: for early prototypes due to schema change (Cassandra), ACID transactions.

### Cassandra

- Column family database
- Started in 2009 and implemented in JAVA
- Used at Digg, Twitter, Reddit, Rackspace, Netflix, etc.
- Characteristics: Fault tolerant, Decentralized (P2P), Shared nothing architecture, Tuneable consistency, Elastic
- Fully distributed (no Single Point of Failure)
- Fast reads and write(’optimize for reads, writes are cheap’)
- Eventually consistent
- Column is a triple with: a key, a value and a timestamp
- Columns and super columns are sorted (customizable and defined by column family)
- Replication set per keyspace, specified in servers config file, tells how duplicate one wants
- Architecture based on ring (consistent hashing)
- Concurrency control: OCC (no locking)
- Support for MapReduce: Yes

### CQL - Cassandra Query language

- SQL like language with DDL (CREATE, ALTER, DROP) and DML (INSERT, UPDATE, DELETE, SELECT) query operations.
- Supported datatypes: numerical, character, date, unstructured and specialized datatypes (JSON).
- Transactions are more AID than ACID : tunable data consistency across a database (from strong to eventual).


### HBase

// TODO

## Summary

- KV Stores

| Benefits | Drawbacks  |
|--|--|
| Session Data  | Joins on keys |
|User preferences and profiles| multioperations transaction|
| Shopping cart| Secondary indexes |
| <center>-</center>  | Operations on sets|

- Graph Stores

| Benefits | Drawbacks  |
|--|--|
| Connected data | Many updates at once |
| Routing and location-based data | <center>-</center> |
| Recommandation | <center>-</center> |

- Document stores

| Benefits | Drawbacks  |
|--|--|
| Event logging | Joins on keys |
| CMS | multioperations transaction |
| Blogging | <center>-</center> |
| Web, real-time analytics | <center>-</center> |
| e-commerce | <center>-</center>|

- Column-family Stores

| Benefits | Drawbacks  |
|--|--|
| Event logging | Early prototypes |
| CMS | Full transaction machinery |
| Blogging | <center>-</center> |
| Counting | <center>-</center> |
| Expiring usage | <center>-</center> |

## Exercise

> We consider a simple blog application containing the following
> information. Blog entries are being written by users characterized by
> an id a name and a email. For each blog, we store the content and a
> date (creation date), the user who has written the post and a category
> which can be sport, music, computer, science... Finally, user can
> subscribe to the blogs of other users.
> 
> The application needs to answer questions such as ’what users
> subscribe to ones blog’ and ’show the most recent entries for the
> blogs one has subscribed to’.

***Model for the relational, document, column family and graph models.***

### Relational

    user(id, name, email)
    category(id, name)
    blog(id, content, date, #user, #category) // #user and #category are foreign keys
    follow(#follower, #followed, date) // #follower and #followed are foreign keys for user

### Column family

![Column](./img/column-exercise.png)

### Document

    users {
	    uuidEntity: string;
	    name: string;
	    email: string;
	    following: string[];
	    followers: string[];
	    blogs: {
		    uuidEntity: string;
		    content: string;
		    date: date;
		    category: {
			    uuidEntity: string;
			    name: string;
		    }
	    }
    }

### Graph

![Graph](./img/graph-exercise.png)

## Polyglot persistence

### Introduction

- Polyglot persistence aims to use different data stores in your applications.
- Imagine an e-commerce application. What would you use for the shopping cart, the completed orders and session data ?

The shopping cart and the session data can be efficiently stored in a Key-Value store. Respectively, their keys are userID and sessionID. Once an order is completed, that data can be stored in an RDBMS or a Document store.

What if we want to add a product recommendation service ? Thing Collaborative Filtering, those who bought that product also like that product or your friends bought. What about inventory and item prices ?

![Polyglot persistence](./img/polyglot-persistence.png)

- A graph database corresponds to storing recommendation data.
- Inventory and item prices fit nicely in an RDBMS
- With Polyglot Persistence, one has to be careful with deployment complexity: all databases are needed in production at the same time.
- It may be a got solution to design services on these databases. It reduces the impact of data storage choices

### Guideline on the database type to use based on the functionality of the data

| Functionality  | Considerations  | DB Type |
|--|--|--|
| User sessions |Rapid Access for reads and writes. No need to be durable. | Key-Value |
| Financial data|Needs transactional updates.Tabular structure fits data. | RDBMS |
| POS Data | Depending on size and rate of ingest. Lots of writes, infrequent reads mostly for analytics. | RDBMS (if modest),  Key Value or Document (if ingest very high) or Column if analytics is key. |
| Shopping Cart | High availability across multiple locations.  Can merge inconsistent writes. | Document,  (Key Value maybe) |
| Recommandations | Rapidly traverse links between friends, product purchases, and ratings. | Graph (column if simple) |
| Product catalog | Lots of reads, infrequent writes.  Products make natural aggregates. | Document |
| Reporting | SQL interfaces well with reporting tools |RDBMS / Column |
| Analytics | Large scale analytics on large cluster | Column |
| User activity logs,  CSR logs,  Social Media analysis | High volume of writes on multiple nodes | Key value / Document |

### Twitter example

> When a user write a tweet : Fanout module is called by WriteAPI to
> send it to all followers. It is stored in a user array of tweets in
> **Redis**. In the Redis cluster, all user's timelines are stored. It keep the last 800 tweets for each user in RAM. Fanout asks the
> **Social Graph** service to know who is following who. In redis, data model is tweetId (8bytes), UserID (8bytes), bits (4bytes) plus retweet (tweetID) Timeline service, provides the Redis server where your home timeline is stored. The WriteAPI also sends tweets to the ’Search Ingester’ then it stores it in a modified **Lucene** index (named Earlybird). Index is in-memory. Blender is the service that enables to access Earlybird. Twitter also have a pull solution (pulls tweets to users). WriteAPI sends tweets to HTTP Push which contains ’Hosebird’ which searches to how to sends that tweet. A similar service exists for mobile devices, named Mobile Push.

