﻿# Big Data - Introduction

## What is Big Data?

> Big data usually includes data sets with sizes beyond the ability of commonly-used software tools to capture, curate, manage, and process the data within a tolerable elapsed time.
Definition from *Wikipédia.*

It refers to technologies and initiatives involving data that is too diverse, fast-changing and massive for conventional technologies, skills and infrastructure to address efficiently.

So the **Volume**, **Velocity** and **Variety** (3V) of data is too great.

2 forms of Big Volumes : 

- Small analytics : SQL on very large data sets. Using aggregate ops of SQL
- Big analytics : Data clustering, regressions, machine learning. Using statistical tools: R, SPSS (Statistical Product and Service Solutions - IBM), SAS.

## Making sense at scale

- Machines: cloud computing
- Algorithms: machine learning and analytics
- People: crowdsourcing and human computation

### Crowdsourcing && Human computation

### Machines

## DBMS History

___
Next course : <a href="NoSQL.md">NoSQL</a>
