﻿# Big Data - NoSQL

<a href="BigData.md">Introduction about big data : </a>

## Motivations

### From relational Databases to NoSQL Databases

Relational databases have dominated the software industry for a long time (twenty years). Store data persistently, concurrency control, transactions are property appears thanks to relational databases. However, the dominance of theses databases is cracking.

### NoSQL

NoSQL means Not Only SQL.

Application developers have been frustrated with the impedance mismatch between the relational data structures and the in-memory data structures of the application. Using NoSQL databases allows developers to develop without having to convert in-memory structures to relational structures.

The rise of the web as a platform also created a vital factor change in data storage as the need to support large volumes of data by running on clusters. Relational databases were not designed to run efficiently on clusters.

No really definition for NoSQL databases but some properties : 

-   Not using the relational model
-   Running well on clusters
-   Mostly open-source
-   Built for the 21st century web estates
-   Schema-less

NoSQL is more a movement than a technology.

Disk read speeds did not accelerate as quickly as memory and
CPU speed. So RDBMS products were not aligned with hardware
capabilities that they had been. 

## NoSQL families
4 categories : 
- Key/Value
- Column family
- Document
- Graph DB

### Key/Value

**Origin** : DynamoDB from Amazon
<br>
**Data model** : Key + value which can be a photo, a number, a date, a text an object etc..
<br>
**Systems** : Voldemort (LinkedIn), Tokyo (Cabinet, Tyrant), Riak (Basho), Oracle NoSQL, Redis

Benefit : easily modular + fast read and write
<br>
Drawback : difficult update + basic query

![Key/Value DB](./img/Key-ValueDB.png)

### Column Family

**Origin** : Bigtable @ Google
<br>
**Data model** : a big table with column families. Close to relational databases but the number of columns may vary
<br>
**Systems** : HBase (Apache), Cassandra (Apache), HyperTable

Benefit : increased storage capacity + quick data access
<br>
Drawback : limited query

![Column DB](./img/ColumnDB.png)

### Document

**Origin** : Lotus notes 
<br>
**Data model** : Collections of documents where a document is a key-value collection 
<br>
**Systems** : CouchDB (Apache), MongoDB (10gen), Terrastore

Benefit : more complete query + flexibility + scalable
<br>
Drawback : data duplication + consistency not necessarily assured

![Column DB](./img/DocumentDB.png)

### GraphDB

**Origin** : Graph theory 
<br>
**Data model (property graph)** : Nodes with properties. Typed relationships with properties 
<br>
**Systems** : Neo4J, InfiniteGraph, Sones GraphDB, Trinity (Microsoft), FlockDB (Apache)

Benefit : suitable for relational data management
<br>
Drawback : architecture sometimes limited

![Graph DB](./img/GraphDB.png)

## Aggregate data model

An aggregate is a collection of data that we interact with as a unit.
Aggregates make it easier for the database to manage data storage over clusters, since the unit of data now could reside on any machine and when retrieved from the database gets all the related data along with it. Aggregate-oriented databases work best when most data interaction is done with the same aggregate, for example when there is need to get an order and all its details, it better to store order as an aggregate object but dealing with these aggregates to get item details on all the orders is not elegant.
<br>
Key-Value, Document and Column family stores can be considered as Aggregate oriented.

## CAP Theorem

### What this theorem says

In a distributed system, managing consistency (C), availability (A) and partition toleration (P) is important, Eric Brewer put forth the CAP theorem which states that in any distributed system we can choose only two of consistency, availability or partition tolerance.
<br>
**Consistency (linearizability)** : any read that follows a write must return the value of the write, or a newer value.
<br>
**Availability** : available system nodes must respond to any request.
<br>
**Network partition tolerance** : the system must tolerate network partitions (when two sub - parts of the system can no longer coordinate).

![CAP Theorem](./img/CAP_Theorem.jpeg)

### Proof in picture

![CAP 1](./img/cap1.png)

The diagram above shows two nodes in a network, N1 and N2. They both share a piece of data V, which has a value V0. Running on N1 is an algorithm called A which we can consider to be safe, bug free, predictable and reliable. Running on N2 is a similar algorithm called B. In this experiment, A writes new values of V and B reads values of V.

![CAP 2](./img/cap2.png)

In a sunny-day scenario this is what happens: (1) First A writes a new value of V, which we’ll call V1. (2) Then a message (M) is passed from N1 to N2 which updates the copy of V there. (3) Now any read by B of V will return V1.

![CAP 3](./img/cap3.png)

If the network partitions (that is messages from N1 to N2 are not delivered) then N2 contains an inconsistent value of V when step (3) occurs.

Scale this is up to even a few hundred transactions and it becomes a major issue. If M is an asynchronous message then N1 has no way of knowing whether N2 gets the message. Even with guaranteed delivery of M, N1 has no way of knowing if a message is delayed by a partition event or something failing in N2. Making M synchronous doesn’t help because that treats the write by A on N1 and the update event from N1 to N2 as an atomic operation, which gives us the same latency issues we have already talked about (or worse). Gilbert and Lynch also prove, using a slight variation on this, that even in a partially-synchronous model (with ordered clocks on each node) atomicity cannot be guaranteed.
So what CAP tells us is that if we want A and B to be highly available and we want our nodes N1 to Nn (where n could be hundreds or even thousands) to remain tolerant of network partitions (lost messages, undeliverable messages, hardware outages, process failures) then sometimes we are going to get cases where some nodes think that V is V0 and other nodes will think that V is V1.

### Deal with CAP

- Drop partition tolerance : 

If you want to run without partitions you have to stop them happening. One way to do this is to put everything (related to that transaction) on one machine, or in one atomically-failing unit like a rack. It’s not 100% guaranteed because you can still have partial failures, but you’re less likely to get partition-like side-effects. There are, of course, significant scaling limits to this.

- Drop availability :

This is the flip side of the drop-partition-tolerance coin. On encountering a partition event, affected services simply wait until data is consistent and therefore remain unavailable during that time. Controlling this could get fairly complex over many nodes, with re-available nodes needing logic to handle coming back online gracefully.

- Drop consistency : 

Or, as Werner Vogels puts it, accept that things will become “Eventually Consistent”. Lots of inconsistencies don’t actually require as much work as you’d think (meaning continuous consistency is probably not something we need anyway).

## Consistency VS Availability 

In larger distributed scale systems, Consistency and Availability cannot be achieved at the same time because each partition is a data.

Database systems designed with traditional ACID guarantees in mind such as RDBMS choose consistency over availability, whereas systems designed around the BASE philosophy, common in the NoSQL movement for example, choose availability over consistency.

___
**REMINDER**

> - Database transaction : A sequence of database operations.
> - ACID is a set of properties of database transactions to guarantee data validity.
> 
> 	- Atomicity : All or none updates are executed 
> 	- Consistency: DB instance must go from one consistent state to another 
> 	- Isolation: Results of a transaction are visible to other users after a commit 
> 	- Durability: Commited transactions are persisted
> - ACID in RDBMS contains two phases commit (2PC) : 
>
> 	- Phase 1 :  transaction coordinator asks each involved DB to
precommit the operation and tell if commit is possible
> 	- Phase 2 : Phase 2: transaction coordinator asks each involved DB to commit the data.	 
___

So we have to choose between 2 options. 
- Relax consistency to ensure high data availability. 
<center>Or</center>

- Strengthen consistency knowing that data availability will not always be possible.

Different solutions to relax consistency are presented by Werner Vogels. <br>
First, Werner introduce 3 types of consistency levels :

- **_Strong consistency_**  : After the update completes, any subsequent access will return the updated value.
-   **_Weak consistency_**  : The system does not guarantee that subsequent accesses will return the updated value. A number of conditions need to be met before the value will be returned.
-   **_Eventual consistency_**  : This is a specific form of weak consistency; the storage system guarantees that if no new updates are made to the object, eventually all accesses will return the last updated value.

The third level is the most interesting. There are several variants of it :

- **_Causal consistency_** : If process A has communicated to process B that it has updated a data item, a subsequent access by process B will return the updated value, and a write is guaranteed to supersede the earlier write. Access by process C that has no causal relationship to process A is subject to the normal eventual consistency rules.
- **_Read-your-writes consistency_** : This is an important model where process A, after having updated a data item, always accesses the updated value and never sees an older value. This is a special case of the causal consistency model.
- **_Session consistency_** : This is a practical version of the previous model, where a process accesses the storage system in the context of a session. As long as the session exists, the system guarantees read-your-writes consistency. If the session terminates because of a certain failure scenario, a new session must be created and the guarantees do not overlap the sessions.
-   **_Monotonic read consistency_** : If a process has seen a particular value for the object, any subsequent accesses will never return any previous values.
-   **_Monotonic write consistency_** : In this case, the system guarantees to serialize the writes by the same process. Systems that do not guarantee this level of consistency are notoriously difficult to program.

These variations can be combined. For instance, monotonic reads + monotonic writes consistency is desirable.

## BASE - ACID alternative

Like we say previously, NoSQL Databases choose to assure the data availability. So ACID properties had to be replace by a new approach : **BASE**.
 <br>
BASE stands for Basic Availability, Soft state, Eventually Consistent.
<br>
Base requires an in-depth analysis of the operations within a logical transaction.
It model is based on consistency patterns and functional partitioning with sharding.

|   |  ACID  |  BASE  |
|---|--------|--------|
| Consistency  |  Strong | Weak  |
| Approach  | Pessimistic  | Optimistic  |
| Focus  | on commit  | on availability  |
| Schema evolution | difficult | flexible |
||| faster |
||| simpler |
||| best effort |

## OLAP VS OLTP

Cloud storage is a model of computer data storage in which the digital data is stored in logical pools, said to be on "the cloud". The physical storage spans multiple servers (sometimes in multiple locations), and the physical environment is typically owned and managed by a hosting company. People and organizations buy or lease storage capacity from the providers to store user, organization, or application data.

In order to migrate data from DBMS to the cloud, there are two main approaches chosen in according to the cloud architecture. 

Three architectures : 

### Shared Nothing

![Shared Nothing](./img/sharedNothing.png)

Generally adopted for the cloud.
+Efficience for scalability
-Height cost for data partitioning

Performance and scalability is depend of how data are distributed across the nodes.
Data parallelism -> reduce data to be transferred during a query

### Shared Disk

![Shared Disk](./img/sharedDisk.png)

No need to partition data across nodes since all nodes
access data via shared storage.
No need to move data across nodes.
Load balancing is easy since all nodes can service any request.

The nodes need to communicate in order to ensure data consistency (via distributed lock manager).
Shared disk are relatively cost effective for small to medium sized data warehouse.

### Shared Memory

![Shared Memory](./img/sharedMemory.png)

### OLTP 

Means On-Line Transactional Protocol. 
Generally used by DBMS.

Frequently used for reservations (airline, concerts...), E-Shopping, financial activities.

Theses systems have a large number of users who make short transactions. Database queries are typically simple, require less than a second response times, and return relatively few records.

Some properties of OLTP operations : 

- Transactions involving small amounts of data
- Frequent queries and updates
- Are short, atomic and isolated transactions

### OLAP

Means On-Line Analytical Processing.
Generally used by DB of very large size.
Reading-intensive so relax ACID properties.

Typical OLAP applications include business reports for Sales, marketing, Management reports, Business Process Management, Budgeting and forecasting, financial reporting, and more.

OLAP data are frequently extracted from operational OLTP DBs.
Sensitives Data an be anonymized (thanks to ETL process).

OLAP is supported by data warehouses (typically RDBMS with extended operations (cube, roll-up, drill-down, etc.)

OLAP has some historical (temporal), summarized, integrated, consolidated and multidimensional data

### OLAP has to be preferred 

- Security thanks to anonymization of sensitive data coming from ETL process. With OLTP no anonymization is possible.

- Elasticity requires a shared nothing cluster architecture. 
	- OLAP : effective data partitioning and parallel query processing. ACID no needed.
	- OLTP : complex concurrency control. Shared disk is more efficient. Hard to maintain data replication across geographically distributed data centers.


## Future trends in NoSQL

More ACIDity : 

- MongoDB adding durable logging storage in 1.7
- Cassandra adding stronger consistency in 1.0

More Query languages : 

- MongoDB had one right from the start
- Cassandra had CQL
- Couchbase : UnQL

Multi-model data stores : 

- Sqrrl: key-value, document, graph and column
- ArangoDB, OrientDB: document, graph and key-value
- Mark Logic: XML, document and RDF store (graph)
