﻿# NewSQL

## Motivations

### DBMS Limitations

- SQL became complex (ISO SQL-96, 89, 92, 1999, 2003, 2006, 2008, 2011 and 2016)
- Need for skilled DBA, tuning and well-defined schemas

### Knew Systems to handle this ?

- Not traditional OLTP because it exceeds their capacities
- Not data warehouses because they are typically stale (=périmé) by tens of minutes to hours. 
- Not for many NoSQL systems because they are eliminating or relaxing transaction support.

### The end of an architectural era

- Most RDBMS inherit many components from the first systems, e.g. R in the 70s.
- They include these features: 
	- Disk oriented storage and indexing structures
	- Multithreading to hide latency
	- Locking-based concurrency control mechanisms
	- Log-based recovery
- Because price of RAM is going down: 
	- OLTP should be considered a main memory market 
	- All the way (no disk recovery log like in Oracle TimesTen)
- SQL commands in a transaction to completion with a single threaded execution model (and no interruption)
- Upgrade cluster without stopping the system
- No knobs (Self-healing, self-maintaining, self-tuning)
- High availability :
	- Shared-nothing architecture 
	- Light logging mechanisms 
	- Hot standby or P2P configuration

> Source :  Stonebraker, M., Madden, S., Abadi, D. J., Harizopoulos, S., Hachem,
> N., and Helland, P. “The End of an Architectural Era (It's Time for a
> Complete Rewrite).” In Proc. VLDB, 2007.

### StandBy Database

A standby database is a database replica created from a backup of a primary database. This is a solution to guarantee high availability.
By applying archived redo logs from the primary database to the standby database, you can keep the two databases synchronized.
If the primary database is destroyed or its data becomes corrupted, you can perform a failover to the standby database, in which case the standby database becomes the new primary database.

Many standby configurations :

- Cold standby
- Warm standby
- Hot standby
- Active-active

**Cold Standby** 🥶 

The secondary node acts as backup of another identical primary system. 
It will be installed and configured only when the primary node breaks down for the first time. Subsequently, in the case of a failure in the primary the secondary node is powered on and the data restored before finally starting the failed component. 
Data from primary system can be backed up on a storage system and restored on secondary system as and when required. 
This generally provides a recovery time of a few hours.

**Warm standby** 🥵 

The software component is installed and available on the secondary node. 
In the case of a failure on the primary node, these software components are started on the secondary node. 
This process is usually automated using a cluster manager. 
Data is regularly mirrored to secondary system using disk based replication or shared disk. 
This generally provides a recovery time of a few minutes.

**Hot standby** 🥵🥵  

Software components are installed and available on both primary and secondary nodes. The software components on the secondary system are up to date but will not process data or requests. 
Data is mirrored in near real time and both systems will have identical data. 
Data replication is typically done through the software’s capabilities. 
This generally provides a recovery time of a few seconds.

**Active-Active** 🔋

In this method both the primary and secondary systems are active and processing requests in parallel. 
Data replication happens through software capabilities and would be bi-directional. Used for load balancing. 
This generally provides a recovery time that is instantaneous.

### Single threading in OLTP

- “The traditional motivations for multi-threading are to allow transaction processing to occur on behalf of one transaction while another waits for data to come from disk, and to prevent long running transactions from keeping short transactions from making progress.“ 
- Some code is required to supervise resources use. 
- If data in main memory, no more disk waits.
- Consider one physical node with multiple processors as multiple nodes in a shared nothing cluster : dynamically allocating resources between these logical nodes.
- Partitioning data to ensure that transactions are executed on a “single-site” => reduce network latencies

## NewSQL 

3 main categories of NewSQL stores which are designed as : 

- a new architecture
- a sharding middleware 
- database as a service approach (DbaaS)

### New architecture

- Distributed architectures operating on shared nothing resources
- Multi-node concurrency control
- Fault tolerance through replication
- Flow control
- Distributed query processing

- DB concurrency control : 
	- MVCC (Multi Version Concurrency control) 
	- Basic Timestamp Concurrency control 
	- Optimistic Concurrency control 
	- T/O with partition-level locking

**MVCC**

- Allows to read data without blocking updates
- Each txn keeps a snapshot
- And reads the snapshot to get a consistent view of the db
- Cons: garbage collection of old snapshot

**Basic Timestamp Concurrency**

- A timestamp on each tuple
- Read or write ops: 
	- Rejects if the timestamp is less than the timestamp of the last write of the tuple
- For write ops: 
	- Rejects if the timestamp is less than the timestamp of the last read on that tuple 	
- Cons: the cost that each site maintains a logical clock that needs to be accurate

**Optimistic Concurrency Control**

- Tracks the read/write txn and writes all write ops in a private workspace 
- The system determines whether that txn’s read set overlaps with the write set of any concurrent txn 
- Txn write their updates to shared memory only at commit time 
- Cons: cost of rollback

**T/O**

- DB is divided into disjoinst subsets called partitions 
- Partition : 
	- Lock 
	- Single-threaded execution engine 
- Apply timestamp on txn and add to queues 
- Execution of the oldest timestamp txn in the queue

### Sharding middleware

- Shard the database across a cluster of single-node DBMS instances (e.g. mySQL).
- Hard to adapt to a main-memory approach
- Redundant query planning and optimization on sharded nodes.

- The middleware : 
	- route queries
	- coordinate transactions
	- Manages data placement, replication and partitioning

### Database as a service (DbaaS)

DBaaS provider is responsible for maintaining the physical configuration of the DB, replication, backups.

Amazon Aurora and ClearDB adopt a new architecture reminiscent of the NewSQL approach.

Other DbaaS (but not NewSQL) are GoogleCloud SQL, Microsoft Azure SQL, Rackspace Cloud database, Salesforce Heroku.

## Some NewSQL Database

### VOLT DB

- Distributed row-based in-memory relational database (no buffer management).
- For high-performance OLTP processing
- Anti-caching: cold data is moved to disk in a transactionally-safe manner as the database grows in size (LRU).

- Each single-threaded partition operates autonomously → no need to locking and latching
- Data is automatically replicated for intra-cluster high availability → no logging

### SAP HANA

- Distributed in-memory DB for the integration of both OLTP and OLAP
- Rich data analytics
- Multiple query languages: SQL, R, SQLScript, etc

### MemSQL

- Distributed, in-memory RDBMS
- Translates SQL queries into (LLVM) machine code
- Apache Spark integration: use Spark to perform ETL tasks

### Google Cloud Spanner

- Main focus: cross-datacenter replicated data
- Schematized semi-relational tables : 
		- Hierarchical approach to grouping tables
		- It allows Spanner to co-locate related data into directories that can be easily 	         stored, replicated, locked and managed on Spanservers
		- It requires a modification of SQL
- TrueTime API represents time with a bounded time uncertainty. The underlying time references are GPS and atomic clocks.

### Snowflake

- DBaaS, HTAP database system
- Relational, elastic high availability, durable (e.g., on AWS S3), compressed (column store), secure (encrypted), No indexes, immutable
- Shared nothing with decoupling of compute and storage
