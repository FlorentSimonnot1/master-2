﻿<div style="width: 100%">
	<center>
	<h1>1</h1>
	<h1>Scala et ses notions de programmation orientée objet - Bases</h1>
	</center>
</div>

## Introduction

Scala est un langage de programmation multi-paradigme. Il intègre notamment le paradigme de programmation orienté objet et de programmation fonctionnel.

Scala est un langage récent, apparu dans les années 2000. Loin d'être le premier langage de programmation fonctionnel, il reste néanmoins le premier langage fonctionnel à utiliser la JVM Java. Il inspirera le langage Kotlin, également langage multi paradigme, s'exécutant sur la JVM et langage reconnu depuis quelques années pour le développement d'application mobile.

Les fichiers Scala (.scala) sont compiler en bytecode Java et exécuter sur les JVM.

## Variables, Types, Blocs, Opérateurs et Structures de contrôles

Tout comme en Java, développer du code en Scala demande d'utiliser des objets et de les faire interagir. Pour faire référence à ces objets on peut déclarer des variables. Il existe deux manières de déclarer ces variables. 

<h2>1) Les variables</h2>

### Variable mutable

Une variable mutable est une variable qui peut être modifier au cours du temps. Pour déclarer une variable mutable on utilise le mot clé **var**.

    var s = "Hello"

Ainsi la variable *s* pourra être modifiée à l'avenir.

### Variable immutable

À l'inverse, en Scala on peut déclarer une variable qui ne pourra être changé au cours du temps, c'est une constante. On peut utiliser le mot clé **val**. 

    val s = "Hello"

En Java, cela reviendrait à écrire `final var s = "hello"`.

___

**NOTE** 

Une bonne pratique est de ne jamais utilisé le mot clé var quand c'est possible. En effet la programmation fonctionnel suggère de ne pas changer l'état interne. On préférera donc créer une nouvelle variable avec la nouvelle valeur. 
___

<h2>2) Les types</h2>

En Scala la notation pour le typage est contrairement au Java le suivant : 

    variable: Type
   
Contrairement à Java encore une fois, il n'existe pas de type primitif. Les entiers (integer, double, char) sont tous considérés comme des objets. On écrira ainsi : 

    val i: Int = 1
    val c: Char = "s"
    val d: Double = 0.3

Il existe 4 types prédéfinis en Scala :

scala.Any est le type racine. Il possède 2 sous classes : 

- scala.AnyVal : valeur Int, Double, Long... (types primitifs de Java)
- scala.AnyRef : tout autre type (java.lang.Object)

scala.Null le sous type par défaut de scala.AnyRef

scala.Nothing le sous type par défaut de tout type

Scala utilise *l'inférence* automatique des types. Cela signifie que l'on peut omettre le type lorsque l'on initialise une variable car Scala sait en général reconnaître le type. 

	val i = 1 // C'est interprété comme un Int pas de problème

<h2>3) Les blocs</h2>

Un bloc est une expression contenant un ensemble d'expressions mises entre accolades.

Évaluer un bloc revient à évaluer sa dernière expression.
Toute variable déclarée à l'intérieur du bloc n'existe qu'à l'intérieur de celui-ci.

<h2>4) Les opérateurs</h2>

- Tout opérateur Scala est un nom de méthode d'instance. Les opérateurs ne sont plus natifs et peuvent être redéfinis.
- Opérateurs unaires = méthodes d'instances commençant par unary_

<h2>5) Les structures de contrôles</h2>

### If / else if / else

Exécuter ou non un bloc de code en fonction de la valeur d'une expression booléenne.

    if (exp_bool1) EXPRESSION1
    else if (exp_bool2) EXPRESSION2
    else EXPRESSION3

___
**Note**
L'instruction Switch n'est pas reconnue en Scala.
___

### While

Exécuter un bloc de code au moins 0 fois tant qu'une condition est vraie.

    while (exp_bool) {
		//Code
	}

### DoWhile

Exécuter un bloc de code au moins 1 fois tant qu'une condition est vraie.

    do{
	    //Code exécuté au moins une fois
	    //Tant que exp_bool est vraie
    }while(exp_bool)

### For

Équivalent à la boucle for étendue de java mais syntaxe différente !

    for (x <- COLLECTION) EXPRESSION

Cas particuliers : 

    for ( i <- Range.apply (MIN, MAX, STEP)) {/∗CODE∗/}
    for ( i <- MIN to MAX by STEP ) {/∗CODE∗/}
    for ( (x, y) <- MY_MAP) {/∗CODE∗/}

### Les débranchements

N'existe pas nativement en Scala. On ne peut pas utilisé le mot clé `break` pour sortir d'une boucle. Cependant on peut utiliser l'objet de type `Breaks`.

    val inner = new breaks;
    inner.breakable {
	  for (x <- xs) {
	    if (done) inner.break()
	    f(x)
	  }
	}

## Classes et Fonctions/Méthodes

<h2>1) Les classes</h2>

Tout comme en Java, Scala permet de créer ses propres objets en créant des classes. La structure des classes est cependant différente.

- Le constructor Primaire

Scala permet de créer un constructor que l'on qualifie de "primaire". On déclare les variables utiles à la construction de l'objet juste derrière le nom de la classe, entre parenthèses : 

    class C(v1: Type, ..., vn: Type) {...}

___
**ATTENTION**

Par défaut, les variables dans le constructor primaire ne sont pas accessibles en dehors de la classe. Il faut rajouter le mot clé val devant la variable pour que ce soit le cas.

    class Foo(x: Int, val y: Int) {
	    val xy = x * y
    }
	

    //val foo = new Foo(3, 4)
    //foo.xy --> get 3*4=16
    //foo.x !! ERROR !!
    //foo.y --> 4
___

Comme le constructor primaire n'a pas de bloc à proprement parler, toutes les opérations que l'on effectuerait dans un constructor basique en Java se fait directement dans le CORPS de la classe.

**Exemple**

    class DatabaseConnection
        (host: String, port: Int, username: String, password: String) {
        // Connexion à la DB
        private val driver = new AwesomeDB.Driver()
        driver.connect(host, port, username, password)
		
		// Méthodes...
		...
    }

___

**NOTE**
Il est considéré comme étant une bonne pratique d'utiliser le moins d'effet possible dans le constructor. En reprenant l'exemple précédent, il faudrait une méthode pour se connecter et se déconnecter afin de responsabiliser celui qui utilise le code.
___

- Les constructors auxiliaires

Scala permet d'utiliser des constructors auxiliaires qui ressembleront plutôt à des méthodes simples.

    class Person(val fullName: String) {
		// Le mot clé def permet de définir une méthode. 
		// Ce constructor auxiliaire fait appel au constructor primaire
		def this(firstName: String, lastName: String) = this(s"$firstName $lastName")
    }

Tout comme en Java, on peut privatiser notre constructor primaire : 

    class Person private(val fullName: Sting) {
	    // Seul constructor disponible
	    def this(firstName: String, lastName: String) = this(s"$firstName $lastName")
    }

___
**NOTE**
Les constructors auxiliaires doivent impérativement appeler le constructor primaire.
___

<h2>2) Fonctions et Méthodes</h2>

### Différences entre fonctions et méthodes

Contrairement à Java, Scala fait la différence entre une fonction et une méthode.

> Une méthode fait partie d'une classe. Elle a un nom, une signature, des annotions (optionnel) et produit du bytecode.

> Une fonction est un objet. En effet en Scala il existe l'objet fonction qui contient d'ailleurs des méthodes, comme la méthode apply. Lorsque l'on appelle une fonction, c'est en réalité la méthode apply de la fonction qui est appelée.

**NB** : Réciproquement, si on associe à une variable le résultat d'une méthode, Scala va en fait créer une fonction, dont la méthode apply sera redéfinie par notre propre méthode.

### Syntaxe

Les méthodes doivent être écrites dans une classe en utilisant le mot clé `def`. La syntaxe est la suivante : 

    def myMethod([params...: Types]): ReturnType = {/*code*/}

Quant aux fonctions, on utilise le mot clé `val`. Elles peuvent être déclarés dans des classes ou en dehors.

    val myFunction: ([...Types]) => ReturnType = (...params) => {/*code*/}

### Appel de méthode: POO Vs Functional

Pour appeler une méthode il y a deux possibilités : 

    obj.method(params...) //Version Prog Orientée objet
    obj method (params...) //Version fonctionnelle

### Nested méthodes/fonctions

Scala permet de créer des méthodes/fonctions imbriquées. Ce sont des méthodes/fonctions écrites dans d'autres méthodes/fonctions. Elles sont pratiques lorsque l'on a besoin de répétée une même logique à l'intérieur d'une méthode/fonction.

Exemple : 

	// Calcul le max entre 3 nombres
    def max3(a: Int, b: Int, c: Int): Int {
	    
	    // Calcul le max entre 2 nombres
	    def max2(a: Int, b: Int): Int = if (x > y) x else y

		max2(a, max2(b,c))
    }

Les méthodes/fonctions imbriquées ne sont pas visibles en dehors de leur méthode/fonctions parent.
Dans cet exemple, max2 n'est visible que dans max3. 

## L'héritage en Scala

Comme en Java, Scala permet à une classe B d'hériter d'une autre classe A grâce au mot clé **`extends`**. Ainsi la classe B héritera des méthodes de la classe A. Les méthodes de cette classe pourront être redéfinies grâce au mot clé **`override`**.

    //Une première classe représentant un point dans un plan en 2D
    class Point(x: Int, y: Int) {
	    def this() = this(0,0)
	    def this(x: Int) = this(x,0)

		def move(dx: Int, dy: Int) {
			x = x + dx
			y = y + dy
		}
		
	    def toString: String = s"($x, $y)"
    }
    
    //Une deuxième classe représentant un point dans un plan en 3D
	//Héritage de la classe Point
	class Point3D(x: Int, y: Int, z: Int) extends Point {
		def move(dx: Int, dy: Int, dz: Int) {
			x = x + dx
			y = y + dy
			z = dz + z
		}
		
		override def toString: String = s"($x, $y, $z)"
	}

Dans cet exemple la méthode toString a été redéfinies afin d'afficher les bonnes informations.

___
**NOTE**
- Une classe hérite par défaut de la classe AnyRef
- L'héritage multiple est impossible
___

Une notion intéressante, permise par Scala est la **restriction de l'héritage**. On peut en effet interdire complètement ou partiellement à des classes d'hériter d'une autre classe.

Prenons une classe A. Nous pouvons empêcher tout autre classe d'hériter de la classe A en définissant notre classe comme ceci : 

    final class A {}

Prenons une classe B définies dans un fichier F. Nous pouvons empêcher toute classe définies en dehors de ce fichier de pouvoir hérité de notre classe B en définissant notre classe comme ceci : 

    sealed class B {}

Ainsi toute classe se trouvant dans le fichier F aura le droit d'hérité de B mais aucune classe implémenté en dehors en aura la permission.
