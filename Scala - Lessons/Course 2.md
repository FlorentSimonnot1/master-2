﻿
<div style="width: 100%" align="center">
	<h1>2</h1>
	<h1>Scala et ses notions de programmation fonctionnelles - Bases</h1>
</div>

Dans ce cours nous étudierons des notions fondamentales de la programmation fonctionnelle, telle que les monades, les fonctions d'ordre supérieur, l'évaluation de fonctions et la récursivité.
Nous présenterons tout d'abord, la notion de pattern matching qui sera utile tout au long de ce chapitre.

## Pattern matching 

Le pattern matching est un mécanisme permettant de vérifier une valeur par rapport à un motif. C'est une version plus puissante que le _switch_ en Java et peut également remplacer une série de _if/else_.

**Syntaxe**

    val  x:  Int  =  Random.nextInt(10)  
    x match {  
	    case  0  =>  "zero"  
	    case  1  =>  "one"  
	    case  2  =>  "two"  
	    case  _  =>  "other"  
    }

Pour vérifier la valeur d'une variable grâce au pattern matching on utilise le mot clé **match**.
Tout comme en Java, on définis des cas grâce au mot clé **case**. 
À noter que le mot clé **default** n'existe pas. On utilise **case _** pour traiter une valeur par défaut.

## Les types "Fonctionnels" / Monades

En théorie des langages fonctionnels, une **monade** est une structure permettant de manipuler des langages fonctionnels _purs_ avec des traits impératifs. Il s'agit alors d'avoir une représentation simulant exactement des notions telles que les exceptions ou les effets de bords, tout en conservant la pureté des langages fonctionnels.

Les types suivant sont des monades :

- Option
- Try
- Either
- "Tuple"
- "List"

### Option 

La classe Option est une structure de données contenant une unique valeur ou aucune valeur. Elle peut être perçue comme une collection avec 0 ou 1 élément.

Option est une classe abstraite avec 2 enfants : 

- **Some** contient une valeur
- **None** ne contient aucune valeur


En Java les méthodes peuvent retourner null ou throw une exception (ou optional depuis Java 8). En Scala, Option est utile dans ces expressions qui utiliseraient null ou les exceptions.

___

**NOTE** : La classe option est une classe dite _"paramétrée"_. 
<br> Voir le cours 3.
___

**Syntaxe**

On utilise la classe **Some** pour créer un élément unique. **None** permet de créer un élément vide. On peut également utiliser **Option.empty[Type]**.

    def maybe(x: Int): Option[Int] = {
	    if (x > 0) Some(x)
	    else None
    }
    
    def maybe2(x: Int): Option[Int] = {
	    if (x > 0) Some(x)
	    else Option.empty[Int]
    }

**Vérifier l'existence ou non**

On utilise la méthode ***isDefined()***.

    val res = maybe(2)
    val res2 = maybe(-2)
	
	res.isDefined // True
	res2.isDefined // False

**Récupérer la valeur proprement**.

On utilise la méthode ***getOrElse()***.

    val value = res.getOrElse(0) // Returns 2
    val value2 = res2.getOrElse(0) // Returns 0

**Fonctions utiles**

La classe Option possède des fonctions _d'ordre supérieur_ permettant de traiter cette classe comme une collection.

    val option: Option[Type] = ...

	option.map(...) // Returns Some(value.functions) or None
	option.foreach(...) // Execute action if Some
	option.forall(...) // Returns True if None or if Some and condition is true
	option.exists(...) // Returns True if Some and condition is true
	option.toList // Returns an actual list

### Try

La classe Try représente un calcul qui peut entrainer une exception ou renvoyer une valeur avec succès. L'utilisation de cette classe est similaire a _Either_ mais la sémantique est différente.

Une instance de Try est soit une instance de la classe **Success**, soit une instance de la classe **Failure**.

___
**NOTE** : Try, Success et Failure sont des classes paramétrées.
___

Example : 

On peut ainsi effectuer une division sans se soucier de diviseur.

    def divide: Try[Int] = {
      val dividend = Try(StdIn.readLine("Enter an Int that you'd like to divide:\n").toInt)
      val divisor = Try(StdIn.readLine("Enter an Int that you'd like to divide by:\n").toInt)
      val problem = dividend.flatMap(x => divisor.map(y => x/y))
      problem match {
        case Success(v) =>
          println("Result of " + dividend.get + "/"+ divisor.get +" is: " + v)
          Success(v)
        case Failure(e) =>
          println("You must've divided by zero or entered something that's not an Int. Try again!")
          println("Info from the exception: " + e.getMessage)
          divide
      }
    }

Pour récupérer le résultat d'un Try, on utilisera le pattern matching.

### Either

La classe Either est une structure de donnée qui permet de gérer des erreurs. La classe est similaire à Option. Seulement le type Either ne peut pas renvoyer quelque chose de vide. Il doit par exemple renvoyer un message d'erreur ou un code d'erreur.

Either possède deux enfants : 

- Right 
- Left

La classe Right est comme la classe Some de Option. Left est comme None mais doit prendre une valeur.

___

**NOTE** : La classe option est une classe dite _"paramétrée"_. 
<br> Voir le cours 3.
___

**Syntaxe**

    def resultOrError(x: Int) = Either[Throwable, Int] = {
		if (x > 0) Right(x)
		else Left(new Exception("Negative value found"))
	}
Ici on renvoie x s'il est positif, sinon on renvoie une exception.

Pour récupérer le résultat ou l'erreur, on pourra utiliser le pattern matching.

### Tuple

Un tuple est une collection **hétérogène** de 2 à 23 valeurs. Il peut être défini à l'aide de parenthèses. 

    val a = (3, "a")
  
Pour les tuples de taille 2 il existe une autre notation.

    val b = 3 -> "a"

**Accéder au éléments**

Pour accéder aux éléments d'un tuple on utilise la syntaxe **._n**.

Exemple : 

    Soit x un tuple de taille 2. 
    Alors x._1 permet d'accéder à l'élément 1.
    x._2 est utilisé pour le second élément.


### List

Une list est une collection d'éléments ayant le même type. 
En Scala une liste est **immutable**. Les éléments ne peuvent donc pas être changé par une affectation. De plus, une liste est une liste chainée avec une tête et une queue.

Le type d'une liste d'éléments de type T est noté **List[T]**.

**Déclarer une liste**

    val fruit: List[String] = List("apples",  "oranges",  "pears")
    val nums: List[Int] = List(1, 2, 3)
    val nums2 = 4 :: 5 :: Nil  // Lisp-style
    val empty1: List[String] = Nil
    val empty2:  List[Nothing]  =  List()
    val empty3 = List.empty[String]
    

## Fonctions d'ordre supérieur

Une fonction d'ordre supérieur est une fonction qui prend une valeur de fonction en tant qu'argument ou retourne une valeur de fonction. Mais dans Scala, comme toutes les opérations sont des méthodes, il est plus général de penser à des méthodes qui reçoivent ou renvoient des paramètres de fonctions.

Un example de fonction d'ordre supérieur **map**.

    List(1, 2, 3).map { x => x * 2 }

La fonction d’ordre supérieur `map` prend en argument une fonction `f: A => B` où `A` est le type de départ (ici `Int`) et `B` le type d’arrivée (ici encore `Int`).

**Fonction prenant en paramètre une fonction**

Écrivons une fonction réalisant la multiplication et la somme de deux entiers.

    val mul: (Int, Int) => Int = (a, b) => a * b
    val sum: (Int, Int) => Int = (a, b) => a + b

On peut maintenant créer une fonction d'ordre supérieur qui prendre une fonction en paramètre dont la signature est la même que nos deux fonctions de calcul.

    def foo(f: (Int, Int) => Int): Int = f(0, 1)
    
	println(foo(sum)) // print 1
	println(foo(mul)) // print 0

**Fonction retournant une fonction**

    def counter() = {
		var count = 0
		() => { count += 1; count }
	}

Dans cet example on retourne une fonction qui incrémente une compteur.

**Utiliser des fonctions anonymes !**

Les fonctions d'ordre supérieur peuvent paraître lourdes puisque l'on doit écrire des fonctions qui seront peu souvent réutilisées autre part. Cependant les fonctions anonymes nous permettent d'alléger le code.

Si l'on reprend l'example des fonctions `mul`, `sum` et `foo` : 

    def foo(f: (Int, Int) => Int): Int = f(0, 1)

	// Équivalent à foo(sum)
	foo({(x: Int, y: Int) => x + y})
	// Équivalent à foo(mul)
	foo({(x: Int, y: Int) => x * y})

## Stratégie d'évaluation

Les stratégies d'évaluation sont l'un des traits les plus importants en programmation.

Langages de programmation utilisent des stratégies d'évaluation pour déterminer quand et comment les arguments d'un appel de fonction sont évalués. Il existe de nombreuses stratégies d'évaluation, mais la plupart d'entre elles se retrouvent dans deux catégories:

- Évaluation stricte :  dans laquelle les arguments sont évalués avant que la fonction ne soit appliquée. Des exemples de langages de programmation qui utilisent des stratégies d'évaluation strictes sont Java, Scheme, JavaScript, etc.

- Évaluation Non stricte : qui reportera l'évaluation des arguments jusqu'à ce qu'ils soient réellement requis/utilisés dans le corps de la fonction. Haskell est probablement le langage de programmation fonctionnel le plus populaire qui utilise une évaluation non stricte.

**Scala prend en charge ces deux types d'évaluation**.

Scala prend en charge deux stratégies d'évaluation prêtes à l'emploi: **call by value** et **call by name**.

### Call by value

Call by value est une stratégie d'évaluation stricte qui est la plus couramment utilisée par les langages de programmation. Dans call by value, l'expression est évaluée et liée au paramètre correspondant avant que le corps de la fonction ne soit évalué. C'est également la stratégie d'évaluation par défaut dans Scala.

    def square(x : Double) = x * x
	def sumOfSquares(x : Double, y : Double) = square(x) + square(y)

	-> sumOfSquares(3, 3 + 1)
	-> sumOfSquares(3, 4)
	-> square(3) + square(4)
	-> 3 * 3 + square(4)
	-> ...

Les paramètres par valeur ont l'avantage d'être évalués une seule fois.

### Call by name

Call by name est une stratégie d'évaluation non stricte qui reportera l'évaluation de l'expression jusqu'à ce que le programme en ait besoin. Pour faire un paramètre call-by-name, ajoutez simplement = > avant le type, comme indiqué ci-dessous :

    def constOne(x: Int, y: => Int) = 1
    def  calculate(input:  =>  Int)  =  input  *  37

Les paramètres par nom ont l'avantage de ne pas être évalués s'ils ne sont pas utilisés dans le corps de la fonction. 
L'inconvénient de cette stratégie d'évaluation est que les arguments sont évalués à chaque utilisation dans le corps de la fonction. Les occurrences multiples de cet argument signifient plusieurs évaluations. Dans ce cas, il est crucial que l'expression passée en tant qu'argument de nom n'ait aucun effet secondaire, sinon cela peut conduire à un comportement inattendu.

    def constOne(x: Int, y: => Int) = 1
    constOne(1, loop) // 1
    constOne(loop, 2) // infinite loop

Ici, y est réévaluer à chaque fois.

### Call by need

Scala ne fait pas que de supporter ces deux types d’évaluation. Il permet de les combiner grâce à la méthode d'évaluation **Call by need**.

Call by need est la version mémorisée de call by name qui fait que l'argument de fonction ne doit être évalué qu'une seule fois (lors de la première utilisation).

    La mémorisation est une technique d'optimisation qui consiste à mettre en cache les résultats des appels de fonction et à renvoyer le résultat mis en cache lorsque la fonction est appelée à nouveau avec la même entrée.

Scala permet d'utiliser cette méthode d'évaluation par l'intermédiaire des **lazy value**, qui sont des valeurs initialiser de manière paresseuse.

> L'initialisation paresseuse est la tactique consistant à retarder la création d'un objet, le calcul d'une valeur ou un autre processus coûteux jusqu'à la première fois qu'il est nécessaire

Il faut cependant noter qu'en Scala les arguments paresseux n'existe pas. Il faut donc utiliser un petit contournement pour recréer l'effet d'un argument paresseux.

	def foo(cond: Boolean)(bar: => String) = {
	    lazy val lazyBar = bar
	    if (cond) lazyBar + lazyBar
	    else ""
	}

	foo(true){ // some computation ... }

## Récursivité

La récursivité est un concept fondamental en programmation fonctionnelle. C'est une approche déclarative (fonctionnelle) et s'oppose à l'itération (impérative). Il existe de nombreux algorithmes et structures récursives : 

- Graphes, Arbres...
- PGCD, Factorielle...

**En programmation fonctionnelle, tout algorithme à plusieurs étapes est résolu grâce à la récursivité !**

Dans une fonction récursive, le type de retour est obligatoire.

Exemple : 

    def factorial(n : Int): Int = if (n == 0) 1  else n * factorial(n − 1)

La récursivité simple crée une série de stack frame (paramètres d'une fonction, adresse de retour, variable locale). Pour les fonctions qui require une grande profondeur de récursivité, cela crée une erreur de type `StackOverflowError`.

	-> factorial(4)
	-> 4 * factorial(4 - 1)
	-> 4 * factorial(3)
	-> 4 * (3 * factorial(3 - 1))
	-> 4 * (3 * factorial(2))
	-> 4 * (3 * (2 * factorial(2 - 1)))
	-> 4 * (3 * (2 * factorial(1)))
	-> 4 * (3 * (2 * (1 * factorial(1 - 1))))
	-> 4 * (3 * (2 * (1 * factorial(0))))
	-> 4 * (3 * (2 * (1 * 1)))
	-> 24	

Pas trop efficace !

### Tail Recursion

Une fonction est dite Tail récursive si sa dernière action est un appel à elle même.
Dans ce cas là, la stack frame peut être réutilisée. Cela va donc permettre de contourner le problème des stack frames grâce à la JVM qui va optimiser le bytecode pour que la fonction ne nécessite qu'une seule trame de pile.

La fonction factoriel rédigée comme suivant n'est pas tail récursive.

    def factorial(n : Int): Int = if (n == 0) 1  else n * factorial(n − 1)

En effet le dernier appel n'est pas factoriel mais c'est la somme de tous les résultats.

**L'annotation @tailrec**

Cette annotation permet au compilateur de savoir qu'une fonction est tail récursive.

___
**NOTE** : Pour transformer une fonction récursive en fonction tail récursive, penser à créer une nested method !
___

    def factorial(n: Int) = {
		@annotation.tailrec
		def helper(n: Int, acc: Int): Int = { // n here "overrides" the n from the enclosing scope
			if (n == 0) acc
			else helper(n-1, n * acc)
		}
		helper(n, 1)
	}


