﻿
<div style="width: 100%" align="center">
	<h1>3</h1>
	<h1>Scala et ses notions de programmation orientée objet - Avancé</h1>
</div>

Dans ce cours nous étudierons les notions avancées de programmation orientée objet. Nous commencerons par voir les Objets, les Singletons et compagnon Objets mais aussi les Case Classes, les Case Objects ainsi que les Traits. Nous verrons comment implémenter des énumérations puis définirons la notion d'ADT en Scala. Nous évoquerons enfin la généricité en Scala.

## Les objets

Les objets sont statiques. Ils sont déjà instanciés.


	object Dog {
	    def bark: String = "Raf"
	}

	Dog.bark()

### Les objets singleton

Scala prend en charge les membres statiques, mais pas de la même manière que Java. Scala fournit une alternative à cela appelé _Singleton Objects_ . Les objets singleton sont similaires à une classe normale, sauf qu'ils ne peuvent pas être instanciés à l'aide du `new` mot clé. 

Voici un exemple de classe singleton :


	object Factorial {
	    private val cache = Map[Int, Int]()
	    def getCache = cache
	}

Comme les objets singleton ne peuvent pas être instanciés, ils ne peuvent pas avoir de paramètres. L'accès à un objet singleton ressemble à ceci :


	Factorial.getCache() //returns the cache


### Les objets companion

Dans Scala, les objets singleton peuvent partager le nom d'une classe correspondante. Dans un tel scénario, l'objet singleton est appelé _objet compagnon_ . Par exemple, la classe `Factorial` est défini et un objet compagnon (également appelé `Factorial` ) est défini en dessous. Par convention, les objets compagnons sont définis dans le même fichier que leur classe compagnon.

```
class Factorial(num : Int) {

  def fact(num : Int) : Int = if (num <= 1) 1 else (num * fact(num - 1))

  def calculate() : Int = {
    if (!Factorial.cache.contains(num)) {    // num does not exists in cache
      val output = fact(num) // calculate factorial
      Factorial.cache += (num -> output)     // add new value in cache
    }

    Factorial.cache(num)
  }
}

object Factorial {
  private val cache = scala.collection.mutable.Map[Int, Int]()
}

val factfive = new Factorial(5)
factfive.calculate  // Calculates the factorial of 5 and stores it
factfive.calculate  // uses cache this time
val factfiveagain = new Factorial(5)
factfiveagain.calculate  // Also uses cache
```

Dans cet exemple, nous utilisons un  `cache`  privé pour stocker la factorielle d'un nombre afin de gagner du temps de calcul pour des nombres répétés.

Ici, l'  `object Factorial`  est un objet compagnon et la  `class Factorial`  est la classe compagnon correspondante. Les objets compagnons et les classes peuvent accéder aux membres privés de l'un et l'autre. Dans l'exemple ci-dessus, la classe  `Factorial`  accède au membre de  `cache`  privé de son objet compagnon.

Notez qu'une nouvelle instanciation de la classe utilisera toujours le même objet compagnon, donc toute modification des variables membres de cet objet sera conservée.

## Les cases classes

Les cases classes sont des classes instanciables, proches des classes basiques. Cependant, elles apportent de la simplicité. 

### Syntaxe 

Pour déclarer une case classe, on utilise le mot clé case précédant le mot clé class.

    case class MyCaseClass(n: Int, m: Int)

### Les champs de la classe

Comme on peut le remarquer ci dessus, nous n'avons pas utilisé le mot clé `val` ou `var` pour les champs de notre classe. Par défaut, les champs sont immuables. Le mot clé val n'est donc pas nécessaire. 
Contrairement au classe basique, nous pouvons accéder aux champs publiquement.

___
**NOTE** : On peut utiliser le mot clé `var` pour casser l'immuabilité d'une variable. Mais à évité.
___

### Equals, toString...

Les cases classes ont pour avantages de générer automatiquement certaines méthodes pratiques. Par exemple, la méthode `equals` et `toString` sont générés pour tester l'égalité de deux cases classes ou afficher proprement une case classe sans avoir à le faire.

    class Brand(val name: String, val nbDoor: Int)

	// Same class with equals method override
    class Brand2(val name: String, val nbDoor: Int) { 
	  override def equals(obj: Any): Boolean = {
	    obj match {
	      case b: Brand2 => b.name.equals(name) && b.nbDoor == nbDoor
	      case _ => false
	    }
	  }
	}

	// case class
    case class Brand3(name: String, nbDoor: Int)

	val bmw = new Brand("BMW", 5)
	val bmw2 = new Brand("BMW", 5)

	val peugeot = new Brand2("Peugeot", 4)
	val peugeot2 = new Brand2("Peugeot", 4)

	val mercedes = new Brand3("Mercedes", 2)
	val mercedes2 = new Brand3("Mercedes", 2)

	println(bmw == bmw2) // false
	println(peugeot == peugeot2) // true
	println(mercedes == mercedes2) // true

### Copy, apply, unapply...

Les case classes génèrent une méthode `copy` qui permet de créer une copie de l'objet. La méthode peut prendre en paramètres des valeurs afin de modifié certains déjà présente.

	case class Brand3(name: String, nbDoor: Int)
	val mercedes = new Brand3("Mercedes", 2)
	val mercedes2 = mercedes.copy(nbDoor = 3)

Rappelons que les méthodes `apply` et `unapply` appartiennent à la classe Object. Les cases classes génèrent donc aussi des méthodes de la classe objet. 

La génération de la méthode `apply` permet de manipuler une case classe comme un objet. Nous n'avons donc pas besoin d'instancier la classe à chaque création d'objet.

	case class Brand3(name: String, nbDoor: Int)

	val mercedes = new Brand3("Mercedes", 2)
	val mercedes2 = Brand3("Mercedes", 2)

**Unapply method**

Les case classes ont également une méthode qui s'appelle `unapply`, méthode hérité de `Object`. Cette méthode est une méthode dite extracteur. Nous développerons ce sujet dans le <a href="Course4.md">Cours 4</a>.
<br>

La méthode `unapply` est un peu l'inverse de la méthode `apply`. En effet la méthode prend un objet et va extraire tous ses champs. C'est assez puissant puisque l'on peut créer des variables a partir d'un objet.

```
case class Person(name: String, age: Int) 
val p = Person("Paola", 42) 

val Person(n, a) = p 
println(n) // Paola
println(a) // 42
```

On peut donc récupérer les champs d'un objet en une seule fois, sans utiliser de getters.

On peut ignoré une variable si l'on en a pas besoin grâce au caractère `_`.

```
case class Person(name: String, age: Int) 
val p = Person("Paola", 42) 

val Person(n, _) = p 
println(n) // Paola
```

Ici, on ne veut pas de l'âge de Paola, donc aucune variable sera créer pour ce champ.

___
**NOTE** : Les extracteurs peuvent s'appliquer à plusieurs niveau. On peut par exemple extraire des objets qui sont dans une liste.
___

```
val p = Person("Paola", 42) 
val p2 = Person("Angela", 1337)

val List(Person(n1, a1), Person(_, a2)) = List(p, p2)
// n1: String = Paola
// a1: Int = 42
// a2: Int = 1337
```

### Pourquoi la génération automatique de ces méthodes est exceptionnelle ?

On en conviendra que le mot exceptionnel est un peu fort. La génération automatique implémente des méthodes qui ne sont pas compliqués en soit. Mais cette génération apporte un gain de temps et une maintenance moins lourde. En effet, si l'on ajoute ou retire certains champs de notre case classe, nous n'aurons pas à modifié les méthodes toString, equals, copy etc... De plus ces méthodes apportent de la cohérence à nos classes, qui possèdent donc toutes les mêmes méthodes de base.

### Choisir entre une classe et une case class

L'immuabilité des champs par défaut et les méthodes générés automatiquement, font des case classes, des classes pratiques pour la manipulation de données. On les utilises souvent comme des classes pour le stockage.
	
## Les traits

Les traits permettent de partager des interfaces et des champs. Ils sont similaires aux interfaces de Java. Les classes et les objets peuvent hériter d'un trait. Un trait ne peut pas être instancié.

    trait Pet {val name: String}  

	class Cat(val  name:  String) extends Pet  
	class Dog(val  name:  String) extends Pet  

	val dog = new Dog("Harry")  
	val cat = new Cat("Sally")

Les champs d'un trait doivent être implémenté dans le constructor des classes enfants. Dans l'exemple ci dessus, les classes Dog et Cat hérite du trait Pet qui comprend un nom. Tout comme en Java et ses interfaces, l'avantage des traits est de pouvoir manipuler plusieurs objets de la même manière. Ainsi on peut stocker les `Cat` et les `Dogs` dans des listes en tant qu'objet `Pet`.


    val pets = new Cat("Miaou") :: new Dog("Wallace") :: Nil
    pets.foreach(p => println(p.name))
    // Miaou
    // Wallace

## Les énumérations

### La classe Enumeration de Scala

Scala comporte une classe Enumeration permettant ainsi de créer des énumérations.
Si l'on souhaite créer une énumération il nous suffit de créer un objet qui hérite de Énumération.

```
object WeekDays extends Enumeration {
  val Mon, Tue, Wed, Thu, Fri, Sat, Sun = Value
}
```

Dans cet exemple on définis les 7 jours de la semaine.

On peut affecter à chaque valeur de l'énumération une valeur que l'Homme pourra comprendre plus facilement.

    object WeekDays2 extends Enumeration {
          val Mon = Value("Monday")
          val Tue = Value("Tuesday")
          val Wed = Value("Wednesday")
          val Thu = Value("Thursday")
          val Fri = Value("Friday")
          val Sat = Value("Saturday")
          val Sun = Value("Sunday")
    }

		println(WeekDays.Mon) // Affiche Mon
		println(WeekDays2.Mon) // Affiche Monday

Les objets énumérations donnent la possibilité d'utiliser le pattern matching, ce qui est pratique pour certaines méthodes : 

	object WeekDays extends Enumeration {
		val Mon, Tue, Wed, Thu, Fri, Sat, Sun = Value
		def isWeekEnd(day: WeekDays) = day match {
			case WeekDays.Sat | WeekDays.Sun => true
			case _ => false
		}
	}

	isWeekend(WeekDays.Sun) // true
	isWeekend(WeekDays.Mon) // false

On peut récupérer toutes les valeurs de notre énumérations grâce au champ `values` qui renvoie un objet `ValueSet`.

	val list = WeekDays.values

	//Renvoie 
	WeekDays.ValueSet(Mon, Tue, Wed, Thu, Fri, Sat, Sun)

___ 
**ATTENTION** : Les énumérations implémentés de la sorte ne sont pas type-safe. Deux énumérations différentes peuvent être évalué comme un même type.

```
object Parity extends Enumeration {
   val Even, Odd = Value
}
  
WeekDays.Mon.isInstanceOf[Parity.Value] // true ! WTF ??!!
```

### Les énumérations avec les sealed traits & case objects

En scala, une alternative intéressante pour créer des énumérations est d'utiliser les traits et les cases objects.

```
sealed trait WeekDay

object WeekDay {
  case object Mon extends WeekDay
  case object Tue extends WeekDay
  case object Wed extends WeekDay
  case object Thu extends WeekDay
  case object Fri extends WeekDay
  case object Sun extends WeekDay
  case object Sat extends WeekDay
}
```

On rappel que le mot clé `sealed` permet de réduire la portée du trait WeekDay au fichier dans lequel le trait est implémenté.

Après avoir déclaré notre trait, nous devons déclaré un objet portant le même nom.
Ensuite, nous pouvons déclaré dans cet objet, toutes les valeurs de notre énumération. Ces éléments sont déclarés en tant que `case object` et implémente le `trait WeekDay`.

Un inconvénient de de cette méthode et que nous ne pouvons pas récupérer automatiquement toutes les valeurs de l'énumération contrairement à la première méthode.

	val list2 = WeekDays.values // Ne fonctionne pas

Un des avantages par contre, avec cette méthode est de pouvoir créer des hiérarchies complexe en créant des class dans notre énumération. 

	sealed trait CelestialBody  
	object CelestialBody {
	  case object Earth extends CelestialBody
	  case object Sun extends CelestialBody
	  case object Moon extends CelestialBody
	  case class Asteroid(name: String) extends CelestialBody
	}
	val asteroid = CelestialBody.Asteroid("Minus")

Un dernier inconvénient est à noter avec cette méthode. Nous avions vu avec les énumérations comment ajouté une valeur plus claire pour un humain. Ici, le procédé est moins évident. Nous ne pouvons pas utilisé la classe `Value` pour chaque énumération tout simplement parce que nos énumérations sont maintenant des `case objects`.
<br>
Pour affecter une valeur à notre énumération, il faut ajouter un champ dans le type parent. Les objets qui hérite de l'objet, pourront utilisé la valeur souhaité.

```
  sealed trait WeekDay { val name: String }

  object WeekDay {
      case object Mon extends WeekDay { val name = "Monday" }
      case object Tue extends WeekDay { val name = "Tuesday" }
      (...)   
  }
```

___
**NOTE** : On peut également utilisé une case classe au lieu du trait si l'on souhaite mettre une valeur.
___

```
  sealed case class WeekDay(name: String)
    
  object WeekDay {
      object Mon extends WeekDay("Monday")
      object Tue extends WeekDay("Tuesday")
      (...)   
  }
```

## ADT (Algebraic Data Type)

Les ADTs sont un moyen de structure de données. Il y a deux catégories d'ADTs : 

- Type produit
- Type somme

Avant de commencer, calculons le nombre de valeurs possibles pour quelque types connus...

	Nothing // 0 valeur possible
	Unit // 1 valeur possible  
	Boolean // 2 [true,false]  
	Byte // 256  
	Int // ~4 million  
	String // Beaucou

### Type produit 

Le type produit est représenté par une case classe, un case object ou encore un Tuple.
Le nombre de valeur possible est alors le produit cartésien de tous les champs, c'est à dire le produit des champs entre eux.

Quelques exemples : 

	case class Student(enrolled: Boolean, age: Byte)  
	// Students consists of a Boolean AND Byte  
	// 2 * 256 = 258
	
	type aTuple2 = (Boolean, Unit)    
	// aTuple2 consists of a Boolean AND Unit  
	// 2 * 1 = 2
	
	type aTuple3 = (String, Boolean, Nothing)    
	// aTuple3 consists of a String AND Boolean and Nothing       
	// A lot * 2 * 0 = 0


### Type somme

En Scala, le type Sum est implémenté par héritage. Les valeurs de Sum sont les valeurs  disjointes (utilisation de `OR`) des sous types de la classe parent. On fait donc la somme des valeur des sous types.

Exemple : 

	sealed trait Color   
	case object Red extends Color  
	case object Blue extends Color  
	case object Green extends Color
	// Color can be Red OR Blue OR Green  
	// 1 + 1 + 1 = 3 distinct possible values

___
**NOTE** Les valeurs de `Sum` peuvent être eux même un produit. Une somme est donc un type constitué de somme et de produit.
___

### Exemple de type ADT

	sealed trait Notification
	final case class Email(sender: String, title: String, body: String) extends Notification
	final case class SMS(caller: String, message: String) extends Notification
	final case class VoiceRecording(contactName: String, link: String) extends Notification

Dans cet exemple, nous avons un type Somme, qui est le `trait` Notification. Il y a donc 3 valeurs possibles. Mais Email, SMS et VoiceRecording sont des types produits, qui ont un nombre incalculable de valeurs possibles car leurs paramètres sont des `String`. Nous avons donc créer un Type qui mesure un nombre infini de possibilité tout en respectant le modèle ADT.

### ADT et le Pattern Matching

ADT et le Pattern Matching sont un mariage parfait en Scala.

Pattern matching permet de décomposer un ADT donné à l'aide de la méthode extractor (`unapply`). Il extrait essentiellement les champs de type de produit. Pour ce faire, les objets compagnons des classes de cas doivent implémenter `unapply` afin d'être extractor.

	def isSMS(s: Notification): Boolean = s match {
		case SMS(_, _) => true
		case _ => false
	}

## Généricité

Comme en Java, Scala permet de créer des classes dites **génériques**. Ce sont des classes qui prennent un type comme paramètre. Ces classes sont pratiques pour les classes qui utilisent des collections par exemple.

**Syntaxe**

    class Stack[A] {  
	    private  var  elements:  List[A]  =  Nil  
	    def  push(x:  A) = {  
		    elements  =  x  ::  elements  
	    }  
	    def  peek:  A  =  elements.head  
	    def  pop():  A  =  {  
		    val  currentTop  =  peek  
		    elements  =  elements.tail  currentTop  
	    }  
	}

Pour paramétrer une classe, on utilise la notation `[TYPE]` après le nom de classe. Ici on a créer une classe qui représente une pile. Cette pile est doit contenir les mêmes éléments mais deux piles différentes peuvent avoir des types différents grâce à notre type paramétré A.

**Usage**

    val stack = new Stack[Int] // Nouvelle pile d'entier
    stack.push(1) 
    stack.push(2) 
    println(stack.pop)
    println(stack.pop)

Cours suivant : <a href="Course 4.md">Scala - Avancé</a>
