﻿
<div style="width: 100%" align="center">
	<h1>4</h1>
	<h1>Scala - Avancé</h1>
</div>

Dans ce cours nous évoquerons la notion d'implicite en Scala. Nous étudierons ensuite les Typeclasses, les PartialFunctions et les Futures.

## Implicits

L’utilisation des implicits permet au développeur de **se faciliter la tâche en laissant au compilateur le soin d’aller chercher de lui-même ce qui lui manque**, que ce soit la façon de convertir un type A en type B, une méthode en elle-même ou l’un de ses paramètres.

Concentrons-nous ici sur l’utilisation des paramètres et des conversions implicites.

### Implicits Parameters

Premier type d’implicite et surement le plus courant, le paramètre _implicit_. Prenons l’exemple d’**une API permettant de** **sérialiser****/désérialiser du texte**, que ce soit en JSON, XML, ou autre. On pourrait imaginer un objet fournissant deux méthodes _ser_ / _deser_ telles que :

	def deser(value:String, format: Format): Entity = ???
	def ser(value:Entity, format: Format): String = ???

La méthode  _deser_ pourrait s’appeler de différentes façons, parmi lesquelles :

	//import des méthodes et des objets

	deser("mon JSON", JSONFormat)
	deser("modn XML", XMLFormat)

À l’utilisation, on peut très vite remarquer une chose : si l’on souhaite utiliser cette API pour traiter du JSON, il est très peu probable de devoir à un moment passer à ces méthodes autre chose qu’un JSONFormat.

La question que pose Scala est la suivante : pourquoi devrait-on le passer à chaque fois ? Scala permet donc de réécrire l’API présentée ci-dessus de la façon suivante :

	def deser(value:String)(implicit format:Format): Entity = ???
	def ser(value:Entity)(implicit format:Format): String = ???

De prime abord, il n’y a pas de grandes différences, si ce n’est une plus grande complexité du code. Néanmoins,  **déclarer les méthodes de l’API de cette manière nous permet la flexibilité**  suivante :

	//on déclare le format qui sera utilisé
	implicit val format = JSONFormat

	deser("mon JSON")
	deser("mon autre JSON")

	deser("mon XML")(XMLFormat) // ici on souhaite passer explicitement le format

Note : Dans la signature d’une méthode, **le mot clé implicit ne sert pas à déclarer un paramètre mais une liste de paramètres.** Tous les paramètres suivant le mot clé seront donc des implicites :

	implicit val n = "John"
	implicit val a = 20

	def greet(greeting:String)(implicit name:String, age:Int) = {
	  print(s"$greeting $name, you are $age")
	}

	greet("Hello") //compile et renvoie Hello John, you are 20

### Implicits Conversions 

Second type d’implicite,  **la conversion implicite**. Reprenons le cas de notre API de sérialisation/désérialisation. Nous avons vu qu’il était désormais possible de faire cela :

	deser("mon XML")(XMLFormat)

Imaginons désormais que, rebelle dans l’âme, vous ayez envie de passer “JSON” ou “XML” au lieu de l’objet Format correspondant. Il serait alors très simple de faire ceci :

	def getFormat(stringFormat:String): Format = {
	  stringFormat match {
	    case "XML" => XMLFormat
	    case "JSON" | _ => JSONFormat
	  }
	}

	deser("mon text")(getFormat(stringFormat))

Néanmoins, il existe une autre solution prévue par Scala, vous permettant, encore une fois, d’omettre l’appel à la méthode  _getFormat_ :

	implicit def StringToFormat(stringFormat: String): Format = {
	  stringFormat match {
	    case "XML" => XMLFormat
	    case "JSON" | _ => JSONFormat
	  }
	}

	deser("mon text")(stringFormat)

Ici, nous avons tout simplement déclaré une méthode implicite (_implicit def_) permettant de convertir une  _String_ en  _Format_, ce qui nous permet d’**appeler la méthode** **_deser_** **directement avec le format sous forme de** **_String_****.** Attention, même si le code suivant semble logique, il ne compilera pas :

	class A(val n: Int)
	class B(val m: Int, val n: Int)
	class C(val m: Int, val n: Int, val o: Int) {
	  def total = m + n + o
	}

	implicit def AtoB(a: A) = new B(a.n, a.n)
	implicit def BtoC(b: B) = new C(b.n, b.m, b.n+b.m)

	new A(0).total //On aimerait pouvoir faire cela mais ça ne compile pas

En effet, il faudrait que le compilateur puisse enchaîner deux conversions implicites, ce qu’il n’est pas capable de faire :

	BtoC(AtoB(new A(0))).total //ce qu’on voudrait que le compilateur fasse

Afin de résoudre ce problème, il faut changer la méthode  _BtoC()_  comme suit :

	implicit def BtoC[T](b: T)(implicit conv: T => B) = new C(b.n, b.m, b.n+b.m)

Ici nous avons rajouté un paramètre implicite dont la résolution n’est pas limitée au niveau de sa recherche. Le paramètre  _conv_ sera donc cherché jusqu’à ce qu’il soit trouvé.

Le compilateur trouvera donc  _AtoB()_ et s’en servira en tant que paramètre implicite, limitant le nombre de conversions nécessaires à une :

	BtoC(new A(0))(x => AtoB(x)).total //une seule conversion

### Implicits Class

Dernier type d’implicite, les classes implicites.
L’exemple précédent montrait comment convertir implicitement une _String_ en _Format_ en passant par une conversion. Mais pourquoi n’avoir pas tout simplement appelé la méthode _toFormat_ de la classe String ?

	val format = "JSON".toFormat //le tour est joué, ou presque.

Le seul problème, c’est que cette méthode n’existe pas. No problem, Scala est là. Il est tout à fait possible d’enrichir la classe String d’une telle méthode, comme ceci :

	implicit class FormatString(val string:String){
	  def toFormat = {
	    string match {
	      case "XML" => XMLFormat
	      case "JSON" | _ => JSONFormat
	    }
	  }
	}

	"JSON".toFormat //compile ! (et fonctionne)

### Ambiguïté 

Dans les exemples donnés jusqu’à présent, je n’ai défini qu’un seul paramètre implicite pour type d’argument attendu. Mais que se passerait-il si l’on en définissait un second ? Prenons un exemple simple :

	def sayHello(implicit name: String) = print(s"Hello $name")

	implicit val name1 = "John"
	implicit val name2 = "Beth"

	sayHello

Ici,  _sayHello_  nécessite un implicite de type  _String,_ mais comment choisir entre  _name1_  et  _name2_  ? Scala ne permet pas de résoudre cette ambiguïté. La compilation aurait donc échoué avec le message : “_Error: ambiguous implicit values_”. En effet, entre un  _String_  et un autre  _String_, rien ne permet de les différencier.

C’est la raison pour laquelle  **il est rarement pertinent d’utiliser des types primitifs en tant qu’implicite**  compte tenu des potentiels conflits avec les autres implicits présents dans le scope. Dans le cas particulier où le contexte nécessite vraiment un tel implicite, il faut privilégier une classe « _wrapper_ » englobant la valeur cible, comme par exemple :

	case class Name(name:String) extends AnyVal
	def sayHello(implicit name: Name) = print(s"Hello ${name.name}")

## TypeClasses

Le polymorphisme, qui permet à une opération d'être appelée sur des types différents, est l'une des bases de la programmation orientée objet. Et pourtant elle ne lui est pas exclusivement réservée. C'est en effet un terme que nous retrouvons en programmation fonctionnelle à travers la notion de typeclasse.

Prenons un exemple : j'ai des chiens et des dinosaures représentés par les types Dog et Dinosaur. Je devrais pouvoir utiliser la même opération `walk` pour voir les chiens et les dinosaures marcher, pour laquelle seul va changer le comportement associé.

En programmation plus ou moins classique, on partirait sur une approche utilisant la réflexion pour connaître le type de l'instance et une structure conditionnelle pour y associer un traitement spécifique.

```lang-java
public String walk(Object animal) {
  if (animal.isInstanceOf[Dog])
    return "Tap tap tap tap tap tap tap tap";
  else if (animal.isInstanceOf[Dinosaur])
    return "Boom boom acka-lacka lacka boom";
  else
    throw new Exception("your animal doesn't walk. HAhaHAha!");
}
```

Le problème de cette approche est que le développeur doit penser au cas d'erreur. En cas d'oubli, seule l'exécution du programme avec la bonne donnée en entrée permet de rappeler que la gestion du cas exceptionnel est manquant. C'est une situation qui peut apparaître OÙ ELLE VEUT ET C'EST SOUVENT DANS LA PROD...

Il y a d'autres aspects qui sont relativement problématiques ici. Par exemple, ajouter un animal comme le canard, mais oublier de l'ajouter dans `walk`. Ce qui va se traduire par une exception. De plus, si ce code vient d'une bibliothèque tierce scellée (ie. les modifications dans cette bibliothèque sont... c'est très compliqué), ajouter le comportement pour le canard... Et bien... on peut pas 😕. Par contre, si je veux ajouter une opération `dance` pour mes animaux, ce ne sera pas un soucis.

La programmation orientée objet apporte une solution à ce problème en ne permettant pas au développeur de faire n'importe quoi, sauf lorsqu'il l'a clairement exprimé.

```lang-scala
trait Animal { /* ... */ }

trait Walking {
  def walk: String
}

case class Dog(name: String) extends Animal with Walking {
  /* ... */
  def walk: String = "Tap tap tap tap tap tap tap tap"
}
case class Dinosaur(name: String) extends Animal with Walking {
  /* ... */
  def walk: String = "Boom boom acka-lacka lacka boom"
}

def walk(walking: Walking): String = walking.walk
```

Si je veux ajouter le canard, je dois alors étendre Animal et Walking. Ce qui m'oblige à définir la méthode `walk` pour cet animal. Après quoi, je peux appeler la fonction `walk(Walking)` pour les canards.

L'approche orientée objet permet clairement de s'éviter des problèmes à la source et d'avoir moins de cas d'erreur à gérer. On gagne en flexibilité sur la mise en place de nouveaux comportements associés à une opération. Mais en contrepartie, on perd en flexibilité sur l'ajout de nouvelles opérations : si le code ci-dessus fait partie d'une bibliothèque scellée, je ne pourrais pas ajouter un comportement `dance`, sauf à le faire en partant sur la solution précédente ou en utilisant d'autres approches basées sur la réflexion. Autre point : Dog et Dinosaur est toujours associé au mixin Walking quelque soit le contexte. On peut imaginer des contextes dans lequel "Dinosaur étend Walking" n'a aucune utilité, par exemple lorsque le dinosaure dort (somnambule), ou est perturbant, par exemple lorsqu'il est décédé (walking dead).

Les typeclasses offrent une réponse à ces cas problématiques.

**Les typeclasses permettent de catégoriser des types existant dans des contextes bien précis et d'associer à ces types des opérations communes.**

Nous déclarons d'abord quelques classes sans les lier directement à une classe ou une interface quelconque.

```lang-scala
case class Dog(name: String)
case class Dinosaur(name: String)
```

Nous déclarons ensuite avec un `trait` un type paramétré qui va permettre de catégoriser n'importe quel type A, sur lequel nous pourrons appeler la méthode `walk`. C'est notre **typeclasse** !

```lang-scala
trait Walking[A] {
  def walk(a: A): String
}
```

Définissons une fonction `walk`.

```lang-scala
def walk[A: Walking](a: A): String = implicitly[Walking[A]].walk(a)
```

Cette fonction se base en entrée sur n'importe quel type A. Néanmoins, dans la signature, nous indiquons que ce type est contraint par le fait qu'il doit être de catégorie Walking (`A: Walking`). Dans le corps de la fonction, nous indiquons que nous recherchons une instance de Walking déclarée implicitement pour le type A donnée (`implicitly[Walking[A]]`). Une fois récupérée, nous pourrons appeler la méthode  `walk`  dessus.

Nous déclarons à présent des instances implicites de Walking pour Dog et Dinosaur avec un comportement spécifique. Ce sont les  **instances**  de la typeclasse.

```lang-scala
implicit val dogWalking =
  new Walking[Dog] {
    def walk(d: Dog): String = "Tap tap tap tap tap tap tap tap"
  }
 
implicit val dinosaurWalking =
  new Walking[Dinosaur] {
    def walk(d: Dinosaur): String = "Boom boom acka-lacka lacka boom"
  }
```

Et donc, à l'utilisation ça donne :

```lang-scala
walk(Dog("Rambo"))          // Tap tap tap tap tap tap tap tap
walk(Dinosaur("P'tit Rex")) // Boom boom acka-lacka lacka boom
```

Si la déclaration d'une instance implicite est manquante, nous avons alors une erreur de compilation.

```lang-scala
case class Duck(name: String)

walk(Duck("Donald")) // KO 🙀 - compilation error
// could not find implicit value for evidence parameter of type Walking[Duck]
```

## PartialFunctions

### Premier exemple et utilisation simple

Vous souhaitez définir une fonction qui ne fonctionne que pour un sous-ensemble de valeurs d'entrée possibles, ou vous souhaitez définir une série de fonctions qui ne fonctionnent que pour un sous-ensemble de valeurs d'entrée, et de combiner ces fonctions pour résoudre définitivement un problème.

Une fonction partielle est une fonction qui ne fournit pas de réponse pour toutes les valeurs d'entrée possibles qu'elle peut être donnée. Elle fournit une réponse uniquement pour un sous-ensemble de données possibles et définit les données qu'il peut gérer. En Scala, une fonction partielle peut également être interrogé pour déterminer si elle peut gérer une valeur particulière.

Un exemple simple est la fonction division.

	val divide = (x: Int) => 42 / x

L'appel de divide(0) provoque évidement une exception arithmétique.

On peut contourner ce problème en utilisant une fonction partielle.

	val divide = new PartialFunction[Int, Int] {
	    def apply(x: Int) = 42 / x
	    def isDefinedAt(x: Int) = x != 0
	}

On pourra donc l'utiliser comme ceci : 

	scala> divide.isDefinedAt(1)
	res0: Boolean = true

	scala> if (divide.isDefinedAt(1)) divide(1)
	res1: AnyVal = 42

	scala> divide.isDefinedAt(0)
	res2: Boolean = false

### Chainer des fonctions partielles

Outre pallier une exception, les fonctions partielles proposent un deuxième avantage, qui est le chainage.

Imaginons une fonction partielle traitant seulement les nombres pairs, et une autre traitant les nombres impaires. En Scala, on peut enchaîner ces deux méthodes.

Exemple :

	// converts 1 to "one", etc., up to 5
	val convert1to5 = new PartialFunction[Int, String] {
	    val nums = Array("one", "two", "three", "four", "five")
	    def apply(i: Int) = nums(i-1)
	    def isDefinedAt(i: Int) = i > 0 && i < 6
	}

	// converts 6 to "six", etc., up to 10
	val convert6to10 = new PartialFunction[Int, String] {
	    val nums = Array("six", "seven", "eight", "nine", "ten")
	    def apply(i: Int) = nums(i-6)
	    def isDefinedAt(i: Int) = i > 5 && i < 11
	}

Dans cet	example, la première fonction converti les nombre allant de 1 à 5 en string. La deuxième fait de même pour les nombre de 6 à 10.

On peut créer une troisième fonction partielle, qui traitera ces deux cas séparément. Pour cela, on utilisera le mot clé ***orElse***.

	val handle1to10 = convert1to5 orElse convert6to10

	scala> handle1to10(3)
	res0: String = three

	scala> handle1to10(8)
	res1: String = eight

Un deuxième opérateur important concernant les fonctions partielles est **collect**.

Observons le cas de l'application de la fonction divide sur une liste.

	val divide: PartialFunction[Int, Int] = {
	    case d: Int if d != 0 => 42 / d
	}
	
	List(0,1,2) map { divide }

Notre liste contient la valeur interdite 0. Cela provoquera une erreur de type MachError.

Scala propose donc un opérateur permettant de réaliser le même procéder que la fonction map, mais en vérifiant si une valeur est applicable, et en l'ignorant si ce n'est pas le cas. Cet opérateur est donc **collect**.

	scala> List(0,1,2) collect { divide }
	res0: List[Int] = List(42, 21)

	scala> List(42, "cat") collect { case i: Int => i + 1 }
	res0: List[Int] = List(43)

## Fonctions - Vocabulaire

Fonction totale : Toutes les valeurs sont transformées en sortie.
Fonction partielle : Le contraire d'une fonction totale.
Fonction pure : Une fonction dont le résultat provient uniquement de ces paramètres, le state n'est pas modifié et n'interagit pas avec une IO.
Fonction impure : Le contraire d'une fonction pure.

## Les Futures

"Un futur représente une valeur qui peut ou non être actuellement disponible, mais qui le sera à un moment donné, ou une exception si cette valeur n'a pas pu être mise à disposition.”

Les futures permettent d'executer plusieurs fonctions en parallèle. Le résultat est asynchrone. De plus les futures sont non bloquant.

Les futures sont des enfants de la classe générique **Awaitable**.

	trait  Future[+T] extends  Awaitable[T]

Lorsque la computation est complété on peut donc récupérer la valeur grâce à ```Future.successful(value)``` ou catcher l'exception ```Future.failed(exception)```.

	// Completed: successful
	Future.successful(1)

	// Completed: failed
	Future.failed(new Exception("failed!!!"))

	// To be computed
	val f: Future[List[User]] = Future {
		db.getUsers()
	}

Comme les valeurs des futures ne sont pas toujours accessibles à tout moment, nous devons utiliser des méthodes dites **callbacks**.

###  Méthode onComplete

Applique une fonction passée en paramètre lorsque la future est complété.

Exemple :

	val future: Future[Int] = ???

	future.onComplete({
		case Success(value) => println(value) // or another side-effect	
		case Failure(exception) => println(exception.getMessage)
	})

Ici on affiche tout simplement soit le nombre soit le message d'erreur.

### Méthode foreach

Applique une fonction passée en paramètre, seulement en cas de succès de la future.

Exemple :

	val future: Future[Int] = ???

	future.foreach(value => println(value))

	// or just
	future.foreach(println)

### Méthode andThen

Applique une fonction partielle passée en paramètre lors de la complétion de la future et renvoi la future.

	val future: Future[Int] = Future.successful(1)

	future
	.andThen{ case Success(x) if x >= 0) => println(x)}
	.andThen{ case Success(x) if x < 0 => println(s"-$x")}

On peut également utiliser des méthodes de transformation sur les futures.

### Map

Créer une nouvelle future en appliquant une fonction sur la partie success d'une future.

Exemple :

	val f: Future[Int] = Future { 1 } // value: Future(Success(1))

	f
	.map(_ + 1)
	.onComplete {
		case Success(value) => println(value) // prints 2
		case Failure(error) => println(error)
	}

### FlatMap

Créer une nouvelle future en appliquant une fonction sur la partie success de cette future, et retourne le résultat de la fonction de cette future.

Exemple :

	val nbUsersA: Future[Int] = Future.successful(1)
	val nbUsersB: Future[Int] = Future.successful(2)

	// Future[Future[Int]] :(
	val result1: Future[Future[Int]] = nbUsersA.map { nbA =>
		val temp: Future[Int] = nbUsersB.map (nbB => nbA + nbB)
		temp
	}

	// Future[Int] :)
	val result2: Future[Int] = nbUsersA.flatMap { nbA =>
		val temp: Future[Int] = nbUsersB.map(nbB => nbA + nbB)
		temp
	}

### Recover

Créer une nouvelle future qui catch les exceptions que pourrait contenir une future.

Exemple :

	Future(6 / 0).recover { case e: ArithmeticException => 0 }

### For comprehension

Les futures supporte le **for-compréhension**.

Exemple :

	val f1 = Future.successful(1)
	val f2 = Future.successful(2)
	val f3 = Future.successful(3)

	for {
		v1 <- f1
		v4 = 4
		v2 <- f2
		v3 <- f3
	} yield v1 + v2 + v3 + v4

Équivalent à :

	val f1 = Future.successful(1)
	val f2 = Future.successful(2)
	val f3 = Future.successful(3)

	// Future[Int]
	f1.flatMap(v1 =>
		val v4 = 4
		f2.flatMap(v2 =>
			f3.map(v3 =>
				v1 + v2 + v3 + v4
			)
		)
	)

### Sequence 

Permet de créer une future d'une liste.

	val f: Future[List[Int]] = Future.sequence(
		List(f1, f2, f3)
	)

### Bloquer une future

	val f: Future[Int] = ...

	Await.result(f, 10.seconds) // blocks for 10s maximum


