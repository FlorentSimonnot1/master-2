﻿# Cloud computing - spark 2

Previously, we developed the term of RDD which is a partitioned and distributed collection of data in read only mode. RDD are distributed through nodes of a cluster.

RDD was the first abstraction provided by Spark.

Since the release 1.3, Spark provides another collection which is the DataFrame.

## DataFrame APIs

### Introduction

Dataframe is an **immutable collection**. 

Like RDD, this is a **distributed collection** of data, but they are organized into **named column**. It is conceptually equal to a **table in a relational database**.

Dataframe using a **relational approach** : DSL or SQL.

Evaluates operations **lazily** to perform some optimizations.

Collections of structures records that can be manipulated with Spark's procedural API or relational APIs for richer optimizations.

Stores data in columnar format that is significantly more compact than Java/Python objects.

Each DataFrame can be viewed as a RDD of Row objects.

Relational operations can be performed using a DSL similar to R data frames and Python Pandas : select, where, join, groupBy.

DataFrame is the main abstraction in Spark SQL's API.
It keeps track of their schema that leads more optimized execution.

### Create DataFrame

genres.txt file : 

	1 Action
	2 Adventure
	3 Animation
	4 Children's
	5 Comedy
	6 Crime
	...
	18 Western

code :

	val genres = sc.textFile("genres.txt)
	.map(_.split(" " ))
	.map(x => (x(0),x(1)) )
	.toDF("id", "genre")

	genres : org.apache.spark.sql.DataFrame = [id: string, genre: string]

___

genres.csv

	id,name
	1,Action
	2,Adventure
	3,Animation
	4,Children's
	5,Comedy
	6,Crime
	...
	18,Western

code : 

	val genres = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load("genres.csv")
	
	genres : org.apache.spark.sql.DataFrame = [id: string, genre: string]

### Data sources

**Reading**

We can read data from DataFrameReader.

	DataFrameReader
	.format(args)
	.option("key", "value")
	.schema(args)
	.load(file)

Format : 

- csv
- txt
- json
- parquet (default value)

Option : 

- "header" : "true"/"false"
- "inferSchema" : "true"/"false"
- "mode" : "PERMISSIVE"/"FAILFAST"/"DROPMALFORMED"

Schema : 

- 'id INT, name STRING'
- StructType(..)

**Writing**

We can save and write data thanks to DataFrameWriter.

Format, same option as reader.

Option : 

- mode : "append"/"overwrite"/"ignore"/"error"
- bucketBy(): (numBuckets, col, col...)

**Schemas**

It's more efficient to provide a schema than to let Spark infer it.

Create a schema :

	val schema = StructType(Array(StructField("num", IntegerType, false), StructField("label", StringType, false)))

	val g = spark.read.option("header", "true").schema(schema).csv("genres.csv")

	g.printSchema

	root 
	| -- num: integer (nullable = true)
	| -- label: string (nullable = true)


	val schema2 = "ide INT, lab STRING"
	schema2: String = ide INT, lab STRING

	val g2 = spark.read.option("header", "true").schema(schema2).csv("genres.csv")
	g2: org.apache.spark.sql.DataFrame = [ide: int, lab: string]

	g2.printSchema

	root 
	| -- ide: integer (nullable = true)
	| -- label: string (nullable = true)
	
**Display**

Some methods exist to display a dataFrame.

	genres.show()
	genres.take(10)
	genres.collect()

### DataFrame operations

DataFrame supports : 

- All common relational operators : select, where, join, groupBy, etc...
- Comparison and arithmetic operators : ===, >, <, +, -, etc...

All mentioned operators build an Abstract Syntax Tree.

The DataFrame registered in the catalog are still unmaterialized views, so some optimizations are possible.

### DSL Vs SQL

We can manipulate dataFrames thanks to SQL or DSL.

**With DSL**

	genres.where("id=15").select("name").show
	genres.where($"name".like("A%")).select("name").show

	df.select(expr(“col1”)).show(2)
	df.select(col(“col1”)).show(2) 
	df.select(“col1”).show(2)

	df.sort(col(“id”).desc) 
	df.sort($”id”.desc)

**With SQL**

	val res = spark.sql("SELECT * FROM Genre")
	res: org.apache.spark.sql.DataFrame = [id: int, name: string]

	res.count 
	res28: Long = 18

## DataSets API

DataSets are an extension of DataFrame API that provides a type safe, object-oriented programming interface.

A DataSet is a strongly-typed, immutable collection of objects that are mapped to a relational schema.

It represents data in in the form of JVM objects of row or a collection of row object. Which is represented in tabular forms through encoders.

Example : 

	val lines = spark.read.text("/wikipedia").as[String] 
	val words = lines.flatMap(_.split(" ")).filter(_ != "")

Read a file :

	val data = sc.textFile(" nom de fichier "). map( ligne => objet).toDS()

## When use what

### RDD

You can use RDDs when you want low-level transformation and actions on your data set.
Use RDDs when you need high-level abstraction.

### DataFrame & DataSet

Use DF or DS for unstructured data, such as media streams.
Use when you need domain specific APIs.
Use when you want to manipulate your data with functional programming constructs than domain specific expression.
We can use either DS or DF in high-level expression (e.g. filter, map, sum, SQL).

## Conclusion

![Comparison](./img/Comparison.png)
