﻿# Cloud Computing - Distributed Graph Processing

## Introduction 

Graphs provide a flexible abstraction for describing relationships between discrete objects.

### Definitions 

**Graph**

> A graph G = (V, E) is an ordered pair of finite sets. Elements of V
> are called vertices or nodes, and elements of E ⊆ VxV, are called
> edges or arcs. We refer to V and E as respectively the vertice (edge)
> set of G. The cardinality of V is called the order of G, and |E| is
> called the size of G. We usually disregard any direction of the edges
> and consider (u, v) and (v, u) as one and the same edge in G. In that
> case, G is referred to as an undirected graph.

**Property graph**

> Property graph: A property graph G( P ) = (V, E, P) extends a graph G =
> (V, E) with P a finite set of properties (attributes). P = (PV , PE )
> with PV the set of vertex properties and PE the set of edge
> properties.

**RDF graph**

> RDF graph: A directed labelled graph where nodes are either URIs,
> blank nodes or literals (only for leaves) and edges are URIs.

![RDF-graph](./img/rdf-graph.svg)

Generally queried with SPARQL (a W3C recommendations).
Systems : Virtuoso, GraphDB, MarkLogic, AllegroGraph, Oracle, BlazeGraph.
Important features : ability to support inferences using ontology languages such as RDFS, OWL.

## Why graph ?

Many problems can be modeled by graphs and solved with appropriate graph algorithms :
- SSSP
- PageRank

### SSSP

> Single source shortest paths (SSSP): Given a source vertex, the
> algorithm computes the shortest paths from this source to all other
> nodes in the graph.

### PageRank

> The PageRank algorithm operates in iterations, where pages distribute
> their scores to their neighbors (pages they have links to) and
> subsequently update their scores based on the partial values they
> receive. Used by search engines.

### Other problems

> A graph is said to be connected if for every pair of distinct vertices
> u, v there is a u-v path joining them. Connected components is the
> number of connected subgraphs in a graph.
> 
> The basic triangle enumeration algorithm groups all edges that share a
> common vertex and builds triads, i.e., triples of vertices that are
> connected by two edges. Used for span discovery, recommendation.

## Complex, Large and Parallel graphs

Some dedicated librairies exist to manage graph: BGL, LEDA, Stanford GraphBase, FGL, JDSL. We call theme **Graph DBMS**.

But they are not adapted to large graphs (billions of nodes and hundred billions of edges) because they run on a single machine.

Parallel graph systems like Parallel BGL and CGMGraph have no fault tolerance strategies.

### MapReduce and Spark

Large graphs (not fitting a single machine or at a high cost) require large-scale processing.

Can we use parallel computing engines such as MapReduce or Spark for these problems ?

**No**, due to the data-driven computation of graph and the unstructuredness of graphs.

MR and Spark adopt a record-centric view of the data and ensure parallelism by processing independent data on different machines.


**Data-driven computation** 

> Structure of computations is motivated by the topology of the graphs
> (nodes and edges) so the structure is not known resulting in
> difficulties to parallelize

**Unstructuredness**

> Irregular structure of graphs: hard to parallelize based on a certain
> partitioning of the data, hard to provide an efficient load balancing.

Graph algorithm are generally based on the exploration of graph structures: the notion of neighborhood is important for computations.

Loose notion of locality on computations and data access.

Many problems can be addressed with a vertex-centric approach (aka think like a vertex): expressing computation from the perspective of a vertex in the graph (see BSP).

Starting in 2009-10, several graph processing engines emerged (Pregel, GraphLab, PowerGraph, GraphX, Gelly). They generally : 

- restrict the types of computations,
- propose new techniques for graph partitioning,
- present better performances than data-parallel systems,
- adopt a vertex-centric view of graphs and ensure parallelism by (1) partitioning the graph and (2) performing iterative computation and communication along edges.

## Systems

### Pregel

- Implemented at Google to address PageRank, Shortests paths, bipartite matching (provide a subset of edges of a bipartite graph with no common endpoints) algorithms.
- Published at Sigmod 2010.
- Based on the Bulk Synchronous Parallel (BSP) model with message passing.
- Master-Workers approach. Graph partitioning via hashing.
- Fault tolerance through checkpointing.

**Bulk Synchronous Parallel (BSP) model**

- An abstract computer to design parallel algorithms
- A BSP computer: a set of connected processors. Each processor has local memory and may follow different threads of computation.
- A BSP computation proceeds in a series of global supersteps.

A superstep has 3 components :

- Concurrent computation on every participating processor. Each process uses values stored in its local memory. Computations execute asynchronously of each others.
- Communication: processes exchange data via 1-sided put and get calls rather than 2-sided send and receive calls.
- Barrier synchronization: A point when a process waits for all other processes to finish their communication actions. It concludes a superstep.

Computation and communication actions are necessarily timely ordered.

Processes are randomly assigned to processors.
The problem to solve is splitted into more logical processes than there is physical processors.
One-sided communication prevents from deadlocks (no circular dependencies), permits fault tolerance.
Apache Hama is pure BSP computing framework on top of HDFS.

BSP in Pregel (Giraph) : 

- programs are expressed as a sequence of iterations
- In each iteration, a vertex can, independently of other vertices, receive messages sent to it in the previous iteration, send messages to other vertices, modify its own and its outgoing edges’ states, and mutate the graph’s topology

Synchronicity of supersteps prevents deadlocks and data races but problem with stragglers remain.

Assignment of vertices to workers is important for good performance

Assigning several partitions per worker permits parallelism among partitions and better load balancing).

Pregel supports commutative and associative message combiners (e.g., combine the message for a given node).

### Distributed Graphab

First implemented for Machine Learning purposes, now broader range of applications.

Adopts BSP but with a shared memory asynchronous approach

### PowerGraph

Some graph follow a power-law degree distribution (few nodes are highly connected and many nodes are weakly connected)

It is important to consider the graph topology when distributing nodes over the cluster.

Highly connected nodes may span several cluster nodes.

Power-law degree distribution : 

- **Work balance** : Power-Law graphs do not have low-cost balanced cuts
- **Partitioning** : Traditional graph-partitioning algorithms perform poorly on Power-Law Graphs.
- **Communication** : communication asymmetry and hence bottleneck
- **Storage** : high-degree vertices may exceed capacity of a machine
- **Computation** : most systems do not parallelize within individual vertex-programs

Necessary to split high degree vertices.

Two ways to partition a graph.

![Graph-Partition](./img/powerGraphPart.png)

**Edge cut** :  Uniquely assigns vertices to machines while allowing edges to span across machines. The communication and storage overhead of an edge-cut is directly proportional to the number of edges that are cut. Therefore we can reduce communication overhead and ensure balanced computation by minimizing both the number of cut edges as well as the number of vertices assigned to the most loaded machine.

**Vertex cut** : Evenly assign edges to machines and allow vertices to span multiple machines. The communication and storage overhead of a vertex-cut is directly proportional to the sum of the number of machines spanned by each vertex. Therefore, we can reduce communication overhead and ensure balanced computation by evenly assigning edges to machines in way that minimizes the number of machines spanned by each vertex.

### GraphX

A component of Apache Spark.

Based on the property graph model.

Extends the Spark RDD by introducing a new Graph abstraction: a directed multigraph with properties attached to each vertex and edge.

Addresses property graphs with 2 RDDs: edge collection (EdgeRDD) and Vertex collection (VertexRDD).

Both RDDs extend the standard RDD class, so RDD operators are inherited.

![GraphX](./img/graphX.png)

GraphX optimizes the representation of vertex (VD) and Edge (ED) types when they corresponds to ’primitives’ data types (e.g., int, double, etc.) reducing the main memory footprint by storing them in specialized arrays.

Property graphs in GraphX have the me main properties of RDDs: immutable, fault-tolerant and distributed.

![GraphX2](./img/graphX2.png)

- GraphX is based on BSP 
- Relies on several optimization techniques to address stragglers. 
- Adopts vertex cut. 
- One system for many operations with no data duplication, fast learning curve.

GraphX includes a set of graph algorithms to simplify analytics tasks.

 - PageRank
 - Connected components
 - Triangle counting

**PageRank** 

PageRank measures the importance of each vertex in a graph, assuming an edge from _u_ to _v_ represents an endorsement of _v_’s importance by _u_. For example, if a Twitter user is followed by many others, the user will be ranked highly.

**Connected Components**

The connected components algorithm labels each connected component of the graph with the ID of its lowest-numbered vertex. For example, in a social network, connected components can approximate clusters.

**Triangle  Counting**

A vertex is part of a triangle when it has two adjacent vertices with an edge between them. GraphX implements a triangle counting algorithm that determines the number of triangles passing through each vertex, providing a measure of clustering.

GraphFrames provide a declarative API similar to “data frames” in R, Python and Spark to perform graph analytics.

It is implemented on top of Spark SQL.

It optimizes the entire computation using graph-aware join optimization and view selection algorithm.

A GraphFrame consists of a vertices, edges and triplets dataFrames (like GraphX) + a pattern (to match shapes of subgraphs).

Patterns are expressed in a language similar to Cypher.

Main challenge of GraphFrame is query planning.

Improvements to Spark SQL’s Catalyst optimizer have been integrated.

The query planer is implemented as a layer on top of Catalyst: taking patterns as input, collecting statistics using the Catalyst APIs, and emitting a Catalyst logical plan.

Materialized views are used to optimize query processing. They are manually created by the user.

### Flink - Gelly

In Gelly, a graph is represented by a DataSet of vertices and a DataSet of edges.

A vertex is defined by its unique ID and a value, whereas an edge is defined by its source ID, target ID, and value.

Transformations on Vertices: mapVertices, joinWithVertices, filterOnVertices, addVertex, . . . , Edges: mapEdges, filterOnEdges, removeEdge, . . . , Triplets (source vertex, target vertex, edge): getTriplets
