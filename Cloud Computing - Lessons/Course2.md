﻿# Cloud Computing - MapReduce

## Introduction 

In more and more situations, data is being so big that it can not be processed on a single machine.

For example, Youtube uploads 400j of video per minute. Twitter receive over 420k tweets per minute, 3.3 million posts for Facebook per minute etc...

Facebook has over 100PB of photos, 250 billion photos, 350 million new ones uploaded every day.

To process big data, **parallelisation** is needed.

MapReduce is a framework (sometimes defined as Design Pattern) created by Google which provides a programming model to support parallel calculations.

An open-source MapReduce framework is Apache Hadoop.

## Google infrastructure

### Google File System

This is a distributed system. This system works on clusters.

It offers some component failures : constant monitoring, error detection, fault tolerance, automatic recovery is integral part of the system.

The system is optimized for large files. 

Write access append data at the end of the file. Read sequential files.

The architecture contains 1 master and multiple chunk servers.

![GFS](./img/GFS.png)

Large files are partitioned in chunks (standard linux file) of 64MB by the chunck servers.

Files must be stored redundantly. A file is replicated on several machines as you can see on the schema.

Master and chunk servers are both nodes of the system.

Each cluster has one master but there is a copy which is be able to replace him if an error occurred.

The master keep all metadata (stored in memory) and verify the integrity of the data, gives the authorization to write on a file, manages load balancing, makes sure that blocks are copied sufficiently (3 copies in general).

Interactions with the master are minimized in order to not strangle the system. 

Most nodes are chunck servers.

**Reading Data**

Client asks the master for addresses of machines storing the searched chunks.

If no read operation is active of this chunck, the master provides this list.

Client then communicates directly with a chunck server.

**Writing Data**

A chunck server storing the data becomes a temporary primary chunck server.

The master provides to the client the primary and secondary chunck servers.

Client sends the data to write to the primary chunck server ad the list of secondaries.

Primary chunck server sends the data to the closet secondary which does the same thing.

Thus all copies receive the data for appending the chunck.

### Coordination System

**Chubby**

Chubby is a highly available and persistent distributed lock service.
It used by google as name server, supplanting DNS.

It keeps its replicas consistent thanks to Paxos algorithm. Elects a machine to play the role of a master on failure.

The master grants clients read/write locks to files. 
Limited to coarse-grained locks.

**Apache Zookeeper**

Hadoop's coordination system. Takes the form of a library.

Used in HBase, Kafka, Storm etc...

Strenghts: Strong consistency, ordering, durability guarantees, typical synchronization primitives, dealing with concurrency aspects

Generally used for: Naming service, Configuration management (HBase), Synchronization, Leader election (HBase), Message Queue (Kafka), Notification system (Kafka to detect crashes, HBase to keep track of available servers).

Zookeeper doesn't elect a master or track live processes out of the box.

It provides the tools for implementing such tasks.

### Work queue

A cluster have a scheduler. This scheduler is called work queue.

Work queue creates a large-scale time sharing system out of an array of computers and their disks.

It schedules job, allocate resources, reports status and collect results.

Tasks run on the same machines as GFS.

Work queue can reschedule jobs on failure.

### Big Table

Big Table is a distributed storage system for managing structured data.

It provides clients with a simple data model : a sparse, distributed, persistent multidimensional sorted map.

The map is indexed by a row, a column and a timestamp.

Bigtable uses GFS to store log and data files.

Bigtable relies on Chubby to elect a master, to allow the master to discover the servers it controls, and to permit clients to find the master.

Bigtable only supports single-row transactions.

**Data model**

A data model is a tablet, a set of rows. This is the unit of distribution and load balancing.

Column keys are grouped into sets called column families (basic unit of access control).

Versioning of cell data with timestamps.

**Building blocks**

Bigtable data is stored using the SSTable file format.

A SSTable provides a persistent ordered immutable map from keys to values.

A SSTable contains sequence of blocks and block index.

**Components**

A client library. Clients rarely communicate with the master.

A master which assigns tablets to tablet servers, detects addition and removal of tablet servers, balances tablets load on tablet servers and handless schema changes.

Tablet servers handle read and write requests.

**Tablet location & assignment**

Like a B-Tree.

BigTable uses Chubby to keep track of tablet servers. When a tablet server starts, it creates and acquires an exclusive lock.

Master keeps track of the set of live tablet servers, including which tablets are unassigned.

Tablet server creates and acquires exclusive lock on uniquely named file in specific chubby directory.

Master monitors this servers directory to discover tablet servers.

A tablet server stops serving if it loses its exclusive lock.

A tablet tries to reacquire lock as long as its file exists. If it doesn't then kills itself.

Master periodically asks each tablet server for the status of its lock.

## Map Reduce

### Origin

Map reduce is a programming model.
This is an implementation for processing and generating large datasets.

Programs are automatically parallelized and executed on a cluster of machines.

It allow to partitioning data, scheduling execution, handling machines failures and managing inter-machine communication.

Map Reduce was born from functional programming language and especially LISP.

The name come from 2 essential functional concepts : 

- map : application of a function over a list of elements
- reduce : reduces a list to a scalar value

So it only takes to write 2 functions: a map and a reduce.

Map : processes key-value pairs and outputs an intermediate set of key-value pairs.

Reduce : processes the intermediate key-value pairs, merging the values for the associated keys.

### Execution

MapReduce framework partitions the input data such that it can be distributed on the cluster of machines for parallel processing.

In MapReduce, a **job** is the process to splits the input data-sets into independent chunks.

Run map tasks in parallel.

After all maps are complete, consolidated all emitted values for each unique key.

Partition space of output map keys and run reduce function in parallel.

![MapReduce](./img/MapReduce.png)

### Process 

As we can see on the previous schema, there are some steps in the MapReduce process.

**Input & splitting**

The input reader reads the upcoming data and splits it into data blocks of the appropriate size (64MB to 128MB). Each data block is associated with a Map function.

Once input reads the data, it generates the corresponding key-value pairs. The input files reside in HDFS.

**Mapping**

The map function process the upcoming key-value pairs and generated the corresponding output key-value. The map input and output type may be different from each other.

**Partition**

The partition function assigns the output of each map to the appropriate reducer. The available key and value provide this function. It returns the index of reducers.

**Shuffling and sorting**

The data are shuffled between/within nodes so that it moves out from the map and get ready to process for reduce function. Sometimes, the shuffling of data can take much computation time.
The shuffle is transparent to the user.

The sorting operation is performed on input data for reduce function. Here, data is compared using comparison function and arranged in sorted form.

**Reducing**

The reduce function is assigned to each unique key. These keys are already arranged in sorted order. The values associated with the keys can iterate the reduce and generates the corresponding output.

**Output**

Once the data flow from all the above phases, Output writer executes. The role of Output writer is to write the Reduce output to the stable storage.

### Pros / Cons 

- MR has one input format (K/V pairs) and 2-stage data flow is simple but too rigid.
- Custom code is needed for common operations (e.g. projection and filtering)
- Many users would prefer to write SQL queries than Map and Reduce tasks.
- These limitations have been addressed in the following extensions

## MapReduce extensions

### Sawzall

A Domain Specific Language built on top of MapReduce.

Interpreted, procedural programming language to define the Map functions.

Only for commutative and associative tasks.

No methods to examine multiple records simulteneously or to have the contents of one record to influence the processing of another.

Aggregators (Reduce functions) are written in another language (Python, C++, ..).

### FlumJava

A Java library from Google for developing and running data-parallel pipelines on top of MR.

Centered on few classes that represent parallel collections which support parallel operations. 

FlumeJava implements parallel operations using deferred evaluation (lazy) to permit optimization. 

Parameters for optimization: data size, compute where the data are, performing independent operations in parallel. 

Apache Crunch is a Java, open-source framework influenced by FlumeJava.

### Pig Latin

Pig is the system that enables the compilation of Pig Latin scripts into physical plan that are executed over Hadoop. 

Pig Latin combines the high-level declarative querying approach of SQL and the low-level programming of MapReduce. 

A Pig Latin program is described as a sequence of steps where each step represents a single data transformation. 

Pig Latin programs are specifying a query execution plan. It has a flexible, fully nested data model and has extensive support for user-defined functions (currently written in Java). 

It is meant for offline, scan-centric workloads. 

Other high level interfaces on top of MR: Hive, Scope, Dryad/Linq, Cascalog, .

### Hive

Bring relational DB concepts and a subset of SQL to Hadoop. 

Queries are expressed in HiveQL (SQL-like declarative language). 

Queries are compiled into MR jobs and executed using Hadoop. 

Metastore component is Hive’s system catalog. 

Built by Facebook and given to ASFBring relational DB concepts and a subset of SQL to Hadoop. 

## Map Reduce and RDBMS

For RDBMS community : no schemas (KV pairs), uses brute force instead of indexing, not compatible with DBMS tools.

Map function is like a Group By clause.

Reduce function is like aggregation function (e.g. average, sum).

Parallell DBs have been around for more than 30 years.

MapReduce completes DBMS techno :

- MapReduce is more like an ETL system than a DBMS.

- It is faster to load data than a parallel DB.

- But parallel DB performs operations faster than MR once data is loaded.

## Couch DB

Document-oriented database.

Project of the Apache foundation, started in 2005 and implemented in Erlang.

The main operations performed are **MAP** and **REDUCE**.

Characteristics: build for the Web, scales, replication built-in, Schema-free, indexable, append-only storage, atomic updates, no locking mechanism of data: first to commit wins, eventually consistent.

Access control: HTTP, REST-based interface (GET, PUT, POST, DELETE)

Query language: Javascript (as MapReduce jobs).

Storage format: JSON

There is an administration interface for CouchDB : Futon


## Other Big Data tools

### Apache Sqooq

Stands for SQL to Hadoop.

Sqoop performs bidirectional data transfer between Hadoop and almost any external structured datastore.

Usually loads data to Hive and HBase.

### Apache Flume

A scalable real-time ingest framework that allows you to route, filter, aggregate, and do mini-operations on data on its way in to the scalable processing platform.

Generally used to ingest logs, clickstream or other event data.

Usually loads data to Hive and HBase.

