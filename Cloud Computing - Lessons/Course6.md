﻿# Cloud Computing - SQL with Spark

## Hive

### Preview


Hive is a standard for SQL queries over petabytes of data in Hadoop.
Provides a SQL-like (HQL) access for data in HDFS.
The Hive metadata store can use either embedded, local or remote databases.
Hive servers are built on Apache Thrift Server Technology.
Hive supports adhoc querying data on HDFS.
Hive has a well-defined architecture for metadata management, authentication, and query optimizations.

## Shark

## Spark SQL

### Introduction

- Builds on the experience of designing Shark.
- With a tighter integration between relational and procedural processing, through a declarative DataFrame API that integrates with procedural Spark code.
- Includes a highly extensible optimizer, **Catalyst**.

### Motivation

SparkSQL tries to solve some problems with Shark.

- Shark could only be use to query external data stored in the Hive catalog.
- Only way to call Shark from Spark was to write a SQL string
- The Hive optimizer was too MapReduce specific and difficult to extend.

### Goals

- Support relational processing both within Spark programs and external data sources.
- Provide high performance using established DBMS techniques.
- Easily support new data sources (semi-structured, external DB to query federation)
- Enable extension with advanced analytics algorithms such as graph processing and ML.

### Data model

- Spark SQL uses a nested data model based on Hive for tables and DataFrames.
- Supports all major SQL data types and some complex ones : structs, arrays, maps and unions.
- Complex data types can be nested together
- Supports UDF

### Example

	val sqlContext = new SQLContext(sc)
	val schemaTriple = StructType(
		StructField("s", LongType, false)::
		StructField("p", LongType, false)::
		StructField("o", LongType, false)::
		Nil
	)

	val triples = sc.textFile(path)
	.map(x => x.split(""))
	.map(t => (
		t(0).toLong, 
		(t(1).toLong, t(2).toLong)
	)

	val rowTriples = triples.map{case(s, (p, o)) => Row(s, p, o)}

	val tripleSchemaRDD = sqlContext.applySchema(rowTriples, schemaTriple)
	tripleSchemaRDD.registerTemplate("triple")

	val res = sqlContext.sql("SELECT t1.s, t3.o FROM triple t1, triple t2, triple t3 WHERE t1.p=1446244352 AND t2.p =1446133760 AND t3.p =5755 AND t1.o=t2.s AND t2.o=t3.s")

### Querying Native Datasets, Caching and Storage

- Spark SQL allows users to construct DataFrame directly against RDDs of objects native to the programming language, e.g. : JavaBean, Scala case.

- Cache hot data by: **Columnar cache cache()**, reduce memory footprint by applying compression schema such as: dictionary encoding and run-length encoding.

- Advantages of columnar storage:
	- Skip unsatisfied data, reduce the IO operations
	- Efficiently apply compression encoding.
	- Read the required column, support vector computing, better scanning performance.

## Catalyst optimizer

### Introduction 

2 goals : 

- Make it easy to add new optimization technique and features to Spark SQL.
- Enable external developers to extend the optimizer (e.g. adding data source specific rules, support new data types).

At the core of Catalyst, it contains a general library for representing trees and applying rule to manipulate them.

Example : 

Cost-based optimization is performed by generating multiple plans using rules, and then computing their costs.

Catalyst is an optimizer for SQL and DataFrames.
Makes it easy to add data sources, optimization rules and data types.

### Tree

The main data type in Catalyst is tree, composed of node and objects.
New node types are defined in Scala as subclasses of the TreeNode.

### Rules

- Rules: Tree_1 => Tree_2 = Rules(Tree_2)
- The most common approach is using pattern matching (transform method in Catalyst).
- Transform is a partial function, i.e. only needs to match a part of input tree. => The rest optimization only needs to be applied on the matched part.
- In practice, rules may need to execute multiple times to fully transform an input tree. Until tree stops changing after applying its rules.


