﻿# Memo - RDD

## Create a RDD from File

There are many ways to create a RDD from file. 

### Text file

We can use the method `textFile` from read package and `rdd` method.

	spark.read.textFile(path).rdd

### CSV file

We can use the method `csv` and `read`.

	spark.read.csv(path).rdd

### JSON file

We can use `json` method.

	spark.read.json(path).rdd

## Create a RDD from List or Seq

We can create a RDD thanks to `parallelize` method.

	spark.sparkContext.parallelize(Seq(“the”, “quick”, “brown”, “fox”, “jumps”, “over”, “the”, “lazy”, “dog”), 2)

The first parameter is the elements of the collection will be copy in the RDD. 
The second parameter is the number of partitions will be create.

## Save a RDD as a file

The `saveAsTextFile` method save a RDD into a path passed as parameter.

Note : This method create a folder. If the folder already exists, an exception is threw.

	val x = sc.parallelize(1 to 20, 3)
	x.saveAsTextFile("rdd_x")

## Collect

### Goal

> Return the RDD as an array.

### Remember

Use this function only if the map is no big

### Example

	val x = sc.parallelize(1 to 10, 2)
	val array = x.collect() // Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

## Foreach

### Goal

Apply a Unit function (no transformation) on each element of a RDD.

### Example

	val x = sc.parallelize(1 to 5, 2)
	x.foreach(w => println(w + ", ")) // 1, 3, 4, 5, 2

## Map

### Goal

> Return a RDD with applying a function passed as argument to each item of RDD source.

### Remember

RDD result have the same number of items as RDD source.

### Example

	// Create a RDD
	val x = sc.parallelize(List(7, 10, 12, 17, 19), 2)

	// Define map functions
	val f = (a: Int) => (a/2, a%2)
	val g = (a: Int) => (a * 100.0)/20

	val zf = x.map f
	val zg = x.map g

## Reduce

### Goal

> Aggregates the elements of the source RDD by applying a commutative and associative function passed as a parameter.


### Example

	// Create a RDD
	val x = sc.parallelize(1 to 10, 3)
	
	// Define a function
	val add = (_: Int, _: Int) => x$1 + x$2

	val s = x.reduce(add)

## Count

### Goal 

> Return the number of items in RDD source.

### Example

	// Create RDD
	val x = sc.parallelize(1 to 10, 3)

	// Count
	x.count()
	// Or 
	x.count
	
## Count by key

### Goal

> Count the number of items associated to a key and create a Map where count are associated to key

Note : The RDD have to be a map of type (K, V)

### Remember

Use this function only if the map is no big

### Example

	val x = sc.parallelize(List((10, 2), (10, 5), (14, 3), (14, 7), (30, 2), (30, 3), (30, 5))

	val r = x.countByKey()

	// r: Map(30 -> 3, 14 -> 2, 10 -> 2)

## First

### Goal

> Return the first element of the RDD source.

### Remember

Throw exception if RDD have no elements.

### Example

	val x = sc.parallelize(1 to 10, 3)
	val y = sc.parallelize(List())

	x.first() 
	// or
	x.first

	y.first -> Exception

## Take 

### Goal

> Return an array of n first elements of RDD source.
> 
> The function take as parameter the number of elements to get.

### Remember

If the number is negative : return an empty Array

If the number is greater than the RDD size, return all elements.

If RDD is empty, throws an exception.

### Example

	val x = sc.parallelize(1 to 10, 3)
	val z = sc.parallelize(List())

	x.take(-3) // Array())
	x.take(2) // Array(1, 2)
	z.take(3) // Exception

## FlatMap

### Goal

> Transform all RDD source items to 0 or many items and return a new RDD.
> 
> FlatMap take a method as first parameter.

### Example 

	val x = sc.parallelize(List(1, 2), (3, 5), (8, 10))

	val f = (a: Tuple2[Int, Int]) => Seq(a._1, a._2)

	val y = x.flatMap(f)

	y.collect()
	// Array(1, 2, 3, 5, 8, 10)

## Filter

### Goal

> Return a new RDD which contains all items corresponding to the predicate.
> 
> Take a predicate function as parameter.

### Remember

The RDD source and RDD result have the same type.

### Example

	val x = sc.parallelize(List(1, 2, 3, 4, 5), 2)
	
	val f = (a: Int) => (a < 3)
	val z = x.filter(f)

## Sample

### Goal 

>     Return random part of a RDD source
>     
>     First parameter is boolean which precise if we authorized repetition or not.
>     Second parameter is % of data.
>     Third is the seed.

### Example

	val x = sc.parallelize(1 to 100, 2)
	val samp1 = x.sample(true, 0.1, 7)

## TakeSample

### Goal 

> Return a random part of a RDD source but we precise the size of RDD and not the %.

### Remember

First parameter and third parameter is the same as Sample method.

### Example

	val x = sc.parallelize(1 to 100, 2)
	val samp1 = x.sample(true, 10, 7) // Take 10 elements

## TakeOrdered

### Goal

> Sort a RDD source in according to the implicit order and return the n first elements.

### Remember

Use this function only if the map is no big

### Example

	val x = sc.parallelize(Seq(6, 8, 9, 1, 2, 3)).takeOrdered(3) //Array(1, 2, 3)

## Aggregate

### Goal

> Aggregate data in two steps : 
> 
> -	Apply seqOperator on partitions
> -	Apply combOperator on results of first step
> 
> The first parameter is the accumulator. The initial value. The second parameter is an associative function will be apply on each partition.
> The last parameter is an associative function apply on results of the previous function

### Example

	val x = parallelize(1 to 6, 2)
	val seq = (a: Int, b: Int) => if (a > b) a else b
	val comb = (a: Int, b: Int) => a + b
	val res = x.aggregate(0)(seq, comb) // Get 19

## Fold

### Goal

> Aggregate data in two steps : 
> 
> - Apply operator on partition
> - Apply operator on results of first step
> 
> Note : Operator have to be an associative operator.
> 
> Take a first parameter name accumulator. The second parameter is the
> operator function.

### Remember

Fold and Aggregate are similar. 
Fold apply the same function in 1 and 2 step.
Aggregate apply a function in each step.

### Example

	val x = sc.parallelize(1 to 6, 2)
	val op = (a: Int, b: Int) => a + b
	x.fold(0)(op) // 0 + 1 + 2 + 3 + 4 + 5 + 6 = 21

## MapValues

### Goal

> Apply function on RDD which is a map (K, V). 
> Return a RDD typed as (K, U).

### Example

	val x = sc.parallelize(List("Banana", "Apple", "Pear"))
	val y = x.map(w => (w, w.length))
	val f = (n: Int) => (n/2, n%2)
	val z = y.mapValues(f)
	z.collect() // Array(("Banana", (6, 0)), ("Apple", (5, 1)), ...)

## Zip

### Goal 

> Create a RDD of type (K, V) from a RDD source typed as K and a RDD pass as parameter typed as V.

### Remember

The two RDD must have : 

- same number of partitions
- partitions with the same size

### Example

	val keys = sc.parallelize(1 to 5, 2)
	val values = sc.parallelize(11 to 15, 2)

	val x = keys.zip(values) 
	x.colect() // Array((1, 11), (2, 12), (3, 13), (4, 14), (5, 15))
 
## Distinct

### Goal 

>  Return RDD which contains distinct elements from a RDD source.

### Example

	val x = sc.parallelize(List(1, 1, 2, 3, 4, 5, 5))
	val res = x.distinct()
	res.collect() // Array(4, 1, 5, 2, 3)

## Union

### Goal 

> Return a RDD which is the union of a RDD source and a RDD passed as parameter.

### Remember

The two RDDs have to be of the same type.
Union keep no distinct elements.
Use distinct to remove it. 😉

### Example

	val x = sc.parallelize(1 to 10)
	val y = sc.parallelize(5 to 15)
	val uwd = x.union(y)
	uwd.collect() // Array(1, 2, 3, ..., 15, 5, 6, 7, 8, 9, 10)
	val uwtd = x.union(y) 
	uwtd.collect() // Array(1, 2, 3, 4, 5, ..., 15)

## Intersection

### Goal 

> Return a RDD which is the intersection of a RDD source and a RDD passed as parameter.

### Remember

The two RDDs have to be of the same type.
This operation doesn't keep double.

### Example

	val x = sc.parallelize(List(1, 1, 2, 3, 4, 7))
	val y = sc.parallelize(List(1, 1, 4, 4, 5, 6))
	val z = x.intersection(y)
	z.collect() // Array(4, 1)

## Subtract

### Goal 

> Execute a subtraction between a RDD source and a RDD pass as parameter.

### Remember

The two RDDs have to be of the same type.

### Example

	val x = sc.parallelize(1 to 5, 2)
	val y = sc.parallelize(3 to 7, 2)
	x.collect() // Array(1, 2, 3, 4, 5)
	y.collect() // Array(3, 4, 5, 6, 7)
	val s = x.subtract(y) 
	s.collect() // Array(1, 2)

## Join

### Goal 

> Create a RDD which is the join between a RDD source of type (K, V) and a RDD pass as parameter of type (K, V). Return a RDD of type (K, (V, W)).

	val l1 = List(("A", 1), ("B", 2), ("C", 3), ("D", 5), ("E", 1))
	val l2 = List(("A", 2), ("G", 1), ("C", 5), ("D", 5), ("E", 2))
	val x = sc.parallelize(l1, 2)
	val y = sc.parallelize(l2, 2)
	val z = x.join(y)
	z.collect() // Array(("D", (5, 5)), ("A", (1, 2)), ...)

### Remember

### Example

## GroupBy

### Goal 

> Regroup the elements of RDD source by key and return a new RDD of type (K, iterable[T])
> 
> Take a function as parameter.

### Remember

Very expensive method.

### Example

	val x = sc.parallelize(1 to 20, 10)
	val y = x.sample(false, 0.1, 19)
	val f = (w: Int) => w%3
	val z = y.groupBy(f)
	z.collect() // Array((0, CompactBuffer(6, 15)))

## Cartesian

### Goal 

> Return a RDD which is the cartesian product of a RDD source and a RDD pass as parameter.

### Remember

The RDD size is `m * n` where m is the size of source RDD and n the size of argument RDD.

### Example

	val x = sc.parallelize(1 to 5, 2)
	val y = sc.parallelize(List(("A", 2), ("E", 2), ("C", 5), 2)
	val z = x.cartesian(y)
	
## Repartition

### Goal 

> Regroup data of RDD source into a number of partitions

### Example

	val x = sc.parallelize(1 to 1000, 50)
	val y = x.partition(10)

## GroupByKey

### Goal 

> Like GroupBy method but use for RDD of type (K, V).

### Example

	val x = sc.parallelize(List((1, 1), (11, 2), (2, 8), (3, 5), (3, 7)), 2)
	val z = x.groupByKey()
	z.collect() // Array((2, CompactBuffer(8)), (11, CompactBuffer(2, 6)), ...)

## ReduceByKey

### Goal 

> Like Reduce method but use for RDD of type (K, V).

### Example

	val t = sc.parallelize(Seq((1,1), (1,8), (2, 7), (2,9), (3, 13), (3, 10)), 2)
	val f = (x: Int, y: Int) => x + y
	val x = t.reduceByKey(f)
	x.collect() // Array((1, 9), (2, 16), (3, 23))

## AggregateByKey

### Goal 

> Like Aggregate method but use for RDD of type (K, V).

## SortByKey

### Goal 

> Sort a RDD of key, value.
