﻿# Cloud Computing - Spark

## Introduction

Spark is an open-source framework for distributed calculation.

Created in 2009, Spark is school project. Today spark is a project from Apache. More than 1000 contributors as Netflix, Facebook, IBM etc...

Spark can be used in Scala, Java, Python, R project.

Note that Scala is the most adapted.

Apache Spark manages and coordinates the execution tasks on data across a cluster of computers.

This management is performed by a cluster manager, YARN, Mesos and Kubernetes.

When apps are submitted to the cluster manager, they will grant resources to them complete work.


## Why Spark ?

Pros : 

- Work distribution
- Fault tolerance
- Locality aware scheduling
- Load balancing

Cons : 

- Lack of abstraction for leveraging distributed main memory
- Hard to reuse intermediate results. One solution is write to external storage systems

Unlike MapReduce, Spark is designed for iterative and interactive jobs because it aims to store data as much as possible in RAM.

For certain jobs, spill to disk may be necessary.

## Terminology

Application : A user program built on Spark using its APIs. It consists of a driver program and executors on the cluster.

Job : A parallel computation consisting of multiple tasks that are generated in response to a Spark action (save, collect).

Stage : Each job gets divided into smaller sets of tasks called stages that depend on each other.

Task : A single unit of work or execution that will be sent to a Spark executor.

Spark Driver : has multiple roles : 

- It communicates with the cluster
- It requests resources (CPU, memory, etc.) from the cluster manager for Spark's executors
- It transforms all the Spark operations into DAG computations
- Schedules them, and distributes their execution as tasks across the Spark executors
- Once the resources are allocated, it communicates directly with the executors

Spark executor : runs on each worker node in the cluster.
The executors communicate with the driver program and are responsible for executing tasks on the workers.
In most deployments modes, only a single executor runs per node.

Cluster manager : responsible for managing and allocating resources for the cluster of nodes on which your Spark applications runs.
Currently, Spark supports four clusters managers : 

- the built-in standalone cluster manager
- Apache Hadoop YARN
- Apache Mesos
- Kubernetes

![SparkClusterManager](./img/SparkClusterManager.png)

Driver and executors are processes. They can live on a single machine or multiple machines. They run as threads on a single machine (local mode).

## Execution

- Spark application consists of a driver, launches parallel operations on cluster.

- Driver contains spark application's main function, defines the distributed datasets on cluster, then apply operations on them.

- Driver accesses Spark through a SparkSession (SparkContext) Object, it represents a connection to a computing cluster.

- SparkContext enables to build RDD, which allows to run series of applications.

- SparkSession provides a single point of entry to interact with Spark and allows programming with DataFrame and Dataset APIs.

- To run these operations, driver manages a number of nodes called executor.

- SparkSession contacts cluster manager which assigns cluster resources.

- Then SparkSession sends application code to executors.

- Executor is responsible of 2 things : execute code assigned to it by the driver and reporting the state of the computation to the driver.

- Spark examines the graph of RDDs on which the job depends and formulates execution plans.

## RDD

RDD means Resilient Distributed Datasets

RDD is a distributed memory abstraction. It is a fault-tolerant, immutable, lazy, collection of records, partitioned data across a cluster.

SparkContext is the entry point for creating RDDs.

Two types of RDDs : 

- generic RDD type
- key-value RDD that provides additional functions (eg. aggregating by key)

Any RDD possesses two types of operations : Transformation and Action.

- Transformation : lazy operators to define new RDDs.
- Action : Actions launch a computation to return a value to the program or write data to external storage.

Another two aspects of RDDs : persistence and partitioning.

No execution happens at the cluster until an action is encountered (lazy).

The driver program ships the execution graph as well as the code to the cluster, every worker server gets a copy.

The execution graph is a DAG.

Each DAG is an atomic unit of execution.

RDD can remember the series of transformations that builds an RDD (its lineage) to recompute lost data (fault-tolerance).

Due to laziness, it is possible to optimize DAGs : 

- in the case of a DAG containing a map and a filter, the system can decide to execute the filter first and then the map (to reduce the number of records to undergo the map operation).

- Driver program when starting execution builds up a graph, nodes are RDD, edges are Transformation.

There are two types of dependencies : 

- Narrow dependencies allow for pipelined execution on one cluster node, which can compute all the parent partitions : 

map -> filter -> reduce... on an element-by-element basis.

Each input partition will contribute to only one output partition.

- Wide dependencies require data from all parent partitions to be available and to be shuffled across the nodes using MapReduce like operation.

Shuffle : exchange partitions across the cluster

### Storage Module

RDD is composed by different partitions, transformation and action are performed on partitions.

In storage module, RDD is composed by blocks, the manipulation on RDD is based on block unit, block is the minimum storage unit in storage module.

Storage module transforms partition to block by RDD id and partition index.

On abstract level, operations run on partitions, but finally partitions need to be mapped to block.

### Partitions

There are two types of partitions in according to the dependencies type : 

- Narrow -> Each input partition will contribute to only one output partition. 
- Wide -> Input partitions will contribute to many output partitions.

An RDD divides a number of partitions, which are atomic pieces of information.

Partition of RDD can be stored on different nodes of a cluster.

RDD is just a collection of partitions.

All input, intermediate and output data will be presented as partitions, **a partition is a basic unit of parallelism**.

### RDD-Fault Resiliency

RDD tracks series of transformations used to build them to re-compute the lost data.

RDDs maintain the lineage information used to reconstruct the lost partitions of RDD.

Recompute only the lost partitions of RDD.

It s helpful to checkpoint some RDDs to stable storage chosen by users.

Whenever a worker server crashes during the execution of a stage, another worker server re-executes the stage from the beginning by pulling the input data from its parent stage that has the output data stored in local files.

### RDD Operations - Transformation

Transformation creates a new RDD from existing RDD.

Transformation allows to create dependencies between RDDs.

Lazy operation.

Spark keeps the tracks of set of dependencies between RDDs.

Examples of transformation : 

- map
- flatMap
- filter
- distinct
- sample

- Set of operations of RDD that define how its data should be transformed.
- Transformation yields new RDD.
- Lazy because of optimization of series of transformation on RDDs.
- Spark is lazy, system does noting without a transformation or action applied on RDDs.
- RDD is not "a set of data". It is just a step in a program, telling spark how get the data and what to do with it (check iterator section).

### RDD Operations - Action

- Actions return the final value to the driver program or write data to an external storage system.

- Actions force the evaluation of transformations required for the RDD they were called on.

Example of action : 

- take
- top
- takeOrdered
- takeSample
- reduce
- fold

## Shared variables

When Spark runs a function in parallel as a set of tasks on different nodes, it ships a copy of each variable used in the function to each task.

These variables are copied to each machine.

No updates to variables are propagated back to driver program.

The shared variables which support read and write are inefficient.

On Spark, there are two kinds of variables : 

- Broadcast
- Accumulator

### Broadcast variables

Allows programmer to keep a read-only variable cached on each machine rather than shipping a copy of it with tasks.

The broadcast value are not shipped to the nodes more than once.

Spark uses broadcast algorithm to reduce communication cost.

### Accumulators

An accumulator is a variable that is broadcast to the worker nodes.

Variables can be only added through associative operation.

For accumulator updates performed inside actions only.

In local mode with a single JVM, the above code will sum the values within the RDD and store it it counter. This is because both the RDD and the variable counter are in the same memory space on the driver node.

In cluster mode, to execute jobs, Spark breaks up the processing of RDD operations into tasks - each of which is operated on by an executor. Prior to execution, Spark computes the closure. The closure is those variables and methods which must be visible for the executor to perform its computations on the RDD.

The variables within the closure sent to each executor are now copies and thus, when counter is referenced within foreach function, it's no longer the counter on the driver node.

There is still a counter in the memory of the driver node but this is no longer visible to the executors. The executors only see the copy from the serialized closure.

Ok for local mode but not for cluster mode.

	rdd.foreach(println) 
	rdd.map(println) 

Ok for cluster mode.

	rdd.collect().foreach(println)

Only driver program can read the accumulator's value not the task.

Each worker node can only access and add to its won local accumulator value.

Only the driver program can access the global value.




