﻿# Map Reduce - Hadoop

## Introduction

Hadoop is an open-source framework, written in Java started in 2006.

It aims to make it easier to create a distributed and scalable application.

Hadoop was inspired by the publication of MapReduce, GoogleFS and Google's BigTable. <a href="Course2.md">(See course 2)</a>

The framework must provide the application with thousands of nodes that can work with peta bytes of data. Thus each node consists of standard machines grouped in a cluster. All Hadoop modules are designed with the idea that hardware failures are frequent and therefore should be handled automatically by the framework.

## Architecture

Hadoop is composed by many modules : 

- Hadoop Common
- Hadoop Distributed File System (HDFS)
- Hadoop Yarn
- Hadoop MapReduce

Hadoop contains also many software, evoked in <a href="Course2.md">Course 2</a> like Apache Pig, Apache HBase, Apache ZooKeeper etc...

### Hadoop Distributed File System (HDFS)

Developed from GoogleFS and written in Java.

Created to stock very large data volumes in lot of machines by using files system.

Distributed files are manipulated as hard drive.

All servers are connected and communicate by TCP protocols.

HDFS is oriented master/slave architecture.

The master is called NameNode. The slaves are called DataNode.

**NameNode**

NameNode regulates file access to the clients. It maintains and manages the slave nodes and assigns tasks to them. NameNode executes file system namespace operations like opening, closing, and renaming files and directories.

NameNode runs on the high configuration hardware.

**DataNode**

There are n number of slaves (where n can be up to 1000) or DataNodes in the Hadoop Distributed File System that manages storage of data. These slave nodes are the actual worker nodes that do the tasks and serve read and write requests from the file system’s clients.

They perform block creation, deletion, and replication upon instruction from the NameNode. Once a block is written on a DataNode, it replicates it to other DataNode, and the process continues until creating the required number of replicas.

DataNodes runs on commodity hardware having an average configuration.

**Files storage**

Hadoop HDFS broke the files into small pieces of data known as blocks. The default block size is 128MB.

These blocks are stored in the cluster in a distributed manner on different nodes. This provides a mechanism for MapReduce to process the data in parallel in the cluster.

HDFS stores multiple copies of each block across the cluster on different nodes. This is a replication of data. By default, the HDFS replication factor is 3. Hadoop HDFS provides high availability, fault tolerance, and reliability.

**Operations**

In Hadoop, we need to interact with the file system either by programming or using the command-line interface (CLI).

Read Operation : 

- Client interact with NameNode because NameNode is the only place that stores the metadata about DataNode. NameNode return the address or the location of the slaves where data is stored.
- Then client interact with the specified DataNodes and read the data from there.

Write Operations : 

- As read operation, client interact first with the NameNode which get the address or location of a dataNode.
- Once the client finishes writing the block, the slave starts replicating the block into another slave, which then copies the block to the third slave. This is the case when the default replication factor of 3 is used. After the required replicas are created, it sends a final acknowledgment to the client. 

**Fault Tolerance**

HDFS is **highly fault-tolerant**. Before Hadoop 3, it handles faults by the process of **replica creation**. It creates a replica of users’ data on different machines in the HDFS cluster. So whenever if any machine in the cluster goes down, then data is accessible from other machines in which the same copy of data was created.

Hadoop 3 introduced **Erasure Coding** to provide Fault Tolerance. Erasure Coding in HDFS improves storage efficiency while providing the same level of fault tolerance and data durability as traditional replication-based HDFS deployment.

### Hadoop Yarn

It's a cluster manager.

### Hadoop HBase

HBase is a distributed non-relational database management system, written in Java, with structured storage for large tables.

HBase is inspired from BigTable (Google). Based on master/slave architecture, this database can manage very large tables (billions of row per table).

### Hadoop ZooKeeper

Apache ZooKeeper is a software which manage the configuration for distributed systems.

ZooKeeper's architecture supports high availability through redundant services. This allows customers to question another ZooKeeper leader if the former does not respond. ZooKeeper nodes store their data in a hierarchical namespace, just like a file system or tree-like data structure. Clients can read and write to nodes and thus have a shared configuration service. Updates are totally orderly.

### Hadoop Hive

Apache Hive is an Integrated Data Warehouse infrastructure on Hadoop that enables analysis, query through a language syntactically close to SQL, and data synthesis.

### Hadoop Pig

Hadoop Pig is a platform used with Hadoop. The language used is Pig Latin.
