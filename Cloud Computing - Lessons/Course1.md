﻿
# Cloud computing

## Introduction 

2011 -> NIST get a definition for cloud computing : 

> Cloud computing is a model for enabling ubiquitous , convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction.

We count 5 attributes for the computing cloud :

- On-demand self-service
- Broad network access
- Resource pooling
- Rapid elasticity
- Measured service

All resources are accessed through services hence **pay-as-you-go pricing model**.
This is based on a **Service Level Agreement (SLA)** between a cloud provider and customers.

> A SLA specifies the responsibilities, guarantees and service commitment.

They are many factors which drive users to adopt cloud computing : 

- Scalability
- Saving money
- Easier management
- Redundancy
- Elasticity 
- Improve hardware utilization
- Security
- Other

> Scalability is the property of a system to handle a growing amount of work by adding ressources to the system.

There are two ways to scale : 

- **Vertical scaling** by adding processors or memory
- **Horizontal scaling** by splitting data within functional area across multiple databases (**sharding**)

| Vertical scaling | Horizontal scaling |
|--|--|
| Upgrade to more powerful server and cluster | Deploy on a grid or cluster of similar servers |
| Appropriate for traditional scale up architecture | Appropriate for recent scale out products |
| More expensive in hardware | Less expensive in hardware |
| Eventually hits a limit | Less likely to hit a limit |

One of essential concept of cloud computing is the **multi tenancy**. 
The multi tenancy is a principle in software architecture where a single instance of the software runs on a server, serving multiple clients organizations which are the tenants. The architecture is designed to virtually partition its data and configuration.

Cloud computing capitalizes on:

- Grid computing (distributed resources over the network)
- Server virtualization
- Cluster computing (to manage lots of computing and storage resources)
- Web services (SOA)
- Utility computing (packaging computing and storage resources as services)

**Cloud computing VS Grid computing :** 

|Cloud computing| Grid computing |
|--|--|
| Designed to support a large number of users | Design to run very large jobs for few users  |
| Involves to select a provider and to run apps in their data centers | Involves a federation of multiple organizations  |

**Server virtualization**

Server virtualization is a technology that enables multiple applications to run on the same physical server as virtual machines. That is the same as if they would run in distinct physical servers.


## Models

### Deployment models

There are 4 deployment models : 

- **Public cloud** : pay-as-you-go approach to the public. What is sold is utility computing.
- **Private cloud** : internal data centers of a business or organization that are not available to the public.
- **Community cloud** : shares infrastructures between several organizations from a specific community with common concerns (security, compliance, jurisdiction, etc...)
- **Hybrid cloud** : a composition of two or more clouds (private, community, or public) that remain unique entities. Can also correspond to multiple cloud systems which are connected such that programs and data can be moved easily from one deployment system to another.

### Delivery models

There are 5 delivery models : 

- Software as a Service (SaaS)
- Infrastructure as a Service (IaaS)
- Platform as a Service (PaaS)
- Database as a Service (DBaaS)
- Data as a Service (DaaS)
- Container as a Service (CaaS)

**Software as a Service**

- Delivery of app software as a service
- A generalization of the ASP model but the cloud provider also provides tools to integrate other apps.
- Apps ranging from email, calendar to CRM, data analysis, etc.
- Example : Salesforce.com

**Infrastructure as a Service**

- Delivery of a computing infrastructure as a service (computing, storage and networking)
- Easy scale up (add resources) and down (release resources) : **elasticity**
- Elasticity is achieved via **server virtualization** : multiple apps run on the same physical server as virtual machines. 
- Example: **Amazon Web Services**, Joyent, Rackspace, Eucalyptus

**Platform as a Service**

- Delivery of a computing platform with development tools and APIs as a service
- Supports the creation and deployment of custom apps directly on the cloud
- Cloud provider maintains the platform for executing theses applications : the hardware of the server, the basic software, the operating system (OS) etc...
- Example : **Google App Engine**, Joyent, Heroku, Cloudbees, Microsoft Azure

**Data as Service**

- Considers that software is becoming a commodity
- Data is the main thing when it can be used with different software
- Princing model : 
	- Volume-based model : either quantity-based pricing (e.g. 5,000 API calls per day) or pay per call (e.g. few cents for each call to the API)
	- Data type-based model : not all data have the same value
- Ensuring data quality and its cleansing are central tasks


## Cloud providers

### Amazon web services (AWS)

First cloud provider started in 2006. It proposes many services (over 150).
It exists many different storage solutions.

|Solution| Description |
|--|--|
| Amazon Simple Storage Service (S3) | Provides scalable and highly durable object storage in the cloud|
| Amazon Glacier | Provides low-cost highly durable archive storage in the cloud |
| Amazon Elastic File System (Amazon EFS)| Provides scalable network file storage for Amazon EC2 instances|
| Amazon Elastic Block Store (Amazon EBS)| Provides block storage volumes for Amazon EC2 instances|
| Amazon EC2 Instance Storage| Temporary block storage volumes for Amazon EC2 instances |
| AWS Storage Gateway| An on-promises storage appliance that integrates with cloud storage|
| AWS Snowball| Transports large amounts of data to and from the cloud|
| Amazon cloud front| Provides a global content delivery network (CDN)|

**Note : Amazon EC2 is a service to rent servers in order to run our applications through this servers. Each customer can create Virtual Machine (VM). The customer pay in according to the time of servers running.**

**Amazon S3**

- Distributed storage, durable object (5TB max)
- Unlimited storage
- Data retrieval via HTTPS API
- Organized in buckets to group objects. Unique names across all AWS customers!
- Data is stored with some metadata (data of last modification, size, owner, content type, ..)
- Use cases: static Web sites, backing up data, storing structured data for analytics (data lake)
- Part of Free tier services (as of 2020): 2000 put requests/month, 5GB
- Eventually consistent
- Versioning possible on buckets

**Amazon Glacier**

- Storage solution for archiving (slow but not expensive)
- Data movement, via S3
- Retrieval options: expedited (1-5’), standard (3-5h), bulk (5-12h)
- Organized in buckets

**Amazon EBS**

- Block = sequence of bytes
- 2 persistent block storage in AWS: EBS (via network, independent of VM) and temporary (attached to VM, low latency, high throughput)
- use cases: operating a RDBMS on a VM, running code requiring a file system to store data on EC2, storing and booting the OS of a VM
- 1 EBS is attached to 1 VM at a time, not to several VMs, see Elastic File System (EFS))

![EC2VSEBS](./img/EC2VSEBS.png)

**Amazon Elastic File System (Amazon EFS)**

- To share data between multiple EC2 instances
- Data is replicated between multiple AZ

**Amazon RDS**

- AWS solution for RDBMS (MySQL, postgresql, SQL sever, Oracle)
- Also AWS Aurora: AWS RDBMS compatible with MySQL and postgresql

**AWS ElastiCache**

- KV store: Redis or memcache

**Amazon DynamoDB**

- NoSQL databases supporting key-value and Document data structures
- Schemaless, Synchronous replication across multiple data centers
- Evolution of AWS’s SimpleDB

Amazon Web Services comprises : 

- Amazon Elastic Block Store (EBS)
- SimpleDB
- Simple Queue Service

AWS supported 3 interfaces : 

- REST
- Query (REST-like)
- SOAP

Amazon is an **IaaS** solution.
It charges *(facturer)* by the number and duration of VM instances used by a customer.

To use Amazon EC2, one need to select a pre-configured image or create an Amazon Machine Image containing apps, libs, data and config settings.

**Price**

To use AWS, an account is necessary. It is free but requires a credit card.
Free account : 750hours/month for 12 months for Amazon EC2, 5GB of storage

### Microsoft Azure

- The offer is as rich as AWS. 
- Proposes IaaS, PaaS and SaaS solutions (e.g., Office 365)
- Prices competition against AWS

- Azure is organized in geographical regions which regroup multiple data centers
- These data centers are regrouped in resources groups.
- In 2018, Azure has 42 regions (2 are in France and Beta).

	**Price**

- To use Azure, an account is necessary. It is free but requires a credit card number.
- free account : 25 services are free for 12 months and 170euros are offered (for the first 30 days) to test other services

### Google Cloud Platform

- Not as rich as AWS and Azure
- Pros: services offering in Big data and machine learning
- tends to be less expensive than AWS and Azure
- based Google Compute Engine (IaaS), Google App Engine (PaaS), Google Cloud Storage and Google Container Engine (management and orchestration for Docker)
- GCP is also organized in geographical regions which regroup multiple data centers
- These data centers are regrouped in resources groups.
- In 2018, GCP has 5 regions and 44 zones (None in France).

	**Price**

- To use GCP, an account is necessary. It is free but requires a credit card number.
- free account : 12 months on “always free” services and 300$ are offered to test all available services

![CCProviders](./img/CCProviders.png)

### Other providers

**IBM Cloud**

- Targeting IT professionals more than developpers (as done by AWS)
- PaaS based on Cloud Foundry (open source PaaS)

**Oracle Cloud**

- PaaS, PaaS, SaaS and DaaS
- Late on the market compared to AWS and Azure
- 11 regions and 3 in Europe (none in France)
- An account is necessary. It is free but requires a credit card number.
- free account : 300$ are offered and have to be spent in 30 days

**Salesforce**

- Leader in SaaS enterprise app market, especially in CRM
- Between 47.000 and 100.000 clients
- Contains different aspects : 
	- Force.com: a PaaS offering with a proprietary language (APEX)
	- Database.com: database part of force.com
	- Heroku: a PaaS competitor to force.com. Java and Ruby focused supports many DBMS: SQL and NoSQL

### Comparison 

The comparison is hard because it's hard to find a common ground between IaaS and PaaS and there are many dimensions: compute, storage, network, scaling.

CloudCmp compares the performance and cost of cloud providers. Challenges: Find what to measure and how to measure it (CPU speed, memory, disk I/O, etc...).


## Serverless computing

Serverless computing is known as Function as a Service (FaaS).
The platform executes the code as needed at any scale. Developers are not concerned with provisioning operating servers. They pay the compute resources used when their code is invoked

Goals of serverless computing : 

- write and deploy your app code without taking care of the underlying infrastructure
- gain in dev productivity
- automatic autoscaling and pay as you go

Principles of Serverless computing : 

- developers register functions with the cloud provider and declare events that trigger each function
- FaaS infrastructure monitors the triggering events, allocates runtime for the function, executes it and persits the results

Limitations (as of 2019) of Serverless computing : 

- Current FaaS solutions are attractive for simple workloads of independent tasks
- FaaS limits the development of distributed systems and open-source solutions
- In many cases, FaaS suffers from high latencies

Reasons for these limitations : 

- Limited duration of function invokations (15’ for AWS Lambda)
- I/O bottlenecks : AWS Lambda uses S3 and DynamoDB to support communication (too slow)
- No support for specialized hardware
- Functions are run on isolated VMs, separate from data. It uses a “shipping data to code” rather than “shipping code to data” approach.
- No support for main-memory databases that maintain large RAM-based data structures (in AWS Lambda max of 3GB of RAM)

Some serverless computing solutions : 

![Serverless](./img/ServerLess.png)
