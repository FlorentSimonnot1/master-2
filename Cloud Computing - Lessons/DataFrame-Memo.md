﻿# DataFrame - Memo

## Requires

To work with DataFrame, we have to use SparkSession.

	val spark = SparkSession.builder.master("local").getOrCreate

## Create DataFrame from file

Spark provides a reader which can read file and convert it into DataFrame.

	val reader = spark.reader

We can read some type of file like text, csv or json.

	val df = reader.text(path)
	val df2 = reader.csv(path)
	val df3 = reader.json(path)

The schema is automatically detected.

We can custom the schema.

	val df = reader.schema(schema).text(path)

Example : 

	val df = spark.read.schema("s STRING, p STRING, o STRING").csv()


## Create DataFrame from RDD

First we have to create a schema.

	val schema = StructType(  
	  StructField("o", StringType, true)::  
	  StructField("p", StringType, false)::  
	  StructField("q", StringType, false)::
	  Nil  
	)

Struct type use a list of StructField.

Struct field is characterized by a name, a type and a boolean for precise if field is nullable.

Then we have to create a RDD.

	val rdd = sc.textFile("./resources/drugbankExt.nt")  
	  .map(x => x.split(" "))  
	  .map(t => Row(t(0), t(1), t(2)))

Note : We have to map the row file as sql row.

Finally, we use the method createDataFrame.

	val df = spark.createDataFrame(rdd, schema)

## Write DataFrame into file

Spark provides a DataFrameWriter to export a file.

	val writer = df.writer

### CSV format

	writer.csv(path)

### JSON format

	writer.json(path)

**Note : Writer create a folder and some files.**

## Print Schema

	df.printSchema()

	root
	 |-- o: string (nullable = false)
	 |-- p: string (nullable = false)
	 |-- q: string (nullable = false)

## Print Data

	df.show()

	+--------------------+--------------------+--------------------+
	|                   o|                   p|                   q|
	+--------------------+--------------------+--------------------+
	|<http://www4.wiwi...|<http://www.w3.or...|<http://dbpedia.o...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://data.link...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://data.link...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://www.dbped...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://dbpedia.o...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://www4.wiwi...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://data.link...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://data.link...|
	|<http://www4.wiwi...|<http://www.w3.or...|<http://www.dbped...|
	|<http://www4.wiwi...|<http://www4.wiwi...|          "Mycanden"|
	|<http://www4.wiwi...|<http://www4.wiwi...|           "Mycilan"|
	|<http://www4.wiwi...|<http://www4.wiwi...|             "Polik"|
	|<http://www4.wiwi...|<http://www4.wiwi...|           "D01AE11"|
	|<http://www4.wiwi...|<http://www4.wiwi...|           "Prolift"|
	|<http://data.link...|<http://www4.wiwi...|            "Solvex"|
	|<http://www4.wiwi...|<http://www4.wiwi...|            "Vestra"|
	|<http://www4.wiwi...|<http://www4.wiwi...|         "Trimysten"|
	|<http://www4.wiwi...|<http://www4.wiwi...|       "Trivagizole"|
	|<http://www4.wiwi...|<http://www4.wiwi...|           "Veltrim"|
	|<http://www4.wiwi...|<http://www4.wiwi...|           "D01AC01"|
	+--------------------+--------------------+--------------------+

## Create a table from DataFrame

To manipulate data with SQL we have to create a table from the DataFrame.

	df.createOrReplaceTempView(name)

Example : 

	df2.createOrReplaceTempView("drugs")

Now, we can access the table as drugs.

## Get DataFrame from table

We can get a DataFrame from a table.

	val df = spark.table(name)

Example : 

	val df = spark.table("drugs")

## Where

We can select data in according to a condition.

	val df = df.where(condition)

Example :

	val dfRes = df2.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName>'")

## Select

We can select columns we want to keep in the result.

	val df = df.select(col1, ..., colN)

Example : 

		val dfRes = df2.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName>'").select("o", "p")

## Join

We can join 2 DataFrames in according to a column.

	val dfJoin = df1.join(df2, colOfJoin)

Example 

	val brandNames = df.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName>'").select("s", "o")  
	brandNames.show()  
	  
	val disease = df.where("p = '<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/possibleDiseaseTarget>'").select("s")  
	disease.show()  
	  
	val disDSL3 = brandNames.join(disease, Seq("s"))  
	disDSL3.show()


