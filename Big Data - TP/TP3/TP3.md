# TP3 - MongoDB

## Prepare your environment

### Installation

Download mongoDB Community : https://www.mongodb.com/try/download/community

Extract tgz file where do you want.

You have to create a folder `data` where do you want.

### Run MongoDB

To use MongoDB you have to run the server and the client in two terminals.

Run server with this command : 

    ./mongod --dbpath path/data

Note that you have to precise your database path with the folder data.

Run client thanks to this command : 

    ./mongo


## Exercises

### Context

In MongoDB we want to create a DB which will be able to save information about films. In a relational Database, this schema would be certainly realized :

![ER-Diagram](./img/ER-diagram.png)

A film have some information like title, a summary, the year of it production. A film can have some genders (label, id) but a gender is link to only one film.

A film have a director or some directors which are a person. It have too some actors which play a role.

A person have an id, a firstname and lastname and a birthday date.

In MongoDB, it's not possible to use joins. We generally not use a schema too. 

The first objective will be to denormalize our ER diagram to construct a model which could be implement with MongoDB.
Unfortunately, we'll see there is no only one model to implement our DB but it depends to data which will retrieve.

### Question 1

In this question, we consider 2 different use cases in according to the data we want to retrieve. We will implement the DB according to this use cases. 

(a)
- First use case : The database requests are essentially on films and not actors
    
	   hivi = { 
	   "title": "A History of Violence", 
	   "year": "2005", 
	   "genre": "Crime", 
	   "summary": "Tom Stall, a humble family man and owner of a popular 
		  neighborhood restaurant, lives a quiet but fulfilling existence in the Midwest. 
		  One night Tom foils a crime at his place of business and, to his chagrin, is plastered all over the news for his heroics. Following this, mysterious people follow the Stalls' every move, 
		  concerning Tom more than anyone else. As this situation is confronted, more lurks out over where all these occurrences have stemmed from compromising his marriage, family relationship and the main characters' former relations in the process.", "country": "USA", 
		  "director": { 
			  "last_name": "Cronenberg", 
			  "first_name": "David", 
			  "birth_date": "1943" 
			}, 
			"actors": [ 
			{ 
				"first_name": "Ed", 
				"last_name": "Harris", 
				"birth_date": "1950", 
				"role": "Carl Fogarty" 
			}, { 
				"first_name": "Vigo", 
				"last_name": "Mortensen", 
				"birth_date": "1958", 
				"role": "Tom Stall" 
			}, 
			{ 
				"first_name": "Maria", 
				"last_name": "Bello", 
				"birth_date": "1967", 
				"role": "Eddie Stall" 
			}, 
			{ 
				"first_name": "William", 
				"last_name": "Hurt", 
				"birth_date": "1950", 
				"role": "Richie Cusack" 
			}] 
		}

	use movieDB

	db.movies.insert(hivi)

In this case, we juste create a film with all information and we insert it into DB.
We can retrieve data thanks to this command line : 

    db.movies.find({"title": "A History of Violence"})

(b)
- Second use case : Users can retrieve data by requests based on films and actors.

The requests about film information have to allow to get all information about a film.
The requests about actors have to allow to retrieve all information or only the title about a film in which this actor have participated.

For films, the structure is similar to the first case. But we have to use an id for represent movie.

	   hivi = { 
	   "idMovie": 1,
	   "title": "A History of Violence", 
	   "year": "2005", 
	   "genre": "Crime", 
	   "summary": "Tom Stall, a humble family man and owner of a popular 
		  neighborhood restaurant, lives a quiet but fulfilling existence in the Midwest. 
		  One night Tom foils a crime at his place of business and, to his chagrin, is plastered all over the news for his heroics. Following this, mysterious people follow the Stalls' every move, 
		  concerning Tom more than anyone else. As this situation is confronted, more lurks out over where all these occurrences have stemmed from compromising his marriage, family relationship and the main characters' former relations in the process.", 
		  "country": "USA", 
		  "director": { 
			  "last_name": "Cronenberg", 
			  "first_name": "David", 
			  "birth_date": "1943" 
			}, 
			"actors": [ 
			{ 
				"first_name": "Ed", 
				"last_name": "Harris", 
				"birth_date": "1950", 
				"role": "Carl Fogarty" 
			}, { 
				"first_name": "Vigo", 
				"last_name": "Mortensen", 
				"birth_date": "1958", 
				"role": "Tom Stall" 
			}, 
			{ 
				"first_name": "Maria", 
				"last_name": "Bello", 
				"birth_date": "1967", 
				"role": "Eddie Stall" 
			}, 
			{ 
				"first_name": "William", 
				"last_name": "Hurt", 
				"birth_date": "1950", 
				"role": "Richie Cusack" 
			}] 
		}

		db.movies.insert(hivi)

Then we have to use a structure to add actors :

    db.actors.insert(
    {
	   "first_name":"Ed",
	   "last_name":"Harris", 
	   "birth_date":1950,
	    movies:[
		    {"title":"A History of Violence","idMovie":1},
		    {"title":"Gravity","idMovie":2}
	    ]
    })

As you can see, we use the idMovie in structure which represents actors.

### Question 2

In this question, we just use the first use case. The objective is to add the next film : 

> "The Social network", 2010, Drama, "On a fall night in 2003, Harvard
> undergrad and computer programming genius Mark Zuckerberg sits down at
> his computer and heatedly begins working on a new idea. In a fury of
> blogging and programming, what begins in his dorm room soon becomes a
> global social network and a revolution in communication. A mere six
> years and 500 million friends later, Mark Zuckerberg is the youngest
> billionaire in history... but for this entrepreneur, success leads to
> both personal and legal complications.", USA. Actors are: Jesse
> Eisenberg (born in 1983) plays the role of Mark Zuckerberg", Rooney
> Mara (1985) plays the role of Erica Albrig. The director is David
> Fincher (1962)

	film = {
		"title": "The social network",
		"year": 2010,
		"genre": "Drama",
		"summary": “On a fall night in 2003, Harvard  
			undergrad and computer programming genius Mark Zuckerberg sits down at  
			his computer and heatedly begins working on a new idea. In a fury of  
			blogging and programming, what begins in his dorm room soon becomes a  
			global social network and a revolution in communication. A mere six  
			years and 500 million friends later, Mark Zuckerberg is the youngest  
			billionaire in history… but for this entrepreneur, success leads to  
			both personal and legal complications.”,
		"country": "USA",
		"director": {
			"last_name": "Fincher",
			"first_name": "David",
			"birth_date": "1962"
		},
		"actors": [
			{
				"last_name": "Eisenberg",
				"first_name": "Jesse",
				"birth_date": "1983",
				"role": "Mark Zuckerberg"
			}, 
			{
				"last_name": "Mara",
				"first_name": "Rooney",
				"birth_date": "1985",
				"role": "Erica Albrig"
			}
		]
	}
	db.movies.insert(film)

### Question 3

In this part, we just write some requests to retrieve data.

**a - Display the 2005 movies.**

    db.movies.find({"year": "2005"})

___
**NOTE**: We can use pretty() after the method find to improve the data displaying.
___

	db.movies.find({"year": "2005"}).pretty()

**b - Provide all the titles of the 2005 films.**

We can add a filter in the method find at the second parameter.

    db.movies.find({"year": "2005"}, {"title": 1}).pretty()

	//Result
	{
		"_id" : ObjectId("5f89670dea8369143de5e8b0"),
		"title" : "A History of Violence"
	}

We use 1 to say we want to have this field. By default, the mongoDB id is always retrieve. 

**c - We still get _id. How to remove it with what we have just seen.**

We can precise in our requests if we want or not to retrieve id.

	db.movies.find({"year": "2005"}, {"title": 1, "_id": 0}).pretty() 

	//Result of request
	{ "title" : "A History of Violence" }

Thanks to this command, we just retrieve the title of all films which were produced in 2005.

**d - Display the summary of «The Social network».**

	db.movies.find({"title": "The social network"}, {"summary": 1, "_id": 0}).pretty()

**e - Retrieve the director of «A History of Violence».**

	db.movies.find({"title": "A History of Violence"}, {"director": 1, "_id": 0}).pretty()

**f - Retrieve the actors of «The Social network».**

	db.movies.find({"title": "The social network"}, {"actors": 1, "_id": 0}).pretty()

**g - Display the list of movies where Cronenberg is the director's name.**

	db.movies.find({"director.last_name": "Cronenberg"}).pretty()

**h - Display the names (last name) of directors whose first name is David.**

	db.movies.find({"director.first_name": "David"}, {"director.last_name": 1}).pretty()

**i - Display the list of films where "Vigo" is the first name of an actor.**

More difficult because we filter a sub-document, element of a table. We have to use a new syntaxe : 

	{arrayName: {$elemMatch: {key: value, ..}}}

	db.movie.find({"actors": {$elemMatch : {"first_name": "Vigo"}}}).pretty()

**j - Display movies whose year is >= to 2005.**

	db.movies.find({"year": {$gte: "2005"}}).pretty()

**k - Retrieve the names of the directors that were born after 1960.**

	db.movies.find({"director.birth_date": {$gte: "1960"}}, {"director.first_name": 1, "director.last_name": 1, "_id": 0}).pretty()

**l - Display the names of the films for which there is a director.**

Like MongoDB is a schemaless store, the condition to know if a key exists is simple. We can use the following syntaxe :

	{field: {$exists: 1}} // to verify is field exists
	{field: {$exists: 0}} // to verify is field doesn't exist

If we want the name of films which have directors we can write this requests :

	db.movies.find({"director": {$exists: 1}}, {"title": 1, "_id": 0}).pretty()	

**m - Display films of the genre Drama or those with a summary.**

With mongoDB, there are some operators which begin by `$` :

    $or
    $and
    $type
    $not 

... 

In our case, we just use the `$or` operator.

	db.movies.find({$or: [{"genre": "Drama"}, {"summary": {$exists: 1}}]}).pretty()


**n - You insert a new 2007 movie called “Into the Wild”, Drama directed by “Sean Penn”. Check how many doc there is in the database.**

	film = {
		"title": "Into the wild", 
		"year": "2007", 
		"genre": 
		"Drama", 
		"director": {
			"first_name": "Sean", 
			"last_name": "Penn"
		}
	}
	db.movies.insert(film)

	db.movies.count() //Count doc in database

**o -You need to add data on Into The wild: the summary: "After graduating from Emory University, top student and athlete Christopher McCandless abandons his possessions, gives his entire $ 24,000 savings account to charity and hitchhikes to Alaska to live in the wilderness. Along the way, Christopher encounters a series of characters that shape his life."**

In order to realize this task, we can create a variable to fetch the movie. Then we can use it id and the method `update`.

	var into = db.movies.findOne({"title": "Into the wild"}) // Retrieve film
	into.summary = "After graduating from Emory University, top student and athlete 
	Christopher McCandless abandons his possessions, gives his entire $ 24,000 savings 
	account to charity and hitchhikes to Alaska to live in the wilderness.
	Along the way, Christopher encounters a series of characters that shape his life."
	db.movies.update({"_id": into._id}, into)

 Add an empty array for actors.

	into.actors = []
	db.movies.update({"_id": into._id}, into)
	

**p - Remove the actor key in that movie :**

	db.movies.update({"_id": into._id},{$unset : {actors:1}})

**q - We now add actors to the Into the Wild movie : Emilie Hirch and William Hurt.**

	var into = db.movies.findOne({"title": "Into the wild"}) // Retrieve film
	db.movies.update(
		{"_id": into._id}, 
		{$addToSet: {actors: {"last_name":"Hirch","first_name":"Emile"}}}
	)

___
**NOTE**: The `$addToSet` operator adds a value to an array unless the value is already present, in which case $addToSet does nothing to that array.
___

**r - To remove a whole document : db.movies.remove({id}). Remove the Into the Wild movie.**

	var into = db.movies.findOne({"title": "Into the wild"}) // Retrieve film
	db.movies.remove(into._id)


