import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class Drug represents drug as defined in <i>cis.sql</i> file.
 */
public class Drug {
    private final String cip7;
    private final String cip13;
    private final String denom;
    private final String cis;
    private final String statadm;
    private final String etacom;
    private final String prese;
    private final String datedecl;
    private final String agrcol;
    private final String prixe;
    private final String liste;
    private final String taux;


    public Drug(
            String cip7,
            String cip13,
            String denom,
            String cis,
            String statadm,
            String etacom,
            String prese,
            String datedecl,
            String agrcol,
            String prixe,
            String liste,
            String taux
    ) {
        this.cip7 = cip7;
        this.cip13 = cip13;
        this.cis = cis;
        this.statadm = statadm;
        this.etacom = etacom;
        this.prese = prese;
        this.datedecl = datedecl;
        this.agrcol = agrcol;
        this.prixe = prixe;
        this.liste = liste;
        this.taux = taux;
        this.denom = denom;
    }

    /**
     * Create a drug from a Postgresql result
     * @param res - a result set from Postgresql
     * @return a new Drug
     * @throws SQLException
     */
    static public Drug mapResultSetToDrug(ResultSet res) throws SQLException {
        return new Drug(
            res.getString("cip7"),
            res.getString("cip13"),
            res.getString("denom"),
            res.getString("cis"),
            res.getString("statadm"),
            res.getString("etacom"),
            res.getString("prese"),
            res.getString("datedecl"),
            res.getString("agrcol"),
            res.getString("prixe"),
            res.getString("liste"),
            res.getString("taux")
        );
    }

    /**
     * Get data to insert in the cache
     * @return data to insert in the cache as a maps
     */
    public Map<String, String> toMap() {
        Map<String,String> map = new HashMap<>();
        map.put("cis", cis);
        map.put("name", denom);
        return map;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "cip7='" + cip7 + '\'' +
                ", cip13='" + cip13 + '\'' +
                ", cis='" + cis + '\'' +
                ", denom='" + denom + '\'' +
                ", statadm='" + statadm + '\'' +
                ", etacom='" + etacom + '\'' +
                ", prese='" + prese + '\'' +
                ", datedecl='" + datedecl + '\'' +
                ", agrcol='" + agrcol + '\'' +
                ", prixe='" + prixe + '\'' +
                ", liste='" + liste + '\'' +
                ", taux='" + taux + '\'' +
                '}';
    }
}
