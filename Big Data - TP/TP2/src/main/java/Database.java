import java.sql.*;
import java.util.Optional;

/**
 * Database is an implementation of a database with postgresql
 */
public class Database {
    private Optional<Connection> connection;

    public Database(final String url, final String user, final String password) {
        this.connection = DatabaseConnection.getConnection(url, user, password);
    }

    /**
     * Execute query on database
     * @param query - query to be executed as String
     * @return the result as Optional object
     * @throws SQLException
     */
    public Optional<ResultSet> query(String query) throws SQLException {
        if (connection.isPresent()) {
            Statement st = connection.get().createStatement();
            return Optional.of(st.executeQuery(query));
        }
        return Optional.empty();
    }

    /**
     * Close database connection
     * @throws SQLException
     */
    public void close() throws SQLException {
        if (connection.isPresent()) {
            connection.get().close();
        }
    }

}
