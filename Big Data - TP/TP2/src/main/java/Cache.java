import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Cache is an implementation of a simple cache with Jedis.
 */
public class Cache {
    private JedisPool pool;
    private Jedis jedis;
    private final long maxSize;
    private long size;

    /**
     * KeyType is an enum which simplify the the type of data associated to a key.
     */
    enum KeyType {
        NONE,
        STRING,
        HASH,
        LIST,
        SET;

        /**
         * Get the type in according to a string
         * @param type - the type of data as string
         * @return an equivalence of the type as KeyType
         */
        public static KeyType getKeyType(final String type) {
            switch(type) {
                case "string": return STRING;
                case "hash": return HASH;
                case "list": return LIST;
                case "set": return SET;
                case "none":
                default: return NONE;
            }
        }
    }

    /**
     * Instantiate a new cache object
     */
    public Cache(long maxSize) {
        pool = new JedisPool(new JedisPoolConfig(),"localhost");
        jedis = pool.getResource();
        this.maxSize = maxSize;
        this.size = getCacheSize();
    }

    /**
     * Get the cache size
     * @return the cache size as integer
     */
    public long getSize() {
        return size;
    }

    /**
     * Get the type of data associated to the key <i>key</i>
     * @param key - the key we want to check type
     * @return the type of value represented by the key as <b>KeyType</b>
     */
    public KeyType getType(String key) {
        return KeyType.getKeyType(jedis.type(key));
    }

    /**
     * Insert new values associated to a key in the cache
     * @param key - the key to index new values
     * @param values - the values we want to add
     */
    public boolean insert(String key, Map<String, String> values) {
        if (size < maxSize) {
            if (jedis.hmset(key, values).equals("OK")) {
                jedis.zadd("time:drug", System.nanoTime() * 1.0, key);
                size++;
                return true;
            }
        }
        // Del the least drug
        else {
            String keyToDelete = getOldestValue();
            System.out.println("Cache is full ! We delete drug " + keyToDelete);
            if (jedis.del(keyToDelete) > 0) {
                if (jedis.hmset(key, values).equals("OK")) {
                    jedis.zadd("time:drug", System.nanoTime() * 1.0, key);
                    jedis.zpopmin("time:drug");
                    size++;
                    return true;
                }
            }
        }
        return false;
    }

    public void updateTimestamp(String drugId) {
        jedis.zrem("time:drug", drugId);
        jedis.zadd("time:drug", System.nanoTime() * 1.0, drugId);
    }

    private String getOldestValue() {
        Set<String> set = jedis.zrange("time:drug", 0, -1);
        Iterator<String> iter = set.iterator();
        return iter.next();
    }

    /**
     * Insert a new value associated to a key in the cache
     * @param key - the key to index new value
     * @param value - the value we want to add
     */
    public boolean insert(String key, String value) {
        if (size < maxSize) {
            if (jedis.set(key, value).equals("OK")) {
                size++;
                return true;
            }
        } else {
            System.out.println("Cache is full");
        }
        return false;
    }

    /**
     * Check if cache contains the key <i>key</i>
     * @param key - the key we want to check
     * @return - true if cache contains key else false
     */
    public boolean containsKey(String key) {
        return fetch(key).size() > 0;
    }

    /**
     * Fetch data according to the key <i>key</i>
     * @param key - the key we want to search
     * @return a map of values associated to the key
     */
    public Map<String, String> fetch(String key) {
        return jedis.hgetAll(key);
    }

    /**
     * Fetch data according to the key <i>key</i>
     * @param key - the key we want to search
     * @return the value as string associated to the key
     */
    public String fetchSimpleData(String key) {
        return jedis.get(key);
    }

    /**
     * Get all keys in the 'table' <i>table</i>
     * Example : getKeys('users') return all users in table users
     * @param table - the table searched
     * @return a set of keys found
     */
    public Set<String> getKeys(String table) {
        return jedis.keys(table + ":*");
    }

    private long getCacheSize() {
        return jedis.dbSize() - getKeys("time:drug").size();
    }

    public void clear() {
        jedis.flushAll();
        size = getCacheSize();
    }

    /**
     * Clear the pool
     */
    public void destroy() {
        pool.destroy();
    }

}
