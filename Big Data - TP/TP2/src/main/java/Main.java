import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Main {

    private static Optional<Drug> getDrugWithDatabase(Database database, String drugId) throws SQLException {
        Optional<ResultSet> result = database.query("SELECT * FROM bdpm_ciscip, cis WHERE bdpm_ciscip.cis = cis.cis AND cip7 = " + drugId);
        if (result.isPresent()) {
            result.get().next();
            return Optional.of(Drug.mapResultSetToDrug(result.get()));
        }
        return Optional.empty();
    }

    private static void getDrugWithCache(Cache cache, String drugId) {
        Map<String, String> map = cache.fetch(drugId);
        System.out.println(drugId);
        map.forEach((k, v) -> System.out.println("\t" + k + " : " + v));
    }


    public static void main(String[] args) throws SQLException {

        Cache cache = new Cache(3);
        Database database = new Database("jdbc:postgresql://localhost/tp2", "tp2", "5VQan5FK");

        long cacheSize = cache.getSize();
        if (cacheSize > 0) {
            cache.clear();
        }

        System.out.println("Cache size " + cache.getSize());

        Scanner scanner =  new java.util.Scanner(System.in);

        while(true) {

            System.out.print("Drug id : ");
            String id = scanner.next();

            if (id.equals("quit")) {
                break;
            }

            if (!cache.containsKey("drug:" + id)) {
                Optional<Drug> drug =  getDrugWithDatabase(database, id);
                if (!drug.isPresent()) {
                    System.out.println("Drug:" + id + " doesn't exist" );
                }

                drug.ifPresent(value -> {
                    // Show cache if insert successfully
                    if (cache.insert("drug:" + id, value.toMap())) {
                        cache.getKeys("drug").forEach(key -> getDrugWithCache(cache, key));
                    }
                });
            }
            // Update timestamp for drugId
            else {
                System.out.println("Drug:" + id + " is already in cache");
                cache.updateTimestamp("drug:" + id);
            }

            System.out.println("Cache size " + cache.getSize());

        }

        System.out.println("GoodBye ! - Cache size " + cache.getSize());

        cache.destroy();
        database.close();
    }
}
