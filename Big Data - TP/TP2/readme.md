# CACHING WITH REDIS & PERFORMANCE COMPARISON WITH POSTGRESQL

## INSTALL REDIS ON MAC 

    wget http://download.redis.io/releases/redis-6.0.8.tar.gz
    tar xzf redis-6.0.8.tar.gz
    cd redis-6.0.8
    make
    
## INSTALL POSTGRESQL ON MAC

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew update
    brew install postgresql

## RUN REDIS SERVER & CLIENT

    ./src/redis-server
    
    ./src/redis-cli
    
## PREPARE POSTGRESQL ENVIRONMENT

Create user named tp2 with a password

    CREATE USER tp2 WITH PASSWORD '5VQan5FK';
    
Create database named tp2

    CREATE DATABASE tp2;
    
Run postgresql 

    psql -h localhost -d tp2 -u tp2
    
Insert tables

    \i ressources/cis.sql
    \i ressources/cispi.sql
   
   
## SOME COMMANDS FOR REDIS

### Set key/value

    set user:1 "joe"

### Get value according to a key

    get user:1
    
### Get value when key is unknown

    get user:2
    -> (nil) // return (nil) when key is not in db
    
### Set key/object 

We can use a a hash structure

    HSET user:1000 name "John Smith"
    HSET user:1000 email "john.smith@example.com"
    HSET user:1000 password "s3cret"
    
### Set key/object in one-line

    HMSET user:1001 name "Mary Jones" password "hidden" email "mjones@example.com"
    
### Get object according to a key

    HGETALL user:1001
    
## CACHE

A cache is a memory allocation of application or computer terminal which stocks temporarily some data which will be probably fetch.
The objective is to improve performance when we want to fetch data. Databases which use the key/value like redis system are appropriate.
When a request is executed, system checks in the cache if data exists. If data is found, we say there is a cache-hit. Else this is a cache-miss 
and system have to read data since the database. An efficient system is a system with a good cache-ration (ratio between cache-hit and cache-miss).

## USE REDIS WITH JAVA

For this TP we use Jedis to create a cache database. It's a Java client to use redis with java. 
In maven project we can add dependency : 

     <dependency>
        <groupId>redis.clients</groupId>
        <artifactId>jedis</artifactId>
        <version>3.1.0</version>
     </dependency>

In order to begin a connection, add the following code in your java class : 

    private JedisPool pool = new JedisPool(new JedisPoolConfig(),"localhost");
    
## USE POSTGRESQL WITH JAVA

For this TP we use postgresql as main database. In maven project we can add dependency :

    <dependency>
        <groupId>postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <version>9.1-901-1.jdbc4</version>
    </dependency>
    
## NOTES : 

The Jedis implementation in this TP is very simple and not really correct. 
We are deadlock on transactions and many exceptions catch.

See https://gist.github.com/allwefantasy/3615650 to have an idea about correct implementation.