package database.dto;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import org.bson.Document;

public class MovieDTO {
    private String title;
    private long id;
    private String releaseDate;
    private int releaseYear;

    public MovieDTO(String title, long id, String releaseDate, int releaseYear) {
        this.title = title;
        this.id = id;
        this.releaseDate = releaseDate;
        this.releaseYear = releaseYear;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public DBObject toDBOObject() {
        BasicDBObjectBuilder docBuilder = BasicDBObjectBuilder.start();

        docBuilder.append("title", title);
        docBuilder.append("id", id);
        docBuilder.append("release_date", releaseDate);
        docBuilder.append("release_year", releaseYear);
        return docBuilder.get();
    }

    public Document toDocument() {
        Document docBuilder = new Document();

        docBuilder.append("title", title);
        docBuilder.append("id", id);
        docBuilder.append("release_date", releaseDate);
        docBuilder.append("release_year", releaseYear);
        return docBuilder;
    }
}
