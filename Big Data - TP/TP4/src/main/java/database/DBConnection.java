package database;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class DBConnection {
    private final MongoClient client;

    public DBConnection() {
        this.client = new MongoClient();
    }

    public DBConnection(String host) {
        this.client = new MongoClient(host);
    }

    public DBConnection(String host, int port) {
        this.client = new MongoClient(host, port);
    }

    public MongoDatabase getDatabase(String dbName) {
        return client.getDatabase(dbName);
    }

    public void close() {
        client.close();
    }
}
