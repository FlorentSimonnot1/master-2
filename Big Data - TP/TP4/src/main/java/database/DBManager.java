package database;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;

public class DBManager {
    private final DBConnection client;
    private final MongoDatabase db;

    public DBManager(String host, int port, String database) {
        this.client = new DBConnection(host, port);
        this.db = new DBConnection(host, port).getDatabase(database);
    }

    public FindIterable<Document> find(String collection, Bson doc) {
        return db.getCollection(collection).find(doc);
    }

    /**
     * Insert a document <i>doc</i> in a collection <i>collection</i>
     * @param collection the collection
     * @param doc the document
     */
    public void insert(String collection, Document doc) {
        db.getCollection(collection).insertOne(doc);
    }

    public Document update(String collection, Document doc, Document newDoc, Boolean upsert) {
        return db.getCollection(collection).findOneAndUpdate(doc, newDoc, new FindOneAndUpdateOptions().upsert(upsert));
    }

    /**
     * Remove a document from a collection
     * @param collection the collection
     * @param doc the document we want to remove
     * @return the document removed
     */
    public Document remove(String collection, Document doc) {
        return db.getCollection(collection).findOneAndDelete(doc);
    }

    /**
     * Count documents in a collection
     * @param collection the collection we want to search
     * @return number of documents as long
     */
    public long countDocuments(String collection) {
        return db.getCollection(collection).countDocuments();
    }

    /**
     * Close connexion with the client
     */
    public void close() {
        client.close();
    }

}
