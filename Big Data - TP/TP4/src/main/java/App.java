import database.DBManager;
import database.dto.MovieDTO;
import org.bson.Document;

public class App {

    public static void main(String[] args) {
        //MongoDatabase db = new DBConnection("localhost", 27017).getDatabase("moviedb");
         /*FindIterable<Document> results = db.getCollection("movies").find();
         for(Document result : results) {
            System.out.println(result.toJson());
         }*/

        /*BasicDBObject query = new BasicDBObject("release_date",
            new BasicDBObject(
                "$gte",
                LocalDateTime
                .of(1995, Month.JANUARY, 1, 0, 0)
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            )
            .append(
                "$lt",
                LocalDateTime
                .of(1996, Month.JANUARY, 1, 0, 0)
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            )
        );*/

        /*BasicDBObject query = new BasicDBObject("release_year", 1995);


        FindIterable<Document> results = db.getCollection("movies").find(query);
        for(Document result : results) {
            System.out.println(result.toJson());
        }*/

        /*long nDocs = db.getCollection("movies").countDocuments();
        Document doc = new Document().append("title", "Logan").append("release_year", 2017).append("id", nDocs);
        db.getCollection("movies").insertOne(doc);*/

        /*BasicDBObject query = new BasicDBObject("title", "Logan");

        Document update = new Document();
        update.append("$set", new Document("release_date", "2017-03-03"));

        Document result = db.getCollection("movies").findOneAndUpdate(query, update, new FindOneAndUpdateOptions().upsert(true));
        System.out.println("New data : " + (result != null ? result.toJson() : "/No update/"));*/

        /*Document query = new Document("title", "Logan");
        Document result = db.getCollection("movies").findOneAndDelete(query);
        System.out.println("Delete data : " + (result != null ? result.toJson() : "/No delete/"));*/

        DBManager db = new DBManager("localhost", 27017, "moviedb");


        //MovieDTO movie = new MovieDTO("Logan", db.countDocuments("movies"), "", 2017);
        //db.insert("movies", movie.toDocument());

        //Document update = new Document();
        //update.append("$set", new Document("release_date", "2017-03-03"));
        //db.update("movies", movie.toDocument(), update, true);

        //db.remove("movies", new Document().append("title", "Logan"));

        //FindIterable<Document> results = db.find("movies", new BasicDBObject("release_year", 1995));
        /*for(Document result : results) {
            System.out.println(result.toJson());
        }*/

        db.close();
    }
}
