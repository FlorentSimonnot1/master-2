package calculator;

import person.Person;

import java.rmi.RemoteException;

public class CalculatorImpl
        extends java.rmi.server.UnicastRemoteObject
        implements Calculator {


    public CalculatorImpl() throws RemoteException {
        super();
    }

    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int sub(int a, int b) {
        return a - b;
    }

    @Override
    public int mul(int a, int b) {
        return a * b;
    }

    @Override
    public int div(int a, int b) {
        return a / b;
    }

    @Override
    public int addAge(Person p1, Person p2) {
        return p1.getAge() + p2.getAge();
    }
}
