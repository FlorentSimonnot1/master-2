package calculator;

import person.Person;

public interface Calculator extends java.rmi.Remote {

    int add(int a, int b) throws java.rmi.RemoteException;
    int sub(int a, int b) throws java.rmi.RemoteException;
    int mul(int a, int b) throws java.rmi.RemoteException;
    int div(int a, int b) throws java.rmi.RemoteException;
    int addAge(Person p1, Person p2);
}
