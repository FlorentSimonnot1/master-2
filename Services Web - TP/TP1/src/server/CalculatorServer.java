package server;

import calculator.Calculator;
import calculator.CalculatorImpl;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class CalculatorServer {

    public CalculatorServer() {
        try {
            LocateRegistry.createRegistry(1100);
            Calculator c = new CalculatorImpl();
            Naming.rebind("CalculatorService", c);
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }

    public static void main(String[] args) {
        new CalculatorServer();
    }
}
