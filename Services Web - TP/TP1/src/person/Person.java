package person;

import java.io.Serializable;

public class Person implements Serializable {
    private final String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() { return age; }
}
