package client;

import models.Book;
import models.IDB;

import java.rmi.Naming;
import java.rmi.RemoteException;

public class BiblioClient {

    public static void main(String[] args) {
        try {
            IDB db = (IDB) Naming.lookup("rmi://localhost:1111/Biblio");
            db.addBook(new Book("The dead Zone", 1234567899, "Stephen King"));
            db.addBook(new Book("The dark knight return", 987654321, "Franck Miller"));
            System.out.println("db size " + db.getBooks().size());
            db.getBooks().forEach((k, v) -> {
                try {
                    System.out.println(k + " : " + v.getName() + " / " + v.getAuthor());
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Book deleted " + db.deleteBook(987654321));
            System.out.println("Book deleted " + db.deleteBook(987654322));
        } catch(Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }
}
