package models;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Book extends UnicastRemoteObject implements IBook {
    private String name;
    private final long ISBN;
    private final String author;

    public Book(String name, long ISBN, String author) throws RemoteException {
        super();
        this.name = name;
        this.ISBN = ISBN;
        this.author = author;
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }

    @Override
    public long getISBN() throws RemoteException {
        return ISBN;
    }

    @Override
    public String getAuthor() throws RemoteException {
        return author;
    }

    @Override
    public void setName(String name) throws RemoteException {
        this.name = name;
    }

}
