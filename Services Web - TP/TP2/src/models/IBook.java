package models;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBook extends Remote {

    String getName() throws RemoteException;
    long getISBN() throws RemoteException;
    String getAuthor() throws RemoteException;
    void setName(String name) throws RemoteException;
}
