package models.observables;

import java.util.List;

public interface Observable {

    void addObservator(List<Observator> list);
    void removeObservator(List<Observator> list);
}
