package models.observables;

public interface Observator {

    void notifyAllObservator();

}
