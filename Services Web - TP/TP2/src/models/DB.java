package models;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DB extends UnicastRemoteObject implements IDB {
    final private Map<Long, IBook> books = new HashMap<>();

    public DB() throws RemoteException {
        super();
    }

    @Override
    public void addBook(IBook b) throws RemoteException {
        b.setName("TROLLLL");
        books.put(b.getISBN(), b);
    }

    @Override
    public Optional<IBook> search(String search) throws RemoteException {
        for(IBook b : books.values()) {
            if (b.getName().equals(search) || b.getAuthor().equals(search)) {
                return Optional.of(b);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean deleteBook(long ISBN) {
        return books.remove(ISBN) != null;
    }

    public Map<Long, IBook> getBooks() {
        return books;
    }
}
