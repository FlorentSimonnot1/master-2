package models;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Optional;

public interface IDB extends Remote {
    void addBook(IBook b) throws RemoteException;
    Optional<IBook> search(String search) throws RemoteException;
    boolean deleteBook(long ISBN) throws RemoteException;
    Map<Long, IBook> getBooks() throws RemoteException;
}
