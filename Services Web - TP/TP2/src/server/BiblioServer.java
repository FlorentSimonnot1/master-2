package server;

import models.DB;
import models.IDB;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class BiblioServer {

    public static void main(String[] args) {

        try {
            LocateRegistry.createRegistry(1111);
            IDB idb = new DB();
            Naming.rebind("rmi://localhost:1111/Biblio", idb);
        } catch(Exception e) {
            System.out.println("Error " + e.getMessage());
        }

    }
}
