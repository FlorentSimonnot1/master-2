package fr.uge.tp3

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import fr.uge.tp3.utils.CardLoader

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("fr.uge.tp3", appContext.packageName)
    }

    @Test
    fun testCardLoader() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val cards = CardLoader.loadCards(appContext, "logos")
        assertEquals(cards!!.size, 32)
    }
}