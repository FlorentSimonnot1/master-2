package fr.uge.tp3

import fr.uge.tp3.models.Card
import fr.uge.tp3.utils.CardLoader
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testGetName() {
        val card = Card("./assets/logos/amazon.png")
        assertEquals(card.getName(), "amazon")
    }
}