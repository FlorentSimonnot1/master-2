package fr.uge.tp3.database.repositories

/**
 * [Repository] is a generic interface which declare methods for a repository
 *
 * @param T
 */
interface Repository<T> {

    /**
     * Opens the Database instance
     *
     */
    fun open()

    /**
     * Closes the Database instance
     *
     */
    fun close()

    /**
     * Gets the query which creates the table
     *
     * @return a SQL query for creates a table
     */
    fun create(): String

    /**
     * Gets the query which deletes the table
     *
     * @return a SQL query for deletes a table
     */
    fun drop(): String

    /**
     * Adds an entity in the table
     *
     * @param entity - the entity we want to add
     */
    fun add(entity: T)

    /**
     * Gets all data from the database table
     *
     * @return all data from the database table as a list of entity
     */
    fun getAll(): List<T>
}