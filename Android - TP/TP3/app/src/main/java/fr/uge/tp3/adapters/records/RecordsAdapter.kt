package fr.uge.tp3.adapters.records

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import fr.uge.tp3.R
import fr.uge.tp3.utils.TimeFormat

class RecordsAdapter(
    private val ctx: Context,
    private val items: List<RecordItem>
): RecyclerView.Adapter<RecordsAdapter.RecordAdapterViewHolder>() {

    inner class RecordAdapterViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val cardView: CardView = view.findViewById(R.id.cardViewRecord)
        val place: TextView = view.findViewById(R.id.place)
        val time: TextView = view.findViewById(R.id.time)
        val attempts: TextView = view.findViewById(R.id.attempts)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.records_item_layout, parent, false) as View
        Log.d("VIEW", "$itemView")
        return RecordAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecordAdapterViewHolder, position: Int) {
        val record = items[position]

        holder.place.text = "${position + 1}"
        holder.time.text = ctx.resources.getString(R.string.timer_win, TimeFormat.format(record.time))
        holder.attempts.text = ctx.resources.getString(R.string.attempts_win, record.attempt)
    }

    override fun getItemCount(): Int = items.size
}