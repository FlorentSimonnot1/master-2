package fr.uge.tp3.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.uge.tp3.R
import fr.uge.tp3.adapters.records.RecordItem
import fr.uge.tp3.adapters.records.RecordsAdapter
import fr.uge.tp3.database.entities.RecordEntity
import fr.uge.tp3.database.repositories.Records
import fr.uge.tp3.utils.CardsNumber

/**
 * [RecordsActivity] is an activity to show records
 *
 */
class RecordsActivity : AppCompatActivity() {
    private lateinit var recordsRecyclerView: RecyclerView
    private lateinit var emptyList: TextView
    private var selectedFilter = 0
    private val filtersButton: IntArray = intArrayOf(
        R.id.cards8, R.id.cards12, R.id.cards16, R.id.cards24, R.id.cards32
    )

    private lateinit var myAdapter: RecordsAdapter

    private lateinit var recordsEntity: List<RecordEntity>
    private lateinit var recordsItem: List<RecordItem>

    private lateinit var recordsRepository: Records

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState != null) {
            selectedFilter = savedInstanceState.getInt("selectedFilter")
        }

        recordsRecyclerView = findViewById(R.id.recyclerViewRecords)
        emptyList = findViewById(R.id.emptyRecords)

        recordsRepository = Records(this)

        recordsEntity = recordsRepository.getAll()
        prepareFilters()
        filterRecordItems()
        myAdapter = RecordsAdapter(this, recordsItem)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recordsRecyclerView.apply {
            setHasFixedSize(true)
            adapter = myAdapter
            layoutManager = llm
        }
        updateEmptyListMessage()
    }

    // Prepares the filters buttons (text, background...)
    private fun prepareFilters() {
        filtersButton.forEachIndexed { index, id ->
            val button: Button = findViewById(id)
            button.text = CardsNumber.getCardNumbersList("cards")[index]
            prepareDrawable(button, index)
            button.setOnClickListener {
                // Remove check icon from older button
                val currentButton = findViewById<Button>(filtersButton[selectedFilter])
                currentButton.setCompoundDrawables(null, null, null, null)
                currentButton.setBackgroundResource(R.drawable.button_filter_not_selected)
                currentButton.setTextColor(ContextCompat.getColor(this, R.color.colorFilter))
                selectedFilter = index
                prepareDrawable(button, index)
                filterRecordItems()
                myAdapter = RecordsAdapter(this, recordsItem)
                recordsRecyclerView.adapter = myAdapter
                updateEmptyListMessage()
            }
        }
    }

    // Prepares the items used by the adapter
    private fun filterRecordItems() {
        val cardsNumber = CardsNumber.numbers[selectedFilter]
        recordsEntity.forEach { i -> Log.d("E", "$i") }
        if (cardsNumber > -1) {
            recordsItem = recordsEntity
                .filter { entity -> entity.cardsNumber == cardsNumber }
                .map { recordEntity -> RecordItem(
                    recordEntity.time,
                    recordEntity.attempt
                )}
                .sortedBy { it.time }
        }
    }

    // Prepares the filters button drawable
    private fun prepareDrawable(button: Button, index: Int) {
        if (index == selectedFilter) {
            button.setBackgroundResource(R.drawable.button_filter)
            button.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_correct), null, null, null)
            button.compoundDrawablePadding = 10
        } else {
            button.setBackgroundResource(R.drawable.button_filter_not_selected)
        }
        button.setPadding(10, 0, 10, 0)
        val color = if (index == selectedFilter) { R.color.colorFilterSelected } else R.color.colorFilter
        button.setTextColor(ContextCompat.getColor(this, color))
    }

    // Updates message when there are no records
    private fun updateEmptyListMessage() {
        emptyList.visibility = if (recordsItem.isEmpty()) { View.VISIBLE } else View.GONE
        recordsRecyclerView.visibility = if (emptyList.visibility == View.GONE) { View.VISIBLE} else View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("selectedFilter", selectedFilter)
    }
}