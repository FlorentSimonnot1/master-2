package fr.uge.tp3.database.entities

import android.content.ContentValues
import fr.uge.tp3.database.repositories.Records
import java.util.*

/**
 * [RecordEntity] is a data class which represents a record
 *
 * @property uuid - the record ID. Created by default
 * @property time - the time of the record
 * @property cardsNumber - the cards number of the record
 * @property attempt - the attempt number of the record
 */
data class RecordEntity(val uuid: String = UUID.randomUUID().toString(), val time: Long, val cardsNumber: Int, val attempt: Int): Entity {

    /**
     * Converts data as [ContentValues]
     *
     * @return - the record as [ContentValues]
     */
    override fun write(): ContentValues {
        val content = ContentValues()
        content.put(Records.recordId, uuid)
        content.put(Records.recordTime, time)
        content.put(Records.recordCardsNumber, cardsNumber)
        content.put(Records.recordAttempt, attempt)
        return content
    }
}