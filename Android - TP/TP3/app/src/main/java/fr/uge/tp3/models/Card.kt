package fr.uge.tp3.models

import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.io.Serializable

/**
 * [Card] represents a card
 *
 * @property path - the path where we can found the card asset
 */
data class Card(val path: String): Serializable {

    /**
     * Gets the card name
     *
     * @return the name of the card as string
     */
    fun getName(): String {
        return path.substringBeforeLast('.').substringAfterLast('/')
    }

    /**
     * Gets the card as BitMap
     *
     * @param asset - the [AssetManager]
     * @return the card as [Bitmap]
     */
    fun getBitmap(asset: AssetManager): Bitmap? {
        var bitmap: Bitmap? = null
        var fileStream: InputStream? = null

        try {
            fileStream = asset.open(path)
            bitmap = BitmapFactory.decodeStream(fileStream)
        } catch(e: IOException) {
            Log.e("FILE ERROR", "Fail to open asset $e")
        } finally {
            fileStream?.close()
        }
        return bitmap
    }
}