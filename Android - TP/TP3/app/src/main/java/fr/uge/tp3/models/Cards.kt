package fr.uge.tp3.models

import fr.uge.tp3.adapters.cards.CardItem
import java.io.Serializable

/**
 * [Cards] provides a serializable wrapper of [CardItem] collection
 * Useful to keep cards in saved bundle instance
 *
 * @property cards - the list of cards item
 */
data class Cards(val cards: List<CardItem>): Serializable {}