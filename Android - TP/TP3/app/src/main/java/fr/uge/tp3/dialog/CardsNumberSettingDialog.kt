package fr.uge.tp3.dialog

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.NumberPicker
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import fr.uge.tp3.R
import fr.uge.tp3.utils.CardsNumber

/**
 * [CardsNumberSettingDialog] represents a dialog to choose the cards number
 *
 * @property onValidateListener - a listener of type [OnValidate]
 * @property currentValue - the current cards number
 */
class CardsNumberSettingDialog(
    private val onValidateListener: OnValidate,
    private val currentValue: Int
): DialogFragment() {
    private lateinit var okButton: Button
    private lateinit var cancelButton: Button
    private lateinit var cardsNumberPicker: NumberPicker
    private lateinit var cardsNumber: Array<String>

    /**
     * Creates the dialog view
     *
     * @param inflater - the layout inflater
     * @param container - the view group
     * @param savedInstanceState - the application bundle
     * @return the dialog as [View]
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.setting_cards_number_dialog, container, false)

        okButton = customView.findViewById(R.id.ok_dialog_button)
        cancelButton = customView.findViewById(R.id.cancel_dialog_button)
        cardsNumberPicker = customView.findViewById(R.id.cards_number_picker)

        cardsNumber = CardsNumber.getCardNumbersList(resources.getString(R.string.card))

        cardsNumberPicker.minValue = 0
        cardsNumberPicker.maxValue = cardsNumber.size - 1
        cardsNumberPicker.displayedValues = cardsNumber
        cardsNumberPicker.value = cardsNumber.indexOf(cardsNumber.find { value -> value.contains("$currentValue") })

        okButton.setOnClickListener {
            onValidateListener.cardsNumber(CardsNumber.getCardsNumber(cardsNumber[cardsNumberPicker.value]))
            dismiss()
        }

        cancelButton.setOnClickListener {
            dismiss()
        }

        return customView
    }

    /**
     * [OnValidate] is an interface used when user want to confirm his choise
     *
     */
    interface OnValidate {
        /**
         * Fixes the cards number
         *
         * @param number - the cards number
         */
        fun cardsNumber(number: Int)
    }

    /**
     * Sets the width of the dialog to match parent
     * Centers the dialog
     *
     */
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
    }



}