package fr.uge.tp3.adapters.cards

import fr.uge.tp3.models.Card
import java.io.Serializable

/**
 * [CardItem] is a class which represents is used in the adapter
 *
 * @property card - the card
 * @property visible - if the card is visible or hidden
 * @property found - if the card was found or not
 */
data class CardItem(val card: Card, var visible: Boolean = false, var found: Boolean = false): Serializable