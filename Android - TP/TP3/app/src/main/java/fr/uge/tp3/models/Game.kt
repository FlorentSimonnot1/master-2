package fr.uge.tp3.models

import android.content.Context
import android.os.Bundle
import fr.uge.tp3.adapters.cards.CardItem
import fr.uge.tp3.preferences.GamePreferences
import fr.uge.tp3.utils.CardLoader
import java.io.Serializable

/**
 * [Game] represents a game
 *
 * @property cards - the game cards
 * @property time - the time of the party
 * @property attempts - the number of attempts realized to win
 */
class Game(val cards: Cards, var time: Long = 0, var attempts: Int = 0): Serializable {

    companion object {

        /**
         * Creates a game object
         * Creates a game in according to the preferences (Number of cards, Deck) and reload the game if bundle exists
         *
         * @param context - the application context
         * @param bundle - the bundle application
         * @return a game object
         */
        fun prepareGame(context: Context, bundle: Bundle?): Game {
            return if (bundle != null) {
                bundle["game"] as Game
            } else {
                val preferences = GamePreferences(context)
                var cards = CardLoader.loadCards(context, preferences.getDeck()!!)!!
                cards = cards.subList(0, preferences.getCardsNumber())
                // Concat the list with itself to duplicate cards
                cards = listOf(cards, cards).flatten().shuffled()
                Game(Cards(cards.map { CardItem(it) }))
            }
        }
    }

    /**
     * Checks if the game is over
     * Checks all cards. Game is over when all cards are foreground
     *
     * @return true if the game is over. False else
     */
    fun gameIsOver(): Boolean {
        return cards.cards.all { card -> card.found }
    }
}