package fr.uge.tp3.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import fr.uge.tp3.R
import fr.uge.tp3.database.DBHelper

/**
 * [MainActivity] is the main activity
 * Display a menu for the player
 *
 */
class MainActivity : AppCompatActivity() {
    private lateinit var playButton: Button
    private lateinit var optionsButton: Button
    private lateinit var recordsButton: Button
    private lateinit var dbHelper: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHelper = DBHelper(this)
        playButton = findViewById(R.id.play_button)
        optionsButton = findViewById(R.id.options_button)
        recordsButton = findViewById(R.id.records_button)

        playButton.setOnClickListener {
            val intent = Intent(this, MemoryActivity::class.java)
            startActivity(intent)
            finish()
        }

        optionsButton.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        recordsButton.setOnClickListener {
            val intent = Intent(this, RecordsActivity::class.java)
            startActivity(intent)
        }

    }
}