package fr.uge.tp3.adapters.records

import java.io.Serializable

data class RecordItem(val time: Long, val attempt: Int): Serializable