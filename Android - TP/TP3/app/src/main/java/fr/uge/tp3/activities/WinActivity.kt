package fr.uge.tp3.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import fr.uge.tp3.R
import fr.uge.tp3.database.repositories.Records
import fr.uge.tp3.utils.TimeFormat

/**
 * [WinActivity] provides an activity showed when user win the game
 *
 */
class WinActivity : AppCompatActivity() {
    private var time: Long = 0
    private var attempts: Int = 0

    private lateinit var playAgainButton: Button
    private lateinit var backToMenuButton: Button
    private lateinit var timerTextView: TextView
    private lateinit var scoreTextView: TextView

    private lateinit var recordsRepository: Records

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_win)

        initView()
        recordsRepository = Records(this)

        val extras = intent.extras
        if (extras != null) {
            time = extras.getLong("time", 0)
            attempts = extras.getInt("attempts", 0)
        }

        timerTextView.text = resources.getString(R.string.timer_win, TimeFormat.format(time))
        scoreTextView.text = resources.getString(R.string.attempts_win, attempts)

    }

    private fun initView() {
        playAgainButton = findViewById(R.id.play_again_button)
        backToMenuButton = findViewById(R.id.back_to_menu_button)
        timerTextView = findViewById(R.id.timer)
        scoreTextView = findViewById(R.id.score)

        playAgainButton.setOnClickListener {
            startActivity(Intent(this, MemoryActivity::class.java))
            finish()
        }
        backToMenuButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}