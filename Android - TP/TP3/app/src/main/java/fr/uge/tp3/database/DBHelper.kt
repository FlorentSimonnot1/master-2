package fr.uge.tp3.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import fr.uge.tp3.database.repositories.Records

/**
 * [DBHelper] is a class which provides a manager for Database
 *
 * @property context - the application context
 */
class DBHelper(val context: Context): SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    companion object {
        const val DB_VERSION = 1
        const val DB_NAME = "memoryCardGame.db"

        private var sInstance: DBHelper? = null
        private val tablesCreation: List<String> = listOf(Records.create)
        private val tablesDrop: List<String> = listOf(Records.drop)

        /**
         * Gets the instance of DBHelper
         * Creates an instance if not exists
         *
         * @param context the application context
         * @return an instance of DBHelper
         */
        @Synchronized
        fun getInstance(context: Context): DBHelper? {
            if (sInstance == null) {
                sInstance = DBHelper(context)
            }
            return sInstance
        }
    }


    /**
     * Creates all DB tables
     *
     * @param db - the SQLite Database
     */
    override fun onCreate(db: SQLiteDatabase?) {
        tablesCreation.forEach { table -> db?.execSQL(table) }
    }

    /**
     * Updates the database
     * Delete all tables and recreate their
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        tablesDrop.forEach { table -> db?.execSQL(table) }
        onCreate(db)
    }
}