package fr.uge.tp3.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import fr.uge.tp3.R
import fr.uge.tp3.adapters.cards.CardAdapter
import fr.uge.tp3.adapters.cards.CardItem
import fr.uge.tp3.database.DBHelper
import fr.uge.tp3.database.entities.RecordEntity
import fr.uge.tp3.database.repositories.Records
import fr.uge.tp3.database.repositories.Repository
import fr.uge.tp3.dialog.AbandonDialog
import fr.uge.tp3.models.Game
import fr.uge.tp3.preferences.GamePreferences
import fr.uge.tp3.utils.TimeFormat

/**
 * [MemoryActivity] is the game activity
 *
 */
class MemoryActivity : AppCompatActivity(), CardAdapter.OnItemClickListener, AbandonDialog.OnAbandon {
    private lateinit var game: Game
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: CardAdapter
    private lateinit var timeView: TextView
    private lateinit var cheatButton: Button
    private lateinit var attemptView: TextView
    private lateinit var quitButton: Button

    private val handler = Handler()
    private val timerHandler = Handler()
    private val cheatHandler = Handler()

    private val runnable: Runnable = Runnable { checkCards() }
    private val timerRunnable: Runnable = Runnable { displayTime() }
    private val cheatRunnable: Runnable = Runnable { hideCards() }

    private lateinit var recordsRepository: Records
    private lateinit var gamePreferences: GamePreferences

    // Hide cards which are not found after cheat time
    private fun hideCards() {
        game.cards.cards.forEach { card -> if (!card.found) card.visible = false}
        adapter.notifyDataSetChanged()
    }

    // Display game time
    private fun displayTime() {
        timeView.text = TimeFormat.format(++game.time)
        timerHandler.postDelayed(timerRunnable, 1000)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memory)

        recyclerView = findViewById(R.id.recyclerView)
        timeView = findViewById(R.id.time)
        cheatButton = findViewById(R.id.cheat)
        attemptView = findViewById(R.id.attempt)
        quitButton = findViewById(R.id.abandon)

        recordsRepository = Records(this)
        gamePreferences = GamePreferences(this)


        // Show all cards briefly
        cheatButton.setOnClickListener {
            game.cards.cards.forEach { card -> card.visible = true }
            adapter.notifyDataSetChanged()
            cheatHandler.postDelayed(cheatRunnable, 100)
        }

        quitButton.setOnClickListener {
            timerHandler.removeCallbacks(timerRunnable)
            val dialog = AbandonDialog(this)
            dialog.show(this.supportFragmentManager, "abandon-dialog")
        }

        game = Game.prepareGame(this, savedInstanceState)

        adapter = CardAdapter(this, game.cards.cards, this)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        updateAttempt()

    }

    /**
     * Updates game when user click on card
     *
     * @param position - the position in the cards list of the card clicked
     */
    override fun onItemClick(position: Int) {
        val clickedCard: CardItem = game.cards.cards[position]
        clickedCard.visible = true
        adapter.notifyItemChanged(position)

        if (haveToCheck()) {
            handler.postDelayed(runnable, 1000)
            game.attempts++
            updateAttempt()
        }
    }

    // Gets the 2 cards which are visible
    private fun getPairs(): Pair<CardItem, CardItem> {
        val tmp = game.cards.cards.filter { card -> card.visible }.filter { card -> !card.found }
        return Pair(tmp.component1(), tmp.component2())
    }

    // Gets if we have to checks cards
    private fun haveToCheck(): Boolean {
        return game.cards.cards.filter { card -> card.visible && !card.found }.size == 2
    }

    // Checks if two cards are same
    private fun checkCards() {
        val pair = getPairs()
        val position1 = game.cards.cards.indexOf(pair.first)
        val position2 = game.cards.cards.indexOf(pair.second)

        pair.first.found = pair.first.card == pair.second.card
        pair.first.visible = pair.first.card == pair.second.card

        pair.second.found = pair.first.card == pair.second.card
        pair.second.visible = pair.first.card == pair.second.card

        adapter.notifyItemChanged(position1)
        adapter.notifyItemChanged(position2)

        // Redirection to WinActivity
        if (game.gameIsOver()) {
            recordsRepository.add(RecordEntity(time = game.time, cardsNumber = gamePreferences.getCardsNumber(), attempt =  game.attempts))
            val intent = Intent(this, WinActivity::class.java)
            intent.putExtra("time", game.time)
            intent.putExtra("attempts", game.attempts)
            startActivity(intent)
            finish()
        }
    }

    /**
     * Saves game
     *
     * @param outState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("game", game)
    }

    /**
     * TODO
     *
     */
    override fun onStart() {
        super.onStart()
        timerHandler.post(timerRunnable)
    }

    /**
     * Removes time callbacks for handler when activity is destroy
     *
     */
    override fun onStop() {
        super.onStop()
        timerHandler.removeCallbacks(timerRunnable)
    }

    /**
     * Removes callbacks for handler when activity is destroy
     * We remove the timer handler, the cheat handler and handler uses to verify card
     *
     */
    override fun onDestroy() {
        super.onDestroy()
        timerHandler.removeCallbacks(timerRunnable)
        handler.removeCallbacks(runnable)
        cheatHandler.removeCallbacks(cheatRunnable)
    }

    // Updates counter of attempts in view
    private fun updateAttempt() {
        attemptView.text = resources.getQuantityString(R.plurals.attempts, game.attempts, game.attempts)
    }

    /**
     * Uses when user close the dialog
     * Quit the game if user want to abandon or continue the game
     *
     * @param value - true if the user wants to abandon.
     */
    override fun userWantQuit(value: Boolean) {
        if (value) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            timerHandler.post(timerRunnable)
        }
    }
}
