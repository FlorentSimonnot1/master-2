package fr.uge.tp3.activities

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import fr.uge.tp3.R
import fr.uge.tp3.dialog.CardsNumberSettingDialog
import fr.uge.tp3.models.Game
import fr.uge.tp3.preferences.GamePreferences

/**
 * [SettingsActivity] is the settings activity
 *
 */
class SettingsActivity : AppCompatActivity(), CardsNumberSettingDialog.OnValidate {
    private lateinit var cardsNumberButton: Button
    private lateinit var gamePreferences: GamePreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        gamePreferences = GamePreferences(this)
        cardsNumberButton = findViewById(R.id.change_cards_number)
        cardsNumberButton.text = "${gamePreferences.getCardsNumber()}"

        cardsNumberButton.setOnClickListener {
            val dialog = CardsNumberSettingDialog(this, gamePreferences.getCardsNumber())
            dialog.show(supportFragmentManager, "cards_number_setting")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun cardsNumber(number: Int) {
        gamePreferences.setCards(number)
        cardsNumberButton.text = "$number"
    }
}