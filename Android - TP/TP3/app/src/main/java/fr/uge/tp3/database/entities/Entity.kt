package fr.uge.tp3.database.entities

import android.content.ContentValues

/**
 * [Entity] is an interface which provides method implemented by an entity
 *
 */
interface Entity {

    /**
     * Converts the entity to a [ContentValues] object which can be add into a table
     *
     * @return - the entity data as [ContentValues]
     */
    fun write(): ContentValues
}