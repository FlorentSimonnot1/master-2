package fr.uge.tp3.adapters.cards

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.uge.tp3.R
import fr.uge.tp3.adapters.cards.CardAdapter.CardAdapterViewHolder

/**
 * [CardAdapter] is an adapter for cardItems
 *
 * @property ctx - the context of application
 * @property list - a list of cardItems
 * @property listener - a listener which implements [OnItemClickListener] interface
 */
class CardAdapter(
    private val ctx: Context,
    private val list: List<CardItem>,
    private val listener: OnItemClickListener
): RecyclerView.Adapter<CardAdapterViewHolder>() {

    /**
     * [CardAdapterViewHolder] represents a list item view holder
     *
     * @constructor - initialize the click interface
     *
     *
     * @param itemView - the item view
     */
    inner class CardAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imageView: ImageView = itemView.findViewById(R.id.card_image)
        val textView: TextView = itemView.findViewById(R.id.card_name)
        val cardViewWrapper: ConstraintLayout = itemView.findViewById(R.id.cardViewWrapper)
        val cardView: CardView = itemView.findViewById(R.id.cardView)

        init {
            itemView.setOnClickListener(this)
        }

        /**
         * Throws the click effect to the item
         *
         * @param v - the current item as view
         */
        override fun onClick(v: View?) {
            if (!haveToCheck()) listener.onItemClick(adapterPosition)
        }
    }

    private fun haveToCheck(): Boolean {
        return list.filter { card -> card.visible && !card.found }.size == 2
    }

    /**
     * Initializes the viewHolders.
     *
     * @param parent - the parent view
     * @param viewType - the view type
     * @return a custom view as [CardAdapterViewHolder]
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_item_layout, parent, false)
        return CardAdapterViewHolder(itemView)
    }

    /**
     * Binds a view
     *
     * @param holder - the custom view as [CardAdapterViewHolder]
     * @param position - the position of view
     */
    override fun onBindViewHolder(holder: CardAdapterViewHolder, position: Int) {
        val currentItem = list[position]

        setBackground(currentItem, holder)
        holder.imageView.setImageBitmap(currentItem.card.getBitmap(ctx.assets))
        holder.textView.text = currentItem.card.getName()
        hideElements(currentItem, holder)

        // Add margin to cardView
        val layoutParams : GridLayoutManager.LayoutParams = holder.cardView.layoutParams as GridLayoutManager.LayoutParams
        layoutParams.setMargins(10, 10, 10, 10)
        holder.cardView.layoutParams = layoutParams
    }

    private fun setBackground(cardItem: CardItem, holder: CardAdapterViewHolder) {
        holder.cardViewWrapper.background =
            if (cardItem.visible) ResourcesCompat.getDrawable(ctx.resources, R.mipmap.card_front, null)
            else ResourcesCompat.getDrawable(ctx.resources, R.mipmap.card_back, null)
    }

    private fun hideElements(cardItem: CardItem, holder: CardAdapterViewHolder) {
        holder.imageView.visibility = if(cardItem.visible) View.VISIBLE else View.INVISIBLE
        holder.textView.visibility = if(cardItem.visible) View.VISIBLE else View.INVISIBLE
    }

    /**
     * Gets the number of items
     *
     * @return
     */
    override fun getItemCount(): Int {
        return list.size
    }

    /**
     * [OnItemClickListener] is an interface which is used when an item is clicked
     *
     */
    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}