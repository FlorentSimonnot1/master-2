package fr.uge.tp3.database.repositories

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import fr.uge.tp3.database.DBHelper
import fr.uge.tp3.database.entities.RecordEntity

/**
 * [Records] represents a repository for [RecordEntity]
 *
 * @property context - the application context
 */
class Records(val context: Context?): Repository<RecordEntity> {
    companion object {
        /**
         * The name of the records table
         */
        const val tableName: String = "records"

        /**
         * The id of record
         */
        const val recordId: String = "record_uuid"

        /**
         * The time of record
         */
        const val recordTime: String = "record_time"

        /**
         * The number of attempts of record
         */
        const val recordAttempt: String = "record_attempt"

        /**
         * The number of cards of record
         */
        const val recordCardsNumber: String = "record_cards_number"

        /**
         * The SQL query to create the table
         */
        const val create: String = "CREATE TABLE IF NOT EXISTS $tableName ($recordId VARCHAR(250) PRIMARY KEY, $recordTime BIGINT NOT NULL, $recordCardsNumber INT NOT NULL, $recordAttempt INT NOT NULL)"

        /**
         * The SQL query to drop the table
         */
        const val drop: String = "DROP TABLE IF EXISTS $tableName"
    }

    private var dbHelper: DBHelper? = null
    private var db: SQLiteDatabase? = null

    init {
        if (context != null) {
            dbHelper = DBHelper.getInstance(context)
        }
    }

    /**
     * Opens the database with writing mode
     *
     */
    override fun open() {
        db = dbHelper!!.writableDatabase
    }

    /**
     * Closes the database
     *
     */
    override fun close() {
        db!!.close()
    }

    /**
     * Gets the SQL query to create the table
     *
     * @return - the SQL query to create the table
     */
    override fun create(): String {
        return create
    }

    /**
     * Adds a record entity of type [RecordEntity]
     *
     * @param entity - the record entity we want to add
     */
    override fun add(entity: RecordEntity) {
        open()
        db!!.insert(tableName, null, entity.write())
        close()
    }

    /**
     * Gets the SQL query to drop the table
     *
     * @return - the SQL query to drop the table
     */
    override fun drop(): String {
        return "DROP TABLE IF EXISTS $tableName"
    }

    /**
     * Gets all entities from record table
     *
     * @return - all entities as a list of [RecordEntity]
     */
    override fun getAll(): List<RecordEntity> {
        open()
        val records = mutableListOf<RecordEntity>()
        val cursor = db!!.rawQuery("SELECT * FROM $tableName", null)
        if (cursor.moveToFirst()) {
            do {
                records.add(
                    RecordEntity(
                        cursor.getString(cursor.getColumnIndex(recordId)),
                        cursor.getString(cursor.getColumnIndex(recordTime)).toLong(),
                        cursor.getString(cursor.getColumnIndex(recordCardsNumber)).toInt(),
                        cursor.getString(cursor.getColumnIndex(recordAttempt)).toInt()
                    )
                )
            }while(cursor.moveToNext())
        }
        cursor.close()
        close()
        return records
    }
}