package fr.uge.tp3.utils

/**
 * [TimeFormat] provides a tool for format time
 */
object TimeFormat {

    private fun numberFormat(number: Long): String {
        return if(number < 10) "0$number" else "$number"
    }

    /**
     * Gets the time as string
     *
     * @param time - the time to format
     * @return the time formatted as mm:ss
     */
    fun format(time: Long): String {
        return "${numberFormat(time/60)} : ${numberFormat(time%60)}"
    }
}