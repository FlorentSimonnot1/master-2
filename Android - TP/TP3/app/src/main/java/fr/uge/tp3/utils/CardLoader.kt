package fr.uge.tp3.utils

import android.content.Context
import fr.uge.tp3.models.Card

/**
 * [CardLoader] represents an object to load cards
 */
object CardLoader {

    /**
     * Loads cards images from assets
     *
     * @param context - the application context
     * @param path - the path of cards
     * @return a list of [Card]
     */
    fun loadCards(context: Context, path: String): List<Card>? {
        val assets = context.assets
        return assets.list(path)?.map { filePath -> Card("$path/$filePath") }
    }
}