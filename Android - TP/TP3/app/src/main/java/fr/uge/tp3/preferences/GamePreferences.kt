package fr.uge.tp3.preferences

import android.content.Context
import android.content.SharedPreferences

/**
 * [GamePreferences] provides a class to manage preferences of application
 *
 * @constructor - Gets the shared preferences of application
 *
 *
 * @param context - the application context
 */
class GamePreferences(context: Context) {

    private val SHARED_PREFS = "sharedPrefs"
    private val pref: SharedPreferences

    init {
        pref = context.applicationContext.getSharedPreferences(SHARED_PREFS, 0)
    }

    /**
     * Sets the number of cards in the game
     *
     * @param number - the number of cards in the game
     */
    fun setCards(number: Int) {
        val editor = pref.edit()
        editor.putInt("cards_number", number)
        editor.apply()
    }

    /**
     * Gets the number of cards
     *
     * @return the number of cards
     */
    fun getCardsNumber(): Int { return pref.getInt("cards_number", 32) }

    /**
     * Sets the game deck
     *
     * @param deck - the game deck
     */
    fun setDeck(deck: String) {
        val editor = pref.edit()
        editor.putString("deck", deck)
        editor.apply()
    }

    /**
     * Gets the name of the game deck
     *
     * @return the name of the game deck as string
     */
    fun getDeck(): String? { return pref.getString("deck", "logos") }

}