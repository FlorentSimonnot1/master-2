package fr.uge.tp3.utils

/**
 * [CardsNumber] provides a tool for cards
 */
object CardsNumber {
    /**
     * List of number of cards available in the game
     */
    val numbers = listOf(8, 12, 16, 24, 32)

    /**
     * Gets an array of text which is the concatenation between number and the text
     *
     * @param s - the text
     * @return an array of string
     */
    fun getCardNumbersList(s: String): Array<String> {
        return numbers.map { number -> "$number $s" }.toTypedArray()
    }

    /**
     * Gets the number found in a text
     *
     * @param s - the text
     * @return the number of cards writing in the text
     */
    fun getCardsNumber(s: String): Int {
        return s.substringBefore(" ").toInt()
    }
}