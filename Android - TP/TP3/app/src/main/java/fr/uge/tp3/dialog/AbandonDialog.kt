package fr.uge.tp3.dialog

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import fr.uge.tp3.R

/**
 * [AbandonDialog] is a class which represents a dialog.
 * Offers the possibility to the user to quit the game
 *
 * @property onAbandon - an object implemented the [OnAbandon] interface
 */
class AbandonDialog(private val onAbandon: OnAbandon): DialogFragment() {
    private lateinit var okButton: Button
    private lateinit var cancelButton: Button

    /**
     * Creates the dialog view
     *
     * @param inflater - the layout inflater
     * @param container - the view group
     * @param savedInstanceState - the application bundle
     * @return the dialog as [View]
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.abandon_dialog, container, false)

        okButton = customView.findViewById(R.id.ok_dialog_button)
        cancelButton = customView.findViewById(R.id.cancel_dialog_button)

        okButton.setOnClickListener {
            onAbandon.userWantQuit(true)
            dismiss()
        }

        cancelButton.setOnClickListener {
            onAbandon.userWantQuit(false)
            dismiss()
        }

        return customView
    }


    /**
     * [OnAbandon] is an interface used when user want to close the dialog
     *
     */
    interface OnAbandon {
        fun userWantQuit(value: Boolean)
    }


    /**
     * Sets the width of the dialog to match parent
     * Centers the dialog
     *
     */
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
    }



}