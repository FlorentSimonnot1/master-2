package com.example.tp0

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class ClickGameActivity : AppCompatActivity() {
    private lateinit var quitButton: Button
    private lateinit var textView: TextView
    private var TAG = "BAD ASSIGN"
    private var MAX_CLICK = 20

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_click_game)

        //Log.w(TAG, "Try to assign a textView to a button")
        //quitButton = findViewById(R.id.textView)

        textView = findViewById(R.id.textView)
        var nClick = 0
        val startTime = System.currentTimeMillis()
        getTextViewStyle(nClick)

        textView.setOnClickListener {
            nClick++
            getTextViewStyle(nClick)

            if (nClick == MAX_CLICK) {
                val freq = nClick.toFloat() / (System.currentTimeMillis() - startTime).toFloat() * 1000
                val t = Toast.makeText(this, "Freq $freq", Toast.LENGTH_SHORT)
                t.show()
                finish()
            }

        }

        quitButton = findViewById(R.id.quit_button)
        quitButton.setOnClickListener {
            val t = Toast.makeText(this, resources.getString(R.string.toast_quit_app), Toast.LENGTH_SHORT)
            t.show()
            finish()
        }
    }

    private fun getTextViewText(counter: Int): String {
        return if (counter < 2) resources.getString(R.string.counter_click, counter) else resources.getString(R.string.counter_click_plur, counter)
    }

    @SuppressLint("NewApi")
    private fun getTextViewStyle(counter: Int) {
        textView.setTextColor(Color.WHITE)
        textView.setBackgroundColor(Color.rgb(0f, counter.toFloat() / MAX_CLICK.toFloat(), 0f))
        textView.text = getTextViewText(counter)
    }
}