package fr.uge.factorizationlib;

import java.math.BigInteger;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Simple main application
 */
public class App {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Type the integer to factorize");
        if (s.hasNextBigInteger()) {
            final BigInteger n = s.nextBigInteger();
            final AtomicBoolean cancelled = new AtomicBoolean(); // thread-safe boolean
            final Thread mainThread = Thread.currentThread();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                cancelled.set(true);
                try {
                    mainThread.join(); // wait for the end of the main thread
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }));
            long startTime = System.nanoTime();
            Map<BigInteger, Integer> result = FactorizationUtils.factorize(n, (progress) -> {
                System.out.println(String.format("Progress: %f %%", progress * 100.0));
                return ! cancelled.get();
            });
            System.out.println((cancelled.get() ? "Partial result: " : "Complete result: ") + result);
            System.out.println(String.format("Duration: %f seconds", (System.nanoTime() - startTime) / 1e9));
        } else
            System.err.println("No integer supplied");
    }

}
