package fr.uge.factorizationlib;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class FactorizationUtils {

    public static Map<BigInteger, Integer> factorize(BigInteger n, Predicate<Double> progressCallback) {
        Map<BigInteger, Integer> map = new HashMap<>();
        if (n.compareTo(BigInteger.valueOf(2)) < 0) return map;

        int i = 2;
        while(n.compareTo(BigInteger.valueOf(i*i)) >= 0) {
            if (progressCallback != null) {
                if (!progressCallback.test(i * 1.0 / Math.sqrt(n.doubleValue()))) {
                    return map;
                }
            }

            if (n.mod(BigInteger.valueOf(i)).compareTo(BigInteger.valueOf(0)) == 0) {
                BigInteger tmpI = BigInteger.valueOf(i);
                map.merge(tmpI, 1, Integer::sum);
                n = n.divide(BigInteger.valueOf(i));
            } else {
                i += (i == 2 ? 1 : 2);
            }
        }

        map.merge(n, 1, Integer::sum);
        return map;
    }
}
