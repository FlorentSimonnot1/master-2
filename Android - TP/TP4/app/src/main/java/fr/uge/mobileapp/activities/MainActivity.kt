package fr.uge.mobileapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.MutableLiveData
import fr.uge.mobileapp.models.FactorizationTask
import fr.uge.mobileapp.R
import fr.uge.mobileapp.adapters.FactorizationItem
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference
import java.math.BigInteger

class MainActivity : AppCompatActivity() {
    private lateinit var numberEditText: EditText
    private lateinit var playStopButton: Button
    private lateinit var progressValue: TextView
    private var list: List<String> = listOf()

    private var task: FactorizationTask = FactorizationTask(WeakReference(this))

    lateinit var observer: MutableLiveData<Int>
    lateinit var runningObserver: MutableLiveData<Boolean>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        observer = MutableLiveData()
        observer.postValue(0)

        runningObserver = MutableLiveData()
        runningObserver.postValue(false)

        if (savedInstanceState != null) {
            if (savedInstanceState.get(saveTask) != null && savedInstanceState.get(bigNumber) != null){
                task = FactorizationTask(WeakReference(this))
                task.execute(savedInstanceState.get(bigNumber) as BigInteger)
            }
        } else {
            updatePlayButton(false)
        }

        observer.observe(this) {
            if (!task.isCancelled) {
                progressBar.progress = it
                progressValue.text = "$it%"
            }
        }

        runningObserver.observe(this) {
            updatePlayButton(it)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(saveTask, task)
        if (numberEditText.text.isNotEmpty()) {
            outState.putSerializable(bigNumber,BigInteger(numberEditText.text.toString()) )
        }
    }

    private fun initView() {
        numberEditText = findViewById(R.id.number)
        playStopButton = findViewById(R.id.play_stop_button)
        progressValue = findViewById(R.id.progressValue)
    }

    override fun onDestroy() {
        super.onDestroy()
        task.cancel(true)
    }

    private fun updatePlayButton(running: Boolean) {
        playStopButton.text = if (running) resources.getText(R.string.stop) else resources.getText(R.string.start)
        val drawable = if (running) ResourcesCompat.getDrawable(resources, R.drawable.ic_stop, null) else ResourcesCompat.getDrawable(resources,
            R.drawable.ic_play, null)
        playStopButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
        if (running) initStopButton() else initStartButton()
    }

    private fun initStopButton() {
        playStopButton.setOnClickListener {
            task.cancel(true)
        }
    }

    private fun initStartButton() {
        playStopButton.setOnClickListener {
            if (numberEditText.text.isNotEmpty()) {
                val number = BigInteger(numberEditText.editableText.toString())
                task = FactorizationTask(WeakReference(this))
                observer.postValue(0)
                task.execute(number)
            }
        }
    }

    companion object {
        private const val saveTask : String = "FACTORISATION_TASK"
        private const val bigNumber : String = "BIG_NUMBER"
    }
}