package fr.uge.mobileapp

import android.os.AsyncTask
import android.util.Log
import android.widget.ProgressBar
import fr.uge.factorizationlib.FactorizationUtils
import java.io.Serializable
import java.lang.ref.WeakReference
import java.math.BigInteger

class FactorizationTask(private val parentActivity: WeakReference<MainActivity>) : AsyncTask<BigInteger, Float, Map<BigInteger, Int>>(), Serializable {

    /**
     * Prepares the task
     * Gets the the parentActivity and the init the progressBar
     */
    override fun onPreExecute() {
        super.onPreExecute()
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        val progressBar: ProgressBar? = activity.findViewById(R.id.progressBar)
        progressBar!!.progress = 0
    }

    override fun doInBackground(vararg params: BigInteger?): Map<BigInteger, Int> {
        val n = params[0]
        return FactorizationUtils.factorize(n) {
                progress ->
                    publishProgress((progress * 100).toFloat())
                    !isCancelled
        }
    }

    override fun onProgressUpdate(vararg values: Float?) {
        super.onProgressUpdate(*values)
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        activity.observer.value = values[0]?.toInt()
    }

    override fun onPostExecute(result: Map<BigInteger, Int>?) {
        super.onPostExecute(result)
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        activity.observer.value = 100
    }
}