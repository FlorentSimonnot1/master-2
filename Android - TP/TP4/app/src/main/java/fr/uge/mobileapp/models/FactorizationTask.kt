package fr.uge.mobileapp.models

import android.os.AsyncTask
import android.widget.ProgressBar
import fr.uge.factorizationlib.FactorizationUtils
import fr.uge.mobileapp.R
import fr.uge.mobileapp.activities.MainActivity
import java.io.Serializable
import java.lang.ref.WeakReference
import java.math.BigInteger

/**
 * [FactorizationTask] is a class which represents an Async Task to get number factorization
 * Uses the factorization library
 *
 * @property parentActivity - the activity which calls the async task
 */
class FactorizationTask(private val parentActivity: WeakReference<MainActivity>) : AsyncTask<BigInteger, Float, Map<BigInteger, Int>>(), Serializable {

    /**
     * Prepares the task
     * Gets the the parentActivity and the init the progressBar
     */
    override fun onPreExecute() {
        super.onPreExecute()
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        val progressBar: ProgressBar? = activity.findViewById(R.id.progressBar)
        progressBar!!.progress = 0
        activity.runningObserver.value = true
    }

    /**
     * Executes the factorization task
     *
     * @param params - the number we want to get factorization
     * @return - the result of factorization as map of <BigInteger, Int> which are the factors
     */
    override fun doInBackground(vararg params: BigInteger?): Map<BigInteger, Int> {
        val n = params[0]
        return FactorizationUtils.factorize(n) {
                progress ->
                    publishProgress((progress * 100).toFloat())
                    !isCancelled
        }
    }

    /**
     * Notify the progressBar observer with the new value
     *
     * @param values
     */
    override fun onProgressUpdate(vararg values: Float?) {
        super.onProgressUpdate(*values)
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        activity.observer.value = values[0]?.toInt()
    }

    /**
     * Notify the running observer that task is finished
     * Notify the observer to the progress value
     *
     * @param result - the result of factorization
     */
    override fun onPostExecute(result: Map<BigInteger, Int>?) {
        super.onPostExecute(result)
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        activity.observer.value = 100
        activity.runningObserver.value = false
    }

    /**
     * Cancels the task
     * Notify the running observer that the task is cancelled
     *
     */
    override fun onCancelled() {
        super.onCancelled()
        val activity: MainActivity? = parentActivity.get()
        if (activity == null || activity.isFinishing) {
            return
        }
        activity.runningObserver.value = false
    }
}