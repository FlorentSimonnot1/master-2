package fr.uge.mobileapp.adapters

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FactorizationItem(val results: List<String>?): Parcelable