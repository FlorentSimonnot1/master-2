package fr.uge.mobileapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.uge.mobileapp.R
import fr.uge.mobileapp.adapters.FactorizationAdapter
import fr.uge.mobileapp.adapters.FactorizationItem

class ResultActivity : AppCompatActivity() {
    private lateinit var factorRecyclerView: RecyclerView
    private lateinit var factorAdapter : FactorizationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val extras = intent.extras
        var results = FactorizationItem(listOf())
        if (extras != null) {
            results = extras.get("results") as FactorizationItem
        }


        factorAdapter = FactorizationAdapter(this, results.results!!)

        factorRecyclerView = findViewById(R.id.recyclerViewResults)
        factorRecyclerView.adapter = factorAdapter
        factorRecyclerView.layoutManager = LinearLayoutManager(this)
        factorRecyclerView.setHasFixedSize(true)
    }
}