package fr.uge.mobileapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.uge.mobileapp.R

class FactorizationAdapter(
    private val ctx: Context,
    private val results: List<String>
): RecyclerView.Adapter<FactorizationAdapter.FactorizationAdapterViewHolder>() {

    inner class FactorizationAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.findViewById(R.id.cardViewResult)
        val result: TextView = itemView.findViewById(R.id.result)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FactorizationAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.factorization_item, parent, false)
        return FactorizationAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FactorizationAdapterViewHolder, position: Int) {
        val currentItem = results[position]
        holder.result.text = currentItem
    }

    override fun getItemCount(): Int {
        return results.size
    }

}