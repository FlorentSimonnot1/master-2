package fr.uge.mobileapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import fr.uge.factorizationlib.FactorizationUtils
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference
import java.math.BigInteger

class MainActivity : AppCompatActivity() {
    private lateinit var numberEditText: EditText
    private lateinit var playStopButton: Button
    // private lateinit var resultTextView: TextView
    private lateinit var progressValue: TextView

    private var task: FactorizationTask = FactorizationTask(WeakReference(this))

    lateinit var observer: MutableLiveData<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        observer = MutableLiveData()
        observer.postValue(0)

        if (savedInstanceState != null) {
            if (savedInstanceState.get(saveTask) != null && savedInstanceState.get(bigNumber) != null){
                task = FactorizationTask(WeakReference(this))
                task.execute(savedInstanceState.get(bigNumber) as BigInteger)
            }
        }

        playStopButton.setOnClickListener {
            if (numberEditText.text.isNotEmpty()) {
                val number = BigInteger(numberEditText.editableText.toString())
                task = FactorizationTask(WeakReference(this))
                observer.postValue(0)
                task.execute(number)

            }
        }

        observer.observe(this) {
            if (!task.isCancelled) {
                progressBar.progress = it
                progressValue.text = "$it%"
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(saveTask, task)
        if (numberEditText.text.isNotEmpty()) {
            outState.putSerializable(bigNumber,BigInteger(numberEditText.text.toString()) )
        }

    }

    private fun initView() {
        numberEditText = findViewById(R.id.number)
        playStopButton = findViewById(R.id.play_stop_button)
        progressValue = findViewById(R.id.progressValue)
        // resultTextView = findViewById(R.id.result)
    }

    override fun onDestroy() {
        super.onDestroy()
        task.cancel(true)
    }

    companion object {
        private const val saveTask : String = "FACTORISATION_TASK"
        private const val bigNumber : String = "BIG_NUMBER"
    }
}