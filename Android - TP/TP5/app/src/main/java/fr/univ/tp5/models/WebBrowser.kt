package fr.univ.tp5.models

import fr.univ.tp5.R

enum class WebBrowser(val browserName: String, val icon: Int) {
    QWANT("Qwant", R.drawable.ic_search)
}