package fr.univ.tp5.dialogs

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import fr.univ.tp5.R

class LoadingDialog(): DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.loading_dialog, container, false)

        val loading: ProgressBar = customView.findViewById(R.id.progressBar)
        loading.isIndeterminate = true

        return customView
    }


    /**
     * Sets the width of the dialog to match parent
     * Centers the dialog
     *
     */
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window!!.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
    }

}