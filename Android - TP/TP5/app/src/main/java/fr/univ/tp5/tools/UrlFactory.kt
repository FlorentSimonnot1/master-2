package fr.univ.tp5.tools

class UrlFactory {

    companion object {
        fun createRequest(query: String, offset: Int, count: Int, type: String): String {
            return "https://api.qwant.com/api/search/web?count=$count&offset=$offset&q=$query&t=$type&uiv=1"
        }
    }

}