package fr.univ.tp5.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.univ.tp5.R
import fr.univ.tp5.models.DownloadImageTask
import fr.univ.tp5.models.RequestResult


class ResultsAdapter(
    private val context: Context,
    private val results: List<RequestResult>,
    private val listener: OnItemClickListener
): RecyclerView.Adapter<ResultsAdapter.RequestResultViewHolder>() {

    inner class RequestResultViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
        var favicon: ImageView = view.findViewById(R.id.favicon)
        var title: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.description)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            listener.onItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestResultViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.result_item, parent, false)
        return RequestResultViewHolder(view)
    }

    override fun onBindViewHolder(holder: RequestResultViewHolder, position: Int) {
        val result = results[position]
        Log.d("FAVICON", result.favicon)
        holder.title.text = result.title
        holder.description.text = result.description
        DownloadImageTask(holder.favicon).execute("https:${result.favicon}")
    }

    override fun getItemCount(): Int = results.size

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}