package fr.univ.tp5.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import fr.univ.tp5.R
import fr.univ.tp5.adapters.ResultsAdapter
import fr.univ.tp5.dialogs.LoadingDialog
import fr.univ.tp5.models.RequestResult
import fr.univ.tp5.tools.UrlFactory


class MainActivity : AppCompatActivity(), ResultsAdapter.OnItemClickListener {
    private lateinit var requestEditText: EditText
    private lateinit var searchButton: Button
    private lateinit var queue: RequestQueue
    private lateinit var recyclerView: RecyclerView
    private lateinit var resultAdapter: ResultsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var loadingDialog: LoadingDialog
    private val results: MutableList<RequestResult> = mutableListOf()
    private var searchPage: Pair<Int, Int> = Pair(0, 10)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestEditText = findViewById(R.id.requestEditText)
        searchButton = findViewById(R.id.searchButton)
        recyclerView = findViewById(R.id.web_results)

        resultAdapter = ResultsAdapter(this, results, this)
        layoutManager = LinearLayoutManager(this);
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = resultAdapter

        loadingDialog = LoadingDialog()
        
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (layoutManager.findLastCompletelyVisibleItemPosition() == results.size - 1) {
                    searchPage = Pair(searchPage.first + searchPage.second, searchPage.second)
                    searchQuery()
                }
            }
        })

        queue = Volley.newRequestQueue(this)

        searchButton.setOnClickListener {
            if (requestEditText.text.isNotEmpty()) {
                searchQuery()
            }
        }

    }

    private fun searchQuery() {
        loadingDialog.show(this.supportFragmentManager, "loading-dialog")
        val request = UrlFactory.createRequest(
            requestEditText.text.toString(),
            searchPage.first,
            searchPage.second,
            "web"
        )
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, request,
            { response ->
                results.addAll(mapResults(response))
                resultAdapter.notifyDataSetChanged()
                loadingDialog.dismiss()
            },
            { error ->
                Log.d("ERROR", error.toString())
                loadingDialog.dismiss()
            }
        )

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

    private fun mapResults(response: String): List<RequestResult> {
        val obj = Gson().fromJson(response, Map::class.java)
        val data = obj["data"] as Map<*, *>
        val results = data["result"] as Map<*, *>
        val items = results["items"] as List<*>
        return items.map {
            val item = it as Map<String, Any>
            RequestResult.createRequestResult(item)
        }
    }

    override fun onItemClick(position: Int) {
        val result = results[position]
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(result.url))
        startActivity(browserIntent)
    }

}