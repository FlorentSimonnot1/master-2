package fr.univ.tp5.adapters

import android.content.Context
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import fr.univ.tp5.R
import fr.univ.tp5.models.WebBrowser

class WebBrowserAdapter(
    context: Context
): ArrayAdapter<WebBrowser>(context, 0, WebBrowser.values()) {

    val layoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.web_browser_item, parent, false)
        } else {
            view = convertView
        }

        getItem(position)?.let { browser -> setBrowserItem(view, browser) }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View

        if (position == 0) {
            view = layoutInflater.inflate(R.layout.web_browser_item, parent, false)
            view.setOnClickListener {
                val root = parent.rootView
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK))
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK))
            }
        } else {
            view = layoutInflater.inflate(R.layout.web_browser_item, parent, false)
            getItem(position)?.let { webBrowser ->
                setBrowserItem(view, webBrowser)
            }
        }

        return view
    }

    override fun getItem(position: Int): WebBrowser? {
        if (position == 0) {
            return null
        }
        return super.getItem(position - 1)
    }

    override fun getCount(): Int = super.getCount() + 1

    override fun isEnabled(position: Int): Boolean = position != 0

    private fun setBrowserItem(view: View, browser: WebBrowser) {
        val browserIcon: ImageView = view.findViewById(R.id.browser_icon)
        val browserText: TextView = view.findViewById(R.id.browser_name)

        browserText.text = browser.name
        browserIcon.setBackgroundResource(browser.icon)
    }

}