package fr.univ.tp5.models

import android.util.Log
import java.io.Serializable

data class RequestResult(val title: String, val url: String, val description: String, val favicon: String): Serializable {

    companion object {
        fun createRequestResult(data: Map<String, Any>): RequestResult {
            return RequestResult(data["title"].toString(), data["url"].toString(), data["desc"].toString(), data["favicon"].toString())
        }
    }

}