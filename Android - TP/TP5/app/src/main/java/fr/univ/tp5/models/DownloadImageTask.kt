package fr.univ.tp5.models

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import java.io.InputStream
import java.lang.Exception
import java.net.URL


class DownloadImageTask(private val imageView: ImageView): AsyncTask<String, Any, Bitmap?>() {

    override fun doInBackground(vararg params: String?): Bitmap? {
        val urlDisplay = params[0]
        var bitmap: Bitmap? = null

        try {
            val inputStream: InputStream = URL(urlDisplay).openStream()
            bitmap = BitmapFactory.decodeStream(inputStream)
        } catch(e: Exception) {
            Log.e("Error", "${e.message}")
            e.printStackTrace()
        }

        return bitmap
    }

    override fun onPostExecute(result: Bitmap?) {
        imageView.setImageBitmap(result)
    }
}