package com.example.contactapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.example.contactapp.R
import com.example.contactapp.models.Contact

class ContactEditActivity : AppCompatActivity() {
    private val DIGIT_BUTTONS: IntArray = intArrayOf(
        R.id.button0, R.id.button1, R.id.button2, R.id.button3,
        R.id.button4, R.id.button5, R.id.button6, R.id.button7,
        R.id.button8, R.id.button9, R.id.buttonBack
    )

    private lateinit var contactNameEditText: EditText
    private lateinit var phoneNumberPreview: TextView
    private lateinit var saveModificationButton: Button
    private lateinit var contact: Contact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_edit)

        contactNameEditText = findViewById(R.id.contactNameEditText)
        phoneNumberPreview = findViewById(R.id.telephoneNumberPreview)
        saveModificationButton = findViewById(R.id.saveContact)

        contact = intent.extras?.get("contact") as Contact

        showContact()

        // Update contact name on change edit text value
        contactNameEditText.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { contact.setName(s.toString()) }

            override fun afterTextChanged(s: Editable?) {}

        })

        saveModificationButton.setOnClickListener {
            // Toast.makeText(this,"${contact.getName()} - ${contact.getPhoneNumber()}", Toast.LENGTH_LONG).show()

            val intent = Intent()
            intent.putExtra("contact", contact)
            setResult(RESULT_OK, intent)
            finish()
        }

        initNumericPad()
    }

    private fun initNumericPad() {
        DIGIT_BUTTONS.forEach { id ->
            val button = findViewById<Button>(id)
            button.setOnClickListener {
                if (id == R.id.buttonBack) {
                    contact.setPhoneNumber(if(contact.getPhoneNumber().isNotEmpty()) {contact.getPhoneNumber().substring(0, contact.getPhoneNumber().length - 1)} else "")
                } else {
                    contact.setPhoneNumber(contact.getPhoneNumber() + DIGIT_BUTTONS.indexOf(id).toString())
                }
                showContact()
            }
        }
    }

    private fun showContact() {
        contactNameEditText.setText(contact.getName())
        phoneNumberPreview.text = contact.getPhoneNumber()
    }
}