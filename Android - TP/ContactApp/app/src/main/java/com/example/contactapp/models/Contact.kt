package com.example.contactapp.models

import android.content.Context
import android.content.Intent
import android.net.Uri
import java.io.Serializable

/**
 * <b>Contact class represents a user associated to a phone number</b>
 */
class Contact(
    private var name: String,
    private var phoneNumber: String
): Serializable {

    /**
     * Get the contact name
     * @return the contact name as string
     */
    fun getName(): String { return name }

    /**
     * Get the contact phone number
     * @return the contact phone number as string
     */
    fun getPhoneNumber(): String { return phoneNumber }

    /**
     * Set the contact name as the new <i>name</i>
     * @param name the new contact name
     */
    fun setName(name: String) { this.name = name }

    /**
     * Set the contact phone number as the new <i>phoneNumber</i>
     * @param phoneNumber the new phoneNumber
     */
    fun setPhoneNumber(phoneNumber: String) { this.phoneNumber = phoneNumber }

    /**
     * Open native intent to call the contact number
     * @param context the current context
     */
    fun call(context: Context) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        context.startActivity(intent)
    }

    /**
     * Open native intent to send sms to the contact number
     * @param context the current context
     */
    fun sendSMS(context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("smsto:${Uri.encode(phoneNumber)}")
        intent.putExtra("sms_body", "Hello $name my number is 0102030405 !")
        context.startActivity(intent)
    }

}