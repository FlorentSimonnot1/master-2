package com.example.contactapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.contactapp.R
import com.example.contactapp.models.Contact

class ContactDisplayActivity : AppCompatActivity() {
    private val UPDATE_CONTACT = 1

    private lateinit var buttonToEdit: Button
    private lateinit var nameTextView: TextView
    private lateinit var phoneNumberTextView: TextView

    private lateinit var contact: Contact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_display)

        nameTextView = findViewById(R.id.nameContact)
        phoneNumberTextView = findViewById(R.id.PhoneNumber)
        buttonToEdit = findViewById(R.id.button_to_edit)

        contact = Contact("Bugdroid", "330758596061")
        showContact()

        buttonToEdit.setOnClickListener {
            val intent = Intent(this, ContactEditActivity::class.java)
            intent.putExtra("contact", contact)
            startActivityForResult(intent, UPDATE_CONTACT)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.contact_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.item_call -> {
                contact.call(this)
            }
            R.id.item_message -> {
                contact.sendSMS(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UPDATE_CONTACT) {
            if (resultCode == RESULT_OK) {
                val newContact = data?.extras?.get("contact") as Contact
                contact = newContact
                showContact()
            }
        }
    }

    private fun showContact() {
        nameTextView.text = contact.getName()
        phoneNumberTextView.text = contact.getPhoneNumber()
    }
}