package fr.uge.tp2.CustomDialog

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.NumberPicker
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import fr.uge.tp2.Preferences.TimerPreferences
import fr.uge.tp2.R


/**
 * [TimerDialog] is a custom dialog used to configure the timer time.
 *
 * @constructor
 * calls DialogFragment constructor
 *
 */
class TimerDialog(): DialogFragment() {
    private lateinit var okButton: Button
    private lateinit var cancelButton: Button
    private lateinit var minutesNumberPicker: NumberPicker
    private lateinit var secondsNumberPicker: NumberPicker
    private lateinit var timerPreferences: TimerPreferences
    private var mOnInputSelected: OnValidate? = null

    /**
     * Creates the timer dialog view
     * Initialize the layout and prepare the different buttons of the dialog
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val customView = inflater.inflate(R.layout.timer_dialog, container, false)

        okButton = customView.findViewById(R.id.ok_dialog_button)
        cancelButton = customView.findViewById(R.id.cancel_dialog_button)

        okButton.setOnClickListener {
            saveTimeIntoPreferences()
            mOnInputSelected!!.updateSettingsTime()
            dismiss()
        }

        cancelButton.setOnClickListener {
            dismiss()
        }

        minutesNumberPicker = customView.findViewById(R.id.minutes)
        secondsNumberPicker = customView.findViewById(R.id.seconds)
        timerPreferences = TimerPreferences(context!!)

        minutesNumberPicker.minValue = 0
        minutesNumberPicker.maxValue = 59
        minutesNumberPicker.value = timerPreferences.getMinutes().toInt()

        secondsNumberPicker.minValue = 0
        secondsNumberPicker.maxValue = 59
        secondsNumberPicker.value = timerPreferences.getSeconds().toInt()

        return customView
    }

    /**
     * [OnValidate] is an interface uses when dialog is dismiss
     *
     */
    interface OnValidate {
        fun updateSettingsTime()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mOnInputSelected = targetFragment as OnValidate?
        } catch (e: ClassCastException) {
            Log.e("ERROR", "onAttach: ClassCastException : " + e.message)
        }
    }

    // Update time in preferences system.
    private fun saveTimeIntoPreferences() {
        timerPreferences.setTime(
            minutesNumberPicker.value.toLong(),
            secondsNumberPicker.value.toLong()
        )
        Toast.makeText(
            context,
            "${resources.getString(R.string.timer_dialog_toast_validation)} ${minutesNumberPicker.value}:${secondsNumberPicker.value}",
            Toast.LENGTH_SHORT
        ).show()
    }

    /**
     * Configures dialog width to max window with and height to wrap content
     * Centers the dialog
     *
     */
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
    }
}