package fr.uge.tp2.Utils

/**
 * [TimeFormat] is class which contains some tools to format time
 *
 */
class TimeFormat {

    companion object {
        /**
         * Formats the time passed in seconds
         *
         * @param number the time in seconds as long
         * @return the time as string
         */
        fun format(number: Long): String = if (number < 10) "0$number" else "$number"
    }
}