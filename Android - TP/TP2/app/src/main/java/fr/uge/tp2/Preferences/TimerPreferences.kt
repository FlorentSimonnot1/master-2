package fr.uge.tp2.Preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import fr.uge.tp2.Fragments.CircularTimerFragment
import fr.uge.tp2.Fragments.TextTimerFragment
import fr.uge.tp2.Models.Timer

/**
 * [TimerPreferences] is a class which manages preferences for timer
 *
 * @constructor
 * Gets shared preferences
 *
 * @param context the application context
 */
class TimerPreferences(context: Context) {
    private val SHARED_PREFS = "sharedPrefs"
    private val pref: SharedPreferences

    init {
        pref = context.applicationContext.getSharedPreferences(SHARED_PREFS, 0)
    }

    /**
     * Saves the timer duration as minutes and second in preferences
     *
     * @param minutes timer duration minutes
     * @param seconds timer duration seconds
     */
    fun setTime(minutes: Long, seconds: Long) {
        val editor = pref.edit()
        editor.putLong("Minutes", minutes)
        editor.putLong("Seconds", seconds)
        editor.apply()
    }

    /**
     * Gets the time save in preferences
     * If there is no time save, return the defaultTime configured in the <i>Timer</i>
     *
     * @return the timer duration as seconds save by the user
     */
    fun getTime(): Long {
        val minutes = pref.getLong("Minutes", 0)
        val seconds = pref.getLong("Seconds", Timer.defaultTime)
        return minutes * 60 + seconds
    }

    /**
     * Gets the number of minutes save in preferences
     *
     * @return the timer duration minutes as long
     */
    fun getMinutes(): Long { return pref.getLong("Minutes", 0) }

    /**
     * Gets the number of seconds save in preferences
     *
     * @return the timer duration seconds as long
     */
    fun getSeconds(): Long { return pref.getLong("Seconds", 10) }

    /**
     * Sets the view mode to true if user want to use the graphic mode
     *
     * @param boolean true if user want to use the graphic mode
     */
    fun setViewMode(boolean: Boolean) {
        val editor = pref.edit()
        editor.putBoolean("GraphicView", boolean)
        editor.apply()
    }

    /**
     * Gets timers fragment according to the view chosen by user
     *
     * @return a fragment which is the view of the timer
     */
    fun getViewFragment(): Fragment {
        val graphicView = pref.getBoolean("GraphicView", false)
        return if (graphicView) CircularTimerFragment() else TextTimerFragment()
    }
}