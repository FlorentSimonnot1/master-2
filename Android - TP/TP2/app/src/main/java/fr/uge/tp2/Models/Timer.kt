package fr.uge.tp2.Models

import java.io.Serializable

/**
 * <i>Timer</i> is a class which represents a count downer timer
 *
 * @property time the start time in seconds
 */
class Timer(private var time: Long): Serializable {

    companion object {
        const val defaultTime: Long = 10
    }
    private var state: TimerState = TimerState.FINISH
    private val maxTime: Long = time

    /**
     * <i>TimerState</i> is an enum class which represents the timer state
     *
     */
    enum class TimerState {
        /**
         * <i>Active</i> is the state used when the timer is active
         *
         */
        ACTIVE,

        /**
         * <i>Pause</i> is the state used when the timer is in pause
         *
         */
        PAUSE,

        /**
         * <i>Finish</i> is the state used when the timer is finish
         *
         */
        FINISH
    }

    /**
     * Checks if the timer state is active
     *
     * @return true if the timer state is active else false
     */
    fun isActive(): Boolean { return state == TimerState.ACTIVE }

    /**
     * Checks if the timer state is finish
     *
     * @return true if the timer state is finish else false
     */
    fun isFinish(): Boolean { return state == TimerState.FINISH }

    /**
     * Starts the timer
     * Changes timer state to active
     *
     */
    fun start() { state = TimerState.ACTIVE }

    /**
     * Pauses the timer
     * Changes timer state to pause
     *
     */
    fun pause() { state = TimerState.PAUSE }

    /**
     * Decreases the timer time
     * If time is up, executes the callback function
     *
     * @param callback function executed when time is up
     */
    fun decrease(callback: () -> Boolean) {
        if(time > 0) time--
        else {
            state = TimerState.FINISH
            callback()
        }
    }

    /**
     * Gets the timer minutes
     *
     * @return the timer minutes as long
     */
    fun minutes(): Long { return time / 60 }

    /**
     * Gets the timer seconds
     *
     * @return the timer seconds as long
     */
    fun seconds(): Long { return time % 60 }

    /**
     * Gets the start time
     *
     * @return the timer start time as long
     */
    fun maxTime(): Long { return maxTime }

    /**
     * Gets the current time in seconds
     *
     * @return the timer current time as long
     */
    fun getTime(): Long { return time }

    /**
     * Resets the timer
     * Changes current time to start time
     * Changes the timer state to finish
     *
     */
    fun reset() {
        this.time = maxTime
        state = TimerState.FINISH
    }
}