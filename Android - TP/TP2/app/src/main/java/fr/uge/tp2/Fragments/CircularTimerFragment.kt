package fr.uge.tp2.Fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import fr.uge.tp2.R
import fr.uge.tp2.Views.SectorView


/**
 * [CircularTimerFragment] is a fragment which represents the circular view for the timer
 */
class CircularTimerFragment : TimerFragment() {
    private lateinit var sectorView: SectorView
    private lateinit var timeoutTextView: TextView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (view != null) {
            sectorView = view!!.findViewById(R.id.sector_view)
            timeoutTextView = view!!.findViewById(R.id.timeout)
        }
    }

    override fun updateView() {
        sectorView.updateView(countDownTimer)
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_circular_timer
    }

    override fun refreshGraphicOnFinish(counter: Int) {
        timeoutTextView.visibility = if (counter % 2 == 0) View.GONE else View.VISIBLE
    }

    override fun clearGraphic() {
        if (view != null) timeoutTextView.visibility = View.GONE
    }
}