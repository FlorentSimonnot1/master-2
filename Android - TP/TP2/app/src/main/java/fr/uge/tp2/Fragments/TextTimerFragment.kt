package fr.uge.tp2.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import android.widget.TextView
import fr.uge.tp2.R
import fr.uge.tp2.Utils.TimeFormat

/**
 * [TextTimerFragment] is a fragment which represents the text view for the timer
 */
class TextTimerFragment : TimerFragment() {
    private lateinit var minutesTextView: TextView
    private lateinit var secondsTextView: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null) {
            minutesTextView = view.findViewById(R.id.minutes_text_view)
            secondsTextView = view.findViewById(R.id.seconds_text_view)
        }
        updateView()
        return view
    }

    override fun updateView() {
        minutesTextView.text = TimeFormat.format(countDownTimer.minutes())
        secondsTextView.text = TimeFormat.format(countDownTimer.seconds())
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_text_timer
    }

    override fun refreshGraphicOnFinish(counter: Int) {
        minutesTextView.visibility = if (counter % 2 == 0) View.GONE else View.VISIBLE
        secondsTextView.visibility = if (counter % 2 == 0) View.GONE else View.VISIBLE
    }

    override fun clearGraphic() {}
}