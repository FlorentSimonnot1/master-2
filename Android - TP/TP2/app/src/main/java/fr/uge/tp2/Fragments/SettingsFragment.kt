package fr.uge.tp2.Fragments

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.google.android.material.switchmaterial.SwitchMaterial
import fr.uge.tp2.CustomDialog.TimerDialog
import fr.uge.tp2.Preferences.TimerPreferences
import fr.uge.tp2.R
import fr.uge.tp2.Utils.TimeFormat


/**
 * [SettingsFragment] is a fragment which represents the settings of this application
 */
class SettingsFragment : Fragment(), TimerDialog.OnValidate {
    private lateinit var changeTime: Button
    private lateinit var switchMode: SwitchMaterial
    private val SHARED_PREFS = "sharedPrefs"
    private lateinit var pref: SharedPreferences
    private lateinit var timerPreferences: TimerPreferences

    /**
     * Creates a view for the fragment
     * Initialize switch button to change timer view and the dialog to change timer values
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return a view which represents the settings view
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        changeTime = view.findViewById(R.id.change_time)
        switchMode = view.findViewById(R.id.change_mode)
        timerPreferences = TimerPreferences(context!!)

        pref = context!!.applicationContext.getSharedPreferences(SHARED_PREFS, 0)

        loadPreferences()


        changeTime.setOnClickListener {
            val dialog = TimerDialog()
            dialog.setTargetFragment(this, 1);
            dialog.show(activity!!.supportFragmentManager, "timer-dialog")
            activity!!.supportFragmentManager.executePendingTransactions()
        }

        switchMode.setOnCheckedChangeListener { _, isChecked ->
            timerPreferences.setViewMode(isChecked)
        }

        return view
    }

    /**
     * Updates settings time values when alert dialog is validate
     *
     */
    override fun updateSettingsTime() {
        loadPreferences()
    }

    @SuppressLint("SetTextI18n")
    private fun loadPreferences() {
        val minutes = pref.getLong("Minutes", 0)
        val seconds = pref.getLong("Seconds", 10)
        changeTime.text = "${TimeFormat.format(minutes)}:${TimeFormat.format(seconds)}"

        val graphicView = pref.getBoolean("GraphicView", false)
        switchMode.isChecked = graphicView
    }

}