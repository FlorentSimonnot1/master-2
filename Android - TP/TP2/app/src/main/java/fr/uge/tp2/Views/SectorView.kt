package fr.uge.tp2.Views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RequiresApi
import fr.uge.tp2.Models.Timer
import fr.uge.tp2.R

/**
 * [SectorView] is a custom view to draw a circular form which represents the time which decrease
 *
 * @constructor
 * Calls the View constructor
 *
 * @param context the application context
 * @param attrs the attribute set for view. Set to null by default
 * @param defStyleAttr the default style attribute. Set to 0 by default
 *
 * @see View
 */
class SectorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : View(context, attrs, defStyleAttr) {

    private var radius: Double = 200.0
    private var angle: Double = 0.0
    private val paint: Paint = Paint()
    private val path: Path = Path()

    private fun setAngle(angle: Double) {
        if (angle < 0) {
            return
        }
        this.angle = angle
    }

    /**
     * Change the circular form angle in according to the time of timer
     * Draws the new circular form
     *
     * @param timer a timer object
     * @see Timer
     */
    fun updateView(timer: Timer) {
        paint.style = Paint.Style.FILL
        setAngle((timer.maxTime() - timer.getTime()).toDouble() / timer.maxTime().toDouble() * 360)
        // Call invalidate and requestLayout to forced redraw
        invalidate()
        requestLayout()
    }

    fun initView() {
        paint.style = Paint.Style.STROKE
        setAngle(360.toDouble())
        invalidate()
        requestLayout()
    }

    /**
     * Draws the circular form in according to the angle
     *
     * @param canvas the view canvas
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint.color = context.resources.getColor(R.color.colorPrimary)
        canvas!!.drawPath(path, paint)
        canvas.drawArc(
            (width/2 - radius).toFloat(),
            (height/2 - radius).toFloat(),
            (width/2 + radius).toFloat(),
            (height/2 + radius).toFloat(),
            270F,
            angle.toFloat(),
            true, paint)
        canvas.drawPath(path, paint)
    }

}