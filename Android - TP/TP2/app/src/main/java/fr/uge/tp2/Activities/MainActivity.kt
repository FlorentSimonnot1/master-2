package fr.uge.tp2.Activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.uge.tp2.Fragments.SettingsFragment
import fr.uge.tp2.Preferences.TimerPreferences
import fr.uge.tp2.R

/**
 * [MainActivity] is the main activity of this application
 *
 */
class MainActivity : AppCompatActivity() {
    private lateinit var menu: BottomNavigationView
    private lateinit var timerPreferences: TimerPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        timerPreferences = TimerPreferences(this)

        menu = findViewById(R.id.nav_view)
        menu.setOnNavigationItemSelectedListener { item ->
            val fragment: Fragment = when(item.itemId) {
                R.id.settings -> SettingsFragment()
                R.id.timer -> timerPreferences.getViewFragment()
                else -> timerPreferences.getViewFragment()
            }
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
            return@setOnNavigationItemSelectedListener true
        }

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, timerPreferences.getViewFragment()).commit()
        }
    }
}