package fr.uge.tp2.Fragments

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import fr.uge.tp2.Models.Timer
import fr.uge.tp2.Preferences.TimerPreferences
import fr.uge.tp2.R

/**
 * [TimerFragment] is an abstract fragment to manage a timer
 */
abstract class TimerFragment : Fragment() {
    private lateinit var playButton: ImageButton
    private lateinit var restartButton: Button
    private lateinit var timerPreferences: TimerPreferences

    /**
     * The timer object
     */
    protected lateinit var countDownTimer: Timer

    private lateinit var handler: Handler
    private lateinit var handlerTimerFinish: Handler
    private var counter = 0

    private val runnable: Runnable = Runnable { doRun() }
    private val runnableTimerFinish = Runnable { doFinish() }

    private fun doRun() {
        if (countDownTimer.isActive()) {
            countDownTimer.decrease(this.onTimerFinish)
            updateView()
            handler.postDelayed(runnable, 1000)
        }
    }

    private fun doFinish() {
        playButton.visibility = if (counter in 1..29) View.GONE else View.VISIBLE
        if (counter < 30) {
            refreshGraphicOnFinish(counter)
            handlerTimerFinish.postDelayed(runnableTimerFinish, 500)
            counter++
        } else {
            finish()
        }
    }

    // Updates graphic elements and counter when timeout effects is finish
    private fun finish() {
        handlerTimerFinish.removeCallbacks(runnableTimerFinish)
        countDownTimer.reset()
        counter = 0
        updateView()
        refreshGraphicElements()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(getLayoutResourceId(), container, false)
        timerPreferences = TimerPreferences(context!!)
        countDownTimer = if (savedInstanceState != null) savedInstanceState.get("timer") as Timer else Timer(timerPreferences.getTime())
        handler = Handler()
        initGraphicElements(view)
        return view
    }

    private fun initGraphicElements(view: View) {
        // Get graphic elements from main activity
        playButton = view.findViewById(R.id.play_pause_button)
        restartButton = view.findViewById(R.id.restart_button)

        playButton.setOnClickListener {
            when {
                countDownTimer.isActive() -> {
                    countDownTimer.pause()
                }
                else -> {
                    countDownTimer.start()
                    handler.post(runnable)
                }
            }
            refreshGraphicElements()
        }

        restartButton.setOnClickListener {
            if (countDownTimer.isFinish()) {
                finish()
            }
            countDownTimer.reset()
            updateView()
        }

    }

    // updates button in according to the timer state
    private fun refreshGraphicElements() {
        clearGraphic()
        playButton.setImageResource( if(countDownTimer.isActive()) R.drawable.ic_pause_button else R.drawable.ic_play_button)
        restartButton.visibility = if(countDownTimer.isActive()) View.GONE else View.VISIBLE
        playButton.visibility = if (counter in 1..29) View.GONE else View.VISIBLE
    }

    // Callback executed when timer is finish
    private var onTimerFinish = {
        handler.removeCallbacks(runnable)
        refreshGraphicElements()
        handlerTimerFinish = Handler()
        handlerTimerFinish.post(runnableTimerFinish)
    }

    /**
     * Manages fragment on start
     * Starts runnable to decrease timer
     */
    override fun onStart() {
        super.onStart()
        handler.post(runnable)
    }

    /**
     * Manages fragment on stop
     * Removes runnable to stop timer decrease
     *
     */
    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(runnable)
    }

    /**
     * Saves instance state
     * Puts the timer in state to keep timer on change screen orientation for example
     *
     * @param outState the bundles
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable("timer", countDownTimer)
        super.onSaveInstanceState(outState)
    }

    /**
     * Refreshes graphic view when timer is finish
     *
     * @param counter a counter to know how repetition were executed
     */
    abstract fun refreshGraphicOnFinish(counter: Int)

    /**
     * Refreshes graphic view when timer is decreasing
     *
     */
    abstract fun updateView()

    /**
     * Gets layout resource of child fragment
     *
     * @return the resources id as int
     */
    abstract fun getLayoutResourceId(): Int

    /**
     * Clears graphic elements when there are useless
     *
     */
    abstract fun clearGraphic()

}