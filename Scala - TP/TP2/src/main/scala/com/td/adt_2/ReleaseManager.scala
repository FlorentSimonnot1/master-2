package com.td.adt_2

import com.td.case_classes.Browser

object ReleaseManager {

  def releaseVersion(currentBrowser: Browser, newVersion: Double): Browser = Browser(newVersion, currentBrowser.kind, currentBrowser.platform)

}
