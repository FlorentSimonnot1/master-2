package com.td.case_classes

import com.td.adt_1.Types.Kind.Kind
import com.td.adt_1.Types.Platform.Platform

/*
case class Browser(version: Double, kind: String, platform: String) {

}
*/

/**
 * Creates a Browser
 * @param version - the browser version
 * @param kind - the browser kind
 * @param platform - the browser platform
 * @see <i>Kind</i>
 * @see <i>Platform</i>
 */
case class Browser(version: Double, kind: Kind, platform: Seq[Platform]) {

}