package com.td.utils

import com.td.adt_1.Types.Kind.{Chrome, Firefox, IE, Opera, Safari}
import com.td.adt_1.Types.Platform.{Linux, MacOs, Windows}
import com.td.case_classes.Browser

object DB {

  val browsers = Seq(
    Browser(1.0, Firefox, Seq(Windows, Linux)),
    Browser(3.0, Firefox, Seq(Windows, Linux, MacOs)),
    Browser(10.0, Opera, Seq(Windows, Linux, MacOs)),
    Browser(9.0, Opera, Seq(Windows, MacOs)),
    Browser(15.0, IE, Seq(Windows)),
    Browser(2.1, Safari, Seq(MacOs)),
    Browser(2.6, Safari, Seq(MacOs)),
    Browser(0.1, Chrome, Seq(Windows)),
    Browser(8.3, Firefox, Seq(Windows, MacOs)),
    Browser(3.4, Chrome, Seq(Linux))
  )

  def getBrowser(predicat: Browser => Boolean): Seq[Browser] = browsers.filter(predicat)


}
