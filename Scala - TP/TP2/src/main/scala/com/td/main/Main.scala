package com.td.main

// import com.td.classes.Browser
import com.td.adt_1.Types.Kind.{Chrome, Firefox, IE, Opera}
import com.td.adt_1.Types.Platform.{Linux, MacOs, Windows}
import com.td.case_classes.Browser
import com.td.utils.DB

object Main extends App {

  // EXERCISE 1

  //val firefox1 = new Browser(1.0, "Firefox", "Windows, MacOs Linux")
  //val opera = new Browser(0.4, "Opera", "MacOs")
  //val chrome1 = new Browser(2.4, "Chrome", "Windows, Linux")
  //val chrome2 = new Browser(2.4, "Chrome", "Windows, Linux")

  //println(firefox1.equals(chrome1)) // false
  //println(chrome1.equals(chrome2)) // false
  // Have to override equals method in Browser !!

  //println(firefox1.equals(chrome1)) // false
  //println(chrome1.equals(chrome2)) // true

  //println(chrome1.updateVersion(2.5))

  //val chrome3 = Browser(2.6, "Chrome", "Windows, Linux")
  //val firefox2 = Browser(1.2, "Firefox", "Windows, MacOs Linux")

  //println(chrome3)
  //println(firefox2)

  // EXERCISE 2

  //val firefox1 = Browser(1.0, "Firefox", "Windows, MacOs Linux")
  //val opera = Browser(0.4, "Opera", "MacOs")
  //val chrome1 = Browser(2.4, "Chrome", "Windows, Linux")
  //val chrome2 = Browser(2.4, "Chrome", "Windows, Linux")

  //println(chrome1 == firefox1) // false
  //println(firefox1.equals(chrome1)) // false
  //println(chrome1 == chrome2) // true with a case class
  //println(chrome1.equals(chrome2)) // true with a case class

  //val chrome3 = chrome1.copy(2.5)

  //println(chrome3)

  //Case class no need companion object

  // EXERCISE 3

  val firefox1 = Browser(1.0, Firefox, MacOs :: Nil)
  val opera = Browser(0.4, Opera, Linux :: Nil)
  val chrome1 = Browser(2.4, Chrome, Windows :: Linux :: MacOs :: Nil)
  val chrome2 = Browser(2.4, Chrome, Windows :: Linux :: MacOs :: Nil)

  println(chrome1 == firefox1) // false
  println(firefox1.equals(chrome1)) // false
  println(chrome1 == chrome2) // true with a case class
  println(chrome1.equals(chrome2)) // true with a case class

  val selectAllFirefox : Browser => Boolean = b => b.kind == Firefox
  val selectAllChromeWithWindow : Browser => Boolean = b => b.kind == Chrome && b.platform.contains(Windows)
  val selectAllBrowserWithVersion1 : Browser => Boolean = b => b.version == 1.0
  val selectAllOperaAndIEBrowsers : Browser => Boolean = b => b.kind == Opera || b.kind == IE
  val selectChromeBrowserWith34VersionAndLinuxPlatform : Browser => Boolean = b => b.kind == Chrome && b.version == 3.4 && b.platform.contains(Linux)

  println(
    s"${DB.getBrowser(selectAllBrowserWithVersion1)}"
  )

  println(
    s"${DB.getBrowser(selectAllFirefox)}"
  )

  println(
    s"${DB.getBrowser(selectAllChromeWithWindow)}"
  )

  println(
    s"${DB.getBrowser(selectChromeBrowserWith34VersionAndLinuxPlatform)}"
  )

  println(
    s"${DB.getBrowser(selectAllOperaAndIEBrowsers)}"
  )
}
