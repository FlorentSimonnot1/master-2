package com.td.classes

import com.td.adt_1.Types

/**
 * Create a class which represents a browser
 *
 * @param version - the browser version
 * @param kind - the browser kind
 * @param platform - the browser platform
 */
class Browser(val version: Double, val kind: String, val platform: String) {

  override def equals(obj: Any): Boolean = {
    obj match {
      case b: Browser => b.version == version && kind.equals(kind) && platform.equals(platform)
      case _ => false
    }
  }

  override def toString: String = s"Browser : ${kind} ${version} on ${platform}"

  /**
   * Update the browser version
   * @param newVersion - the new version
   * @return a Browser with the version updated
   */
  def updateVersion(newVersion: Double): Browser = Browser(newVersion, kind, platform)

}

/**
 * Create a companion object for Browser
 */
object Browser {

  def apply(version: Double, kind: String, platform: String) = new Browser(version, kind, platform)

}