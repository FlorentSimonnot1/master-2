package com.td.adt_1

object Types {

  /**
   * Defines kind objects
   */
  object Kind {
    sealed trait Kind

    case object Firefox extends Kind
    case object Chrome extends Kind
    case object Opera extends Kind
    case object Safari extends Kind
    case object IE extends Kind
  }

  /**
   * Defines platform objects
   */
  object Platform {
    sealed trait Platform

    case object Windows extends Platform
    case object MacOs extends Platform
    case object Linux extends Platform
  }


}
