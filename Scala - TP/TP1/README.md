# FROM OOP TO FUNCTIONAL PROG - SCALA

## RULES

- Don't use mutable (var) variable
- Don't use private variable

Methods in Functional paradigm :

- Doesn't modify the internal state
- Doesn't throw an exception
- Returns value

Example : 

    def speedUp(increase: Int): Unit = {
        if (increase < 0) throw new IllegalArgumentException(s"$increase must be positive")
        speed += increase
    }
    
    def brake(decrease: Int): Unit = {
        if (decrease < 0) throw new IllegalArgumentException(s"$decrease must be positive")
        speed = if(speed > decrease) speed - decrease else 0
    }
    
These two methods don't respect the rules for methods in Functional paradigm.
<br>

!! We have to return a new Car with a new state which is a copy of the former car but with speed changed. !!
<br>
    
    def speedUp(increase: Int): Car = new Car(nbDoor, color, nbPlaces, speed + increase)
    def brake(decrease: Int): Car = new Car(nbDoor, color, nbPlaces, speed - decrease)
    
This implementation is now correct. However, here we no longer handle an error on the given value. 
Fortunately, there is a type in Scala named Either. This type is a "monadic" type like Optional type. A modal type is like a wrapper which contain an object.

<br>

      def speedUp(increase: Int): Either[String, Car] = if (speed < 0) Left("Speed must be positive") else Right(new Car(nbDoor, color, nbPlaces, speed + increase))
      def break(decrease: Int): Either[String, Car] = if (speed < 0) Left("Speed must be positive") else Right(new Car(nbDoor, color, nbPlaces, speed - decrease))
      
An Either type is divided into 2 real types. Left and Right.

How get result ?

    println(
      newCar match {
        case Right(c) => s"The new speed is ${c.speed}"
        case Left(error) => s"Error: $error"
      }
    )
    
We can use a match in order to check if result is Left or Right.

