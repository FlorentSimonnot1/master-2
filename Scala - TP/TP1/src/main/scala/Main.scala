import td.Classes.{Car, CarConstructor}

object Main extends App {

    /*
     EXERCISE 1
    println("Drive your car here")
    val car = new Car(5, "Pink", 5)

    car.speedUp(5)
    car.speedUp(5)
    println(s"car ${car.speed}")
    car.brake(11)
    println(s"car ${car.speed}")
    */

    val car = new Car(5, CarConstructor.Blue, 5)
    val newCar: Either[String, Car] = car.speedUp(5)

    println(
      newCar match {
        case Right(c) => s"The new speed is ${c.speed}"
        case Left(error) => s"Error: $error"
      }
    )

}
