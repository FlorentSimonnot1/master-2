package td.Classes

/*
EXERCISE 1
class Car(private val nbDoor: Int, var color: String, val nbPlaces: Int, var speed: Int = 0) {

  def speedUp(increase: Int): Unit = {
    if (increase < 0) throw new IllegalArgumentException(s"$increase must be positive")
    speed += increase
  }

  def brake(decrease: Int): Unit = {
    if (decrease < 0) throw new IllegalArgumentException(s"$decrease must be positive")
    speed = if(speed > decrease) speed - decrease else 0
  }

}
*/

class Car(val nbDoor: Int, val color: CarConstructor.Color, val nbPlaces: Int, val speed: Int = 0) {

  // Without checking speed value
  /*def speedUp(increase: Int): Car = new Car(nbDoor, color, nbPlaces, speed + increase)
  def brake(decrease: Int): Car = new Car(nbDoor, color, nbPlaces, speed - decrease)*/

  def speedUp(increase: Int): Either[String, Car] = if (speed < 0) Left("Speed must be positive") else Right(new Car(nbDoor, color, nbPlaces, speed + increase))
  def break(decrease: Int): Either[String, Car] = if (speed < 0) Left("Speed must be positive") else Right(new Car(nbDoor, color, nbPlaces, speed - decrease))

}