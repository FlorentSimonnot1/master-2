package main.scala.utils

object Helpers {

  implicit class ExtendIntOption(val option: Option[Int]) {
    def addIntOption(otherOption: Option[Int]): Some[Int] = (option, otherOption) match {
      case (Some(v1), Some(v2)) => Some(v1 + v2)
      case (None, v@Some(_)) => v
      case (v@Some(_), None) => v
      case (None, None) => Some(0)
    }
  }

  implicit class ExtendOption[A](val option: Option[A]) {
    def addOptionAndValue(value: A): Option[A] = (option, value) match {
      case (Some(v1), v2) => v1 + v2
    }
  }


}
