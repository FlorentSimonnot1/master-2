package main.scala.main

import main.scala.utils.Helpers._

object Main extends App {

  /*val addOptions = (o1: Option[Int], o2: Option[Int]) => (o1, o2) match {
    case (Some(v1), Some(v2)) => Some(v1 + v2)
    case (None, v) => v
    case (v, None) => v
    case (None, None) => Some(0)
  }

  println(addOptions(Some(1), None))
  println(addOptions(Some(5), Some(5)))
  println(addOptions(None, Some(1)))*/

  val o1 = Some(1)
  val o2 = Some(5)
  val o3 = None

  println(o1 addIntOption Some(5))
  println(o1 addIntOption Some(0))
  println(o3 addIntOption Some(1))

}
