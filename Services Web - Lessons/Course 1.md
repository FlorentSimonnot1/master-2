﻿# Middlewares RMI

Aujourd'hui le nombre d'application ne cesse de croitre. Il est très difficile de ne pas trouver à un endroit un fragment de code que l'on pourrait réutiliser. On pourrait évidement partager le code. Mais nous pourrions aussi proposer des services afin d'accéder à une fonctionnalité sans pour autant partager notre code.
Nous avons besoin de standards, de protocoles pour faciliter la communication entre applications indépendantes.

C'est ce qui s'appelle un **middleware**.

Dans ce cours, nous étudierons le partage d'information entre plusieurs applications écrites en programmation orientée objet. Pour cela nous découvrirons l'API **RMI**.

## Qu'est ce que RMI ?

RMI qui signifie **Remote Method Invocation** est une API qui permet l'appel, l'exécution et le renvoi du résultat d'une méthode exécutée dans une machine virtuelle (JVM) différente de celle de l'objet l'appelant. Cette machine virtuelle peut être sur une machine différente pourvu qu'elle soit accessible par le réseau.

La machine sur laquelle s'exécute la méthode distante est appelée **serveur**.

L'appel coté **client** d'une telle méthode est un peu plus compliqué que l'appel d'une méthode d'un objet local mais il reste simple. Il consiste à obtenir une ***référence*** sur l'objet distant puis à simplement appeler la méthode à partir de cette référence.

La technologie RMI se charge de rendre transparente la localisation de l'objet distant, son appel et le renvoi du résultat.

En fait, elle utilise deux classes particulières, le **stub** et le **skeleton**, qui doivent être générées avec l'outil rmic fourni avec le JDK.

Le stub est une classe qui se situe côté client et le skeleton est son homologue coté serveur. Ces deux classes se chargent d'assurer tous les mécanismes d'appel, de communication, d'exécution, de renvoi et de réception du résultat.

## Implémentation basique

L'implémentation pour partager un objet entre deux machines virtuelles grâce à l'API RMI nécessite de mettre en place : 

- un objet à partager
- un serveur
- un client

___
**NOTE** : Nous prendrons comme exemple basique une simple classe possédant une méthode `HelloTo` qui dit bonjour au nom passé en paramètre.
La signature de la méthode sera `String helloTo(String name)`.
___

### Implémentation de l'objet à partager (OD)

C'est le serveur qui partage l'objet voulu, appelé **objet distant (OD)**. Ses méthodes sont invoquées depuis une autre JVM.

Lorsque l'on veut partager un objet, nous devons d'abord créer une interface que l'on appelle **interface distante**. Cette interface doit hérité de la classe **Remote** du package RMI. De plus toutes les méthodes de l'interface doivent lever l'exception **RemoteException**.

**Exemple**

    public interface IHello extends Remote {
	    String helloTo(String name) throws RemoteException;
    }

Après avoir rédigé l'interface, nous devons créer une classe qui l'implémente.
Cette classe devra également hériter de la classe **UnicastRemoteObject** du package server de RMI. Le constructeur doit être surchargé pour lever l'exception **RemoteException**.

**Exemple**

    public class Hello extends UnicastRemoteObject implements IHello {
	    public Hello() throws RemoteException { 
		    super(); 
	    }

		public String helloTo(String name) throws RemoteException {
			return "Hello " + name;
		}
	}

### Implémentation du serveur

Dans le serveur, nous devons d'abord créer une registre.

    LocateRegistry.createRegistry(1100);

Un registre sera créer sur le port 1100. Il permettra au client de récupérer l'objet sur ce même port.

Nous devons ensuite instancier l'objet que nous voulons transmettre. 

___
**NOTE** : Le serveur doit envoyer une interface. L'instanciation d'un objet sera donc typer par son interface
___

    IHello hello = new Hello();

Pour finir, nous utilisons la classe **Naming** et sa méthode **remind** pour faire passer notre objet. 

    Naming.rebind("rmi://localhost:1100/HelloService", hello);

Le premier argument sert à "configurer" l'adresse où les clients pourront récupérer l'objet. On note le port 1100 que nous avons déclarer grâce au registre. 

___
**NOTE**: Lors d'une mise en production, nous devons renseigner l'adresse IP du serveur au lieu de "localhost". Exemple : `rmi://207.142.131.245:1100/HelloService`
___

Comme les méthodes **createRegistry** et **rebind** lèvent de nombreuses exceptions il est de bonne augure d'utiliser une clause **try-catch** dans le serveur.

    class HelloServer {
	    public static void main(String args[]) {
		    try {
				  LocateRegistry.createRegistry(1100); 
				  IHello hello = new Hello();
				  Naming.rebind("rmi://localhost:1100/HelloService", hello);
		    } catch(Exception e) {
			    // What do you want..
		    }
	    }
    }


### Implémentation du client et récupération de notre objet

Maintenant que notre serveur est prêt pour permettre de partager un objet, nous devons implémenter un client qui pourra récupérer l'objet.

Pour récupérer l'objet distant nous devon utiliser la méthode **lookup** de la classe **Naming**. En paramètre nous devons passer l'adresse ou se trouve notre objet.

Si l'on reprends l'exemple précédent, l'adresse devrait être `rmi://localhost:1100/HelloService`.

    IHello hello = (IHello) Naming.lookup("rmi://localhost:1100/HelloService")

Nous devons caster l'objet avec son interface.

Tout comme dans notre serveur, nous devons utiliser une clause **try-catch** car la méthode **lookup** lève des exceptions.

    public class HelloClient {
	    public static void main(String args[]) {
		    try {
			    IHello hello = (IHello) Naming.lookup("rmi://localhost:1100/HelloService");
			    hello.helloTo("Michel");
			    hello.helloTo("Georges");
		    } catch(Exception e) {
			    // What do you want..
		    }
	    }
    }

## Objet sérializable

Pour l'instant tout vas bien. Nous avons créer une objet simple. Sa seule méthode prends un type primitif et renvoie un type primitif.
<br>
Lorsque l'on utilise une méthode qui prend un paramètre ou renvoie un objet que nous avons nous même créé, nous devons nous assurer que cet objet est Serializable sous peine de lever une exception.
Ainsi toutes nos classes appelées en tant que paramètres ou valeurs de retour doivent implémenter l'interface **Serializable**.

Reprenons notre exemple de **IHello** mais cette fois ci, notre méthode **helloTo** prendra en paramètre un objet **Person**.

    public class Person implements Serializable {
	    private final String firstName;
	    private final String lastName;

		Person(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName
		}

		public String getFirstName() { return firstName; }
		public String getLastName() { return lastName; }
	}


    public class Hello extends UnicastRemoteObject implements IHello {
	    public Hello() throws RemoteException { 
		    super(); 
	    }

		public String helloTo(Person person) throws RemoteException {
			return "Hello " + person.getFirstName() + " " + person.getLastName();
		}
	}

Notre classe est sérialisée,  nous pourrons partager notre objet **Hello** et nous servir de sa méthode avec un objet de type **Person**.

## Passage par valeur Vs. Passage par référence

### Passage par valeur

En local, Java utilise le passage par valeur pour tous les types primitifs. Ainsi les variables de type int, char, boolean etc... sont copiés quand on les passes à une méthode.

Exemple :

    public class ExempleValeur  {
		static public  int  incremente(int  i) {
			i++;  
			return  i;  
		}
	}

	public class TestPassageValeur  {
		public static void  main(String[] args) {  
			int  i = 0;  
			int  j =  ExempleValeur.incremente(i);  
			System.out.println("i=" + i);  //Affiche 0
			System.out.println("j=" + j);  //Affiche 1
		}
	}

Comme le montre cet exemple, les valeurs de i et j sont différentes. La variable i n'a pas été modifié car il s'agit d'une copie.

### Passage par référence

Dans le cas d'un passage par référence, c'est un pointeur qui est passé. La modification de l'état de l'objet passé par référence dans une routine a donc une incidence. Les objets sont passés par référence.

Exemple : 

    public class MutableInt  {
	    private int value;
	    
	    public MutableInt(int v) {  
		    this.value = v;  
	    }
	    
	    public void setValue(int v) { this.value = v; }  
	    public int getValue() { return this.value; }
    }
    
	public class ExempleReference  {
		static public MutableInt incremente(MutableInt i) {
			i.setValue(i.getValue() + 1);  
			return  i;  
		}
	}
	
	public class TestPassageReference  {
		public static void  main(String[] args) {  
			MutableInt i =  new  MutableInt(0);  
			MutableInt j =  ExempleReference.incremente(i);  
			System.out.println("i.getValue() = " + i.getValue());  //Affiche 1
			System.out.println("j.getValue() = " + j.getValue());  //Affiche 1
		}  
	}

Ici, on voit bien que i a été modifié.

### RMI : Passage par valeur ou par référence

Le passage des objets est plus souple dans RMI. On peut en fait passé des objets en tant que valeur.

Le passage de paramètre/type de retour comporte 4 cas :

- Le paramètre est un type **primitif** => passage par valeur
- Le paramètre est un objet **sérialisé** => passage par valeur. Il s'agit donc d'une copie
- Le paramètre est un objet **distribué** (interface Remote et classe fille UnicastRemoteObject) => passage par référence.
- Aucun des cas précédents => implémentation impossible
