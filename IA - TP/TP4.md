﻿# TP4 - SPARQL

## Question 1

> Write a query to display all the triples in the dataset. How many
> triples are there?

`SELECT * WHERE {?subject ?predicate ?object}`

`SELECT (count(?subject) as ?count) WHERE {?subject ?predicate ?object}`

## Question 2 

> Write a query displaying the subjects and objects whose property is
> rdf:type. How many rows do you have in the result ?

    SELECT * WHERE {?subject rdf:type ?object}

    SELECT (count(?subject) as ?count) WHERE {?subject rdf:type ?object}

## Question 3

> Write a query showing the subjects of triples whose property is
> rdf:type and the subject is
> http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs. How
> many lines do you get?

	select ?x WHERE {?x rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>}

	select (count(?x) as ?count) WHERE {?x rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>}

## Question 4

> Take the request from question 3 and impose an ascending order on the
> subject.

	select * WHERE {?x rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>} ORDER BY ?x

## Question 5

> Click on the subject whose URI ends with DB01146. 3 tables are
> displayed: outgoing, ingoing and attributes. After a visual search,
> provide the value of molecularWeightAverage.

## Question 6

> Write a request providing all drugs with a triplet where the object
> contains the string ”Dextromethorphan”. How many distinct identifiers
> do you get?

	select DISTINCT ?s WHERE {?s ?p ?o. FILTER regex(?o, "Dextromethorphan") } ORDER BY ?s

Count : 11

## Question 7

> Provide the value of the ATC code (atcCode) for each of these drugs.
> How many lines do you get?

    select DISTINCT ?s ?n WHERE {?s ?p ?o. ?s <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n . FILTER regex(?o, "Dextromethorphan") } ORDER BY ?s

## Question 8

> Write a Boolean SPARQL query to find out if there is a drug with the
> ATC code ”R05DA09”.

    ask {?s rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>. ?s <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> "R05DA09"} 

## Question 9

> Write a DESCRIBE-type query to obtain all the information on the
> subject of the query 8. How many rows do you get in the result?

    describe ?s {?s rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>. ?s <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> "R05DA09"} 

## Question 10

> Write a query that only displays triples whose object is a literal. 
> Write a query that displays only triples whose object has a data type
> of type integer.


    SELECT * WHERE { ?s ?p ?o. FILTER isLiteral(?o) }
   
	Select * WHERE {?s ?p ?o .  FILTER(datatype(?o) = xsd:integer) }


## Question 11

> Repeat request 3 but require that an ATC code be associated with
> each drug. How many lines do you have?

	select DISTINCT ?s ?n 
	WHERE {
		?s rdf:type <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drugs>. 
		?s <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n. 
	} ORDER BY ?n

## Question 12

> Write a request to obtain the number of ATC codes per drug. How many
> rows do you have in the result? How many ATC codes does the drug
> DB00759 have?

    select ?x (count(?n) as ?count) WHERE {?x <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n.  } GROUP BY ?x

## Question 13

> Write the request to obtain the number of drugs by ATC code. Number of
> lines?

    select ?n (count(?x) as ?ct) WHERE {?x <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n.  } GROUP BY ?n

## Question 14

> Modify query 13 to only have ATC codes starting with ”R05”. How many
> lines

    select ?n (count(?x) as ?ct) 
    WHERE {
	    ?x <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n. 
	    FILTER REGEX(?n, "^R05")  
    } 
    GROUP BY ?n


## Question 15

> Query 14 is not very relevant. You must change it to find out how many
> drug names exist by ATC code of the category starting with “R05”. How
> many lines? How many names for R05CA03?

    select ?n (count(?name) as ?countName) 
    WHERE {
	    ?x <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/brandName> ?name. 
	    ?x <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/atcCode> ?n. 
	    FILTER(regex(?n,"^R05"))
     } 
     GROUP BY ?n

## Question 16

> We want to create a graph from the data on interactions between drugs.
> The nodes in this graph contain nodes that are of drugBank id and the properties are http://example.com/contraIndicatedTo. To retrieve the values of the nodes, you must retrieve the subjects of the triples whose type is http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drug interactions, from these, retrieve the drugs involved.

    construct { ?d1 <http://example.com/contraintdicatedTo> ?d2 } 
    where {
    	?i rdf:type	<http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/drug_interactions>.
    	?i <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/interactionDrug1> ?d1.
    	?i <http://www4.wiwiss.fu-berlin.de/drugbank/resource/drugbank/interactionDrug2> ?d2.
    }


